import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import Loader from '../Loader';
import StylesLink from '../Styles/Link';
import REFS from './_config';
import { INPUT_TYPE } from './_constants';
import { StyledButton, StyledButtonInner, StyledButtonLink, StyledButtonLinkInner } from './_style';

export default class Button extends PureComponent {
  static REFS = { ...REFS };

  static inputType = INPUT_TYPE;

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Type */
    type: PropTypes.oneOf(Object.values(REFS.TYPES)),
    /** CSS-property display */
    display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Emphasis flag */
    isEmphasis: PropTypes.bool,
    /** Loading flag */
    isLoading: PropTypes.bool,
    /** With Icon flag (= true if Icon component is in props.children) */
    isWithIcon: PropTypes.bool,
    /** Icon flag (= true if only icon displayed in props.children) */
    isOnlyIcon: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * */
    onBlur: PropTypes.func,
    /** Click handler
     * @param {string} id Component Id
     * */
    onClick: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    type: REFS.TYPES.BASE_SECONDARY,
    display: REFS.DISPLAY.INLINE_BLOCK,
    dimension: REFS.DIMENSIONS.M,
    width: null,
    isEmphasis: false,
    isLoading: false,
    isWithIcon: false,
    isOnlyIcon: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
    };

    this.inputNode = React.createRef();
  }

  componentDidMount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  componentDidUpdate() {
    if (this.props.disabled && this.state.isFocused) this.handleBlur();
  }

  componentWillUnmount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  handleFocus = () => {
    this.setState({ isFocused: true });

    this.props.onFocus({ id: this.props.id });
  };

  handleBlur = () => {
    this.setState({ isFocused: false });

    this.props.onBlur({ id: this.props.id });
  };

  handleClick = () => {
    if (!this.props.disabled) this.props.onClick({ id: this.props.id });
  };

  render() {
    if (
      [REFS.TYPES.LINK, REFS.TYPES.LINK_PRIMARY, REFS.TYPES.LINK_SECONDARY, REFS.TYPES.LINK_DANGER].includes(
        this.props.type,
      )
    ) {
      return (
        <StyledButtonLink
          {...addDataAttributes({ component: INPUT_TYPE })}
          innerRef={ref => {
            this.inputNode.current = ref;
          }}
          sType={this.props.type}
          sDimension={this.props.dimension}
          sWidth={this.props.width}
          sDisplay={this.props.display}
          isDisabled={this.props.disabled || this.props.isLoading}
          isEmphasis={this.props.isEmphasis}
        >
          <StyledButtonLinkInner isLoading={this.props.isLoading}>
            <StylesLink
              isPseudo
              isPrimary={this.props.type === REFS.TYPES.LINK_PRIMARY}
              isSecondary={this.props.type === REFS.TYPES.LINK_SECONDARY}
              isDanger={this.props.type === REFS.TYPES.LINK_DANGER}
              isWithIcon={this.props.isWithIcon}
              disabled={this.props.disabled || this.props.isLoading}
              id={this.props.id}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur}
              onClick={this.handleClick}
            >
              {this.props.children}
            </StylesLink>
          </StyledButtonLinkInner>
          {this.props.isLoading && (
            <Loader
              isCentered
              dimension={
                [REFS.DIMENSIONS.XS, REFS.DIMENSIONS.S].includes(this.props.dimension)
                  ? Loader.REFS.DIMENSIONS.XS
                  : Loader.REFS.DIMENSIONS.S
              }
            />
          )}
        </StyledButtonLink>
      );
    }

    return (
      <StyledButton
        {...addDataAttributes({ component: INPUT_TYPE })}
        innerRef={ref => {
          this.inputNode.current = ref;
        }}
        sType={this.props.type}
        sDimension={this.props.dimension}
        sWidth={this.props.width}
        sDisplay={this.props.display}
        isEmphasis={this.props.isEmphasis}
        isOnlyIcon={this.props.isOnlyIcon}
        isDisabled={this.props.disabled || this.props.isLoading}
        isLoading={this.props.isLoading}
        isFocused={this.state.isFocused}
        id={this.props.id}
        disabled={this.props.disabled || this.props.isLoading}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onClick={this.handleClick}
      >
        <StyledButtonInner isLoading={this.props.isLoading}>{this.props.children}</StyledButtonInner>
        {this.props.isLoading && (
          <Loader
            isCentered
            dimension={
              [REFS.DIMENSIONS.XS, REFS.DIMENSIONS.S].includes(this.props.dimension)
                ? Loader.REFS.DIMENSIONS.XS
                : Loader.REFS.DIMENSIONS.S
            }
          />
        )}
      </StyledButton>
    );
  }
}
