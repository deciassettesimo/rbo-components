### REFS

```js static
{
  TYPES: {
    BASE_PRIMARY: 'base-primary',
    BASE_SECONDARY: 'base-secondary',
    ACCENT_PRIMARY: 'accent-primary',
    ACCENT_SECONDARY: 'accent-secondary',
    DANGER_PRIMARY: 'danger-primary',
    DANGER_SECONDARY: 'danger-secondary',
    LINK: 'link',
    LINK_PRIMARY: 'link-primary',
    LINK_SECONDARY: 'link-secondary',
    LINK_DANGER: 'link-danger',
  },
  DISPLAY: {
    BLOCK: 'block',
    INLINE_BLOCK: 'inline-block',
  },
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### Button Dimensions

```js
<div className="list">
  <div>
    <Button
      id="ButtonXS"
      dimension={Button.REFS.DIMENSIONS.XS}
      onFocus={({ id })=>{ console.log('focus:', id); }}
      onBlur={({ id })=>{ console.log('blur:', id); }}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Button XS
    </Button>
  </div>
  <div>
    <Button
      id="ButtonS"
      dimension={Button.REFS.DIMENSIONS.S}
      onFocus={({ id })=>{ console.log('focus:', id); }}
      onBlur={({ id })=>{ console.log('blur:', id); }}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Button S
    </Button>
  </div>
  <div>
    <Button
      id="ButtonM"
      dimension={Button.REFS.DIMENSIONS.M}
      onFocus={({ id })=>{ console.log('focus:', id); }}
      onBlur={({ id })=>{ console.log('blur:', id); }}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Button M
    </Button>
  </div>
  <div>
    <Button
      id="ButtonL"
      dimension={Button.REFS.DIMENSIONS.L}
      onFocus={({ id })=>{ console.log('focus:', id); }}
      onBlur={({ id })=>{ console.log('blur:', id); }}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Button L
    </Button>
  </div>
  <div>
    <Button
      id="ButtonXL"
      dimension={Button.REFS.DIMENSIONS.XL}
      onFocus={({ id })=>{ console.log('focus:', id); }}
      onBlur={({ id })=>{ console.log('blur:', id); }}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Button XL
    </Button>
  </div>
</div>
```

### Button Types

```js
<div className="list">
  <div className="inline">
    <Button
      id="ButtonBasePrimary"
      type={Button.REFS.TYPES.BASE_PRIMARY}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Base Primary
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonBaseSecondary"
      type={Button.REFS.TYPES.BASE_SECONDARY}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Base Secondary
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonAccentPrimary"
      type={Button.REFS.TYPES.ACCENT_PRIMARY}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Accent Primary
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonAccentSecondary"
      type={Button.REFS.TYPES.ACCENT_SECONDARY}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Accent Secondary
    </Button>
  </div>
</div>
<div className="list">
  <div className="inline">
    <Button
      id="ButtonDangerPrimary"
      type={Button.REFS.TYPES.DANGER_PRIMARY}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Danger Primary
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonDangerSecondary"
      type={Button.REFS.TYPES.DANGER_SECONDARY}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Danger Secondary
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonLinkPrimary"
      type={Button.REFS.TYPES.LINK_PRIMARY}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Link Primary
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonLink"
      type={Button.REFS.TYPES.LINK}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Link
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonLinkSecondary"
      type={Button.REFS.TYPES.LINK_SECONDARY}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Link Secondary
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonLinkDanger"
      type={Button.REFS.TYPES.LINK_DANGER}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      Link Danger
    </Button>
  </div>
</div>
```

### Button isEmphasis

```js
<div className="list">
  <div>
    <Button
      id="ButtonEmphasisXS"
      dimension={Button.REFS.DIMENSIONS.XS}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      Button
    </Button>
  </div>
  <div>
    <Button
      id="ButtonEmphasisS"
      dimension={Button.REFS.DIMENSIONS.S}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      Button
    </Button>
  </div>
  <div>
    <Button
      id="ButtonEmphasisM"
      dimension={Button.REFS.DIMENSIONS.M}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      Button
    </Button>
  </div>
  <div>
    <Button
      id="ButtonEmphasisL"
      dimension={Button.REFS.DIMENSIONS.L}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      Button
    </Button>
  </div>
  <div>
    <Button
      id="ButtonEmphasisXL"
      dimension={Button.REFS.DIMENSIONS.XL}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      Button
    </Button>
  </div>
</div>
```

### Button with Icon

```js
<div className="list">
  <div className="inline">
    <Button
      id="ButtonIconXS1"
      type={Button.REFS.TYPES.BASE_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.XS}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconClip dimension={IconClip.REFS.DIMENSIONS.XS} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXS2"
      type={Button.REFS.TYPES.BASE_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.XS}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.XS} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXS3"
      type={Button.REFS.TYPES.ACCENT_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.XS}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <span>Button</span>
      <IconArchive dimension={IconArchive.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXS4"
      type={Button.REFS.TYPES.ACCENT_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.XS}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <span>Button</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXS5"
      type={Button.REFS.TYPES.DANGER_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.XS}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconBurger dimension={IconBurger.REFS.DIMENSIONS.XS} />
      <span>Button</span>
      <IconCards dimension={IconCards.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXS6"
      type={Button.REFS.TYPES.DANGER_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.XS}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.XS} />
      <span>Button</span>
      <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXS7"
      dimension={Button.REFS.DIMENSIONS.XS}
      onClick={({ id })=>{ console.log('click:', id); }}
      isOnlyIcon
    >
      <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
</div>

<div className="list">
  <div className="inline">
    <Button
      id="ButtonIconS1"
      type={Button.REFS.TYPES.BASE_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.S}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconClip dimension={IconClip.REFS.DIMENSIONS.XS} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconS2"
      type={Button.REFS.TYPES.BASE_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.S}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.XS} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconS3"
      type={Button.REFS.TYPES.ACCENT_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.S}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <span>Button</span>
      <IconArchive dimension={IconArchive.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconS4"
      type={Button.REFS.TYPES.ACCENT_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.S}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <span>Button</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconS5"
      type={Button.REFS.TYPES.DANGER_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.S}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconBurger dimension={IconBurger.REFS.DIMENSIONS.XS} />
      <span>Button</span>
      <IconCards dimension={IconCards.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconS6"
      type={Button.REFS.TYPES.DANGER_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.S}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.XS} />
      <span>Button</span>
      <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconS7"
      dimension={Button.REFS.DIMENSIONS.S}
      onClick={({ id })=>{ console.log('click:', id); }}
      isOnlyIcon
    >
      <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
</div>

<div className="list">
  <div className="inline">
    <Button
      id="ButtonIconM1"
      type={Button.REFS.TYPES.BASE_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.M}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconClip dimension={IconClip.REFS.DIMENSIONS.XS} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconM2"
      type={Button.REFS.TYPES.BASE_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.M}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.XS} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconM3"
      type={Button.REFS.TYPES.ACCENT_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.M}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <span>Button</span>
      <IconArchive dimension={IconArchive.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconM4"
      type={Button.REFS.TYPES.ACCENT_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.M}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <span>Button</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconM5"
      type={Button.REFS.TYPES.DANGER_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.M}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconBurger dimension={IconBurger.REFS.DIMENSIONS.XS} />
      <span>Button</span>
      <IconCards dimension={IconCards.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconM6"
      type={Button.REFS.TYPES.DANGER_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.M}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.XS} />
      <span>Button</span>
      <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconM7"
      dimension={Button.REFS.DIMENSIONS.M}
      onClick={({ id })=>{ console.log('click:', id); }}
      isOnlyIcon
    >
      <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
</div>

<div className="list">
  <div className="inline">
    <Button
      id="ButtonIconL1"
      type={Button.REFS.TYPES.BASE_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.L}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconClip dimension={IconClip.REFS.DIMENSIONS.S} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconL2"
      type={Button.REFS.TYPES.BASE_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.L}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.S} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconL3"
      type={Button.REFS.TYPES.ACCENT_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.L}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <span>Button</span>
      <IconArchive dimension={IconArchive.REFS.DIMENSIONS.S} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconL4"
      type={Button.REFS.TYPES.ACCENT_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.L}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <span>Button</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.S} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconL5"
      type={Button.REFS.TYPES.DANGER_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.L}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconBurger dimension={IconBurger.REFS.DIMENSIONS.S} />
      <span>Button</span>
      <IconCards dimension={IconCards.REFS.DIMENSIONS.S} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconL6"
      type={Button.REFS.TYPES.DANGER_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.L}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.S} />
      <span>Button</span>
      <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.S} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconL7"
      dimension={Button.REFS.DIMENSIONS.L}
      onClick={({ id })=>{ console.log('click:', id); }}
      isOnlyIcon
    >
      <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.S} />
    </Button>
  </div>
</div>

<div className="list">
  <div className="inline">
    <Button
      id="ButtonIconXL1"
      type={Button.REFS.TYPES.BASE_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.XL}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconClip dimension={IconClip.REFS.DIMENSIONS.S} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXL2"
      type={Button.REFS.TYPES.BASE_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.XL}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.S} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXL3"
      type={Button.REFS.TYPES.ACCENT_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.XL}
      onClick={({ id })=>{ console.log('click:', id); }}
    >
      <span>Button</span>
      <IconArchive dimension={IconArchive.REFS.DIMENSIONS.S} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXL4"
      type={Button.REFS.TYPES.ACCENT_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.XL}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <span>Button</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.S} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXL5"
      type={Button.REFS.TYPES.DANGER_PRIMARY}
      dimension={Button.REFS.DIMENSIONS.XL}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconBurger dimension={IconBurger.REFS.DIMENSIONS.S} />
      <span>Button</span>
      <IconCards dimension={IconCards.REFS.DIMENSIONS.S} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXL6"
      type={Button.REFS.TYPES.DANGER_SECONDARY}
      dimension={Button.REFS.DIMENSIONS.XL}
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
    >
      <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.S} />
      <span>Button</span>
      <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.S} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconXL7"
      dimension={Button.REFS.DIMENSIONS.XL}
      onClick={({ id })=>{ console.log('click:', id); }}
      isOnlyIcon
    >
      <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.S} />
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonLinkDisabled"
      type={Button.REFS.TYPES.LINK}
      onClick={({ id })=>{ console.log('click:', id); }}
      isWithIcon
    >
      <span>Link Secondary</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />
    </Button>
  </div>
</div>
```

### Button disabled

```js
<div className="list">
  <div className="inline">
    <Button
      id="ButtonDisabled"
      onClick={({ id })=>{ console.log('click:', id); }}
      disabled
    >
      Button
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonLinkPrimaryDisabled"
      type={Button.REFS.TYPES.LINK_PRIMARY}
      onClick={({ id })=>{ console.log('click:', id); }}
      disabled
    >
      Link Primary
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonTextAndIconDisabled"
      onClick={({ id })=>{ console.log('click:', id); }}
      isEmphasis
      disabled
    >
      <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.S} />
      <span>Button</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonLinkSecondaryDisabled"
      type={Button.REFS.TYPES.LINK_SECONDARY}
      onClick={({ id })=>{ console.log('click:', id); }}
      isWithIcon
      isEmphasis
      disabled
    >
      <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.S} />
      <span>Link Secondary</span>
    </Button>
  </div>
  <div className="inline">
    <Button
      id="ButtonIconDisabled"
      onClick={({ id })=>{ console.log('click:', id); }}
      isOnlyIcon
      disabled
    >
      <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.S} />
    </Button>
  </div>
</div>
```

### Button isLoading

```js
<div className="list">
  <div>
    <Button
      id="ButtonIsLoadingXS"
      dimension={Button.REFS.DIMENSIONS.XS}
      onClick={({ id })=>{ console.log('click:', id); }}
      isLoading
    >
      Button text
    </Button>
  </div>
  <div>
    <Button
      id="ButtonIsLoadingS"
      dimension={Button.REFS.DIMENSIONS.S}
      onClick={({ id })=>{ console.log('click:', id); }}
      isLoading
    >
      Button text
    </Button>
  </div>
  <div>
    <Button
      id="ButtonIsLoadingM"
      type={Button.REFS.TYPES.LINK}
      dimension={Button.REFS.DIMENSIONS.M}
      onClick={({ id })=>{ console.log('click:', id); }}
      isLoading
    >
      Button text
    </Button>
  </div>
  <div>
    <Button
      id="ButtonIsLoadingL"
      dimension={Button.REFS.DIMENSIONS.L}
      onClick={({ id })=>{ console.log('click:', id); }}
      isLoading
    >
      Button text
    </Button>
  </div>
  <div>
    <Button
      id="ButtonIsLoadingXL"
      dimension={Button.REFS.DIMENSIONS.XL}
      onClick={({ id })=>{ console.log('click:', id); }}
      isLoading
    >
      Button text
    </Button>
  </div>
</div>
```
