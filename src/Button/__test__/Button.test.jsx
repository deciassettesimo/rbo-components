import React from 'react';
import { mount } from 'enzyme';
import ButtonOld from '../Button';

const getWrapper = props => mount(<ButtonOld {...props} />);

describe('Button Component', () => {
  it('should render button with normal props', () => {
    const id = 'Button';
    const text = 'testText';
    const props = {
      id,
      children: text,
      onClick: f => f,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.text()).toEqual(text);
  });

  it('should render button with disabled props', () => {
    const id = 'Button';
    const text = 'testText';
    const props = {
      id,
      children: text,
      disabled: true,
      onClick: f => f,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.is('[disabled]')).toEqual(true);
  });

  it('should call onClick props Button', () => {
    const id = 'Button';
    const text = 'testText';
    const spy = jest.fn();
    const props = {
      id,
      children: text,
      onClick: spy,
    };
    const wrapper = getWrapper(props);
    wrapper.simulate('click');
    expect(spy.mock.calls.length).toEqual(1);
  });
});
