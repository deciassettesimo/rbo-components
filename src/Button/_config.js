import { TYPES, DISPLAY, DIMENSIONS } from './_constants';

const REFS = {
  TYPES,
  DISPLAY,
  DIMENSIONS,
};

export default REFS;
