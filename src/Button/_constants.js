import { GLOBAL, STYLES } from '../_constants';
import { DIMENSIONS as INPUT_DIMENSIONS, TYPES as INPUT_TYPES } from '../Input/_constants';

export const TYPES = {
  BASE_PRIMARY: 'base-primary',
  BASE_SECONDARY: 'base-secondary',
  ACCENT_PRIMARY: 'accent-primary',
  ACCENT_SECONDARY: 'accent-secondary',
  DANGER_PRIMARY: 'danger-primary',
  DANGER_SECONDARY: 'danger-secondary',
  LINK: 'link',
  LINK_PRIMARY: 'link-primary',
  LINK_SECONDARY: 'link-secondary',
  LINK_DANGER: 'link-danger',
};

export const DISPLAY = {
  BLOCK: STYLES.DISPLAY.BLOCK,
  INLINE_BLOCK: STYLES.DISPLAY.INLINE_BLOCK,
};

export const DIMENSIONS = {
  ...INPUT_DIMENSIONS,
};

export const INPUT_TYPE = INPUT_TYPES.BUTTON;

export const WIDTH_MAP = {
  ...GLOBAL.WIDTH_MAP,
};
