import styled from 'styled-components';
import { STYLES } from '../_constants';
import { TYPES, DIMENSIONS, DISPLAY, WIDTH_MAP } from './_constants';
import { StyledIcon, StyledIconSvg } from '../Icon/_style';

const buttonFontSize = (sDimension, isEmphasis) => {
  if (sDimension === DIMENSIONS.XS) {
    if (isEmphasis) return 10;
    return 12;
  }
  if (isEmphasis) return 12;
  return 14;
};

const buttonLineHeight = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 16;
  if (sDimension === DIMENSIONS.XL) return 24;
  return 20;
};

const buttonPaddingV = sDimension => (sDimension - buttonLineHeight(sDimension)) / 2 - 1;

const buttonPaddingH = (sDimension, isOnlyIcon) => {
  if (isOnlyIcon) return 0;
  if (sDimension === DIMENSIONS.XS) return 8;
  if (sDimension === DIMENSIONS.S) return 12;
  if (sDimension === DIMENSIONS.L) return 20;
  if (sDimension === DIMENSIONS.XL) return 24;
  return 16;
};

const buttonBackgroundColor = (sType, isDisabled) => {
  if (isDisabled) return STYLES.COLORS.DISABLED_GRAY;
  switch (sType) {
    case TYPES.BASE_PRIMARY:
      return STYLES.COLORS.BLACK;
    case TYPES.ACCENT_PRIMARY:
      return STYLES.COLORS.CORPORATE_YELLOW;
    case TYPES.ACCENT_SECONDARY:
      return STYLES.COLORS.PEACOCK_BLUE;
    case TYPES.DANGER_PRIMARY:
      return STYLES.COLORS.TOMATO_RED;
    case TYPES.BASE_SECONDARY:
    case TYPES.DANGER_SECONDARY:
    default:
      return STYLES.COLORS.WHITE;
  }
};

const buttonBorderColor = (sType, isDisabled, isFocused) => {
  if (isFocused) return STYLES.COLORS.PERRY_WINKLE;
  if (isDisabled) return STYLES.COLORS.BLACK_20;
  switch (sType) {
    case TYPES.DANGER_SECONDARY:
      return STYLES.COLORS.TOMATO_RED;
    case TYPES.BASE_PRIMARY:
    case TYPES.BASE_SECONDARY:
    case TYPES.ACCENT_PRIMARY:
    case TYPES.ACCENT_SECONDARY:
    case TYPES.DANGER_PRIMARY:
    default:
      return STYLES.COLORS.BLACK_24;
  }
};

const buttonColor = (sType, isDisabled) => {
  if (isDisabled) return STYLES.COLORS.BLACK_48;
  switch (sType) {
    case TYPES.BASE_PRIMARY:
    case TYPES.ACCENT_SECONDARY:
    case TYPES.DANGER_PRIMARY:
      return STYLES.COLORS.WHITE;
    case TYPES.DANGER_SECONDARY:
      return STYLES.COLORS.TOMATO_RED;
    case TYPES.BASE_SECONDARY:
    case TYPES.ACCENT_PRIMARY:
    default:
      return STYLES.COLORS.BLACK;
  }
};

const buttonHoverBackgroundColor = (sType, isDisabled) => {
  if (isDisabled) return STYLES.COLORS.DISABLED_GRAY;
  switch (sType) {
    case TYPES.BASE_PRIMARY:
      return STYLES.COLORS.CORPORATE_YELLOW;
    case TYPES.ACCENT_PRIMARY:
    case TYPES.ACCENT_SECONDARY:
    case TYPES.DANGER_PRIMARY:
      return STYLES.COLORS.BLACK;
    case TYPES.DANGER_SECONDARY:
      return STYLES.COLORS.TOMATO_RED;
    case TYPES.BASE_SECONDARY:
    default:
      return STYLES.COLORS.WHITE;
  }
};

const buttonHoverBorderColor = (sType, isDisabled) => {
  if (isDisabled) return STYLES.COLORS.BLACK_20;
  switch (sType) {
    case TYPES.BASE_PRIMARY:
    case TYPES.ACCENT_PRIMARY:
    case TYPES.ACCENT_SECONDARY:
    case TYPES.DANGER_PRIMARY:
    case TYPES.DANGER_SECONDARY:
    case TYPES.BASE_SECONDARY:
    default:
      return STYLES.COLORS.BLACK;
  }
};

const buttonHoverColor = (sType, isDisabled) => {
  if (isDisabled) return STYLES.COLORS.BLACK_48;
  switch (sType) {
    case TYPES.ACCENT_PRIMARY:
    case TYPES.ACCENT_SECONDARY:
    case TYPES.DANGER_PRIMARY:
    case TYPES.DANGER_SECONDARY:
      return STYLES.COLORS.WHITE;
    case TYPES.BASE_SECONDARY:
    case TYPES.BASE_PRIMARY:
    default:
      return STYLES.COLORS.BLACK;
  }
};

const buttonWidth = (sWidth, sDisplay, isOnlyIcon, sDimension) => {
  if (isOnlyIcon) return `${sDimension}px`;
  if (typeof sWidth === 'number') return `${sWidth}px`;
  return WIDTH_MAP[sWidth] || sWidth || (sDisplay === DISPLAY.BLOCK ? '100%' : 'auto');
};

const buttonIconMarginH = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 4;
  if (sDimension === DIMENSIONS.S) return 6;
  if (sDimension === DIMENSIONS.L) return 10;
  if (sDimension === DIMENSIONS.XL) return 12;
  return 8;
};

const StyledButtonPrimitive = styled.div`
  position: relative;
  box-sizing: border-box;
  display: ${props => props.sDisplay};
  vertical-align: middle;
  max-width: 100%;
  height: ${props => props.sDimension}px;
  line-height: ${props => buttonLineHeight(props.sDimension)}px;
  font-family: inherit;
  font-size: ${props => buttonFontSize(props.sDimension, props.isEmphasis)}px;
  font-weight: ${props => (props.isEmphasis ? 'bold' : 'normal')};
  text-transform: ${props => (props.isEmphasis ? 'uppercase' : 'intherit')};
  letter-spacing: ${props => (props.isEmphasis ? '0.08em' : 'auto')};
  text-align: center;
  color: ${props => buttonColor(props.sType, props.isDisabled)};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  transition: all 0.32s ease-out;
`;

export const StyledButton = StyledButtonPrimitive.withComponent('button').extend.attrs({ type: 'button' })`
  outline: 0 none;
  width: ${props => buttonWidth(props.sWidth, props.sDisplay, props.isOnlyIcon, props.sDimension)};
  padding: ${props => buttonPaddingV(props.sDimension)}px
    ${props => buttonPaddingH(props.sDimension, props.isOnlyIcon)}px;
  background: ${props => buttonBackgroundColor(props.sType, props.isDisabled)};
  border: 1px solid ${props => buttonBorderColor(props.sType, props.isDisabled, props.isFocused)};
  cursor: ${props => (props.isDisabled ? 'default' : 'pointer')};

  ${StyledIcon} {
    top: -1px;
    margin: 0 ${props => buttonIconMarginH(props.sDimension)}px;

    :first-child {
      margin-left: -${props => buttonIconMarginH(props.sDimension)}px;
    }

    :last-child {
      margin-right: -${props => buttonIconMarginH(props.sDimension)}px;
    }
  }

  ${StyledIconSvg} {
    fill: ${props => buttonColor(props.sType, props.isDisabled)};
    transition: fill 0.32s ease-out;
  }

  :hover {
    background: ${props => buttonHoverBackgroundColor(props.sType, props.isDisabled)};
    border: 1px solid ${props => buttonHoverBorderColor(props.sType, props.isDisabled)};
    color: ${props => buttonHoverColor(props.sType, props.isDisabled)};

    ${StyledIconSvg} {
      fill: ${props => buttonHoverColor(props.sType, props.isDisabled)};
    }
  }
`;

export const StyledButtonInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  opacity: ${props => (props.isLoading ? 0 : 1)};
`;

export const StyledButtonLink = StyledButtonPrimitive.extend`
  width: ${props => buttonWidth(props.sWidth, props.sDisplay)};
  padding: ${props => buttonPaddingV(props.sDimension) + 1}px 0;
  cursor: default;
`;

export const StyledButtonLinkInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  opacity: ${props => (props.isLoading ? 0.16 : 1)};
`;
