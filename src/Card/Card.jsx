import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENT } from './_constants';
import REFS from './_config';
import { StyledCard } from './_style';

const Card = props => (
  <StyledCard
    {...addDataAttributes({ component: COMPONENT })}
    sDisplay={props.display}
    isBordered={props.isBordered}
    isShadowed={props.isShadowed}
  >
    {props.children}
  </StyledCard>
);

Card.propTypes = {
  children: PropTypes.node.isRequired,
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** bordered flag */
  isBordered: PropTypes.bool,
  /** shadowed flag */
  isShadowed: PropTypes.bool,
};

Card.defaultProps = {
  display: REFS.DISPLAY.BLOCK,
  isBordered: true,
  isShadowed: false,
};

Card.REFS = {
  ...REFS,
};

export default Card;
