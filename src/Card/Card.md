```js
<Card>
  <div style={{ padding: 32 }}>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
    fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
    sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
    ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
    venenatis egestas scelerisque. In hac habitasse platea dictumst.
    Mauris ut ex a elit auctor suscipit in ut nibh.
  </div>
</Card>
```

```js
<Card isShadowed>
  <div style={{ padding: 32 }}>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
    fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
    sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
    ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
    venenatis egestas scelerisque. In hac habitasse platea dictumst.
    Mauris ut ex a elit auctor suscipit in ut nibh.
  </div>
</Card>
```

```js
<Card isBordered={false} isShadowed>
  <div style={{ padding: 32 }}>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
    fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
    sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
    ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
    venenatis egestas scelerisque. In hac habitasse platea dictumst.
    Mauris ut ex a elit auctor suscipit in ut nibh.
  </div>
</Card>
```

```js
<Card display={Card.REFS.DISPLAY.INLINE_BLOCK}>
  <div style={{ width: 320, padding: 16 }}>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
    fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
    sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
    ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
    venenatis egestas scelerisque. In hac habitasse platea dictumst.
    Mauris ut ex a elit auctor suscipit in ut nibh.
  </div>
</Card>
```
