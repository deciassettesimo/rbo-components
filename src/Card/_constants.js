import { STYLES } from '../_constants';

export const COMPONENT = 'Card';

export const DISPLAY = {
  BLOCK: STYLES.DISPLAY.BLOCK,
  INLINE_BLOCK: STYLES.DISPLAY.INLINE_BLOCK,
};

export default DISPLAY;
