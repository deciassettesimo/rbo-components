import styled from 'styled-components';

import { STYLES } from '../_constants';

const cardBorder = isBordered => (isBordered ? `1px solid ${STYLES.COLORS.BLACK_24}` : 'none');

const cardBoxShadow = isShadowed => (isShadowed ? `0 6px 12px ${STYLES.COLORS.BLACK_12}` : 'none');

export const StyledCard = styled.div`
  position: relative;
  box-sizing: border-box;
  display: ${props => props.sDisplay};
  background: ${STYLES.COLORS.WHITE};
  border: ${props => cardBorder(props.isBordered)};
  box-shadow: ${props => cardBoxShadow(props.isShadowed)};
`;

export default StyledCard;
