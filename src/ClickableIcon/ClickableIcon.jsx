import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENT } from './_constants';
import StyledClickableIcon from './_style';
import IconMain from '../Icon';
import { TYPES } from '../Icon/_constants';

const ClickableIcon = props => {
  const handleClick = e => {
    if (!props.isActive && !props.isDisabled) props.onClick(e);
  };

  return (
    <StyledClickableIcon
      {...addDataAttributes({ component: COMPONENT, type: props.type })}
      title={props.title}
      sDisplay={props.display}
      sDimension={props.dimension}
      isExpanded={props.isExpanded}
      isActive={props.isActive}
      isDisabled={props.isDisabled}
      onClick={handleClick}
    >
      <IconMain
        type={props.type}
        display={IconMain.REFS.DISPLAY.BLOCK}
        dimension={props.dimension}
        color={props.color}
      />
    </StyledClickableIcon>
  );
};

ClickableIcon.propTypes = {
  /** type of icon */
  type: PropTypes.oneOf(Object.values(TYPES)).isRequired,
  /** css-property display */
  display: PropTypes.oneOf(Object.values(IconMain.REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(IconMain.REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
  /** flag for expand (add padding) clickable area */
  isExpanded: PropTypes.bool,
  /** flag for active icon */
  isActive: PropTypes.bool,
  /** flag for disabled icon */
  isDisabled: PropTypes.bool,
  /** click handler */
  onClick: PropTypes.func.isRequired,
};

ClickableIcon.defaultProps = {
  display: IconMain.REFS.DISPLAY.INLINE_BLOCK,
  dimension: IconMain.REFS.DIMENSIONS.M,
  color: undefined,
  title: null,
  isExpanded: false,
  isActive: false,
  isDisabled: false,
};

ClickableIcon.REFS = {
  ...IconMain.REFS,
};

export default ClickableIcon;
