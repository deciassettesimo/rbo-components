Clickable icon supports all simple icons and contains additional props onClick, isActive, isDisabled, isExpanded

###  Icon with click handler

```js
<div className="list">
    <ClickableIcon type={ClickableIcon.REFS.TYPES.LOGOUT} onClick={()=>alert('click')} />
    <ClickableIcon type={ClickableIcon.REFS.TYPES.DOCUMENT_SEND} onClick={()=>alert('click')} />
    <ClickableIcon type={ClickableIcon.REFS.TYPES.VIEW_CARDS} onClick={()=>alert('click')} />
    <ClickableIcon type={ClickableIcon.REFS.TYPES.VIEW_LIST} onClick={()=>alert('click')} />
</div>
 ```

###  Icon with isExpanded flag

```js
<div className="list">
    <ClickableIcon type={ClickableIcon.REFS.TYPES.LOGOUT} isExpanded onClick={()=>alert('click')} />
    <ClickableIcon type={ClickableIcon.REFS.TYPES.DOCUMENT_SEND} isExpanded onClick={()=>alert('click')} />
    <ClickableIcon type={ClickableIcon.REFS.TYPES.VIEW_CARDS} isExpanded onClick={()=>alert('click')} />
    <ClickableIcon type={ClickableIcon.REFS.TYPES.VIEW_LIST} isExpanded onClick={()=>alert('click')} />
</div>

 ```