import React from 'react';
import { mount } from 'enzyme';
import ClickableIcon from '../ClickableIcon';

const getWrapper = props => mount(<ClickableIcon type={ClickableIcon.REFS.TYPES.ACCOUNT_CHECKED} {...props} />);

describe('ClickableIcon Component', () => {
  it('should render ClickableIcon with normal props', () => {
    const props = {
      onClick: f => f,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.text()).toEqual('');
  });

  it('should call onClick props ClickableIcon', () => {
    const spy = jest.fn();
    const props = {
      onClick: spy,
    };
    const wrapper = getWrapper(props);

    // test default props, must call onClick
    wrapper.simulate('click');
    expect(spy.mock.calls.length).toEqual(1);

    // test disabled flag
    wrapper.setProps({ isDisabled: true });
    wrapper.simulate('click');
    expect(spy.mock.calls.length).toEqual(1);

    // test isActive flag
    wrapper.setProps({ isDisabled: false, isActive: true });
    wrapper.simulate('click');
    expect(spy.mock.calls.length).toEqual(1);

    // test cancel isDisabled and isActive flag, must call onClick
    wrapper.setProps({ isDisabled: false, isActive: false });
    wrapper.simulate('click');
    expect(spy.mock.calls.length).toEqual(2);
  });
});
