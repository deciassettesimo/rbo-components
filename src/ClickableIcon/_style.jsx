import styled from 'styled-components';

const clickableIconDimension = (sDimension, isExpanded) => {
  if (isExpanded && sDimension) {
    if (sDimension < 20) return sDimension + 16;
    if (sDimension < 24) return sDimension + 12;
    if (sDimension < 32) return sDimension + 8;
    return sDimension;
  }
  return sDimension;
};

const clickableIconPadding = (sDimension, isExpanded) => {
  if (isExpanded && sDimension) {
    if (sDimension < 20) return 8;
    if (sDimension < 24) return 6;
    if (sDimension < 32) return 4;
  }
  return 0;
};

const clickableIconCursor = (isActive, isDisabled) => {
  if (isActive || isDisabled) return 'default';
  return 'pointer';
};

const clickableIconOpacity = (isActive, isDisabled, isHovered) => {
  if (isActive) return 1;
  if (isDisabled) return 0.4;
  if (isHovered) return 1;
  return 0.8;
};

const StyledClickableIcon = styled.div`
  position: relative;
  box-sizing: border-box;
  vertical-align: middle;
  display: ${props => props.sDisplay};
  height: ${props => clickableIconDimension(props.sDimension, props.isExpanded)}px;
  width: ${props => clickableIconDimension(props.sDimension, props.isExpanded)}px;
  padding: ${props => clickableIconPadding(props.sDimension, props.isExpanded)}px;
  cursor: ${props => clickableIconCursor(props.isActive, props.isDisabled)};
  opacity: ${props => clickableIconOpacity(props.isActive, props.isDisabled, false)};
  transition: opacity 0.32s ease-out;

  :hover {
    opacity: ${props => clickableIconOpacity(props.isActive, props.isDisabled, true)};
  }
`;

export default StyledClickableIcon;
