import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import Button from '../Button';
import Popup, { Opener, Box } from '../Popup';
import Card from '../Card';
import IconArrowDownFilled from '../Icon/IconArrowDownFilled';

import { COMPONENTS, EXTRA_BUTTON_PREFIX } from './_constants';
import REFS from './_config';
import {
  StyledComboButton,
  StyledComboButtonInner,
  StyledComboButtonMain,
  StyledComboButtonExtra,
  StyledComboButtonPopup,
  StyledComboButtonItems,
} from './_style';
import ComboButtonItem from './_internal/ComboButtonItem';

export default class ComboButton extends PureComponent {
  static REFS = { ...REFS };

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Type */
    type: PropTypes.oneOf(Object.values(REFS.TYPES)),
    /** CSS-property display */
    display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Emphasis flag */
    isEmphasis: PropTypes.bool,
    /** With Icon flag (= true if Icon component is in props.children) */
    isWithIcon: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    popupWidth: PropTypes.number,
    popupMaxHeight: PropTypes.number,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        items: PropTypes.arrayOf(PropTypes.shape()),
        itemRenderer: PropTypes.func,
      }),
    ).isRequired,
    itemRenderer: PropTypes.func,
    /** Focus handler
     * @param {string} id Component Id
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * */
    onBlur: PropTypes.func,
    /** Click handler
     * @param {string} id Component Id
     * */
    onClick: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    type: REFS.TYPES.BASE_SECONDARY,
    display: REFS.DISPLAY.INLINE_BLOCK,
    dimension: REFS.DIMENSIONS.M,
    width: 'auto',
    isEmphasis: false,
    isWithIcon: false,
    disabled: false,
    popupWidth: null,
    popupMaxHeight: 400,
    itemRenderer: null,
    onFocus: () => null,
    onBlur: () => null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isPopupOpen: false,
    };
  }

  getExtraIconDimension = () => {
    switch (this.props.dimension) {
      case REFS.DIMENSIONS.S:
      case REFS.DIMENSIONS.XS:
        return IconArrowDownFilled.REFS.DIMENSIONS.XS;
      case REFS.DIMENSIONS.L:
      case REFS.DIMENSIONS.XL:
        return IconArrowDownFilled.REFS.DIMENSIONS.M;
      case REFS.DIMENSIONS.M:
      default:
        return IconArrowDownFilled.REFS.DIMENSIONS.S;
    }
  };

  handlePopupOpen = () => {
    this.setState({ isPopupOpen: true });
  };

  handlePopupClose = () => {
    this.setState({ isPopupOpen: false });
  };

  handleButtonClick = () => {
    this.props.onClick({ id: this.props.id, items: [] });
  };

  handleItemClick = (...args) => {
    this.handlePopupClose();
    this.props.onClick({ id: this.props.id, items: args });
  };

  render() {
    return (
      <StyledComboButton
        {...addDataAttributes({ component: COMPONENTS.GENERAL })}
        sDisplay={this.props.display}
        sWidth={this.props.width}
      >
        <Popup isOpened={this.state.isPopupOpen} onClose={this.handlePopupClose}>
          <Opener display={Opener.REFS.DISPLAY.BLOCK} isNotAutoOpen>
            <StyledComboButtonInner>
              <StyledComboButtonMain>
                <Button
                  id={this.props.id}
                  type={this.props.type}
                  display={Button.REFS.DISPLAY.BLOCK}
                  dimension={this.props.dimension}
                  isEmphasis={this.props.isEmphasis}
                  isWithIcon={this.props.isWithIcon}
                  disabled={this.props.disabled}
                  onFocus={this.props.onFocus}
                  onBlur={this.props.onBlur}
                  onClick={this.handleButtonClick}
                >
                  {this.props.children}
                </Button>
              </StyledComboButtonMain>
              <StyledComboButtonExtra>
                <Button
                  id={`${this.props.id}${EXTRA_BUTTON_PREFIX}`}
                  type={this.props.type}
                  dimension={this.props.dimension}
                  isEmphasis={this.props.isEmphasis}
                  isOnlyIcon
                  disabled={this.props.disabled}
                  onFocus={this.props.onFocus}
                  onBlur={this.props.onBlur}
                  onClick={this.handlePopupOpen}
                >
                  <IconArrowDownFilled dimension={this.getExtraIconDimension()} />
                </Button>
              </StyledComboButtonExtra>
            </StyledComboButtonInner>
          </Opener>
          <Box placement={Box.REFS.PLACEMENT.BOTTOM} align={Box.REFS.ALIGN.START}>
            <Card isShadowed>
              <StyledComboButtonPopup sWidth={this.props.popupWidth} sMaxHeight={this.props.popupMaxHeight}>
                <StyledComboButtonItems>
                  {this.props.items.map(item => (
                    <ComboButtonItem
                      key={item.id}
                      id={item.id}
                      dimension={this.props.dimension}
                      title={item.title}
                      items={item.items}
                      renderer={this.props.itemRenderer}
                      itemRenderer={item.itemRenderer}
                      data={item}
                      onClick={this.handleItemClick}
                    />
                  ))}
                </StyledComboButtonItems>
              </StyledComboButtonPopup>
            </Card>
          </Box>
        </Popup>
      </StyledComboButton>
    );
  }
}
