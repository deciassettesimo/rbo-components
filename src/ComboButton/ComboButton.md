### ComboButton Dimensions and Types

```js
const items = [
  {
    id: 'button1',
    title: 'Кнопка номер один',
  },
  {
    id: 'button2',
    title: 'Кнопка номер два',
  },
  {
    id: 'button3',
    title: 'Кнопка номер три:',
    items: [
      {
        id: 'button31',
        title: 'Кнопка номер три точка один',
        describe: 'Описание кнопки номер три точка один',
      },
      {
        id: 'button32',
        title: 'Кнопка номер три точка два',
        describe: 'Описание кнопки номер три точка два',
      },
      {
        id: 'button33',
        title: 'Кнопка номер три точка три',
        describe: 'Описание кнопки номер три точка три',
      },
    ]
  },
];

<div className="list">
  <div>
    <ComboButton
      id="ComboButtonXS"
      dimension={ComboButton.REFS.DIMENSIONS.XS}
      type={ComboButton.REFS.TYPES.BASE_PRIMARY}
      items={items}
      onClick={({ id, items })=>{ console.log('click:', id, items); }}
    >
      ComboButton XS
    </ComboButton>
  </div>
  <div>
    <ComboButton
      id="ComboButtonS"
      dimension={ComboButton.REFS.DIMENSIONS.S}
      type={ComboButton.REFS.TYPES.BASE_SECONDARY}
      items={items}
      onClick={({ id, items })=>{ console.log('click:', id, items); }}
    >
      ComboButton S
    </ComboButton>
  </div>
  <div>
    <ComboButton
      id="ComboButtonM"
      dimension={ComboButton.REFS.DIMENSIONS.M}
      type={ComboButton.REFS.TYPES.ACCENT_PRIMARY}
      items={items}
      onClick={({ id, items })=>{ console.log('click:', id, items); }}
    >
      ComboButton M
    </ComboButton>
  </div>
  <div>
    <ComboButton
      id="ComboButtonL"
      dimension={ComboButton.REFS.DIMENSIONS.L}
      type={ComboButton.REFS.TYPES.ACCENT_SECONDARY}
      items={items}
      onClick={({ id, items })=>{ console.log('click:', id, items); }}
    >
      ComboButton L
    </ComboButton>
  </div>
  <div>
    <ComboButton
      id="ComboButtonXL"
      dimension={ComboButton.REFS.DIMENSIONS.XL}
      type={ComboButton.REFS.TYPES.DANGER_PRIMARY}
      items={items}
      onClick={({ id, items })=>{ console.log('click:', id, items); }}
    >
      ComboButton XL
    </ComboButton>
  </div>
  <div>
    <ComboButton
      id="ComboButtonXL"
      dimension={ComboButton.REFS.DIMENSIONS.XL}
      type={ComboButton.REFS.TYPES.DANGER_SECONDARY}
      items={items}
      onClick={({ id, items })=>{ console.log('click:', id, items); }}
    >
      ComboButton XL
    </ComboButton>
  </div>
</div>
```
### ComboButton Display Block, isEmphasis, Disabled

```js
const items = [
  {
    id: 'button1',
    title: 'Кнопка номер один',
  },
  {
    id: 'button2',
    title: 'Кнопка номер два',
  },
  {
    id: 'button3',
    title: 'Кнопка номер три:',
    items: [
      {
        id: 'button31',
        title: 'Кнопка номер три точка один',
        describe: 'Описание кнопки номер три точка один',
      },
      {
        id: 'button32',
        title: 'Кнопка номер три точка два',
        describe: 'Описание кнопки номер три точка два',
      },
      {
        id: 'button33',
        title: 'Кнопка номер три точка три',
        describe: 'Описание кнопки номер три точка три',
      },
    ]
  },
];

<div className="list">
  <div>
    <ComboButton
      id="ComboButtonDisplayBlock"
      display={ComboButton.REFS.DISPLAY.BLOCK}
      items={items}
      onClick={({ id, items })=>{ console.log('click:', id, items); }}
    >
      ComboButton Display Block
    </ComboButton>
  </div>
  <div>
    <ComboButton
      id="ComboButtonDisabled"
      disabled
      items={items}
      onClick={({ id, items })=>{ console.log('click:', id, items); }}
    >
      ComboButton Disabled
    </ComboButton>
  </div>
  <div>
    <ComboButton
      id="ComboButtonWithIcon"
      isWithIcon
      isEmphasis
      items={items}
      onClick={({ id, items })=>{ console.log('click:', id, items); }}
    >
      <IconWarning dimension={IconWarning.REFS.DIMENSIONS.S} />
      <span>ComboButton With Icon</span>
    </ComboButton>
  </div>
</div>
```
