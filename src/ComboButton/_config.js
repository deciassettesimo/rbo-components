import BUTTON_REFS from '../Button/_config';

const REFS = { ...BUTTON_REFS };

export default REFS;
