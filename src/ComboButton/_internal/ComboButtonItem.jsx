import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS, DIMENSIONS } from '../_constants';
import {
  StyledComboButtonBlock,
  StyledComboButtonBlockTitle,
  StyledComboButtonBlockContent,
  StyledComboButtonItems,
  StyledComboButtonItem,
} from '../_style';

export default class ComboButtonItem extends PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
    title: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        items: PropTypes.arrayOf(PropTypes.shape()),
        itemRenderer: PropTypes.func,
      }),
    ),
    renderer: PropTypes.func,
    itemRenderer: PropTypes.func,
    data: PropTypes.shape(),
    onClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    dimension: DIMENSIONS.M,
    items: null,
    renderer: null,
    itemRenderer: null,
    data: {},
  };

  handleClick = () => {
    this.props.onClick(this.props.id);
  };

  handleItemClick = (...args) => {
    this.props.onClick(this.props.id, ...args);
  };

  render() {
    if (this.props.items) {
      return (
        <StyledComboButtonBlock>
          <StyledComboButtonBlockTitle sDimension={this.props.dimension}>
            {this.props.title}
          </StyledComboButtonBlockTitle>
          <StyledComboButtonBlockContent>
            <StyledComboButtonItems>
              {this.props.items.map(item => (
                <ComboButtonItem
                  key={item.id}
                  id={item.id}
                  dimension={this.props.dimension}
                  title={item.title}
                  items={item.items}
                  renderer={this.props.itemRenderer}
                  itemRenderer={item.itemRenderer}
                  data={item}
                  onClick={this.handleItemClick}
                />
              ))}
            </StyledComboButtonItems>
          </StyledComboButtonBlockContent>
        </StyledComboButtonBlock>
      );
    }

    return (
      <StyledComboButtonItem
        {...addDataAttributes({ component: COMPONENTS.ITEM })}
        sDimension={this.props.dimension}
        onClick={this.handleClick}
      >
        {this.props.renderer ? this.props.renderer({ id: this.props.id, data: this.props.data }) : this.props.title}
      </StyledComboButtonItem>
    );
  }
}
