import styled from 'styled-components';

import { STYLES } from '../_constants';

import { DIMENSIONS } from './_constants';

const itemFontSize = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 12;
  return 14;
};

const itemLineHeight = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 16;
  if (sDimension === DIMENSIONS.XL) return 24;
  return 20;
};

const itemPaddingV = sDimension => (sDimension - itemLineHeight(sDimension)) / 2 - 1;

const itemPaddingH = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 8;
  if (sDimension === DIMENSIONS.S) return 12;
  if (sDimension === DIMENSIONS.L) return 20;
  if (sDimension === DIMENSIONS.XL) return 24;
  return 16;
};

export const StyledComboButton = styled.div`
  position: relative;
  box-sizing: border-box;
  display: ${props => props.sDisplay};
  vertical-align: middle;
  width: ${props => props.sWidth};
`;

export const StyledComboButtonInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
`;

export const StyledComboButtonMain = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  flex-grow: 1;
  padding-right: 1px;
`;

export const StyledComboButtonExtra = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  flex-grow: 0;
  padding-left: 1px;
`;

export const StyledComboButtonPopup = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px 0;
  width: ${props => (props.sWidth ? `${props.sWidth}px` : 'auto')};
  max-height: ${props => props.sMaxHeight}px;
  overflow-y: auto;
`;

export const StyledComboButtonItems = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledComboButtonBlock = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledComboButtonBlockTitle = styled.div`
  position: relative;
  box-sizing: border-box;
  min-height: ${props => props.sDimension}px;
  width: 100%;
  line-height: ${props => itemLineHeight(props.sDimension)}px;
  padding: ${props => itemPaddingV(props.sDimension)}px ${props => itemPaddingH(props.sDimension)}px;
  border-top: 1px solid ${STYLES.COLORS.BLACK_20};
  font-family: inherit;
  font-size: ${props => itemFontSize(props.sDimension)}px;
  font-weight: bold;
  word-wrap: break-word;
  background-color: ${STYLES.COLORS.WHITE};
`;

export const StyledComboButtonBlockContent = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledComboButtonItem = styled.div`
  position: relative;
  box-sizing: border-box;
  min-height: ${props => props.sDimension}px;
  width: 100%;
  line-height: ${props => itemLineHeight(props.sDimension)}px;
  padding: ${props => itemPaddingV(props.sDimension)}px ${props => itemPaddingH(props.sDimension)}px;
  font-family: inherit;
  font-size: ${props => itemFontSize(props.sDimension)}px;
  word-wrap: break-word;
  background-color: ${STYLES.COLORS.WHITE};
  cursor: pointer;

  &:hover {
    background-color: ${STYLES.COLORS.ICE_BLUE};
  }
`;
