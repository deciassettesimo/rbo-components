import PropTypes from 'prop-types';

import REFS from './_config';
import { TYPES } from './_constants';
import * as Items from './_items';

const DoubleColorIconMap = {
  [TYPES.CASE]: Items.DoubleColorIconCase,
  [TYPES.PERCENT]: Items.DoubleColorIconPercent,
  [TYPES.WALLET]: Items.DoubleColorIconWallet,
};

const DoubleColorIcon = props =>
  DoubleColorIconMap[props.type]({
    display: props.display,
    dimension: props.dimension,
    color: props.color,
    secondaryColor: props.secondaryColor,
    title: props.title,
  });

DoubleColorIcon.propTypes = {
  /** type of icon */
  type: PropTypes.oneOf(Object.values(TYPES)).isRequired,
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#000000') */
  color: PropTypes.string,
  /** secondary color for svg fill (e.g. '#FFF2AE') */
  secondaryColor: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

DoubleColorIcon.REFS = {
  ...REFS,
  TYPES,
};

export default DoubleColorIcon;
