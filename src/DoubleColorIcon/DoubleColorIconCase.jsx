import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import DoubleColorIconPrimitive from './_internal/DoubleColorIconPrimitive';

const DoubleColorIconCase = props => (
  <DoubleColorIconPrimitive {...props} type={TYPES.CASE} viewBox={REFS.DIMENSIONS.M}>
    <path d="M23,32 L11,32 L11,29 L17,29 L17,15 L20,15 L20,29 L23,29 L23,25 L33,25 L33,35 L23,35 L23,32 Z M45,32 L33,32 L33,29 L45,29 L45,15 L11,15 L11,39 L45,39 L45,32 Z M48,39 L52,39 L52,42 L4,42 L4,39 L8,39 L8,12 L20,12 L20,6 L36,6 L36,12 L48,12 L48,39 Z M33,12 L33,9 L23,9 L23,12 L33,12 Z M36,15 L39,15 L39,29 L36,29 L36,15 Z M26,28 L26,32 L30,32 L30,28 L26,28 Z" />
  </DoubleColorIconPrimitive>
);

DoubleColorIconCase.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#000000') */
  color: PropTypes.string,
  /** secondary color for svg fill (e.g. '#FFF2AE') */
  secondaryColor: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

DoubleColorIconCase.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.BLACK,
  secondaryColor: REFS.COLORS.PARCHMENT,
  title: null,
};

DoubleColorIconCase.REFS = {
  ...REFS,
};

export default DoubleColorIconCase;
