import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import DoubleColorIconPrimitive from './_internal/DoubleColorIconPrimitive';

const DoubleColorIconPercent = props => (
  <DoubleColorIconPrimitive {...props} type={TYPES.PERCENT} viewBox={REFS.DIMENSIONS.M}>
    <path d="M45,9 L11,9 L11,34 L45,34 L45,9 Z M48,9 L48,37 L8,37 L8,6 L48,6 L48,9 Z M2,39 L54,39 L54,42 L2,42 L2,39 Z M34.5,30 C32.8923048,30 31.4067332,29.1423049 30.6028856,27.75 C29.799038,26.3576952 29.799038,24.6423048 30.6028856,23.25 C31.4067332,21.8576951 32.8923048,21 34.5,21 C36.9852814,21 39,23.0147186 39,25.5 C39,27.9852814 36.9852814,30 34.5,30 Z M22.5,21 C20.0147186,21 18,18.9852814 18,16.5 C18,14.0147186 20.0147186,12 22.5,12 C24.9852814,12 27,14.0147186 27,16.5 C27,18.9852814 24.9852814,21 22.5,21 Z M22.097,30.023 L19.903,27.977 L33.903,12.977 L36.097,15.023 L22.097,30.023 Z M22.5,18 C23.3284271,18 24,17.3284271 24,16.5 C24,15.6715729 23.3284271,15 22.5,15 C21.6715729,15 21,15.6715729 21,16.5 C21,17.3284271 21.6715729,18 22.5,18 Z M34.5,27 C35.0358984,27 35.531089,26.7141016 35.7990382,26.25 C36.0669874,25.7858984 36.0669874,25.2141016 35.7990382,24.75 C35.531089,24.2858984 35.0358984,24 34.5,24 C33.6715729,24 33,24.6715729 33,25.5 C33,26.3284271 33.6715729,27 34.5,27 Z" />
  </DoubleColorIconPrimitive>
);

DoubleColorIconPercent.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#000000') */
  color: PropTypes.string,
  /** secondary color for svg fill (e.g. '#FFF2AE') */
  secondaryColor: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

DoubleColorIconPercent.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.BLACK,
  secondaryColor: REFS.COLORS.PARCHMENT,
  title: null,
};

DoubleColorIconPercent.REFS = {
  ...REFS,
};

export default DoubleColorIconPercent;
