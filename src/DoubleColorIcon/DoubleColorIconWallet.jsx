import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import DoubleColorIconPrimitive from './_internal/DoubleColorIconPrimitive';

const DoubleColorIconWallet = props => (
  <DoubleColorIconPrimitive {...props} type={TYPES.WALLET} viewBox={REFS.DIMENSIONS.M}>
    <path d="M8,39 L8,14 L8.733,14 L33.536,4.5 L33.536,10.062 L41.482,8.801 L41.482,14 L48,14 L48,39 L51,39 L51,42 L5,42 L5,39 L8,39 Z M45,39 L45,34 L33,34 L33,21 L45,21 L45,17 L17,17 L17,39 L45,39 Z M14,39 L14,17 L11,17 L11,39 L14,39 Z M27.864,14 L38.482,14 L38.482,12.315 L27.864,14 Z M30.536,10.538 L30.536,8.861 L23.059,11.725 L30.536,10.538 Z M36,24 L36,31 L45,31 L45,24 L36,24 Z M39,26 L42,26 L42,29 L39,29 L39,26 Z" />
  </DoubleColorIconPrimitive>
);

DoubleColorIconWallet.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#000000') */
  color: PropTypes.string,
  /** secondary color for svg fill (e.g. '#FFF2AE') */
  secondaryColor: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

DoubleColorIconWallet.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.BLACK,
  secondaryColor: REFS.COLORS.PARCHMENT,
  title: null,
};

DoubleColorIconWallet.REFS = {
  ...REFS,
};

export default DoubleColorIconWallet;
