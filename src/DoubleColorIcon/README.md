### REFS

```js static
{
  DISPLAY: {
    BLOCK: 'block',
    INLINE_BLOCK: 'inline-block',
  },
  DIMENSIONS: {
    M: 56,
  },
  COLORS: STYLES.COLORS,
}
```

### DoubleColorIcon SET

```js
<div className="list">
  <div className="inline"><DoubleColorIconCase title="DoubleColorIconCase" /></div>
  <div className="inline"><DoubleColorIconPercent title="DoubleColorIconPercent" /></div>
  <div className="inline"><DoubleColorIconWallet title="DoubleColorIconWallet" /></div>
</div>
```