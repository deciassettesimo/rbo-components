import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import DoubleColorIcon from '../DoubleColorIcon';

describe('DoubleColorIcon', () => {
  const typeKeys = Object.keys(DoubleColorIcon.REFS.TYPES);

  // loop by types DoubleColorIcon.REFS.TYPES (all icons)
  for (let i = 0; i < typeKeys.length; i += 1) {
    const iconType = DoubleColorIcon.REFS.TYPES[typeKeys[i]];

    describe(`${typeKeys[i]} (${iconType})`, () => {
      const dimensions = Object.values(DoubleColorIcon.REFS.DIMENSIONS);

      // loop by dimensions of DoubleColorIcon
      for (let j = 0; j < dimensions.length; j += 1) {
        const iconDimension = dimensions[j];

        it(`dimensions ${iconDimension}`, () => {
          const wrapper = renderer.create(<DoubleColorIcon type={iconType} dimension={iconDimension} />).toJSON();
          // expect with snapshot
          expect(wrapper).toMatchSnapshot();
        });
      }
    });
  }
});
