import { STYLES } from '../_constants';

export const COMPONENT = 'DoubleColorIcon';

export const TYPES = {
  CASE: 'case',
  PERCENT: 'percent',
  WALLET: 'wallet',
};

export const DISPLAY = {
  BLOCK: STYLES.DISPLAY.BLOCK,
  INLINE_BLOCK: STYLES.DISPLAY.INLINE_BLOCK,
};

export const DIMENSIONS = {
  M: 56,
};

export const { COLORS } = STYLES;
