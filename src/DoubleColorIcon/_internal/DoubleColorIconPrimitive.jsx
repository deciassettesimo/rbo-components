import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENT, TYPES } from '../_constants';
import REFS from '../_config';
import {
  StyledDoubleColorIconIcon,
  StyledDoubleColorIconPrimarySvg,
  StyledDoubleColorIconSecondarySvg,
} from '../_style';

const DoubleColorIconPrimitive = props => (
  <StyledDoubleColorIconIcon
    {...addDataAttributes({ component: COMPONENT, type: props.type })}
    title={props.title}
    sDisplay={props.display}
    sDimension={props.dimension}
  >
    <StyledDoubleColorIconSecondarySvg
      viewBox={`0 0 ${props.viewBox} ${props.viewBox}`}
      focusable="false"
      sColor={props.secondaryColor}
    >
      {props.children}
    </StyledDoubleColorIconSecondarySvg>
    <StyledDoubleColorIconPrimarySvg
      viewBox={`0 0 ${props.viewBox} ${props.viewBox}`}
      focusable="false"
      sColor={props.color}
    >
      {props.children}
    </StyledDoubleColorIconPrimarySvg>
  </StyledDoubleColorIconIcon>
);

DoubleColorIconPrimitive.propTypes = {
  type: PropTypes.oneOf(Object.values(TYPES)),
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  color: PropTypes.string,
  secondaryColor: PropTypes.string,
  title: PropTypes.string,
  viewBox: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  children: PropTypes.node.isRequired,
};

DoubleColorIconPrimitive.defaultProps = {
  type: null,
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  viewBox: REFS.DIMENSIONS.M,
  color: REFS.COLORS.BLACK,
  secondaryColor: REFS.COLORS.PARCHMENT,
  title: null,
};

export default DoubleColorIconPrimitive;
