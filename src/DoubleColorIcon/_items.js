export { default as DoubleColorIconCase } from './DoubleColorIconCase';
export { default as DoubleColorIconPercent } from './DoubleColorIconPercent';
export { default as DoubleColorIconWallet } from './DoubleColorIconWallet';
