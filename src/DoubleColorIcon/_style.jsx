import styled from 'styled-components';

export const StyledDoubleColorIconIcon = styled.div`
  position: relative;
  box-sizing: border-box;
  vertical-align: middle;
  display: ${props => props.sDisplay};
  height: ${props => props.sDimension}px;
  width: ${props => props.sDimension}px;
  overflow: hidden;
`;

export const StyledDoubleColorIconSvg = styled.svg`
  position: absolute;
  display: block;
  width: 100%;
  height: 100%;
  fill: ${props => props.sColor};
`;

export const StyledDoubleColorIconPrimarySvg = styled(StyledDoubleColorIconSvg)`
  z-index: 2;
  top: 0;
  left: 0;
`;

export const StyledDoubleColorIconSecondarySvg = styled(StyledDoubleColorIconSvg)`
  z-index: 1;
  top: 2px;
  left: 2px;
`;
