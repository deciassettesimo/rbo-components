import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import { v4 } from 'uuid';

import { addDataAttributes } from '../_utils';

import Loader from '../Loader';

import FilesManagerItem from './_internal/FilesManagerItem';
import FilesManagerAdd from './_internal/FilesManagerAdd';

import REFS from './_config';
import { COMPONENTS, LABELS, COMMENT_INPUT_DIMENSIONS } from './_constants';
import {
  StyledFilesManager,
  StyledFilesManagerLoading,
  StyledFilesManagerItems,
  StyledFilesManagerLabel,
} from './_style';
import { checkConstraints, calcFilesTotalSize } from './_utils';

export default class FilesManager extends PureComponent {
  static REFS = { ...REFS };

  static propTypes = {
    /** language for text */
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        type: PropTypes.string,
        name: PropTypes.string,
        size: PropTypes.number,
        date: PropTypes.string,
        comment: PropTypes.string,
        isDownloadable: PropTypes.bool,
        file: PropTypes.shape(),
      }),
    ),
    isEditable: PropTypes.bool,
    isLoading: PropTypes.bool,
    maxFilesQty: PropTypes.number,
    maxFilesTotalSize: PropTypes.number,
    maxFileSize: PropTypes.number,
    isWithComment: PropTypes.bool,
    commentMaxLength: PropTypes.number,
    addLabel: PropTypes.node,
    addError: PropTypes.node,
    minHeight: PropTypes.number,
    maxFilesAddedLabel: PropTypes.string,
    fileDateLabel: PropTypes.string,
    onChange: PropTypes.func,
    onDownloadClick: PropTypes.func,
    /** @ignore */
    commentInputDimension: PropTypes.oneOf(Object.values(COMMENT_INPUT_DIMENSIONS)),
  };

  static defaultProps = {
    locale: REFS.LOCALES.RU,
    items: [],
    isEditable: true,
    isLoading: false,
    maxFilesQty: Infinity,
    maxFilesTotalSize: Infinity,
    maxFileSize: Infinity,
    isWithComment: false,
    commentMaxLength: null,
    addLabel: null,
    addError: null,
    minHeight: 64,
    maxFilesAddedLabel: null,
    fileDateLabel: null,
    onChange: () => null,
    onDownloadClick: () => null,
    commentInputDimension: COMMENT_INPUT_DIMENSIONS.M,
  };

  constructor(props) {
    super(props);

    this.state = {
      items: checkConstraints(
        this.props.items || [],
        this.props.maxFilesQty,
        this.props.maxFilesTotalSize,
        this.props.maxFileSize,
      ),
    };

    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    if (JSON.stringify(props.items) !== JSON.stringify(state.items)) {
      return {
        items: checkConstraints(props.items || [], props.maxFilesQty, props.maxFilesTotalSize, props.maxFileSize),
      };
    }
    return null;
  }

  getMaxFileSize = () => {
    const calcMaxFileSize = this.props.maxFilesTotalSize - calcFilesTotalSize(this.state.items);
    return calcMaxFileSize < this.props.maxFileSize ? calcMaxFileSize : this.props.maxFileSize;
  };

  handleAdd = files => {
    const items = this.state.items.concat(
      files.map(file => ({
        id: v4(),
        type: file.type,
        name: file.name,
        size: file.size,
        date: moment(file.lastModified).format(),
        file,
      })),
    );
    const verifiedFiles = checkConstraints(
      items,
      this.props.maxFilesQty,
      this.props.maxFilesTotalSize,
      this.props.maxFileSize,
    );
    this.setState({ items: verifiedFiles });
    this.props.onChange(verifiedFiles);
  };

  handleItemRemoveClick = id => {
    const items = this.state.items.filter(item => item.id !== id);
    this.setState({ items });
    this.props.onChange(items);
  };

  handleItemCommentChange = ({ id, value }) => {
    const items = this.state.items.map(item => (item.id === id ? { ...item, comment: value } : item));
    this.setState({ items });
    this.props.onChange(items);
  };

  render() {
    return (
      <StyledFilesManager {...addDataAttributes({ component: COMPONENTS.GENERAL })} sMinHeight={this.props.minHeight}>
        {this.props.isLoading && (
          <StyledFilesManagerLoading sMinHeight={this.props.minHeight}>
            <Loader isCentered />
          </StyledFilesManagerLoading>
        )}
        {!this.props.isLoading &&
          this.props.isEditable &&
          this.props.maxFilesQty > this.state.items.length && (
            <FilesManagerAdd
              locale={this.props.locale}
              maxSize={this.getMaxFileSize()}
              isMultiple={this.props.maxFilesQty !== 1}
              label={this.props.addLabel}
              error={this.props.addError}
              onAdd={this.handleAdd}
              minHeight={this.props.minHeight}
            />
          )}
        {this.props.isEditable &&
          this.props.maxFilesQty <= this.state.items.length && (
            <StyledFilesManagerLabel>
              {this.props.maxFilesAddedLabel}
              {!this.props.maxFilesAddedLabel && this.props.locale === REFS.LOCALES.RU && LABELS.RU.MAX_FILES_ADDED}
              {!this.props.maxFilesAddedLabel && this.props.locale === REFS.LOCALES.EN && LABELS.EN.MAX_FILES_ADDED}
            </StyledFilesManagerLabel>
          )}
        {!!this.state.items.length && (
          <StyledFilesManagerItems>
            {this.state.items.map(item => (
              <FilesManagerItem
                key={item.id}
                {...item}
                isEditable={this.props.isEditable}
                dateLabel={this.props.fileDateLabel}
                locale={this.props.locale}
                isWithComment={this.props.isWithComment}
                commentMaxLength={this.props.commentMaxLength}
                commentInputDimension={this.props.commentInputDimension}
                onDownloadClick={this.props.onDownloadClick}
                onRemoveClick={this.handleItemRemoveClick}
                onCommentChange={this.handleItemCommentChange}
              />
            ))}
          </StyledFilesManagerItems>
        )}
        {!this.props.isEditable &&
          !this.state.items.length && (
            <StyledFilesManagerLabel>
              {this.props.locale === REFS.LOCALES.RU && LABELS.RU.NO_FILES}
              {this.props.locale === REFS.LOCALES.EN && LABELS.EN.NO_FILES}
            </StyledFilesManagerLabel>
          )}
      </StyledFilesManager>
    );
  }
}
