```js
<FilesManager minHeight={200} />
```

```js
<FilesManager locale={FilesManager.REFS.LOCALES.EN} />
```

```jsx
const FilesManagerExample = require('./__examples__/FilesManager.example').default;

<FilesManagerExample />
```

#### FilesManager Example Source Code
```js { "file": "./__examples__/FilesManager.example.jsx" }
```
