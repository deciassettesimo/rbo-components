import React, { PureComponent } from 'react';

import FilesManager from '../FilesManager';

const items = [
  {
    id: '1',
    name: 'файл1.txt',
    size: 1201922,
    date: '2013-02-08T09:30:26.123+03:00',
    isDownloadable: true,
  },
  {
    id: '2',
    name: 'файл2.txt',
    size: 1201922,
    date: '2013-02-08T09:30:26.123+03:00',
    isDownloadable: true,
  },
  {
    id: '3',
    name: 'файл3.txt',
    size: 2201922,
    date: '2013-02-08T09:30:26.123+03:00',
    isDownloadable: false,
    comment: 'Комментарий к файлу 3',
  },
];
const AddLabel = () => <span>Кидай сюда файл!</span>;
const AddError = () => <span>Ошибка!</span>;

export default class FilesManagerExample extends PureComponent {
  static propTypes = {};

  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <FilesManager
        items={items}
        maxFilesQty={15}
        maxFilesTotalSize={5000000}
        maxFileSize={5000000}
        addLabel={<AddLabel />}
        addError={<AddError />}
        isWithComment
        {...this.props}
      />
    );
  }
}
