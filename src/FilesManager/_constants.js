import { GLOBAL } from '../_constants';

export { DIMENSIONS as COMMENT_INPUT_DIMENSIONS } from '../Input/_constants';

export const COMPONENTS = {
  GENERAL: 'FilesManager',
  ADD: 'FilesManagerAdd',
  ITEM: 'FilesManagerItem',
  ITEM_COMMENT: 'FilesManagerItemComment',
};

export const LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const LABELS = {
  ...GLOBAL.LABELS.FILES_MANAGER,
};
