import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DropZone from 'react-dropzone';

import { addDataAttributes } from '../../_utils';

import StylesLink from '../../Styles/Link';
import { COMPONENTS, LOCALES, LABELS } from '../_constants';
import { getFormattedFileSize } from '../_utils';
import { StyledFilesManagerAdd, StyledFilesManagerAddDropZone, StyledFilesManagerAddInner } from '../_style';

const FilesManagerAddLabel = ({ locale }) => (
  <div>
    <StylesLink isPseudo>
      {locale === LOCALES.RU && LABELS.RU.ADD_LABEL_LINK}
      {locale === LOCALES.EN && LABELS.EN.ADD_LABEL_LINK}
    </StylesLink>{' '}
    <span>
      {locale === LOCALES.RU && LABELS.RU.ADD_LABEL_TEXT}
      {locale === LOCALES.EN && LABELS.EN.ADD_LABEL_TEXT}
    </span>
  </div>
);

FilesManagerAddLabel.propTypes = { locale: PropTypes.oneOf(Object.values(LOCALES)).isRequired };

const FilesManagerAddError = ({ maxSize, locale }) => (
  <div>
    <div>
      {locale === LOCALES.RU && LABELS.RU.ADD_ERROR_TEXT}
      {locale === LOCALES.EN && LABELS.EN.ADD_ERROR_TEXT} ({getFormattedFileSize(maxSize, locale)})
    </div>
    <div>
      <StylesLink isPseudo>
        {locale === LOCALES.RU && LABELS.RU.ADD_ERROR_LINK}
        {locale === LOCALES.EN && LABELS.EN.ADD_ERROR_LINK}
      </StylesLink>
    </div>
  </div>
);

FilesManagerAddError.propTypes = {
  locale: PropTypes.oneOf(Object.values(LOCALES)).isRequired,
  maxSize: PropTypes.number.isRequired,
};

export default class FilesManagerAdd extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    isMultiple: PropTypes.bool,
    maxSize: PropTypes.number,
    minHeight: PropTypes.number,
    onAdd: PropTypes.func.isRequired,
    label: PropTypes.node,
    error: PropTypes.node,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    isMultiple: true,
    maxSize: 1000000,
    minHeight: 64,
    label: null,
    error: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isActive: false,
      isError: false,
    };
  }

  onDrop = acceptedFiles => {
    this.setState({ isActive: false, isError: false });
    if (acceptedFiles.length) this.props.onAdd(acceptedFiles);
  };

  onDropRejected = () => {
    this.setState({ isError: true });
  };

  onDragEnter = () => {
    this.setState({ isActive: true });
  };

  onDragLeave = () => {
    this.setState({ isActive: false });
  };

  onClick = () => {
    this.setState({ isError: false });
  };

  render() {
    return (
      <StyledFilesManagerAdd
        {...addDataAttributes({ component: COMPONENTS.ADD })}
        sMinHeight={this.props.minHeight}
        isActive={this.state.isActive}
        isError={this.state.isError}
        onMouseDown={this.onMouseDown}
      >
        <DropZone
          style={StyledFilesManagerAddDropZone}
          multiple={this.props.isMultiple}
          maxSize={this.props.maxSize}
          onDrop={this.onDrop}
          onDragEnter={this.onDragEnter}
          onDragLeave={this.onDragLeave}
          onDropRejected={this.onDropRejected}
          onFileDialogCancel={this.onFileDialogCancel}
        >
          <StyledFilesManagerAddInner onClick={this.onClick}>
            {!this.state.isError && (this.props.label || <FilesManagerAddLabel locale={this.props.locale} />)}
            {this.state.isError &&
              (this.props.error || <FilesManagerAddError locale={this.props.locale} maxSize={this.props.maxSize} />)}
          </StyledFilesManagerAddInner>
        </DropZone>
      </StyledFilesManagerAdd>
    );
  }
}
