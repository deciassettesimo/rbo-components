import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import IconDocument from '../../Icon/IconDocument';
import ClickableIcon from '../../ClickableIcon';
import StylesLink from '../../Styles/Link';
import {
  StyledFilesManagerItem,
  StyledFilesManagerItemIcon,
  StyledFilesManagerItemRemove,
  StyledFilesManagerItemTitle,
  StyledFilesManagerItemName,
  StyledFilesManagerItemDescribe,
} from '../_style';
import { getFormattedFileDate, getFormattedFileSize } from '../_utils';
import { COMPONENTS, COMMENT_INPUT_DIMENSIONS, LOCALES } from '../_constants';
import FilesManagerItemComment from './FilesManagerItemComment';

const FilesManagerItem = props => {
  const handleDownloadClick = () => {
    props.onDownloadClick(props.id);
  };

  const handleRemoveClick = () => {
    props.onRemoveClick(props.id);
  };

  const handleCommentChange = value => {
    props.onCommentChange({ id: props.id, value });
  };

  return (
    <StyledFilesManagerItem {...addDataAttributes({ component: COMPONENTS.ITEM, id: props.id })}>
      <StyledFilesManagerItemIcon>
        <IconDocument dimension={IconDocument.REFS.DIMENSIONS.XS} display={IconDocument.REFS.DISPLAY.BLOCK} />
      </StyledFilesManagerItemIcon>
      <StyledFilesManagerItemTitle>
        <StyledFilesManagerItemName>
          {props.isDownloadable && (
            <StylesLink isPseudo onClick={handleDownloadClick}>
              {props.name}
            </StylesLink>
          )}
          {!props.isDownloadable && props.name}
        </StyledFilesManagerItemName>{' '}
        <StyledFilesManagerItemDescribe>
          {getFormattedFileSize(props.size, props.locale)}{' '}
          {getFormattedFileDate(props.date, props.dateLabel, props.locale)}
        </StyledFilesManagerItemDescribe>
      </StyledFilesManagerItemTitle>
      {props.isWithComment && (
        <FilesManagerItemComment
          locale={props.locale}
          id={props.id}
          value={props.comment}
          dimension={props.commentInputDimension}
          maxLength={props.commentMaxLength}
          isEditable={props.isEditable}
          onChange={handleCommentChange}
        />
      )}
      {props.isEditable && (
        <StyledFilesManagerItemRemove>
          <ClickableIcon
            type={ClickableIcon.REFS.TYPES.REMOVE}
            dimension={ClickableIcon.REFS.DIMENSIONS.XS}
            onClick={handleRemoveClick}
            display={ClickableIcon.REFS.DISPLAY.BLOCK}
          />
        </StyledFilesManagerItemRemove>
      )}
    </StyledFilesManagerItem>
  );
};

FilesManagerItem.propTypes = {
  id: PropTypes.string.isRequired,
  locale: PropTypes.oneOf(Object.values(LOCALES)),
  name: PropTypes.string,
  size: PropTypes.number,
  date: PropTypes.string,
  comment: PropTypes.string,
  commentMaxLength: PropTypes.number,
  commentInputDimension: PropTypes.oneOf(Object.values(COMMENT_INPUT_DIMENSIONS)),
  isDownloadable: PropTypes.bool,
  isEditable: PropTypes.bool,
  isWithComment: PropTypes.bool,
  dateLabel: PropTypes.string,
  onDownloadClick: PropTypes.func.isRequired,
  onRemoveClick: PropTypes.func.isRequired,
  onCommentChange: PropTypes.func.isRequired,
};

FilesManagerItem.defaultProps = {
  locale: LOCALES.RU,
  name: null,
  size: null,
  date: null,
  comment: null,
  commentMaxLength: null,
  commentInputDimension: COMMENT_INPUT_DIMENSIONS.M,
  isDownloadable: false,
  isEditable: true,
  isWithComment: false,
  dateLabel: null,
};

export default FilesManagerItem;
