import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import ClickableIcon from '../../ClickableIcon';
import Form, { Row, Cell, Field } from '../../Form';
import InputTextArea from '../../Input/InputTextArea';
import Button from '../../Button';
import StylesLink from '../../Styles/Link';
import { StyledFilesManagerItemComment, StyledFilesManagerItemCommentValue } from '../_style';
import { COMPONENTS, LOCALES, LABELS, COMMENT_INPUT_DIMENSIONS } from '../_constants';

export default class FilesManagerItemComment extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    id: PropTypes.string.isRequired,
    value: PropTypes.string,
    isEditable: PropTypes.bool,
    dimension: PropTypes.oneOf(Object.values(COMMENT_INPUT_DIMENSIONS)),
    maxLength: PropTypes.number,
    onChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    value: null,
    isEditable: true,
    dimension: COMMENT_INPUT_DIMENSIONS.M,
    maxLength: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      defaultValue: this.props.value,
      value: this.props.value,
    };
  }

  getPlaceholder = locale => {
    switch (locale) {
      case LOCALES.EN:
        return LABELS.EN.FILE_ADD_COMMENT_PLACEHOLDER;
      case LOCALES.RU:
      default:
        return LABELS.RU.FILE_ADD_COMMENT_PLACEHOLDER;
    }
  };

  handleEditClick = () => {
    this.setState({ isOpen: true });
  };

  handleChange = ({ value }) => {
    this.setState({ value });
  };

  handleCancel = () => {
    this.setState({ isOpen: false, value: this.state.defaultValue });
  };

  handleSubmit = () => {
    this.setState({ isOpen: false, defaultValue: this.state.value });
    this.props.onChange(this.state.value);
  };

  render() {
    return (
      <StyledFilesManagerItemComment {...addDataAttributes({ component: COMPONENTS.ITEM_COMMENT })}>
        {!this.state.isOpen &&
          !this.state.value &&
          this.props.isEditable && (
            <StylesLink isPseudo isDashed onClick={this.handleEditClick}>
              {this.props.locale === LOCALES.RU && LABELS.RU.FILE_ADD_COMMENT}
              {this.props.locale === LOCALES.EN && LABELS.EN.FILE_ADD_COMMENT}
            </StylesLink>
          )}
        {!this.state.isOpen &&
          this.state.value && (
            <StyledFilesManagerItemCommentValue>
              <span>{this.state.value}</span>{' '}
              {this.props.isEditable && (
                <ClickableIcon
                  type={ClickableIcon.REFS.TYPES.EDIT}
                  dimension={ClickableIcon.REFS.DIMENSIONS.XS}
                  onClick={this.handleEditClick}
                />
              )}
            </StyledFilesManagerItemCommentValue>
          )}
        {this.state.isOpen && (
          <Form autoFocus={`Comment_${this.props.id}`}>
            <Row>
              <Cell>
                <Field>
                  <InputTextArea
                    id={`Comment_${this.props.id}`}
                    value={this.state.value}
                    dimension={this.props.dimension}
                    maxLength={this.props.maxLength}
                    onChange={this.handleChange}
                    placeholder={this.getPlaceholder(this.props.locale)}
                  />
                </Field>
              </Cell>
            </Row>
            <Row>
              <Cell width="50%">
                <Field>
                  <Button
                    id={`Cancel_${this.props.id}`}
                    type={Button.REFS.TYPES.BASE_SECONDARY}
                    onClick={this.handleCancel}
                  >
                    {this.props.locale === LOCALES.RU && LABELS.RU.FILE_ADD_COMMENT_CANCEL}
                    {this.props.locale === LOCALES.EN && LABELS.EN.FILE_ADD_COMMENT_CANCEL}
                  </Button>
                </Field>
              </Cell>
              <Cell width="50%">
                <Field align={Field.REFS.ALIGN.RIGHT}>
                  <Button
                    id={`Submit_${this.props.id}`}
                    type={Button.REFS.TYPES.BASE_PRIMARY}
                    onClick={this.handleSubmit}
                  >
                    {this.props.locale === LOCALES.RU && LABELS.RU.FILE_ADD_COMMENT_SUBMIT}
                    {this.props.locale === LOCALES.EN && LABELS.EN.FILE_ADD_COMMENT_SUBMIT}
                  </Button>
                </Field>
              </Cell>
            </Row>
          </Form>
        )}
      </StyledFilesManagerItemComment>
    );
  }
}
