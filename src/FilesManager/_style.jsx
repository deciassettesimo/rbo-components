import styled from 'styled-components';

import { STYLES } from '../_constants';

const filesManagerAddBackgroundColor = (isActive, isError) => {
  if (isActive) return STYLES.COLORS.WHITE;
  if (isError) return STYLES.COLORS.TOMATO_RED_12;
  return STYLES.COLORS.BLACK_04;
};

export const StyledFilesManager = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  min-height: ${props => props.sMinHeight}px;
`;

export const StyledFilesManagerLoading = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  min-height: ${props => props.sMinHeight}px;
  background: ${STYLES.COLORS.BLACK_04};
`;

export const StyledFilesManagerAdd = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  min-height: ${props => props.sMinHeight}px;
  background: ${props => filesManagerAddBackgroundColor(props.isActive, props.isError)};
  text-align: center;
  transition: border 0.32s ease-out, background 0.32s ease-out;
`;

export const StyledFilesManagerAddDropZone = {
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
};

export const StyledFilesManagerAddInner = styled.div`
  position: absolute;
  box-sizing: border-box;
  top: 50%;
  left: 0;
  width: 100%;
  padding: 0 16px;
  transform: translateY(-50%);
`;

export const StyledFilesManagerLabel = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_08};
  font-style: italic;

  :last-child {
    border-bottom: none;
  }
`;

export const StyledFilesManagerItems = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledFilesManagerItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px 32px;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_08};

  :last-child {
    border-bottom: none;
  }
`;

export const StyledFilesManagerItemIcon = styled.div`
  position: absolute;
  box-sizing: border-box;
  display: block;
  height: 16px;
  width: 16px;
  top: 8px;
  left: 8px;
`;

export const StyledFilesManagerItemRemove = styled.div`
  position: absolute;
  box-sizing: border-box;
  display: block;
  height: 16px;
  width: 16px;
  top: 8px;
  right: 8px;
`;

export const StyledFilesManagerItemTitle = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  word-wrap: break-word;
`;

export const StyledFilesManagerItemName = styled.span``;

export const StyledFilesManagerItemDescribe = styled.span`
  font-style: italic;
  color: ${STYLES.COLORS.WARM_GRAY};
`;

export const StyledFilesManagerItemComment = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  margin-top: 4px;
`;

export const StyledFilesManagerItemCommentValue = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  color: ${STYLES.COLORS.WARM_GRAY};
  word-wrap: break-word;
`;
