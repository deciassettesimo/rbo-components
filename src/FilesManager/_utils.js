import moment from 'moment-timezone';
import { LOCALES, LABELS } from './_constants';

export const calcFilesTotalSize = files => {
  if (!files) return 0;
  let result = 0;
  files.forEach(file => {
    if (file.size) result += file.size;
  });
  return result;
};

export const checkConstraints = (files, maxFilesQty, maxFilesTotalSize, maxFileSize) => {
  let filesTotalSize = 0;
  const result = [];
  files.forEach(file => {
    const size = file.size || 0;
    if (size <= maxFileSize && filesTotalSize + size <= maxFilesTotalSize) {
      result.push(file);
      filesTotalSize += size;
    }
  });
  if (result.length > maxFilesQty) result.length = maxFilesQty;
  return result;
};

export const getFormattedFileDate = (date, label, locale) => {
  if (!date) return null;
  const formattedDate = moment(date).format('DD.MM.YYYY');
  if (label) return `${label} ${formattedDate}`;
  if (locale === LOCALES.RU) return `${LABELS.RU.FILE_DATE} ${formattedDate}`;
  if (locale === LOCALES.EN) return `${LABELS.EN.FILE_DATE} ${formattedDate}`;
  return formattedDate;
};

export const getFormattedFileSize = (fileSize, locale) => {
  const size = fileSize || 0;
  if (locale === LOCALES.RU) {
    if (size >= 1073741824) {
      return `${(size / 1073741824).toFixed(2)} ${LABELS.RU.FILE_SIZE.GB}`;
    } else if (size >= 1048576) {
      return `${(size / 1048576).toFixed(2)} ${LABELS.RU.FILE_SIZE.MB}`;
    } else if (size >= 1024) {
      return `${(size / 1024).toFixed(2)} ${LABELS.RU.FILE_SIZE.KB}`;
    }
    return `${size} ${LABELS.RU.FILE_SIZE.BYTES}`;
  }
  if (locale === LOCALES.EN) {
    if (size >= 1073741824) {
      return `${(size / 1073741824).toFixed(2)} ${LABELS.EN.FILE_SIZE.GB}`;
    } else if (size >= 1048576) {
      return `${(size / 1048576).toFixed(2)} ${LABELS.EN.FILE_SIZE.MB}`;
    } else if (size >= 1024) {
      return `${(size / 1024).toFixed(2)} ${LABELS.EN.FILE_SIZE.KB}`;
    }
    return `${size} ${LABELS.EN.FILE_SIZE.BYTES}`;
  }
  return `${size}`;
};
