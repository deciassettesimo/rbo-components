import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import Popup, { Box, Opener } from '../Popup';
import Card from '../Card';
import IconPlusFilled from '../Icon/IconPlusFilled';
import StylesLink from '../Styles/Link';
import { COMPONENTS } from './_constants';
import REFS from './_config';
import { StyledFilter, StyledFilterInner, StyledFilterParamsAdd } from './_style';
import { getLabel } from './_utils';
import FilterParamsAddPopup from './_internal/FilterParamsAddPopup';
import FilterParamsItem from './_internal/FilterParamsItem';

export default class Filter extends PureComponent {
  static REFS = { ...REFS };

  static propTypes = {
    /** language for text */
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    conditions: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        type: PropTypes.oneOf(Object.values(REFS.CONDITIONS_TYPES)).isRequired,
        inputType: PropTypes.oneOf(Object.values(REFS.CONDITIONS_INPUT_TYPES)),
        inputWidth: PropTypes.number,
        rangeType: PropTypes.oneOf(Object.values(REFS.CONDITIONS_RANGE_TYPES)),
        maxLength: PropTypes.number,
        isNegative: PropTypes.bool,
        options: PropTypes.arrayOf(
          PropTypes.shape({
            value: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
          }),
        ),
      }),
    ).isRequired,
    params: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string), PropTypes.shape()]),
        label: PropTypes.string,
        isChanging: PropTypes.bool,
      }),
    ).isRequired,
    optionRenderer: PropTypes.func,
    onSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: REFS.LOCALES.RU,
    optionRenderer: undefined,
  };

  constructor(props) {
    super(props);

    this.state = {
      isParamsAddPopupOpen: !this.props.params.length,
    };
  }

  handleParamsAddPopupOpen = () => {
    this.setState({ isParamsAddPopupOpen: true });
  };

  handleParamsAddPopupClose = () => {
    this.setState({ isParamsAddPopupOpen: false });
    if (!this.props.params.length) this.props.onChange({ params: null, isChanged: false });
  };

  handleParamsAdd = id => {
    const params = this.props.params.map(param => param);
    params.push({ id, isChanging: true });
    this.setState({ isParamsAddPopupOpen: false });
    this.props.onChange({ params, isChanged: false });
  };

  handleParamsItemChange = ({ id, value }) => {
    if (!value) this.handleParamsItemRemove(id);
    else {
      const params = this.props.params.map(param => (param.id === id ? { ...param, value, isChanging: false } : param));

      this.props.onChange({ params, isChanged: true });
    }
  };

  handleParamsItemPopupClose = id => {
    const selectedParam = this.props.params.find(param => param.id === id);
    if (!selectedParam.value) {
      const params = this.props.params.filter(param => param.id !== id);
      this.props.onChange({ params, isChanged: false });
    }
  };

  handleParamsItemRemove = id => {
    const params = this.props.params.filter(param => param.id !== id);
    this.props.onChange({ params: params.length ? params : null, isChanged: true });
  };

  render() {
    const isConditionChanging = this.props.params.find(param => param.isChanging);

    const addConditionPopupItems = this.props.conditions.filter(
      condition => !this.props.params.find(param => condition.id === param.id),
    );

    return (
      <StyledFilter {...addDataAttributes({ component: COMPONENTS.GENERAL })}>
        <StyledFilterInner>
          {this.props.params.map(item => (
            <FilterParamsItem
              key={item.id}
              id={item.id}
              locale={this.props.locale}
              condition={this.props.conditions.find(condition => condition.id === item.id)}
              value={item.value}
              label={item.label}
              optionRenderer={this.props.optionRenderer}
              isChanging={item.isChanging}
              onChange={this.handleParamsItemChange}
              onPopupClose={this.handleParamsItemPopupClose}
              onRemoveClick={this.handleParamsItemRemove}
              onSearch={this.props.onSearch}
            />
          ))}
          {!!addConditionPopupItems.length &&
            !isConditionChanging && (
              <StyledFilterParamsAdd>
                <Popup
                  isOpened={this.state.isParamsAddPopupOpen}
                  onOpen={this.handleParamsAddPopupOpen}
                  onClose={this.handleParamsAddPopupClose}
                >
                  <Opener display={Opener.REFS.DISPLAY.INLINE_BLOCK}>
                    <StylesLink isPseudo isWithIcon>
                      <IconPlusFilled dimension={IconPlusFilled.REFS.DIMENSIONS.XXS} />
                      <span>{getLabel('ADD_CONDITION', this.props.locale)}</span>
                    </StylesLink>
                  </Opener>
                  <Box>
                    <Card isShadowed>
                      <FilterParamsAddPopup items={addConditionPopupItems} onItemClick={this.handleParamsAdd} />
                    </Card>
                  </Box>
                </Popup>
              </StyledFilterParamsAdd>
            )}
        </StyledFilterInner>
      </StyledFilter>
    );
  }
}
