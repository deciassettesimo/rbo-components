import { GLOBAL } from '../_constants';
import { TYPES } from '../Input/_constants';

export const COMPONENTS = {
  GENERAL: 'Filter',
  PARAMS_ADD_POPUP: 'FilterParamsAddPopup',
  PARAMS_ADD_POPUP_ITEM: 'FilterParamsAddPopupItem',
  PARAMS_ITEM: 'FilterParamsItem',
  PARAMS_ITEM_FORM: 'FilterParamsItemForm',
};

export const CONDITIONS_TYPES = {
  STRING: 'string',
  SELECT: 'select',
  MULTI_SELECT: 'multiSelect',
  RANGE: 'range',
  SEARCH: 'search',
  SUGGEST: 'suggest',
};

export const CONDITIONS_RANGE_TYPES = {
  DATE: 'date',
  AMOUNT: 'amount',
  INTEGER: 'integer',
};

export const CONDITIONS_INPUT_TYPES = {
  TEXT: TYPES.TEXT,
  DIGITAL: TYPES.DIGITAL,
  ACCOUNT: TYPES.ACCOUNT,
};

export const FORM_IDS = {
  STRING_INPUT: 'filterParamsItemFormStringInput',
  SELECT_INPUT: 'filterParamsItemFormSelectInput',
  RANGE_INPUT_FROM: 'filterParamsItemFormRangeInputFrom',
  RANGE_INPUT_TO: 'filterParamsItemFormRangeInputTo',
  SEARCH_INPUT: 'filterParamsItemFormSearchInput',
  BUTTON: 'filterParamsItemFormButton',
};

export const LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const LABELS = {
  ...GLOBAL.LABELS.FILTER,
};
