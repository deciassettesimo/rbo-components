import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledFilterParamsAddPopup } from '../_style';
import FilterParamsAddPopupItem from './FilterParamsAddPopupItem';

const FilterParamsAddPopup = props => (
  <StyledFilterParamsAddPopup {...addDataAttributes({ component: COMPONENTS.PARAMS_ADD_POPUP })}>
    {props.items.map(item => (
      <FilterParamsAddPopupItem key={item.id} id={item.id} title={item.title} onClick={props.onItemClick} />
    ))}
  </StyledFilterParamsAddPopup>
);

FilterParamsAddPopup.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    }),
  ).isRequired,
  onItemClick: PropTypes.func.isRequired,
};

export default FilterParamsAddPopup;
