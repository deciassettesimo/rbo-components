import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledFilterParamsAddPopupItem } from '../_style';

const FilterParamsAddPopupItem = props => {
  const handleClick = () => {
    props.onClick(props.id);
  };

  return (
    <StyledFilterParamsAddPopupItem
      {...addDataAttributes({ component: COMPONENTS.PARAMS_ADD_POPUP_ITEM })}
      onClick={handleClick}
    >
      {props.title}
    </StyledFilterParamsAddPopupItem>
  );
};

FilterParamsAddPopupItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default FilterParamsAddPopupItem;
