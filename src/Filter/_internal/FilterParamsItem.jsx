import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import ClickableIcon from '../../ClickableIcon';
import Card from '../../Card';
import Popup, { Box, Opener } from '../../Popup';
import { COMPONENTS, LOCALES, CONDITIONS_TYPES, CONDITIONS_INPUT_TYPES, CONDITIONS_RANGE_TYPES } from '../_constants';
import {
  StyledFilterParamsItem,
  StyledFilterParamsItemInner,
  StyledFilterParamsItemLabel,
  StyledFilterParamsItemRemove,
} from '../_style';
import { getLabel } from '../_utils';
import FilterParamsItemForm from './FilterParamsItemForm';

export default class FilterParamsItem extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    id: PropTypes.string.isRequired,
    condition: PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      type: PropTypes.oneOf(Object.values(CONDITIONS_TYPES)).isRequired,
      inputType: PropTypes.oneOf(Object.values(CONDITIONS_INPUT_TYPES)),
      rangeType: PropTypes.oneOf(Object.values(CONDITIONS_RANGE_TYPES)),
      maxLength: PropTypes.number,
      isNegative: PropTypes.bool,
      inputWidth: PropTypes.number,
      options: PropTypes.arrayOf(
        PropTypes.shape({
          value: PropTypes.string.isRequired,
          title: PropTypes.string.isRequired,
        }),
      ),
    }).isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string), PropTypes.shape()]),
    label: PropTypes.string,
    optionRenderer: PropTypes.func,
    isChanging: PropTypes.bool,
    onSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onPopupClose: PropTypes.func.isRequired,
    onRemoveClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    value: null,
    label: null,
    optionRenderer: ({ option }) => option.title,
    isChanging: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      isPopupOpen: this.props.isChanging,
    };
  }

  optionRenderer = ({ option }) => this.props.optionRenderer({ id: this.props.id, option });

  handleRemove = e => {
    e.stopPropagation();
    this.props.onRemoveClick(this.props.id);
  };

  handleClose = () => {
    this.handlePopupClose();
  };

  handlePopupOpen = () => {
    this.setState({ isPopupOpen: true });
  };

  handlePopupClose = () => {
    this.setState({ isPopupOpen: false });
    this.props.onPopupClose(this.props.id);
  };

  handleSearch = ({ value }) => {
    this.props.onSearch({ id: this.props.id, value });
  };

  handleChange = ({ value }) => {
    this.setState({ isPopupOpen: false });
    this.props.onChange({ id: this.props.id, value });
  };

  render() {
    return (
      <StyledFilterParamsItem {...addDataAttributes({ component: COMPONENTS.PARAMS_ITEM })}>
        <Popup isOpened={this.state.isPopupOpen} onOpen={this.handlePopupOpen} onClose={this.handlePopupClose}>
          <Opener display={Opener.REFS.DISPLAY.BLOCK}>
            <StyledFilterParamsItemInner>
              <StyledFilterParamsItemLabel>
                {this.props.condition.title}: {this.props.label}
              </StyledFilterParamsItemLabel>
              {!this.props.isChanging && (
                <StyledFilterParamsItemRemove>
                  <ClickableIcon
                    type={ClickableIcon.REFS.TYPES.CLOSE}
                    dimension={ClickableIcon.REFS.DIMENSIONS.XXS}
                    title={getLabel('PARAMS_ITEM_REMOVE', this.props.locale)}
                    onClick={this.handleRemove}
                    color={ClickableIcon.REFS.COLORS.WHITE}
                    display={ClickableIcon.REFS.DISPLAY.BLOCK}
                  />
                </StyledFilterParamsItemRemove>
              )}
            </StyledFilterParamsItemInner>
          </Opener>
          <Box>
            <Card isShadowed isBordered={false}>
              <FilterParamsItemForm
                locale={this.props.locale}
                value={this.props.value}
                condition={this.props.condition}
                optionRenderer={this.optionRenderer}
                onSearch={this.handleSearch}
                onSubmit={this.handleChange}
                onClose={this.handleClose}
              />
            </Card>
          </Box>
        </Popup>
      </StyledFilterParamsItem>
    );
  }
}
