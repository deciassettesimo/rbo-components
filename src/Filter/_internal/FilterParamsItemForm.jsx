import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS, LOCALES, CONDITIONS_TYPES, CONDITIONS_INPUT_TYPES, CONDITIONS_RANGE_TYPES } from '../_constants';
import { StyledFilterParamsItemForm } from '../_style';
import FilterParamsItemFormString from './FilterParamsItemFormString';
import FilterParamsItemFormSelect from './FilterParamsItemFormSelect';
import FilterParamsItemFormMultiSelect from './FilterParamsItemFormMultiSelect';
import FilterParamsItemFormRange from './FilterParamsItemFormRange';
import FilterParamsItemFormSearch from './FilterParamsItemFormSearch';

export default class FilterParamsItemForm extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    condition: PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      type: PropTypes.oneOf(Object.values(CONDITIONS_TYPES)).isRequired,
      inputType: PropTypes.oneOf(Object.values(CONDITIONS_INPUT_TYPES)),
      rangeType: PropTypes.oneOf(Object.values(CONDITIONS_RANGE_TYPES)),
      maxLength: PropTypes.number,
      isNegative: PropTypes.bool,
      inputWidth: PropTypes.number,
      options: PropTypes.arrayOf(
        PropTypes.shape({
          value: PropTypes.string.isRequired,
          title: PropTypes.string.isRequired,
        }),
      ),
    }).isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string), PropTypes.shape()]),
    optionRenderer: PropTypes.func.isRequired,
    onSearch: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    value: null,
  };

  handleSubmit = ({ value }) => {
    let isChanged = false;
    switch (this.props.condition.type) {
      case CONDITIONS_TYPES.RANGE:
        if (!this.props.value && !value) isChanged = false;
        else if (!this.props.value && value) isChanged = true;
        else if (this.props.value && !value) isChanged = true;
        else if (this.props.value.from !== value.from) isChanged = true;
        else if (this.props.value.to !== value.to) isChanged = true;
        break;
      case CONDITIONS_TYPES.MULTI_SELECT:
        if (!this.props.value && !value) isChanged = false;
        else if (!this.props.value && value) isChanged = true;
        else if (this.props.value && !value) isChanged = true;
        else if (this.props.value.find(item => !value.includes(item))) isChanged = true;
        else if (value.find(item => !this.props.value.includes(item))) isChanged = true;
        break;
      case CONDITIONS_TYPES.STRING:
      case CONDITIONS_TYPES.SELECT:
      case CONDITIONS_TYPES.SEARCH:
      case CONDITIONS_TYPES.SUGGEST:
      default:
        if (value !== this.props.value) isChanged = true;
        break;
    }
    if (isChanged) this.props.onSubmit({ value });
    else this.props.onClose();
  };

  render() {
    return (
      <StyledFilterParamsItemForm {...addDataAttributes({ component: COMPONENTS.PARAMS_ITEM_FORM })}>
        {this.props.condition.type === CONDITIONS_TYPES.STRING && (
          <FilterParamsItemFormString
            locale={this.props.locale}
            inputType={this.props.condition.inputType}
            value={this.props.value}
            placeholder={this.props.condition.title}
            maxLength={this.props.condition.maxLength}
            inputWidth={this.props.condition.inputWidth}
            onSubmit={this.handleSubmit}
          />
        )}
        {this.props.condition.type === CONDITIONS_TYPES.SELECT && (
          <FilterParamsItemFormSelect
            locale={this.props.locale}
            value={this.props.value}
            options={this.props.condition.options}
            onSubmit={this.handleSubmit}
          />
        )}
        {this.props.condition.type === CONDITIONS_TYPES.MULTI_SELECT && (
          <FilterParamsItemFormMultiSelect
            locale={this.props.locale}
            value={this.props.value}
            options={this.props.condition.options}
            onSubmit={this.handleSubmit}
          />
        )}
        {this.props.condition.type === CONDITIONS_TYPES.RANGE && (
          <FilterParamsItemFormRange
            locale={this.props.locale}
            rangeType={this.props.condition.rangeType}
            value={this.props.value}
            maxLength={this.props.condition.maxLength}
            isNegative={this.props.condition.isNegative}
            inputWidth={this.props.condition.inputWidth}
            onSubmit={this.handleSubmit}
          />
        )}
        {[CONDITIONS_TYPES.SEARCH, CONDITIONS_TYPES.SUGGEST].includes(this.props.condition.type) && (
          <FilterParamsItemFormSearch
            type={this.props.condition.type}
            locale={this.props.locale}
            inputType={this.props.condition.inputType}
            value={this.props.value}
            placeholder={this.props.condition.title}
            maxLength={this.props.condition.maxLength}
            inputWidth={this.props.condition.inputWidth}
            options={this.props.condition.options}
            optionRenderer={this.props.optionRenderer}
            onSearch={this.props.onSearch}
            onSubmit={this.handleSubmit}
          />
        )}
      </StyledFilterParamsItemForm>
    );
  }
}
