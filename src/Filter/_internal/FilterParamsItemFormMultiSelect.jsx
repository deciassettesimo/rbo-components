import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Form, { Row, Cell, Column, Field } from '../../Form';
import InputCheckbox from '../../Input/InputCheckbox';
import Button from '../../Button';
import { getLabel } from '../_utils';
import { FORM_IDS, LOCALES } from '../_constants';

export default class FilterParamsItemFormMultiSelect extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    value: PropTypes.arrayOf(PropTypes.string),
    options: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
      }),
    ).isRequired,
    onSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    value: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value || [],
    };
  }

  handleChange = ({ id, value }) => {
    const result = value ? this.state.value.map(item => item) : this.state.value.filter(item => item !== id);
    if (value) result.push(id);
    this.setState({ value: result });
  };

  handleSubmit = () => {
    this.props.onSubmit({ value: this.state.value.length ? this.state.value : null });
  };

  render() {
    return (
      <Form dimension={Form.REFS.DIMENSIONS.XS} autoFocus={this.props.options[0].value} onSubmit={this.handleSubmit}>
        <Row>
          <Column>
            {this.props.options.map(option => (
              <Field key={option.value}>
                <InputCheckbox
                  id={option.value}
                  checked={this.state.value.includes(option.value)}
                  onChange={this.handleChange}
                >
                  {option.title}
                </InputCheckbox>
              </Field>
            ))}
          </Column>
        </Row>
        <Row>
          <Cell>
            <Field>
              <Button id={FORM_IDS.BUTTON} type={Button.REFS.TYPES.BASE_PRIMARY} onClick={this.handleSubmit}>
                {getLabel('FORM_BUTTON', this.props.locale)}
              </Button>
            </Field>
          </Cell>
        </Row>
      </Form>
    );
  }
}
