import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Form, { Row, Cell, Fieldset, Field } from '../../Form';
import { InputDateCalendar, InputNumberDecimal, InputNumberInteger } from '../../Input';
import Button from '../../Button';
import { getLabel, getStartOfDay, getEndOfDay } from '../_utils';
import { FORM_IDS, LOCALES, CONDITIONS_RANGE_TYPES } from '../_constants';

const Input = ({ type, width, maxLength, isNegative, ...props }) => {
  switch (type) {
    case CONDITIONS_RANGE_TYPES.DATE:
      return (
        <Field width={width || 140}>
          <InputDateCalendar {...props} />
        </Field>
      );
    case CONDITIONS_RANGE_TYPES.AMOUNT:
      return (
        <Field width={width || 180}>
          <InputNumberDecimal isFormatted maxLength={maxLength || 14} isNegative={isNegative} {...props} />
        </Field>
      );
    case CONDITIONS_RANGE_TYPES.INTEGER:
    default:
      return (
        <Field width={width || 160}>
          <InputNumberInteger maxLength={maxLength || 14} {...props} />
        </Field>
      );
  }
};

export default class FilterParamsItemFormRange extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    rangeType: PropTypes.oneOf(Object.values(CONDITIONS_RANGE_TYPES)).isRequired,
    value: PropTypes.shape({
      from: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      to: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    }),
    maxLength: PropTypes.number,
    isNegative: PropTypes.bool,
    inputWidth: PropTypes.number,
    onSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    value: null,
    maxLength: null,
    isNegative: false,
    inputWidth: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value || {},
    };
  }

  handleChange = ({ id, value }) => {
    if (id === FORM_IDS.RANGE_INPUT_FROM) {
      const formattedValue = this.props.rangeType === CONDITIONS_RANGE_TYPES.DATE ? getStartOfDay(value) : value;
      this.setState({ value: { from: formattedValue, to: this.state.value.to } });
    }
    if (id === FORM_IDS.RANGE_INPUT_TO) {
      const formattedValue = this.props.rangeType === CONDITIONS_RANGE_TYPES.DATE ? getEndOfDay(value) : value;
      this.setState({ value: { from: this.state.value.from, to: formattedValue } });
    }
  };

  handleSubmit = () => {
    this.props.onSubmit({ value: this.state.value.from || this.state.value.to ? this.state.value : null });
  };

  render() {
    return (
      <Form dimension={Form.REFS.DIMENSIONS.XS} autoFocus={FORM_IDS.RANGE_INPUT_FROM} onSubmit={this.handleSubmit}>
        <Row>
          <Cell width="auto">
            <Fieldset>
              <Field.Separator>
                {getLabel(
                  this.props.rangeType === CONDITIONS_RANGE_TYPES.AMOUNT ? 'FORM_RANGE_AMOUNT_FROM' : 'FORM_RANGE_FROM',
                  this.props.locale,
                )}
              </Field.Separator>
              <Input
                type={this.props.rangeType}
                width={this.props.inputWidth}
                id={FORM_IDS.RANGE_INPUT_FROM}
                onChange={this.handleChange}
                value={this.state.value.from}
                maxLength={this.props.maxLength}
                isNegative={this.props.isNegative}
              />
              <Field.Separator>
                {getLabel(
                  this.props.rangeType === CONDITIONS_RANGE_TYPES.AMOUNT ? 'FORM_RANGE_AMOUNT_TO' : 'FORM_RANGE_TO',
                  this.props.locale,
                )}
              </Field.Separator>
              <Input
                type={this.props.rangeType}
                width={this.props.inputWidth}
                id={FORM_IDS.RANGE_INPUT_TO}
                onChange={this.handleChange}
                value={this.state.value.to}
                maxLength={this.props.maxLength}
                isNegative={this.props.isNegative}
              />
            </Fieldset>
          </Cell>
          <Cell width="auto">
            <Field>
              <Button id={FORM_IDS.BUTTON} type={Button.REFS.TYPES.BASE_PRIMARY} onClick={this.handleSubmit}>
                {getLabel('FORM_BUTTON', this.props.locale)}
              </Button>
            </Field>
          </Cell>
        </Row>
      </Form>
    );
  }
}
