import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Form, { Row, Cell, Field } from '../../Form';
import { InputSearch, InputSuggest, InputText, InputAccount, InputDigital } from '../../Input';
import Button from '../../Button';
import { getLabel } from '../_utils';
import { FORM_IDS, LOCALES, CONDITIONS_TYPES, CONDITIONS_INPUT_TYPES } from '../_constants';

const Input = ({ type, width, ...props }) => {
  switch (type) {
    case CONDITIONS_TYPES.SEARCH:
    default:
      return (
        <Field width={width || 206}>
          <InputSearch {...props} />
        </Field>
      );
    case CONDITIONS_TYPES.SUGGEST:
      return (
        <Field width={width || 206}>
          <InputSuggest {...props} />
        </Field>
      );
  }
};

const getInputElement = type => {
  switch (type) {
    case CONDITIONS_INPUT_TYPES.ACCOUNT:
      return InputAccount;
    case CONDITIONS_INPUT_TYPES.DIGITAL:
      return InputDigital;
    case CONDITIONS_INPUT_TYPES.TEXT:
    default:
      return InputText;
  }
};

export default class FilterParamsItemFormSearch extends PureComponent {
  static propTypes = {
    type: PropTypes.oneOf([CONDITIONS_TYPES.SEARCH, CONDITIONS_TYPES.SUGGEST]).isRequired,
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    inputType: PropTypes.oneOf(Object.values(CONDITIONS_INPUT_TYPES)),
    value: PropTypes.string,
    placeholder: PropTypes.string,
    maxLength: PropTypes.number,
    inputWidth: PropTypes.number,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
      }),
    ).isRequired,
    optionRenderer: PropTypes.func.isRequired,
    onSearch: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    inputType: CONDITIONS_INPUT_TYPES.TEXT,
    value: null,
    placeholder: null,
    maxLength: null,
    inputWidth: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
    };
  }

  handleChange = ({ value }) => {
    this.setState({ value });
  };

  handleSubmit = () => {
    this.props.onSubmit({ value: this.state.value });
  };

  render() {
    return (
      <Form dimension={Form.REFS.DIMENSIONS.XS} autoFocus={FORM_IDS.SEARCH_INPUT} onSubmit={this.handleSubmit}>
        <Row>
          <Cell width="auto">
            <Input
              type={this.props.type}
              width={this.props.inputWidth}
              locale={this.props.locale}
              inputElement={getInputElement(this.props.inputType)}
              id={FORM_IDS.SEARCH_INPUT}
              placeholder={this.props.placeholder}
              onChange={this.handleChange}
              onSearch={this.props.onSearch}
              value={this.state.value}
              maxLength={this.props.maxLength}
              options={this.props.options}
              optionRenderer={this.props.optionRenderer}
            />
          </Cell>
          <Cell width="auto">
            <Field>
              <Button id={FORM_IDS.BUTTON} type={Button.REFS.TYPES.BASE_PRIMARY} onClick={this.handleSubmit}>
                {getLabel('FORM_BUTTON', this.props.locale)}
              </Button>
            </Field>
          </Cell>
        </Row>
      </Form>
    );
  }
}
