import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Form, { Row, Cell, Column, Field } from '../../Form';
import InputRadioGroup from '../../Input/InputRadioGroup';
import Button from '../../Button';
import { getLabel } from '../_utils';
import { FORM_IDS, LOCALES } from '../_constants';

export default class FilterParamsItemFormSelect extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    value: PropTypes.string,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
      }),
    ).isRequired,
    onSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    value: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
    };
  }

  handleChange = ({ value }) => {
    this.setState({ value });
  };

  handleSubmit = () => {
    this.props.onSubmit({ value: this.state.value });
  };

  render() {
    return (
      <Form dimension={Form.REFS.DIMENSIONS.XS} autoFocus={FORM_IDS.SELECT_INPUT} onSubmit={this.handleSubmit}>
        <InputRadioGroup id={FORM_IDS.SELECT_INPUT} value={this.state.value} onChange={this.handleChange}>
          <Row>
            <Column>
              {this.props.options.map(option => (
                <Field key={option.value}>
                  <InputRadioGroup.Option value={option.value}>{option.title}</InputRadioGroup.Option>
                </Field>
              ))}
            </Column>
          </Row>
        </InputRadioGroup>
        <Row>
          <Cell>
            <Field>
              <Button id={FORM_IDS.BUTTON} type={Button.REFS.TYPES.BASE_PRIMARY} onClick={this.handleSubmit}>
                {getLabel('FORM_BUTTON', this.props.locale)}
              </Button>
            </Field>
          </Cell>
        </Row>
      </Form>
    );
  }
}
