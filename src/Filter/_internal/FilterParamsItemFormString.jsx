import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Form, { Row, Cell, Field } from '../../Form';
import { InputText, InputAccount, InputDigital } from '../../Input';
import Button from '../../Button';
import { getLabel } from '../_utils';
import { FORM_IDS, LOCALES, CONDITIONS_INPUT_TYPES } from '../_constants';

const Input = ({ type, width, ...props }) => {
  switch (type) {
    case CONDITIONS_INPUT_TYPES.ACCOUNT:
      return (
        <Field width={width || 206}>
          <InputAccount {...props} />
        </Field>
      );
    case CONDITIONS_INPUT_TYPES.DIGITAL:
      return (
        <Field width={width || 206}>
          <InputDigital {...props} />
        </Field>
      );
    case CONDITIONS_INPUT_TYPES.TEXT:
    default:
      return (
        <Field width={width || 206}>
          <InputText {...props} />
        </Field>
      );
  }
};

export default class FilterParamsItemFormString extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    inputType: PropTypes.oneOf(Object.values(CONDITIONS_INPUT_TYPES)),
    value: PropTypes.string,
    placeholder: PropTypes.string,
    maxLength: PropTypes.number,
    inputWidth: PropTypes.number,
    onSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    inputType: CONDITIONS_INPUT_TYPES.TEXT,
    value: null,
    placeholder: null,
    maxLength: null,
    inputWidth: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
    };
  }

  handleChange = ({ value }) => {
    this.setState({ value });
  };

  handleSubmit = () => {
    this.props.onSubmit({ value: this.state.value });
  };

  render() {
    return (
      <Form dimension={Form.REFS.DIMENSIONS.XS} autoFocus={FORM_IDS.STRING_INPUT} onSubmit={this.handleSubmit}>
        <Row>
          <Cell width="auto">
            <Input
              type={this.props.inputType}
              width={this.props.inputWidth}
              id={FORM_IDS.STRING_INPUT}
              placeholder={this.props.placeholder}
              onChange={this.handleChange}
              value={this.state.value}
              maxLength={this.props.maxLength}
            />
          </Cell>
          <Cell width="auto">
            <Field>
              <Button id={FORM_IDS.BUTTON} type={Button.REFS.TYPES.BASE_PRIMARY} onClick={this.handleSubmit}>
                {getLabel('FORM_BUTTON', this.props.locale)}
              </Button>
            </Field>
          </Cell>
        </Row>
      </Form>
    );
  }
}
