import styled from 'styled-components';

import { STYLES } from '../_constants';

export const StyledFilter = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledFilterInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  margin: -2px;
`;

export const StyledFilterParamsItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  margin: 2px;
`;

export const StyledFilterParamsItemInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  background: ${STYLES.COLORS.DARK_SKY_BLUE};
  color: ${STYLES.COLORS.WHITE};
  height: 20px;
  padding: 2px 8px;
  line-height: 16px;
  font-size: 12px;
  cursor: pointer;
`;

export const StyledFilterParamsItemLabel = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  max-width: 400px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  margin-right: 16px;
`;

export const StyledFilterParamsItemRemove = styled.div`
  position: absolute;
  box-sizing: border-box;
  display: block;
  top: 4px;
  right: 4px;
`;

export const StyledFilterParamsItemForm = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px 16px;
`;

export const StyledFilterParamsAdd = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  height: 20px;
  padding: 2px;
  line-height: 16px;
  font-size: 12px;
  margin: 2px;
`;

export const StyledFilterParamsAddPopup = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  min-width: 140px;
  padding: 8px 0;
`;

export const StyledFilterParamsAddPopupItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px;
  font-size: 14px;
  cursor: pointer;
  transition: background 0.32s ease-out;

  :hover {
    background: ${STYLES.COLORS.ICE_BLUE};
  }
`;
