import moment from 'moment';

import { LABELS, LOCALES } from './_constants';

export const getLabel = (key, locale) => {
  switch (locale) {
    case LOCALES.EN:
      return LABELS.EN[key];
    case LOCALES.RU:
    default:
      return LABELS.RU[key];
  }
};

export const getStartOfDay = date => {
  if (!date) return null;
  const momentDate = moment(date);
  return momentDate.isValid() ? momentDate.startOf('day').format() : null;
};

export const getEndOfDay = date => {
  if (!date) return null;
  const momentDate = moment(date);
  return momentDate.isValid() ? momentDate.endOf('day').format() : null;
};
