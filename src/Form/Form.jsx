import React, { PureComponent, createContext } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import FormRow from './_internal/FormRow';
import FormColumn from './_internal/FormColumn';
import FormCell from './_internal/FormCell';
import FormFieldset from './_internal/FormFieldset';
import FormField from './_internal/FormField';
import FormLabel from './_internal/FormLabel';
import FormMinimizer from './_internal/FormMinimizer';

import { COMPONENTS } from './_constants';
import { FORM_REFS as REFS } from './_config';
import { StyledForm, StyledFormDiv, StyledFormSubmitButton } from './_style';

export const FormContext = createContext({});

export default class Form extends PureComponent {
  static REFS = { ...REFS };

  static propTypes = {
    children: PropTypes.node.isRequired,
    /** Default Dimension size for Inputs in Form */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Component Id what need focus on mount */
    autoFocus: PropTypes.string,
    /** Submit handler
     * @param {SyntheticEvent} event The react `SyntheticEvent`
     * */
    onSubmit: PropTypes.func,
  };

  static defaultProps = {
    autoFocus: null,
    dimension: null,
    onSubmit: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      dimension: this.props.dimension,
    };

    this.node = React.createRef();
    this.inputsNodes = {};
  }

  componentDidMount() {
    if (this.props.autoFocus) this.setInitialFocus();
  }

  setInputNode = (id, node) => {
    if (!this.inputsNodes[id]) this.inputsNodes[id] = node;
  };

  setInitialFocus = () => {
    if (this.inputsNodes[this.props.autoFocus]) this.inputsNodes[this.props.autoFocus].current.focus();
  };

  removeInputNode = id => {
    delete this.inputsNodes[id];
  };

  handleSubmit = e => {
    e.preventDefault();
    if (this.props.onSubmit) this.props.onSubmit(e);
  };

  render() {
    if (this.props.onSubmit) {
      return (
        <StyledForm
          {...addDataAttributes({ component: COMPONENTS.GENERAL })}
          innerRef={ref => {
            this.node.current = ref;
          }}
          sDimension={this.props.dimension}
          onSubmit={this.handleSubmit}
        >
          <FormContext.Provider
            value={{ ...this.state, setFormInputNode: this.setInputNode, removeFormInputNode: this.removeInputNode }}
          >
            {this.props.children}
          </FormContext.Provider>
          <StyledFormSubmitButton />
        </StyledForm>
      );
    }

    return (
      <StyledFormDiv
        {...addDataAttributes({ component: COMPONENTS.GENERAL })}
        innerRef={ref => {
          this.node.current = ref;
        }}
        sDimension={this.props.dimension}
      >
        <FormContext.Provider
          value={{ ...this.state, setFormInputNode: this.setInputNode, removeFormInputNode: this.removeInputNode }}
        >
          {this.props.children}
        </FormContext.Provider>
      </StyledFormDiv>
    );
  }
}

Form.Row = FormRow;
Form.Column = FormColumn;
Form.Cell = FormCell;
Form.Fieldset = FormFieldset;
Form.Field = FormField;
Form.Label = FormLabel;
Form.Minimizer = FormMinimizer;
