### Form

```js
<div>
  <Form dimension={Form.REFS.DIMENSIONS.S} autoFocus="DocNumber">
    <Form.Row>
      <Form.Cell width="20%">
        <Form.Label htmlFor="DocNumber">Номер документа</Form.Label>
        <Form.Field>
          <InputDigital id="DocNumber" maxLength={7} />
        </Form.Field>
      </Form.Cell>
      <Form.Cell width="30%">
        <Form.Label htmlFor="DocDate">Дата документа</Form.Label>
        <Form.Field>
          <InputDateCalendar id="DocDate" width={124} />
        </Form.Field>
      </Form.Cell>
      <Form.Cell width="50%">
        <Form.Field align={Form.Field.REFS.ALIGN.RIGHT}>
          <InputCheckbox id="Correction">Корректировка</InputCheckbox>
        </Form.Field>
      </Form.Cell>
    </Form.Row>
    <Form.Row>
      <Form.Cell>
        <Form.Label htmlFor="AccountingContarctPt1">Номер УК</Form.Label>
        <Form.Fieldset withAutoTransitions>
          <Form.Field width="27%">
            <InputSuggest
              inputElement={InputDigital}
              id="AccountingContarctPt1"
              maxLength={8}
              options={[]}
              notFoundLabel="Ничего не найдено"
              onSearch={() => {}}
            />
          </Form.Field>
          <Form.Field.Separator>/</Form.Field.Separator>
          <Form.Field width="19%">
            <InputDigital id="AccountingContarctPt2" maxLength={4} />
          </Form.Field>
          <Form.Field.Separator>/</Form.Field.Separator>
          <Form.Field width="19%">
            <InputText id="AccountingContarctPt3" maxLength={4} />
          </Form.Field>
          <Form.Field.Separator>/</Form.Field.Separator>
          <Form.Field width="17.5%">
            <InputSearch
              inputElement={InputDigital}
              id="AccountingContarctPt4"
              maxLength={1}
              options={[
                { value: '1', title: '1'},
                { value: '2', title: '2'},
                { value: '3', title: '3'},
              ]}
              notFoundLabel="Ничего не найдено"
              onSearch={() => {}}
            />
          </Form.Field>
          <Form.Field.Separator>/</Form.Field.Separator>
          <Form.Field width="17.5%">
            <InputSearch
              inputElement={InputDigital}
              id="AccountingContarctPt5"
              maxLength={1}
              options={[
                { value: '1', title: '1'},
                { value: '2', title: '2'},
                { value: '3', title: '3'},
              ]}
              notFoundLabel="Ничего не найдено"
              onSearch={() => {}}
            />
          </Form.Field>
        </Form.Fieldset>
      </Form.Cell>
    </Form.Row>
    <InputRadioGroup
      id="InputRadioGroup"
      dimension={InputRadioGroup.REFS.DIMENSIONS.XS}
      value="1"
    >
        <Form.Row>
          <Form.Cell>
            <Form.Field>
              <InputRadioGroup.Option value="1">
                InputRadioGroup Option 1
              </InputRadioGroup.Option>
            </Form.Field>
          </Form.Cell>
        </Form.Row>
        <Form.Row>
          <Form.Cell>
            <Form.Field>
              <InputRadioGroup.Option value="2">
                InputRadioGroup Option 2
              </InputRadioGroup.Option>
            </Form.Field>
          </Form.Cell>
        </Form.Row>
    </InputRadioGroup>
    <Form.Row alignItems={Form.Row.REFS.ALIGN_ITEMS.CENTER}>
      <Form.Cell width="50%">
        <Form.Field>
          <InputFiles id="InputFiles" />
        </Form.Field>
      </Form.Cell>
    </Form.Row>
  </Form>
</div>
```
