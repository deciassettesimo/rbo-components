import { GLOBAL } from '../_constants';
import { DIMENSIONS as INPUT_DIMENSIONS } from '../Input/_constants';

export const COMPONENTS = {
  GENERAL: 'Form',
  CELL: 'FormCell',
  COLUMN: 'FormColumn',
  FIELD: 'FormField',
  FIELD_SEPARATOR: 'FormFieldSeparator',
  FIELDSET: 'FormFieldset',
  LABEL: 'FormLabel',
  MINIMIZER: 'FormMinimizer',
  ROW: 'FormRow',
};

export const WIDTH_MAP = {
  ...GLOBAL.WIDTH_MAP,
};

export const DIMENSIONS = {
  ...INPUT_DIMENSIONS,
};

export const V_ALIGN_ITEMS = {
  TOP: 'top',
  CENTER: 'center',
  BOTTOM: 'bottom',
};

export const ALIGN_ITEMS = {
  LEFT: 'left',
  CENTER: 'center',
  RIGHT: 'right',
};

export const FIELD_ALIGN = {
  LEFT: 'left',
  CENTER: 'center',
  RIGHT: 'right',
};
