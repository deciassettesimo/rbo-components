import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledFormCell } from '../_style';

const FormCell = props => (
  <StyledFormCell {...addDataAttributes({ component: COMPONENTS.CELL })} sWidth={props.width}>
    {props.children}
  </StyledFormCell>
);

FormCell.propTypes = {
  children: PropTypes.node,
  /** Width in px (number), % or one of WIDTH_MAP key (string) */
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

FormCell.defaultProps = {
  children: null,
  width: '100%',
};

export default FormCell;
