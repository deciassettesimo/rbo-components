import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { ROW_REFS as REFS } from '../_config';
import { StyledFormColumn } from '../_style';

const FormColumn = props => (
  <StyledFormColumn {...addDataAttributes({ component: COMPONENTS.COLUMN })} sWidth={props.width}>
    {props.children}
  </StyledFormColumn>
);

FormColumn.propTypes = {
  children: PropTypes.node,
  /** Width in px (number), % or one of WIDTH_MAP key (string) */
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

FormColumn.defaultProps = {
  children: null,
  width: '100%',
};

FormColumn.REFS = { ...REFS };

export default FormColumn;
