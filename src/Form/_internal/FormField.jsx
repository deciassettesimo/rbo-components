import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { FormContext } from '../Form';
import { StyledFormField } from '../_style';
import { COMPONENTS, DIMENSIONS } from '../_constants';
import { FIELD_REFS as REFS } from '../_config';
import { prepareFieldChildrenWithContext } from '../_utils';
import { FormFieldsetContext } from './FormFieldset';
import FormFieldSeparator from './FormFieldSeparator';

const FormFieldWithContext = props => {
  const setInputNode = () => {
    if (!props.setFormInputNode && !props.setFieldsetInputNode) return null;
    return (id, node, remove) => {
      if (remove) {
        if (props.removeFormInputNode) props.removeFormInputNode(id);
        if (props.removeFieldsetInputNode) props.removeFieldsetInputNode(node);
      } else {
        if (props.setFormInputNode) props.setFormInputNode(id, node);
        if (props.setFieldsetInputNode) props.setFieldsetInputNode(node);
      }
    };
  };

  return (
    <StyledFormField {...addDataAttributes({ component: COMPONENTS.FIELD })} sWidth={props.width} sAlign={props.align}>
      {prepareFieldChildrenWithContext(props.children, props.dimension, setInputNode())}
    </StyledFormField>
  );
};

FormFieldWithContext.propTypes = {
  children: PropTypes.element.isRequired,
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  align: PropTypes.oneOf(Object.values(REFS.ALIGN)),
  setFormInputNode: PropTypes.func,
  setFieldsetInputNode: PropTypes.func,
  removeFormInputNode: PropTypes.func,
  removeFieldsetInputNode: PropTypes.func,
};

FormFieldWithContext.defaultProps = {
  dimension: null,
  align: REFS.ALIGN.LEFT,
  setFormInputNode: null,
  setFieldsetInputNode: null,
  removeFormInputNode: null,
  removeFieldsetInputNode: null,
};

const FormField = props => (
  <FormContext.Consumer>
    {({ dimension, setFormInputNode, removeFormInputNode }) => (
      <FormFieldsetContext.Consumer>
        {({ setFieldsetInputNode, removeFieldsetInputNode }) => (
          <FormFieldWithContext
            {...props}
            dimension={props.dimension || dimension}
            setFormInputNode={setFormInputNode}
            setFieldsetInputNode={setFieldsetInputNode}
            removeFormInputNode={removeFormInputNode}
            removeFieldsetInputNode={removeFieldsetInputNode}
          />
        )}
      </FormFieldsetContext.Consumer>
    )}
  </FormContext.Consumer>
);

FormField.propTypes = {
  /** children must be only one react element (Input Component) */
  children: PropTypes.element.isRequired,
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  /** Width in px (number), % or one of WIDTH_MAP key (string) */
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

FormField.defaultProps = {
  width: '100%',
  dimension: null,
};

FormField.REFS = { ...REFS };

FormField.Separator = FormFieldSeparator;

export default FormField;
