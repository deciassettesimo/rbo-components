import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS, DIMENSIONS } from '../_constants';
import { FormContext } from '../Form';
import { StyledFormFieldSeparator } from '../_style';

const FormFieldSeparatorWithContext = props => (
  <StyledFormFieldSeparator
    {...addDataAttributes({ component: COMPONENTS.FIELD_SEPARATOR })}
    sDimension={props.dimension}
  >
    {props.children}
  </StyledFormFieldSeparator>
);

FormFieldSeparatorWithContext.propTypes = {
  children: PropTypes.node,
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
};

FormFieldSeparatorWithContext.defaultProps = {
  children: null,
  dimension: DIMENSIONS.M,
};

const FormFieldSeparator = props => (
  <FormContext.Consumer>
    {({ dimension }) => <FormFieldSeparatorWithContext {...props} dimension={dimension} />}
  </FormContext.Consumer>
);

export default FormFieldSeparator;
