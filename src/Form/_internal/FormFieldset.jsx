import React, { Component, createContext } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { FIELDSET_REFS as REFS } from '../_config';
import { StyledFormFieldset } from '../_style';

export const FormFieldsetContext = createContext({});

class AutoTransitions extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    vAlignItems: PropTypes.oneOf(Object.values(REFS.V_ALIGN_ITEMS)).isRequired,
  };

  constructor(props) {
    super(props);

    this.node = React.createRef();
    this.inputsNodes = [];
  }

  setInputNode = node => {
    const inputDOMElement = node.current;
    inputDOMElement.addEventListener('input', this.handleInput, true);
    inputDOMElement.addEventListener('rboComponentsInputChange', this.handleChange, true);
    this.inputsNodes.push(node);
  };

  removeInputNode = node => {
    const inputDOMElement = node.current;
    inputDOMElement.removeEventListener('input', this.handleInput, true);
    inputDOMElement.removeEventListener('rboComponentsInputChange', this.handleChange, true);
    this.inputsNodes = this.inputsNodes.filter(inputNode => inputNode !== node);
  };

  handleInput = e => {
    this.inputsNodes.every((inputNode, index) => {
      if (inputNode.current === e.target) {
        const targetValueLength = e.target.value.length;
        const nextInputIndex = index + 1;
        const maxLength = parseInt(e.target.getAttribute('maxlength'), 10);
        if (nextInputIndex < this.inputsNodes.length && targetValueLength === maxLength) {
          this.goToNextFiled(nextInputIndex);
        }
        return false;
      }
      return true;
    });
  };

  handleChange = e => {
    this.inputsNodes.every((inputNode, index) => {
      if (inputNode.current === e.target) {
        const nextInputIndex = index + 1;
        if (nextInputIndex < this.inputsNodes.length) {
          this.goToNextFiled(nextInputIndex);
        }
        return false;
      }
      return true;
    });
  };

  goToNextFiled = index => {
    setTimeout(() => {
      this.inputsNodes[index].current.focus();
    }, 0);
  };

  render() {
    return (
      <StyledFormFieldset
        {...addDataAttributes({ component: COMPONENTS.FIELDSET })}
        sVAlignItems={this.props.vAlignItems}
        innerRef={ref => {
          this.node.current = ref;
        }}
      >
        <FormFieldsetContext.Provider
          value={{ setFieldsetInputNode: this.setInputNode, removeFieldsetInputNode: this.removeInputNode }}
        >
          {this.props.children}
        </FormFieldsetContext.Provider>
      </StyledFormFieldset>
    );
  }
}

const FormFieldset = props => {
  if (props.withAutoTransitions) {
    return <AutoTransitions vAlignItems={props.vAlignItems}>{props.children}</AutoTransitions>;
  }
  return (
    <StyledFormFieldset {...addDataAttributes({ component: COMPONENTS.FIELDSET })} sVAlignItems={props.vAlignItems}>
      {props.children}
    </StyledFormFieldset>
  );
};

FormFieldset.propTypes = {
  children: PropTypes.node,
  withAutoTransitions: PropTypes.bool,
  vAlignItems: PropTypes.oneOf(Object.values(REFS.V_ALIGN_ITEMS)),
};

FormFieldset.defaultProps = {
  children: null,
  withAutoTransitions: false,
  vAlignItems: REFS.V_ALIGN_ITEMS.TOP,
};

FormFieldset.REFS = { ...REFS };

export default FormFieldset;
