import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledFormLabel } from '../_style';

const FormLabel = props => (
  <StyledFormLabel
    {...addDataAttributes({ component: COMPONENTS.LABEL })}
    isOneLine={props.isOneLine}
    htmlFor={props.htmlFor}
  >
    {props.children}
  </StyledFormLabel>
);

FormLabel.propTypes = {
  children: PropTypes.node,
  htmlFor: PropTypes.string,
  isOneLine: PropTypes.bool,
};

FormLabel.defaultProps = {
  children: null,
  htmlFor: null,
  isOneLine: false,
};

export default FormLabel;
