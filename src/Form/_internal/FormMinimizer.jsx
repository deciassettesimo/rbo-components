import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS, DIMENSIONS } from '../_constants';
import { FormContext } from '../Form';
import StylesLink from '../../Styles/Link';
import IconArrowUp from '../../Icon/IconArrowUp';
import IconArrowDown from '../../Icon/IconArrowDown';
import { StyledFormMinimizer } from '../_style';

const FormMinimizerWithContext = props => {
  const getLabel = () => {
    if (props.label) return props.label;
    else if (props.labels) {
      if (!props.isMinimized) return props.labels[0];
      return props.labels[1] || props.label[0];
    }
    return null;
  };

  return (
    <StyledFormMinimizer {...addDataAttributes({ component: COMPONENTS.MINIMIZER })} sDimension={props.dimension}>
      <StylesLink isPseudo isWithIcon onClick={props.onToggle}>
        <span>{getLabel()}</span>
        {props.isMinimized && <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XXS} />}
        {!props.isMinimized && <IconArrowUp dimension={IconArrowUp.REFS.DIMENSIONS.XXS} />}
      </StylesLink>
    </StyledFormMinimizer>
  );
};

FormMinimizerWithContext.propTypes = {
  label: PropTypes.string,
  labels: PropTypes.arrayOf(PropTypes.string),
  isMinimized: PropTypes.bool,
  onToggle: PropTypes.func.isRequired,
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
};

FormMinimizerWithContext.defaultProps = {
  label: null,
  labels: null,
  isMinimized: false,
  dimension: DIMENSIONS.M,
};

const FormMinimizer = props => (
  <FormContext.Consumer>
    {({ dimension }) => <FormMinimizerWithContext {...props} dimension={dimension} />}
  </FormContext.Consumer>
);

export default FormMinimizer;
