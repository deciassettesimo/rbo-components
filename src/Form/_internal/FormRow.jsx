import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { ROW_REFS as REFS } from '../_config';
import { StyledFormRow } from '../_style';

const FormRow = props => (
  <StyledFormRow
    {...addDataAttributes({ component: COMPONENTS.ROW })}
    sAlignItems={props.alignItems}
    sVAlignItems={props.vAlignItems}
  >
    {props.children}
  </StyledFormRow>
);

FormRow.propTypes = {
  children: PropTypes.node,
  alignItems: PropTypes.oneOf(Object.values(REFS.ALIGN_ITEMS)),
  vAlignItems: PropTypes.oneOf(Object.values(REFS.V_ALIGN_ITEMS)),
};

FormRow.defaultProps = {
  children: null,
  alignItems: REFS.ALIGN_ITEMS.LEFT,
  vAlignItems: REFS.V_ALIGN_ITEMS.BOTTOM,
};

FormRow.REFS = { ...REFS };

export default FormRow;
