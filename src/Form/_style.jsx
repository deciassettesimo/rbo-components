import styled from 'styled-components';

import { STYLES } from '../_constants';
import { WIDTH_MAP, ALIGN_ITEMS, V_ALIGN_ITEMS, DIMENSIONS } from './_constants';

const formFontSize = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 12;
  return 14;
};

const formLineHeight = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 16;
  if (sDimension === DIMENSIONS.XL) return 24;
  return 20;
};

const formElementPaddingV = sDimension => (sDimension - formLineHeight(sDimension)) / 2;

const formElementWidth = sWidth => {
  if (typeof sWidth === 'number') return `${sWidth}px`;
  return WIDTH_MAP[sWidth] || sWidth || '100%';
};

const formRowAlignItems = sAlignItems => {
  switch (sAlignItems) {
    case ALIGN_ITEMS.RIGHT:
      return 'flex-end';
    case ALIGN_ITEMS.CENTER:
      return 'center';
    case ALIGN_ITEMS.LEFT:
    default:
      return 'flex-start';
  }
};

const verticalAlignItems = sVAlignItems => {
  switch (sVAlignItems) {
    case V_ALIGN_ITEMS.TOP:
      return 'flex-start';
    case V_ALIGN_ITEMS.CENTER:
      return 'center';
    case V_ALIGN_ITEMS.BOTTOM:
    default:
      return 'flex-end';
  }
};

export const StyledForm = styled.form`
  position: relative;
  box-sizing: border-box;
  display: block;
  font-size: ${props => formFontSize(props.sDimension)}px;
  line-height: ${props => formLineHeight(props.sDimension)}px;
`;

export const StyledFormDiv = StyledForm.withComponent('div');

export const StyledFormCell = styled.div.attrs({
  style: ({ sWidth }) => ({
    width: formElementWidth(sWidth),
  }),
})`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledFormRow = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: ${props => formRowAlignItems(props.sAlignItems)};
  align-items: ${props => verticalAlignItems(props.sVAlignItems)};
  margin: 0 -12px;
  padding: 6px 0;

  ${StyledFormCell} {
    padding: 0 12px;
  }
`;

export const StyledFormColumn = styled.div.attrs({
  style: ({ sWidth }) => ({
    width: formElementWidth(sWidth),
  }),
})`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: column nowrap;
  margin: -6px 0;
  padding: 0 12px;

  ${StyledFormCell} {
    padding: 6px 0;
  }
`;

export const StyledFormField = styled.div.attrs({
  style: ({ sWidth }) => ({
    width: formElementWidth(sWidth),
  }),
})`
  position: relative;
  box-sizing: border-box;
  display: block;
  text-align: ${props => props.sAlign};
`;

export const StyledFormFieldset = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  align-items: ${props => verticalAlignItems(props.sVAlignItems)};
  margin: 0 -6px;

  ${StyledFormField} {
    padding: 0 6px;
  }
`;

export const StyledFormLabel = styled.label`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: bottom;
  min-height: 16px;
  max-width: 100%;
  padding-bottom: 2px;
  font-size: 12px;
  color: ${STYLES.COLORS.WARM_GRAY};
  white-space: ${props => (props.isOneLine ? 'nowrap' : 'normal')};
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const StyledFormFieldSeparator = styled.div`
  padding: ${props => formElementPaddingV(props.sDimension)}px 0;
`;

export const StyledFormMinimizer = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: ${props => formElementPaddingV(props.sDimension)}px 0;
`;

export const StyledFormSubmitButton = styled.button.attrs({
  type: 'submit',
  tabIndex: -1,
})`
  position: absolute;
  top: 0;
  left: 0;
  height: 0;
  width: 0;
  border: none;
  background: transparent;
  outline: none;
  opacity: 0;
  z-index: -1;
`;
