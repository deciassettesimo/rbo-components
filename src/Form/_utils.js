import { FIELD_INPUT_TYPES } from './_config';

export const prepareFieldChildrenWithContext = (children, dimension, setInputNode) => {
  let result = children;
  if (children.type && FIELD_INPUT_TYPES.includes(children.type.inputType)) {
    result = { ...result, props: { ...result.props, setInputNode } };
    if (dimension) {
      result = { ...result, props: { ...result.props, dimension } };
    }
  }
  return result;
};

export default prepareFieldChildrenWithContext;
