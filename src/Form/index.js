import Form from './Form';

export const { Row, Column, Cell, Fieldset, Field, Label, Minimizer } = Form;
export default Form;
