import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import IconWarningFilled from '../Icon/IconWarningFilled';
import { COMPONENT } from './_constants';
import REFS from './_config';
import {
  StyledGeneralError,
  StyledGeneralErrorInner,
  StyledGeneralErrorIcon,
  StyledGeneralErrorContent,
} from './_style';

const GeneralError = props => (
  <StyledGeneralError {...addDataAttributes({ component: COMPONENT })}>
    <StyledGeneralErrorInner>
      <StyledGeneralErrorIcon>{props.icon}</StyledGeneralErrorIcon>
      <StyledGeneralErrorContent>{props.children}</StyledGeneralErrorContent>
    </StyledGeneralErrorInner>
  </StyledGeneralError>
);

GeneralError.propTypes = {
  icon: PropTypes.element,
  children: PropTypes.node.isRequired,
};

GeneralError.defaultProps = {
  icon: (
    <IconWarningFilled
      dimension={IconWarningFilled.REFS.DIMENSIONS.L}
      color={IconWarningFilled.REFS.COLORS.TOMATO_RED}
    />
  ),
};

GeneralError.REFS = {
  ...REFS,
};

export default GeneralError;
