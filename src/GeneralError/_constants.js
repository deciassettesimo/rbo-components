export const COMPONENT = 'GeneralError';

export const DIMENSIONS = {
  XS: 16,
  S: 20,
  M: 24,
  L: 32,
  XL: 48,
};
