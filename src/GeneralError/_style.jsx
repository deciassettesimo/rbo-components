import styled from 'styled-components';

export const StyledGeneralError = styled.div`
  position: absolute;
  display: block;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;

export const StyledGeneralErrorInner = styled.div`
  position: absolute;
  box-sizing: border-box;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  display: block;
  text-align: center;
`;

export const StyledGeneralErrorIcon = styled.div`
  position: relative;
  box-sizing: border-box;
`;

export const StyledGeneralErrorContent = styled.div`
  position: relative;
  box-sizing: border-box;
  margin-top: 8px;
`;
