import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconAccountChecked = props => (
  <IconPrimitive {...props} type={TYPES.ACCOUNT_CHECKED} viewBox={REFS.DIMENSIONS.M}>
    <path d="M18,18.707 L18,11.707 L20,11.707 L20,20.707 L4,20.707 L4,4.707 L13,4.707 L13,6.707 L6,6.707 L6,18.707 L18,18.707 Z M21.293,4 L22.707,5.414 L17,11.121 L13.793,7.914 L15.207,6.5 L17,8.293 L21.293,4 Z M8,13.707 L16,13.707 L16,15.707 L8,15.707 L8,13.707 Z M8,9.707 L13,9.707 L13,11.707 L8,11.707 L8,9.707 Z" />
  </IconPrimitive>
);

IconAccountChecked.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconAccountChecked.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconAccountChecked.REFS = {
  ...REFS,
};

export default IconAccountChecked;
