```js
<div className="list">
  <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.XXS} />
  <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.XS} />
  <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.S} />
  <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.M} />
  <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.L} />
  <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.XL} />
  <IconAccountChecked dimension={IconAccountChecked.REFS.DIMENSIONS.XXL} />
</div>
```