import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArchive = props => (
  <IconPrimitive {...props} type={TYPES.ARCHIVE} viewBox={REFS.DIMENSIONS.M}>
    <path d="M10,15 C10,16.1045695 10.8954305,17 12,17 C13.1045695,17 14,16.1045695 14,15 L20,15 L20,4 L4,4 L4,15 L10,15 Z M15.4648712,17 C14.7732524,18.1956027 13.4805647,19 12,19 C10.5194353,19 9.22674762,18.1956027 8.53512878,17 L4,17 L4,20 L20,20 L20,17 L15.4648712,17 Z M2,2 L22,2 L22,22 L2,22 L2,2 Z M6,9 L6,7 L18,7 L18,9 L6,9 Z M6,13 L6,11 L18,11 L18,13 L6,13 Z" />
  </IconPrimitive>
);

IconArchive.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArchive.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArchive.REFS = {
  ...REFS,
};

export default IconArchive;
