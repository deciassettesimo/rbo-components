```js
<div className="list">
  <IconArchive dimension={IconArchive.REFS.DIMENSIONS.XS} />
  <IconArchive dimension={IconArchive.REFS.DIMENSIONS.S} />
  <IconArchive dimension={IconArchive.REFS.DIMENSIONS.M} />
  <IconArchive dimension={IconArchive.REFS.DIMENSIONS.L} />
  <IconArchive dimension={IconArchive.REFS.DIMENSIONS.XL} />
</div>
```