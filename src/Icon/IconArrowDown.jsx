import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowDown = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_DOWN} viewBox={REFS.DIMENSIONS.XS}>
    <polygon points="2.53060662 4.71000004 8.00000024 10.1717509 13.4693939 4.71000004 14.5300004 5.76912457 8.00000024 12.29 1.47000003 5.76912457" />
  </IconPrimitive>
);

IconArrowDown.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowDown.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowDown.REFS = {
  ...REFS,
};

export default IconArrowDown;
