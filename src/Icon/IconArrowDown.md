```js
<div className="list">
  <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />
  <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.S} />
  <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.M} />
  <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.L} />
  <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XL} />
</div>
```