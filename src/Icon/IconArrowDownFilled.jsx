import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowDownFilled = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_DOWN_FILLED} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M3.24142136,6 L12.7585786,6 C12.8138071,6 12.8585786,6.04477153 12.8585786,6.1 C12.8585786,6.12652165 12.848043,6.15195704 12.8292893,6.17071068 L8.07071068,10.9292893 C8.03165825,10.9683418 7.96834175,10.9683418 7.92928932,10.9292893 L3.17071068,6.17071068 C3.13165825,6.13165825 3.13165825,6.06834175 3.17071068,6.02928932 C3.18946432,6.01053568 3.21489971,6 3.24142136,6 Z" />
  </IconPrimitive>
);

IconArrowDownFilled.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowDownFilled.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowDownFilled.REFS = {
  ...REFS,
};

export default IconArrowDownFilled;
