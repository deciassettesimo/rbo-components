```js
<div className="list">
  <IconArrowDownFilled dimension={IconArrowDownFilled.REFS.DIMENSIONS.XS} />
  <IconArrowDownFilled dimension={IconArrowDownFilled.REFS.DIMENSIONS.S} />
  <IconArrowDownFilled dimension={IconArrowDownFilled.REFS.DIMENSIONS.M} />
  <IconArrowDownFilled dimension={IconArrowDownFilled.REFS.DIMENSIONS.L} />
  <IconArrowDownFilled dimension={IconArrowDownFilled.REFS.DIMENSIONS.XL} />
</div>
```