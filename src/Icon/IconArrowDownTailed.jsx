import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowDownTailed = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_DOWN_TAILED} viewBox={REFS.DIMENSIONS.M}>
    <polygon points="10.7928932 18.3786797 10.7928932 1.79289322 12.7928932 1.79289322 12.7928932 18.3786797 20.0857864 11.0857864 21.5 12.5 11.7928932 22.2071068 2.08578644 12.5 3.5 11.0857864 10.7928932 18.3786797" />
  </IconPrimitive>
);

IconArrowDownTailed.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowDownTailed.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowDownTailed.REFS = {
  ...REFS,
};

export default IconArrowDownTailed;
