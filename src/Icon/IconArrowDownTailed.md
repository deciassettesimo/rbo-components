```js
<div className="list">
  <IconArrowDownTailed dimension={IconArrowDownTailed.REFS.DIMENSIONS.XS} />
  <IconArrowDownTailed dimension={IconArrowDownTailed.REFS.DIMENSIONS.S} />
  <IconArrowDownTailed dimension={IconArrowDownTailed.REFS.DIMENSIONS.M} />
  <IconArrowDownTailed dimension={IconArrowDownTailed.REFS.DIMENSIONS.L} />
  <IconArrowDownTailed dimension={IconArrowDownTailed.REFS.DIMENSIONS.XL} />
</div>
```