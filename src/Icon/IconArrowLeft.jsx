import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowLeft = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_LEFT} viewBox={REFS.DIMENSIONS.XS}>
    <polygon points="11.29 2.5306602 5.82824911 8.00033011 11.29 13.47 10.2308754 14.5306602 3.71000004 8.00033011 10.2308754 1.47000003" />
  </IconPrimitive>
);

IconArrowLeft.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowLeft.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowLeft.REFS = {
  ...REFS,
};

export default IconArrowLeft;
