```js
<div className="list">
  <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.XS} />
  <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.S} />
  <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.M} />
  <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.L} />
  <IconArrowLeft dimension={IconArrowLeft.REFS.DIMENSIONS.XL} />
</div>
```