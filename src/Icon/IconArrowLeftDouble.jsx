import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowLeftDouble = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_LEFT_DOUBLE} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M8.78999996,2.5306602 L3.32824911,8.00033011 L8.78999996,13.47 L7.73087543,14.5306602 L1.21000004,8.00033011 L7.73087543,1.47000003 L8.78999996,2.5306602 Z M13.79,2.5306602 L8.32824911,8.00033011 L13.79,13.47 L12.7308754,14.5306602 L6.21000004,8.00033011 L12.7308754,1.47000003 L13.79,2.5306602 Z" />
  </IconPrimitive>
);

IconArrowLeftDouble.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowLeftDouble.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowLeftDouble.REFS = {
  ...REFS,
};

export default IconArrowLeftDouble;
