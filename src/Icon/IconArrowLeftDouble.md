```js
<div className="list">
  <IconArrowLeftDouble dimension={IconArrowLeftDouble.REFS.DIMENSIONS.XS} />
  <IconArrowLeftDouble dimension={IconArrowLeftDouble.REFS.DIMENSIONS.S} />
  <IconArrowLeftDouble dimension={IconArrowLeftDouble.REFS.DIMENSIONS.M} />
  <IconArrowLeftDouble dimension={IconArrowLeftDouble.REFS.DIMENSIONS.L} />
  <IconArrowLeftDouble dimension={IconArrowLeftDouble.REFS.DIMENSIONS.XL} />
</div>
```