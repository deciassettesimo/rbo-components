import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowLeftFilled = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_LEFT_FILLED} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M10,3.10291076 L10,12.8970892 C10,12.9539253 9.95485448,13 9.89916465,13 C9.87242145,13 9.84677359,12.9891576 9.82786329,12.9698581 L5.02953399,8.07276889 C4.99015534,8.03257974 4.99015534,7.96742026 5.02953399,7.92723111 L9.82786329,3.03014186 C9.86724195,2.98995271 9.93108736,2.98995271 9.97046601,3.03014186 C9.98937631,3.04944137 10,3.07561713 10,3.10291076 Z" />
  </IconPrimitive>
);

IconArrowLeftFilled.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowLeftFilled.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowLeftFilled.REFS = {
  ...REFS,
};

export default IconArrowLeftFilled;
