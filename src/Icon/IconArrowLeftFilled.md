```js
<div className="list">
  <IconArrowLeftFilled dimension={IconArrowLeftFilled.REFS.DIMENSIONS.XS} />
  <IconArrowLeftFilled dimension={IconArrowLeftFilled.REFS.DIMENSIONS.S} />
  <IconArrowLeftFilled dimension={IconArrowLeftFilled.REFS.DIMENSIONS.M} />
  <IconArrowLeftFilled dimension={IconArrowLeftFilled.REFS.DIMENSIONS.L} />
  <IconArrowLeftFilled dimension={IconArrowLeftFilled.REFS.DIMENSIONS.XL} />
</div>
```