import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowLeftTailed = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_LEFT_TAILED} viewBox={REFS.DIMENSIONS.M}>
    <path d="M5.41421356,11 L22,11 L22,13 L5.41421356,13 L12.7071068,20.2928932 L11.2928932,21.7071068 L1.58578644,12 L11.2928932,2.29289322 L12.7071068,3.70710678 L5.41421356,11 Z" />
  </IconPrimitive>
);

IconArrowLeftTailed.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowLeftTailed.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowLeftTailed.REFS = {
  ...REFS,
};

export default IconArrowLeftTailed;
