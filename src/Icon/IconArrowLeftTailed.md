```js
<div className="list">
  <IconArrowLeftTailed dimension={IconArrowLeftTailed.REFS.DIMENSIONS.XS} />
  <IconArrowLeftTailed dimension={IconArrowLeftTailed.REFS.DIMENSIONS.S} />
  <IconArrowLeftTailed dimension={IconArrowLeftTailed.REFS.DIMENSIONS.M} />
  <IconArrowLeftTailed dimension={IconArrowLeftTailed.REFS.DIMENSIONS.L} />
  <IconArrowLeftTailed dimension={IconArrowLeftTailed.REFS.DIMENSIONS.XL} />
</div>
```