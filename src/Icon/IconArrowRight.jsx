import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowRight = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_RIGHT} viewBox={REFS.DIMENSIONS.XS}>
    <polygon points="4.71000004 13.47 10.1717509 8.00033011 4.71000004 2.5306602 5.76912457 1.47000003 12.29 8.00033011 5.76912457 14.5306602" />
  </IconPrimitive>
);

IconArrowRight.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowRight.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowRight.REFS = {
  ...REFS,
};

export default IconArrowRight;
