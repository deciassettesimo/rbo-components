```js
<div className="list">
  <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.XS} />
  <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.S} />
  <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.M} />
  <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.L} />
  <IconArrowRight dimension={IconArrowRight.REFS.DIMENSIONS.XL} />
</div>
```