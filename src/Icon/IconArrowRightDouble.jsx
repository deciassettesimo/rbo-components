import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowRightDouble = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_RIGHT_DOUBLE} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M7.21000004,13.47 L12.6717509,8.00033011 L7.21000004,2.5306602 L8.26912457,1.47000003 L14.79,8.00033011 L8.26912457,14.5306602 L7.21000004,13.47 Z M2.21000004,13.47 L7.67175089,8.00033011 L2.21000004,2.5306602 L3.26912457,1.47000003 L9.78999996,8.00033011 L3.26912457,14.5306602 L2.21000004,13.47 Z" />
  </IconPrimitive>
);

IconArrowRightDouble.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowRightDouble.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowRightDouble.REFS = {
  ...REFS,
};

export default IconArrowRightDouble;
