```js
<div className="list">
  <IconArrowRightDouble dimension={IconArrowRightDouble.REFS.DIMENSIONS.XS} />
  <IconArrowRightDouble dimension={IconArrowRightDouble.REFS.DIMENSIONS.S} />
  <IconArrowRightDouble dimension={IconArrowRightDouble.REFS.DIMENSIONS.M} />
  <IconArrowRightDouble dimension={IconArrowRightDouble.REFS.DIMENSIONS.L} />
  <IconArrowRightDouble dimension={IconArrowRightDouble.REFS.DIMENSIONS.XL} />
</div>
```