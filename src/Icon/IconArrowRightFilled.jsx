import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowRightFilled = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_RIGHT_FILLED} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M6,12.8970892 L6,3.10291076 C6,3.04607472 6.04514552,3 6.10083535,3 C6.12757855,3 6.15322641,3.01084235 6.17213671,3.03014186 L10.970466,7.92723111 C11.0098447,7.96742026 11.0098447,8.03257974 10.970466,8.07276889 L6.17213671,12.9698581 C6.13275805,13.0100473 6.06891264,13.0100473 6.02953399,12.9698581 C6.01062369,12.9505586 6,12.9243829 6,12.8970892 Z" />
  </IconPrimitive>
);

IconArrowRightFilled.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowRightFilled.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowRightFilled.REFS = {
  ...REFS,
};

export default IconArrowRightFilled;
