```js
<div className="list">
  <IconArrowRightFilled dimension={IconArrowRightFilled.REFS.DIMENSIONS.XS} />
  <IconArrowRightFilled dimension={IconArrowRightFilled.REFS.DIMENSIONS.S} />
  <IconArrowRightFilled dimension={IconArrowRightFilled.REFS.DIMENSIONS.M} />
  <IconArrowRightFilled dimension={IconArrowRightFilled.REFS.DIMENSIONS.L} />
  <IconArrowRightFilled dimension={IconArrowRightFilled.REFS.DIMENSIONS.XL} />
</div>
```