import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowUp = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_UP} viewBox={REFS.DIMENSIONS.XS}>
    <polygon points="13.4693939 11.29 8.00000024 5.82824911 2.53060662 11.29 1.47000003 10.2308754 8.00000024 3.71000004 14.5300004 10.2308754" />
  </IconPrimitive>
);

IconArrowUp.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowUp.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowUp.REFS = {
  ...REFS,
};

export default IconArrowUp;
