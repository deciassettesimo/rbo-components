```js
<div className="list">
  <IconArrowUp dimension={IconArrowUp.REFS.DIMENSIONS.XS} />
  <IconArrowUp dimension={IconArrowUp.REFS.DIMENSIONS.S} />
  <IconArrowUp dimension={IconArrowUp.REFS.DIMENSIONS.M} />
  <IconArrowUp dimension={IconArrowUp.REFS.DIMENSIONS.L} />
  <IconArrowUp dimension={IconArrowUp.REFS.DIMENSIONS.XL} />
</div>
```