import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconArrowUpFilled = props => (
  <IconPrimitive {...props} type={TYPES.ARROW_UP_FILLED} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M12.8970892,10 L3.10291076,10 C3.04607472,10 3,9.95485448 3,9.89916465 C3,9.87242145 3.01084235,9.84677359 3.03014186,9.82786329 L7.92723111,5.02953399 C7.96742026,4.99015534 8.03257974,4.99015534 8.07276889,5.02953399 L12.9698581,9.82786329 C13.0100473,9.86724195 13.0100473,9.93108736 12.9698581,9.97046601 C12.9505586,9.98937631 12.9243829,10 12.8970892,10 Z" />
  </IconPrimitive>
);

IconArrowUpFilled.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconArrowUpFilled.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconArrowUpFilled.REFS = {
  ...REFS,
};

export default IconArrowUpFilled;
