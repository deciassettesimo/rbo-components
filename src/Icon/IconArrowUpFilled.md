```js
<div className="list">
  <IconArrowUpFilled dimension={IconArrowUpFilled.REFS.DIMENSIONS.XS} />
  <IconArrowUpFilled dimension={IconArrowUpFilled.REFS.DIMENSIONS.S} />
  <IconArrowUpFilled dimension={IconArrowUpFilled.REFS.DIMENSIONS.M} />
  <IconArrowUpFilled dimension={IconArrowUpFilled.REFS.DIMENSIONS.L} />
  <IconArrowUpFilled dimension={IconArrowUpFilled.REFS.DIMENSIONS.XL} />
</div>
```