```js
<div className="list">
  <IconBlock dimension={IconBlock.REFS.DIMENSIONS.XS} />
  <IconBlock dimension={IconBlock.REFS.DIMENSIONS.S} />
  <IconBlock dimension={IconBlock.REFS.DIMENSIONS.M} />
  <IconBlock dimension={IconBlock.REFS.DIMENSIONS.L} />
  <IconBlock dimension={IconBlock.REFS.DIMENSIONS.XL} />
</div>
```