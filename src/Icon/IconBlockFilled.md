```js
<div className="list">
  <IconBlockFilled dimension={IconBlockFilled.REFS.DIMENSIONS.XS} />
  <IconBlockFilled dimension={IconBlockFilled.REFS.DIMENSIONS.S} />
  <IconBlockFilled dimension={IconBlockFilled.REFS.DIMENSIONS.M} />
  <IconBlockFilled dimension={IconBlockFilled.REFS.DIMENSIONS.L} />
  <IconBlockFilled dimension={IconBlockFilled.REFS.DIMENSIONS.XL} />
</div>
```