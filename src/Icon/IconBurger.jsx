import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconBurger = props => (
  <IconPrimitive {...props} type={TYPES.BURGER} viewBox={REFS.DIMENSIONS.M}>
    <path d="M2,13 L2,11 L22,11 L22,13 L2,13 Z M2,19 L2,17 L22,17 L22,19 L2,19 Z M2,7 L2,5 L22,5 L22,7 L2,7 Z" />
  </IconPrimitive>
);

IconBurger.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconBurger.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconBurger.REFS = {
  ...REFS,
};

export default IconBurger;
