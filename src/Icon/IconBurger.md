```js
<div className="list">
  <IconBurger dimension={IconBurger.REFS.DIMENSIONS.XS} />
  <IconBurger dimension={IconBurger.REFS.DIMENSIONS.S} />
  <IconBurger dimension={IconBurger.REFS.DIMENSIONS.M} />
  <IconBurger dimension={IconBurger.REFS.DIMENSIONS.L} />
  <IconBurger dimension={IconBurger.REFS.DIMENSIONS.XL} />
</div>
```