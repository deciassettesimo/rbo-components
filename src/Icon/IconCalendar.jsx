import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconCalendar = props => (
  <IconPrimitive {...props} type={TYPES.CALENDAR} viewBox={REFS.DIMENSIONS.M}>
    <path d="M18,2 L22,2 L22,22 L2,22 L2,2 L6,2 L6,0 L8,0 L8,2 L16,2 L16,0 L18,0 L18,2 Z M20,7 L20,4 L4,4 L4,7 L20,7 Z M20,9 L4,9 L4,20 L20,20 L20,9 Z M6,11 L8,11 L8,13 L6,13 L6,11 Z M6,16 L8,16 L8,18 L6,18 L6,16 Z M11,11 L13,11 L13,13 L11,13 L11,11 Z M11,16 L13,16 L13,18 L11,18 L11,16 Z M16,11 L18,11 L18,13 L16,13 L16,11 Z M16,16 L18,16 L18,18 L16,18 L16,16 Z" />
  </IconPrimitive>
);

IconCalendar.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconCalendar.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconCalendar.REFS = {
  ...REFS,
};

export default IconCalendar;
