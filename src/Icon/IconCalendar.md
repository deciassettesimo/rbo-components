```js
<div className="list">
  <IconCalendar dimension={IconCalendar.REFS.DIMENSIONS.XS} />
  <IconCalendar dimension={IconCalendar.REFS.DIMENSIONS.S} />
  <IconCalendar dimension={IconCalendar.REFS.DIMENSIONS.M} />
  <IconCalendar dimension={IconCalendar.REFS.DIMENSIONS.L} />
  <IconCalendar dimension={IconCalendar.REFS.DIMENSIONS.XL} />
</div>
```