```js
<div className="list">
  <IconCards dimension={IconCards.REFS.DIMENSIONS.XS} />
  <IconCards dimension={IconCards.REFS.DIMENSIONS.S} />
  <IconCards dimension={IconCards.REFS.DIMENSIONS.M} />
  <IconCards dimension={IconCards.REFS.DIMENSIONS.L} />
  <IconCards dimension={IconCards.REFS.DIMENSIONS.XL} />
</div>
```