```js
<div className="list">
  <IconCase dimension={IconCase.REFS.DIMENSIONS.XS} />
  <IconCase dimension={IconCase.REFS.DIMENSIONS.S} />
  <IconCase dimension={IconCase.REFS.DIMENSIONS.M} />
  <IconCase dimension={IconCase.REFS.DIMENSIONS.L} />
  <IconCase dimension={IconCase.REFS.DIMENSIONS.XL} />
</div>
```