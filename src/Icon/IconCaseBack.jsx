import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconCaseBack = props => (
  <IconPrimitive {...props} type={TYPES.CASE_BACK} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M5.5,4.5 L5.5,14.5 L10.5,14.5 L10.5,4.5 L5.5,4.5 Z M4,3 L4,0 L12,0 L12,3 L16,3 L16,16 L0,16 L0,3 L4,3 Z M4,4.5 L1.5,4.5 L1.5,14.5 L4,14.5 L4,4.5 Z M12,4.5 L12,14.5 L14.5,14.5 L14.5,4.5 L12,4.5 Z M5.5,3 L10.5,3 L10.5,1.5 L5.5,1.5 L5.5,3 Z" />
  </IconPrimitive>
);

IconCaseBack.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconCaseBack.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconCaseBack.REFS = {
  ...REFS,
};

export default IconCaseBack;
