```js
<div className="list">
  <IconCaseBack dimension={IconCaseBack.REFS.DIMENSIONS.XS} />
  <IconCaseBack dimension={IconCaseBack.REFS.DIMENSIONS.S} />
  <IconCaseBack dimension={IconCaseBack.REFS.DIMENSIONS.M} />
  <IconCaseBack dimension={IconCaseBack.REFS.DIMENSIONS.L} />
  <IconCaseBack dimension={IconCaseBack.REFS.DIMENSIONS.XL} />
</div>
```