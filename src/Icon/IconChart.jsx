import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconChart = props => (
  <IconPrimitive {...props} type={TYPES.CHART} viewBox={REFS.DIMENSIONS.S}>
    <g fill="none">
      <path d="M0 0h20v20H0z" />
      <path
        fill={props.color}
        d="M9 2v9h9a9 9 0 1 1-9-9zm-7 9a7 7 0 0 0 13.71 2H7V4.29C4.109 5.15 2 7.829 2 11zm9-2V0a9 9 0 0 1 9 9h-9zm6.709-2A7.031 7.031 0 0 0 13 2.291V7h4.709z"
      />
    </g>
  </IconPrimitive>
);

IconChart.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconChart.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconChart.REFS = {
  ...REFS,
};

export default IconChart;
