import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconChat = props => (
  <IconPrimitive {...props} type={TYPES.CHAT} viewBox={REFS.DIMENSIONS.M}>
    <path d="M19,19 L19,22 L16.5857864,22 L13.5857864,19 L1,19 L1,2 L23,2 L23,19 L19,19 Z M17,19.5857864 L17,17 L21,17 L21,4 L3,4 L3,17 L14.4142136,17 L17,19.5857864 Z M5,6 L19,6 L19,8 L5,8 L5,6 Z M5,10 L14,10 L14,12 L5,12 L5,10 Z" />
  </IconPrimitive>
);

IconChat.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconChat.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconChat.REFS = {
  ...REFS,
};

export default IconChat;
