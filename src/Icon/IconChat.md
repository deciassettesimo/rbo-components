```js
<div className="list">
  <IconChat dimension={IconChat.REFS.DIMENSIONS.XS} />
  <IconChat dimension={IconChat.REFS.DIMENSIONS.S} />
  <IconChat dimension={IconChat.REFS.DIMENSIONS.M} />
  <IconChat dimension={IconChat.REFS.DIMENSIONS.L} />
  <IconChat dimension={IconChat.REFS.DIMENSIONS.XL} />
</div>
```