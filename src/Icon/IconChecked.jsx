import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconChecked = props => (
  <IconPrimitive {...props} type={TYPES.CHECKED} viewBox={REFS.DIMENSIONS.S}>
    <polygon points="17.2928932 4.29289322 18.7071068 5.70710678 7 17.4142136 1.29289322 11.7071068 2.70710678 10.2928932 7 14.5857864" />
  </IconPrimitive>
);

IconChecked.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconChecked.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconChecked.REFS = {
  ...REFS,
};

export default IconChecked;
