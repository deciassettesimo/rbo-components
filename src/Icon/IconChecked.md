```js
<div className="list">
  <IconChecked dimension={IconChecked.REFS.DIMENSIONS.XS} />
  <IconChecked dimension={IconChecked.REFS.DIMENSIONS.S} />
  <IconChecked dimension={IconChecked.REFS.DIMENSIONS.M} />
  <IconChecked dimension={IconChecked.REFS.DIMENSIONS.L} />
  <IconChecked dimension={IconChecked.REFS.DIMENSIONS.XL} />
</div>
```