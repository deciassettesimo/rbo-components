import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconCheckedCheckbox = props => (
  <IconPrimitive {...props} type={TYPES.CHECKED_CHECKBOX} viewBox={REFS.DIMENSIONS.S}>
    <polygon points="7 12.732233 15.6161165 4.11611652 17.3838835 5.88388348 7 16.267767 2.61611652 11.8838835 4.38388348 10.1161165" />
  </IconPrimitive>
);

IconCheckedCheckbox.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconCheckedCheckbox.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconCheckedCheckbox.REFS = {
  ...REFS,
};

export default IconCheckedCheckbox;
