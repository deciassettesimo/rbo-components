```js
<div className="list">
  <IconCheckedCheckbox dimension={IconCheckedCheckbox.REFS.DIMENSIONS.XS} />
  <IconCheckedCheckbox dimension={IconCheckedCheckbox.REFS.DIMENSIONS.S} />
  <IconCheckedCheckbox dimension={IconCheckedCheckbox.REFS.DIMENSIONS.M} />
  <IconCheckedCheckbox dimension={IconCheckedCheckbox.REFS.DIMENSIONS.L} />
  <IconCheckedCheckbox dimension={IconCheckedCheckbox.REFS.DIMENSIONS.XL} />
</div>
```