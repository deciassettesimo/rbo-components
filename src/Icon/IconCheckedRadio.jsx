import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconCheckedRadio = props => (
  <IconPrimitive {...props} type={TYPES.CHECKED_RADIO} viewBox={REFS.DIMENSIONS.S}>
    <circle cx="10" cy="10" r="6.25" />
  </IconPrimitive>
);

IconCheckedRadio.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconCheckedRadio.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconCheckedRadio.REFS = {
  ...REFS,
};

export default IconCheckedRadio;
