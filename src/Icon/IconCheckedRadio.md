```js
<div className="list">
  <IconCheckedRadio dimension={IconCheckedRadio.REFS.DIMENSIONS.XS} />
  <IconCheckedRadio dimension={IconCheckedRadio.REFS.DIMENSIONS.S} />
  <IconCheckedRadio dimension={IconCheckedRadio.REFS.DIMENSIONS.M} />
  <IconCheckedRadio dimension={IconCheckedRadio.REFS.DIMENSIONS.L} />
  <IconCheckedRadio dimension={IconCheckedRadio.REFS.DIMENSIONS.XL} />
</div>
```