```js
<div className="list">
  <IconCircle dimension={IconCircle.REFS.DIMENSIONS.XS} />
  <IconCircle dimension={IconCircle.REFS.DIMENSIONS.S} />
  <IconCircle dimension={IconCircle.REFS.DIMENSIONS.M} />
  <IconCircle dimension={IconCircle.REFS.DIMENSIONS.L} />
  <IconCircle dimension={IconCircle.REFS.DIMENSIONS.XL} />
</div>
```