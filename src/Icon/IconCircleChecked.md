```js
<div className="list">
  <IconCircleChecked dimension={IconCircleChecked.REFS.DIMENSIONS.XS} />
  <IconCircleChecked dimension={IconCircleChecked.REFS.DIMENSIONS.S} />
  <IconCircleChecked dimension={IconCircleChecked.REFS.DIMENSIONS.M} />
  <IconCircleChecked dimension={IconCircleChecked.REFS.DIMENSIONS.L} />
  <IconCircleChecked dimension={IconCircleChecked.REFS.DIMENSIONS.XL} />
</div>
```