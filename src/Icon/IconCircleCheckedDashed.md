```js
<div className="list">
  <IconCircleCheckedDashed dimension={IconCircleCheckedDashed.REFS.DIMENSIONS.XS} />
  <IconCircleCheckedDashed dimension={IconCircleCheckedDashed.REFS.DIMENSIONS.S} />
  <IconCircleCheckedDashed dimension={IconCircleCheckedDashed.REFS.DIMENSIONS.M} />
  <IconCircleCheckedDashed dimension={IconCircleCheckedDashed.REFS.DIMENSIONS.L} />
  <IconCircleCheckedDashed dimension={IconCircleCheckedDashed.REFS.DIMENSIONS.XL} />
</div>
```