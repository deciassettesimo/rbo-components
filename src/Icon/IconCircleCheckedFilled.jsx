import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconCircleCheckedFilled = props => (
  <IconPrimitive {...props} type={TYPES.CIRCLE_CHECKED_FILLED} viewBox={REFS.DIMENSIONS.M}>
    <path d="M12,22 C6.4771525,22 2,17.5228475 2,12 C2,6.4771525 6.4771525,2 12,2 C17.5228475,2 22,6.4771525 22,12 C22,17.5228475 17.5228475,22 12,22 Z M17.2928932,7.29289322 L10,14.5857864 L7.70710678,12.2928932 L6.29289322,13.7071068 L10,17.4142136 L18.7071068,8.70710678 L17.2928932,7.29289322 Z" />
  </IconPrimitive>
);

IconCircleCheckedFilled.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconCircleCheckedFilled.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconCircleCheckedFilled.REFS = {
  ...REFS,
};

export default IconCircleCheckedFilled;
