```js
<div className="list">
  <IconCircleCheckedFilled dimension={IconCircleCheckedFilled.REFS.DIMENSIONS.XS} />
  <IconCircleCheckedFilled dimension={IconCircleCheckedFilled.REFS.DIMENSIONS.S} />
  <IconCircleCheckedFilled dimension={IconCircleCheckedFilled.REFS.DIMENSIONS.M} />
  <IconCircleCheckedFilled dimension={IconCircleCheckedFilled.REFS.DIMENSIONS.L} />
  <IconCircleCheckedFilled dimension={IconCircleCheckedFilled.REFS.DIMENSIONS.XL} />
</div>
```