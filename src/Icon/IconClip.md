```js
<div className="list">
  <IconClip dimension={IconClip.REFS.DIMENSIONS.XS} />
  <IconClip dimension={IconClip.REFS.DIMENSIONS.S} />
  <IconClip dimension={IconClip.REFS.DIMENSIONS.M} />
  <IconClip dimension={IconClip.REFS.DIMENSIONS.L} />
  <IconClip dimension={IconClip.REFS.DIMENSIONS.XL} />
</div>
```