```js
<div className="list">
  <IconClock dimension={IconClock.REFS.DIMENSIONS.XS} />
  <IconClock dimension={IconClock.REFS.DIMENSIONS.S} />
  <IconClock dimension={IconClock.REFS.DIMENSIONS.M} />
  <IconClock dimension={IconClock.REFS.DIMENSIONS.L} />
  <IconClock dimension={IconClock.REFS.DIMENSIONS.XL} />
</div>
```