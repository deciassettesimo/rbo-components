import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconClose = props => (
  <IconPrimitive {...props} type={TYPES.CLOSE} viewBox={REFS.DIMENSIONS.M}>
    <path d="M19.3241128,3.29289322 L20.7383263,4.70710678 L4.70710678,20.7383263 L3.29289322,19.3241128 L19.3241128,3.29289322 Z M13.4298233,12.0156098 L20.7383263,19.3241128 L19.3241128,20.7383263 L12.0156098,13.4298233 L4.70710678,20.7383263 L3.29289322,19.3241128 L10.6013962,12.0156098 L3.29289322,4.70710678 L4.70710678,3.29289322 L12.0156098,10.6013962 L19.3241128,3.29289322 L20.7383263,4.70710678 L13.4298233,12.0156098 Z" />
  </IconPrimitive>
);

IconClose.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconClose.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconClose.REFS = {
  ...REFS,
};

export default IconClose;
