```js
<div className="list">
  <IconClose dimension={IconClose.REFS.DIMENSIONS.XS} />
  <IconClose dimension={IconClose.REFS.DIMENSIONS.S} />
  <IconClose dimension={IconClose.REFS.DIMENSIONS.M} />
  <IconClose dimension={IconClose.REFS.DIMENSIONS.L} />
  <IconClose dimension={IconClose.REFS.DIMENSIONS.XL} />
</div>
```