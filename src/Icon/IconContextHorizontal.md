```js
<div className="list">
  <IconContextHorizontal dimension={IconContextHorizontal.REFS.DIMENSIONS.XS} />
  <IconContextHorizontal dimension={IconContextHorizontal.REFS.DIMENSIONS.S} />
  <IconContextHorizontal dimension={IconContextHorizontal.REFS.DIMENSIONS.M} />
  <IconContextHorizontal dimension={IconContextHorizontal.REFS.DIMENSIONS.L} />
  <IconContextHorizontal dimension={IconContextHorizontal.REFS.DIMENSIONS.XL} />
</div>
```