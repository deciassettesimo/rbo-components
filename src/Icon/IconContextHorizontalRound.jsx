import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconContextHorizontalRound = props => (
  <IconPrimitive {...props} type={TYPES.CONTEXT_HORIZONTAL_ROUND} viewBox={REFS.DIMENSIONS.S}>
    <path d="M10,19 C5.02943725,19 1,14.9705627 1,10 C1,5.02943725 5.02943725,1 10,1 C14.9705627,1 19,5.02943725 19,10 C19,14.9705627 14.9705627,19 10,19 Z M10,17 C13.8659932,17 17,13.8659932 17,10 C17,6.13400675 13.8659932,3 10,3 C6.13400675,3 3,6.13400675 3,10 C3,13.8659932 6.13400675,17 10,17 Z M5,9 L7,9 L7,11 L5,11 L5,9 Z M9,9 L11,9 L11,11 L9,11 L9,9 Z M13,9 L15,9 L15,11 L13,11 L13,9 Z" />
  </IconPrimitive>
);

IconContextHorizontalRound.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconContextHorizontalRound.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconContextHorizontalRound.REFS = {
  ...REFS,
};

export default IconContextHorizontalRound;
