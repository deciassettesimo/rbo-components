```js
<div className="list">
  <IconContextHorizontalRound dimension={IconContextHorizontalRound.REFS.DIMENSIONS.XS} />
  <IconContextHorizontalRound dimension={IconContextHorizontalRound.REFS.DIMENSIONS.S} />
  <IconContextHorizontalRound dimension={IconContextHorizontalRound.REFS.DIMENSIONS.M} />
  <IconContextHorizontalRound dimension={IconContextHorizontalRound.REFS.DIMENSIONS.L} />
  <IconContextHorizontalRound dimension={IconContextHorizontalRound.REFS.DIMENSIONS.XL} />
</div>
```