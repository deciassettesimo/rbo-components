```js
<div className="list">
  <IconContextVertical dimension={IconContextVertical.REFS.DIMENSIONS.XS} />
  <IconContextVertical dimension={IconContextVertical.REFS.DIMENSIONS.S} />
  <IconContextVertical dimension={IconContextVertical.REFS.DIMENSIONS.M} />
  <IconContextVertical dimension={IconContextVertical.REFS.DIMENSIONS.L} />
  <IconContextVertical dimension={IconContextVertical.REFS.DIMENSIONS.XL} />
</div>
```