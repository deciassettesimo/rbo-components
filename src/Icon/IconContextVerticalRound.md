```js
<div className="list">
  <IconContextVerticalRound dimension={IconContextVerticalRound.REFS.DIMENSIONS.XS} />
  <IconContextVerticalRound dimension={IconContextVerticalRound.REFS.DIMENSIONS.S} />
  <IconContextVerticalRound dimension={IconContextVerticalRound.REFS.DIMENSIONS.M} />
  <IconContextVerticalRound dimension={IconContextVerticalRound.REFS.DIMENSIONS.L} />
  <IconContextVerticalRound dimension={IconContextVerticalRound.REFS.DIMENSIONS.XL} />
</div>
```