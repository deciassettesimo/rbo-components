import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconCopy = props => (
  <IconPrimitive {...props} type={TYPES.COPY} viewBox={REFS.DIMENSIONS.M}>
    <path d="M5,19 L19,19 L19,5 L21,5 L21,21 L5,21 L5,19 Z M17,3 L17,17 L3,17 L3,3 L17,3 Z M5,15 L15,15 L15,5 L5,5 L5,15 Z" />
  </IconPrimitive>
);

IconCopy.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconCopy.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconCopy.REFS = {
  ...REFS,
};

export default IconCopy;
