```js
<div className="list">
  <IconCopy dimension={IconCopy.REFS.DIMENSIONS.XS} />
  <IconCopy dimension={IconCopy.REFS.DIMENSIONS.S} />
  <IconCopy dimension={IconCopy.REFS.DIMENSIONS.M} />
  <IconCopy dimension={IconCopy.REFS.DIMENSIONS.L} />
  <IconCopy dimension={IconCopy.REFS.DIMENSIONS.XL} />
</div>
```