```js
<div className="list">
  <IconCurrencyEur dimension={IconCurrencyEur.REFS.DIMENSIONS.XS} />
  <IconCurrencyEur dimension={IconCurrencyEur.REFS.DIMENSIONS.S} />
  <IconCurrencyEur dimension={IconCurrencyEur.REFS.DIMENSIONS.M} />
  <IconCurrencyEur dimension={IconCurrencyEur.REFS.DIMENSIONS.L} />
  <IconCurrencyEur dimension={IconCurrencyEur.REFS.DIMENSIONS.XL} />
</div>
```