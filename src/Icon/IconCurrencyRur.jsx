import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconCurrencyRur = props => (
  <IconPrimitive {...props} type={TYPES.CURRENCY_RUR} viewBox={REFS.DIMENSIONS.M}>
    <path d="M12,23 C5.92486775,23 1,18.0751322 1,12 C1,5.92486775 5.92486775,1 12,1 C18.0751322,1 23,5.92486775 23,12 C23,18.0751322 18.0751322,23 12,23 Z M12,21 C16.9705627,21 21,16.9705627 21,12 C21,7.02943725 16.9705627,3 12,3 C7.02943725,3 3,7.02943725 3,12 C3,16.9705627 7.02943725,21 12,21 Z M13.2744,15.8716 L11.0364,15.8716 L11.0364,17.236 L8.928,17.236 L8.928,15.8716 L7.5,15.8716 L7.5,14.164 L8.928,14.164 L8.928,13.63 L7.5,13.63 L7.5,11.92 L8.928,11.92 L8.928,7 L12.9084,7 C13.6764,7 14.352,7.138 14.9328,7.414 C15.5148,7.69 15.9648,8.086 16.2864,8.5996 C16.608,9.112 16.7676,9.6976 16.7676,10.354 C16.7676,11.3656 16.4244,12.166 15.738,12.7516 C15.0516,13.3372 14.0964,13.63 12.8724,13.63 L11.0376,13.63 L11.0376,14.164 L13.2732,14.164 L13.2732,15.8728 L13.2744,15.8716 Z M11.0376,11.92 L12.8592,11.92 C14.04,11.92 14.6304,11.4028 14.6304,10.366 C14.6304,9.874 14.4804,9.4768 14.1804,9.1744 C13.8804,8.872 13.4664,8.7184 12.936,8.7136 L11.0376,8.7136 L11.0376,11.92 Z" />
  </IconPrimitive>
);

IconCurrencyRur.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconCurrencyRur.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconCurrencyRur.REFS = {
  ...REFS,
};

export default IconCurrencyRur;
