```js
<div className="list">
  <IconCurrencyRur dimension={IconCurrencyRur.REFS.DIMENSIONS.XS} />
  <IconCurrencyRur dimension={IconCurrencyRur.REFS.DIMENSIONS.S} />
  <IconCurrencyRur dimension={IconCurrencyRur.REFS.DIMENSIONS.M} />
  <IconCurrencyRur dimension={IconCurrencyRur.REFS.DIMENSIONS.L} />
  <IconCurrencyRur dimension={IconCurrencyRur.REFS.DIMENSIONS.XL} />
</div>
```