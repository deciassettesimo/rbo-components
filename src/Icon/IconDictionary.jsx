import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDictionary = props => (
  <IconPrimitive {...props} type={TYPES.DICTIONARY} viewBox={REFS.DIMENSIONS.S}>
    <path d="M9,6.73606798 L2,3.23606798 L2,13.763932 L9,17.263932 L9,6.73606798 Z M11,6.73606798 L11,17.263932 L18,13.763932 L18,3.23606798 L11,6.73606798 Z M0,15 L0,0 L10,5 L20,0 L20,15 L10,20 L0,15 Z M8,12.3000002 L8,14.5360682 L3,12.0360682 L3,9.80000019 L8,12.3000002 Z M8,8.30000019 L8,10.5360682 L3,8.03606817 L3,5.80000019 L8,8.30000019 Z M12,12.3000002 L17,9.80000019 L17,12.0360682 L12,14.5360682 L12,12.3000002 Z M12,8.30000019 L17,5.80000019 L17,8.03606817 L12,10.5360682 L12,8.30000019 Z" />
  </IconPrimitive>
);

IconDictionary.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDictionary.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDictionary.REFS = {
  ...REFS,
};

export default IconDictionary;
