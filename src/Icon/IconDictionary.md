```js
<div className="list">
  <IconDictionary dimension={IconDictionary.REFS.DIMENSIONS.XS} />
  <IconDictionary dimension={IconDictionary.REFS.DIMENSIONS.S} />
  <IconDictionary dimension={IconDictionary.REFS.DIMENSIONS.M} />
  <IconDictionary dimension={IconDictionary.REFS.DIMENSIONS.L} />
  <IconDictionary dimension={IconDictionary.REFS.DIMENSIONS.XL} />
</div>
```