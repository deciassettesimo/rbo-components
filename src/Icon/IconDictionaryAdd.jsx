import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDictionaryAdd = props => (
  <IconPrimitive {...props} type={TYPES.DICTIONARY_ADD} viewBox={REFS.DIMENSIONS.S}>
    <path d="M9,6.73606798 L2,3.23606798 L2,13.763932 L10,17.763932 L10,20 L0,15 L0,0 L10,5 L20,0 L20,10 L18,10 L18,3.23606798 L11,6.73606798 L11,12 L9,12 L9,6.73606798 Z M16.5,14.4642292 L16.5,12.4642292 L14.5,12.4642292 L14.5,14.4642292 L12.5,14.4642292 L12.5,16.4642292 L14.5,16.4642292 L14.5,18.4642292 L16.5,18.4642292 L16.5,16.4642292 L18.5,16.4642292 L18.5,14.4642292 L16.5,14.4642292 Z M15.5,20 C13.0147186,20 11,17.9852814 11,15.5 C11,13.0147186 13.0147186,11 15.5,11 C17.9852814,11 20,13.0147186 20,15.5 C20,17.9852814 17.9852814,20 15.5,20 Z M12,8.30000019 L17,5.80000019 L17,8.03606817 L12,10.5360682 L12,8.30000019 Z M8,12.3000002 L8,14.5360682 L3,12.0360682 L3,9.80000019 L8,12.3000002 Z M8,8.30000019 L8,10.5360682 L3,8.03606817 L3,5.80000019 L8,8.30000019 Z" />
  </IconPrimitive>
);

IconDictionaryAdd.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDictionaryAdd.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDictionaryAdd.REFS = {
  ...REFS,
};

export default IconDictionaryAdd;
