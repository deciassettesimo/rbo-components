```js
<div className="list">
  <IconDictionaryAdd dimension={IconDictionaryAdd.REFS.DIMENSIONS.XS} />
  <IconDictionaryAdd dimension={IconDictionaryAdd.REFS.DIMENSIONS.S} />
  <IconDictionaryAdd dimension={IconDictionaryAdd.REFS.DIMENSIONS.M} />
  <IconDictionaryAdd dimension={IconDictionaryAdd.REFS.DIMENSIONS.L} />
  <IconDictionaryAdd dimension={IconDictionaryAdd.REFS.DIMENSIONS.XL} />
</div>
```