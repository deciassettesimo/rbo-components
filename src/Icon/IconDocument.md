```js
<div className="list">
  <IconDocument dimension={IconDocument.REFS.DIMENSIONS.XS} />
  <IconDocument dimension={IconDocument.REFS.DIMENSIONS.S} />
  <IconDocument dimension={IconDocument.REFS.DIMENSIONS.M} />
  <IconDocument dimension={IconDocument.REFS.DIMENSIONS.L} />
  <IconDocument dimension={IconDocument.REFS.DIMENSIONS.XL} />
</div>
```