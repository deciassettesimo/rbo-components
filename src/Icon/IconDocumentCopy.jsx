import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentCopy = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_COPY} viewBox={REFS.DIMENSIONS.M}>
    <path d="M17.9142136,5 L18,5 L18,5.08578644 L22.9142136,10 L23,10 L23,10.0857864 L23,24 L6,24 L6,4 L16.9142136,4 L17.9142136,5 Z M16,6 L8,6 L8,22 L21,22 L21,12 L16,12 L16,6 Z M18,7.91421356 L18,10 L20.0857864,10 L18,7.91421356 Z M4,2 L4,20 L2,20 L2,0 L13,0 L13,2 L4,2 Z" />
  </IconPrimitive>
);

IconDocumentCopy.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentCopy.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentCopy.REFS = {
  ...REFS,
};

export default IconDocumentCopy;
