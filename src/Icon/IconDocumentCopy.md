```js
<div className="list">
  <IconDocumentCopy dimension={IconDocumentCopy.REFS.DIMENSIONS.XS} />
  <IconDocumentCopy dimension={IconDocumentCopy.REFS.DIMENSIONS.S} />
  <IconDocumentCopy dimension={IconDocumentCopy.REFS.DIMENSIONS.M} />
  <IconDocumentCopy dimension={IconDocumentCopy.REFS.DIMENSIONS.L} />
  <IconDocumentCopy dimension={IconDocumentCopy.REFS.DIMENSIONS.XL} />
</div>
```