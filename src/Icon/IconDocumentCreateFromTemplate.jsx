import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentCreateFromTemplate = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_CREATE_FROM_TEMPLATE} viewBox={REFS.DIMENSIONS.M}>
    <path d="M13,3 L5,3 L5,21 L19,21 L19,9 L13,9 L13,3 Z M15,3.91421356 L15,7 L18.0857864,7 L15,3.91421356 Z M3,1 L14.9142136,1 L21,7.08578644 L21,23 L3,23 L3,1 Z" />
  </IconPrimitive>
);

IconDocumentCreateFromTemplate.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentCreateFromTemplate.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentCreateFromTemplate.REFS = {
  ...REFS,
};

export default IconDocumentCreateFromTemplate;
