```js
<div className="list">
  <IconDocumentCreateFromTemplate dimension={IconDocumentCreateFromTemplate.REFS.DIMENSIONS.XS} />
  <IconDocumentCreateFromTemplate dimension={IconDocumentCreateFromTemplate.REFS.DIMENSIONS.S} />
  <IconDocumentCreateFromTemplate dimension={IconDocumentCreateFromTemplate.REFS.DIMENSIONS.M} />
  <IconDocumentCreateFromTemplate dimension={IconDocumentCreateFromTemplate.REFS.DIMENSIONS.L} />
  <IconDocumentCreateFromTemplate dimension={IconDocumentCreateFromTemplate.REFS.DIMENSIONS.XL} />
</div>
```