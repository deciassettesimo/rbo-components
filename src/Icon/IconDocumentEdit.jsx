import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentEdit = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_EDIT} viewBox={REFS.DIMENSIONS.M}>
    <path d="M15.162,4.083 L14.097,3 L5,3 L5,21 L19,21 L19,10.993 L21,7.843 L21,23 L3,23 L3,1 L14.907,1 L16.257,2.35 L15.162,4.083 Z M6,17.36 L18,17.36 L18,19.36 L6,19.36 L6,17.36 Z M17.676,2 L21.892,4.53 L15.172,15.168 L11,16.838 L11,12.57 L17.676,2 Z M13,13.15 L13,13.883 L13.828,13.553 L19.108,5.191 L18.324,4.721 L13,13.15 Z" />
  </IconPrimitive>
);

IconDocumentEdit.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentEdit.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentEdit.REFS = {
  ...REFS,
};

export default IconDocumentEdit;
