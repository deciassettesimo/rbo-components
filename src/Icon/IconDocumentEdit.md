```js
<div className="list">
  <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.XS} />
  <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.S} />
  <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.M} />
  <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.L} />
  <IconDocumentEdit dimension={IconDocumentEdit.REFS.DIMENSIONS.XL} />
</div>
```