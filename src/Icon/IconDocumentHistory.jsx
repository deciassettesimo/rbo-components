import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentHistory = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_HISTORY} viewBox={REFS.DIMENSIONS.M}>
    <path d="M19,9 L13,9 L13,3 L5,3 L5,21 L12,21 L12,23 L3,23 L3,1 L14.9142136,1 L21,7.08578644 L21,12 L19,12 L19,9 Z M18.0857864,7 L15,3.91421356 L15,7 L18.0857864,7 Z M7,17 L7,15 L12,15 L12,17 L7,17 Z M7,13 L7,11 L15,11 L15,13 L7,13 Z M18.5,24 C15.4624339,24 13,21.5375661 13,18.5 C13,15.4624339 15.4624339,13 18.5,13 C21.5375661,13 24,15.4624339 24,18.5 C24,21.5375661 21.5375661,24 18.5,24 Z M18.5,22 C20.4329966,22 22,20.4329966 22,18.5 C22,16.5670034 20.4329966,15 18.5,15 C16.5670034,15 15,16.5670034 15,18.5 C15,20.4329966 16.5670034,22 18.5,22 Z M19.5,18.0857864 L21.2071068,19.7928932 L19.7928932,21.2071068 L17.5,18.9142136 L17.5,16 L19.5,16 L19.5,18.0857864 Z" />
  </IconPrimitive>
);

IconDocumentHistory.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentHistory.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentHistory.REFS = {
  ...REFS,
};

export default IconDocumentHistory;
