```js
<div className="list">
  <IconDocumentHistory dimension={IconDocumentHistory.REFS.DIMENSIONS.XS} />
  <IconDocumentHistory dimension={IconDocumentHistory.REFS.DIMENSIONS.S} />
  <IconDocumentHistory dimension={IconDocumentHistory.REFS.DIMENSIONS.M} />
  <IconDocumentHistory dimension={IconDocumentHistory.REFS.DIMENSIONS.L} />
  <IconDocumentHistory dimension={IconDocumentHistory.REFS.DIMENSIONS.XL} />
</div>
```