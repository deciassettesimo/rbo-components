import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentReissue = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_REISSUE} viewBox={REFS.DIMENSIONS.M}>
    <path d="M7.106,12 L5,12 L8,9 L11,12 L8.818,12 C9.034,14.392 11.045,16.293 13.493,16.293 C16.0854246,16.293 18.187,14.1914246 18.187,11.599 C18.187,11.169 18.125,10.753 18.016,10.357 L18.012,7.084 C19.213155,8.27994025 19.8901156,9.90399813 19.894,11.599 C19.8942653,13.2967321 19.2199613,14.9250056 18.0194835,16.1254835 C16.8190056,17.3259613 15.1907321,18.0002653 13.493,18 C10.101,18 7.326,15.336 7.106,12 Z M15,8 L9,8 L9,2 L2,2 L2,20 L15,20 L15,19 L17,19 L17,22 L0,22 L0,0 L10.414,0 L17,6.586 L17,11 L15,11 L15,8 Z M11,3.414 L11,6 L13.586,6 L11,3.414 Z" />
  </IconPrimitive>
);

IconDocumentReissue.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentReissue.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentReissue.REFS = {
  ...REFS,
};

export default IconDocumentReissue;
