```js
<div className="list">
  <IconDocumentReissue dimension={IconDocumentReissue.REFS.DIMENSIONS.XS} />
  <IconDocumentReissue dimension={IconDocumentReissue.REFS.DIMENSIONS.S} />
  <IconDocumentReissue dimension={IconDocumentReissue.REFS.DIMENSIONS.M} />
  <IconDocumentReissue dimension={IconDocumentReissue.REFS.DIMENSIONS.L} />
  <IconDocumentReissue dimension={IconDocumentReissue.REFS.DIMENSIONS.XL} />
</div>
```