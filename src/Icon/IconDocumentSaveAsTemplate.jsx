import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentSaveAsTemplate = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_SAVE_AS_TEMPLATE} viewBox={REFS.DIMENSIONS.M}>
    <path d="M5,21 L12,21 L12,23 L3,23 L3,18 L5,18 L5,21 Z M5,15 L3,15 L3,9 L5,9 L5,15 Z M19,9 L13,9 L13,3 L5,3 L5,6 L3,6 L3,1 L14.9142136,1 L21,7.08578644 L21,12 L19,12 L19,9 Z M18.0857864,7 L15,3.91421356 L15,7 L18.0857864,7 Z M14,14 L24,14 L24,24 L14,24 L14,14 Z M16,16 L16,22 L22,22 L22,16 L16,16 Z M19,21.5 C18.4477153,21.5 18,21.0522847 18,20.5 C18,19.9477153 18.4477153,19.5 19,19.5 C19.5522847,19.5 20,19.9477153 20,20.5 C20,21.0522847 19.5522847,21.5 19,21.5 Z M16,19 L16,17 L22,17 L22,19 L16,19 Z" />
  </IconPrimitive>
);

IconDocumentSaveAsTemplate.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentSaveAsTemplate.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentSaveAsTemplate.REFS = {
  ...REFS,
};

export default IconDocumentSaveAsTemplate;
