```js
<div className="list">
  <IconDocumentSaveAsTemplate dimension={IconDocumentSaveAsTemplate.REFS.DIMENSIONS.XS} />
  <IconDocumentSaveAsTemplate dimension={IconDocumentSaveAsTemplate.REFS.DIMENSIONS.S} />
  <IconDocumentSaveAsTemplate dimension={IconDocumentSaveAsTemplate.REFS.DIMENSIONS.M} />
  <IconDocumentSaveAsTemplate dimension={IconDocumentSaveAsTemplate.REFS.DIMENSIONS.L} />
  <IconDocumentSaveAsTemplate dimension={IconDocumentSaveAsTemplate.REFS.DIMENSIONS.XL} />
</div>
```