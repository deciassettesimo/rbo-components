import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentSend = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_SEND} viewBox={REFS.DIMENSIONS.M}>
    <path d="M21,9 L15,9 L15,3 L7,3 L7,6 L5,6 L5,1 L16.9142136,1 L23,7.08578644 L23,23 L5,23 L5,18 L7,18 L7,21 L21,21 L21,9 Z M20.0857864,7 L17,3.91421356 L17,7 L20.0857864,7 Z M5,16 L5,14 L10,14 L10,16 L5,16 Z M3,13 L3,11 L10,11 L10,13 L3,13 Z M1,10 L1,8 L10,8 L10,10 L1,10 Z" />
  </IconPrimitive>
);

IconDocumentSend.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentSend.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentSend.REFS = {
  ...REFS,
};

export default IconDocumentSend;
