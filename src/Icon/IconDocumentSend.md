```js
<div className="list">
  <IconDocumentSend dimension={IconDocumentSend.REFS.DIMENSIONS.XS} />
  <IconDocumentSend dimension={IconDocumentSend.REFS.DIMENSIONS.S} />
  <IconDocumentSend dimension={IconDocumentSend.REFS.DIMENSIONS.M} />
  <IconDocumentSend dimension={IconDocumentSend.REFS.DIMENSIONS.L} />
  <IconDocumentSend dimension={IconDocumentSend.REFS.DIMENSIONS.XL} />
</div>
```