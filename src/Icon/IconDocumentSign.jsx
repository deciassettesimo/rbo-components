import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentSign = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_SIGN} viewBox={REFS.DIMENSIONS.M}>
    <path d="M19,9 L13,9 L13,3 L5,3 L5,21 L9,21 L9,23 L3,23 L3,1 L14.9142136,1 L21,7.08578644 L21,12 L19,12 L19,9 Z M18.0857864,7 L15,3.91421356 L15,7 L18.0857864,7 Z M17,21.4142136 L17,23 L14.9142136,23 L13.9142136,24 L11,24 L11,20.0857864 L14.0008875,17.0848989 C14.0002961,17.0566319 14,17.0283311 14,17 C14,14.790861 15.790861,13 18,13 C20.209139,13 22,14.790861 22,17 C22,19.209139 20.209139,21 18,21 C17.8154334,21 17.6323084,20.9874314 17.4516138,20.9625998 L17,21.4142136 Z M15,21 L15,20.5857864 L16.84514,18.7406464 L17.418597,18.9144567 C17.6049437,18.970937 17.7999559,19 18,19 C19.1045695,19 20,18.1045695 20,17 C20,15.8954305 19.1045695,15 18,15 C16.8954305,15 16,15.8954305 16,17 C16,17.105133 16.0080254,17.2088792 16.0238552,17.3107682 L16.1017937,17.8124199 L13,20.9142136 L13,22 L13.0857864,22 L14.0857864,21 L15,21 Z M18,17.9 C17.5029437,17.9 17.1,17.4970563 17.1,17 C17.1,16.5029437 17.5029437,16.1 18,16.1 C18.4970563,16.1 18.9,16.5029437 18.9,17 C18.9,17.4970563 18.4970563,17.9 18,17.9 Z M7,13 L7,11 L15,11 L15,13 L7,13 Z M7,17 L7,15 L12,15 L12,17 L7,17 Z" />
  </IconPrimitive>
);

IconDocumentSign.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentSign.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentSign.REFS = {
  ...REFS,
};

export default IconDocumentSign;
