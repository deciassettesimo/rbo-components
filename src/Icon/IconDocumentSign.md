```js
<div className="list">
  <IconDocumentSign dimension={IconDocumentSign.REFS.DIMENSIONS.XS} />
  <IconDocumentSign dimension={IconDocumentSign.REFS.DIMENSIONS.S} />
  <IconDocumentSign dimension={IconDocumentSign.REFS.DIMENSIONS.M} />
  <IconDocumentSign dimension={IconDocumentSign.REFS.DIMENSIONS.L} />
  <IconDocumentSign dimension={IconDocumentSign.REFS.DIMENSIONS.XL} />
</div>
```