import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentSignAndSend = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_SIGN_AND_SEND} viewBox={REFS.DIMENSIONS.M}>
    <path d="M21,9 L15,9 L15,3 L7,3 L7,6 L5,6 L5,1 L16.9142136,1 L23,7.08578644 L23,12 L21,12 L21,9 Z M20.0857864,7 L17,3.91421356 L17,7 L20.0857864,7 Z M19,21.4142136 L19,23 L16.9142136,23 L15.9142136,24 L13,24 L13,20.0857864 L16.0008875,17.0848989 C16.0002961,17.0566319 16,17.0283311 16,17 C16,14.790861 17.790861,13 20,13 C22.209139,13 24,14.790861 24,17 C24,19.209139 22.209139,21 20,21 C19.8154334,21 19.6323084,20.9874314 19.4516138,20.9625998 L19,21.4142136 Z M17,21 L17,20.5857864 L18.84514,18.7406464 L19.418597,18.9144567 C19.6049437,18.970937 19.7999559,19 20,19 C21.1045695,19 22,18.1045695 22,17 C22,15.8954305 21.1045695,15 20,15 C18.8954305,15 18,15.8954305 18,17 C18,17.105133 18.0080254,17.2088792 18.0238552,17.3107682 L18.1017937,17.8124199 L15,20.9142136 L15,22 L15.0857864,22 L16.0857864,21 L17,21 Z M20,17.9 C19.5029437,17.9 19.1,17.4970563 19.1,17 C19.1,16.5029437 19.5029437,16.1 20,16.1 C20.4970563,16.1 20.9,16.5029437 20.9,17 C20.9,17.4970563 20.4970563,17.9 20,17.9 Z M3,13 L3,11 L10,11 L10,13 L3,13 Z M5,16 L5,14 L10,14 L10,16 L5,16 Z M1,10 L1,8 L10,8 L10,10 L1,10 Z M7,21 L11,21 L11,23 L5,23 L5,18 L7,18 L7,21 Z" />
  </IconPrimitive>
);

IconDocumentSignAndSend.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentSignAndSend.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentSignAndSend.REFS = {
  ...REFS,
};

export default IconDocumentSignAndSend;
