```js
<div className="list">
  <IconDocumentSignAndSend dimension={IconDocumentSignAndSend.REFS.DIMENSIONS.XS} />
  <IconDocumentSignAndSend dimension={IconDocumentSignAndSend.REFS.DIMENSIONS.S} />
  <IconDocumentSignAndSend dimension={IconDocumentSignAndSend.REFS.DIMENSIONS.M} />
  <IconDocumentSignAndSend dimension={IconDocumentSignAndSend.REFS.DIMENSIONS.L} />
  <IconDocumentSignAndSend dimension={IconDocumentSignAndSend.REFS.DIMENSIONS.XL} />
</div>
```