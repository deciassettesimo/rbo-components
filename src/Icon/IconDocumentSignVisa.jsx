import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocumentSignVisa = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENT_SIGN_VISA} viewBox={REFS.DIMENSIONS.M}>
    <path d="M18.1991432,10.6133568 L7.8125,21 L22,21 L22,23 L5.8125,23 L1,23 L1,18.1875 L18.1875,1 L23,5.8125 L19.6133568,9.19914322 L21.9142136,11.5 L17.2071068,16.2071068 L15.7928932,14.7928932 L19.0857864,11.5 L18.1991432,10.6133568 Z M18.1991432,7.78492966 L20.1715729,5.8125 L18.1875,3.82842712 L3,19.0159271 L3,21 L4.98407288,21 L16.7849297,9.19914322 L15.7928932,8.20710678 L17.2071068,6.79289322 L18.1991432,7.78492966 Z" />
  </IconPrimitive>
);

IconDocumentSignVisa.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocumentSignVisa.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocumentSignVisa.REFS = {
  ...REFS,
};

export default IconDocumentSignVisa;
