```js
<div className="list">
  <IconDocumentSignVisa dimension={IconDocumentSignVisa.REFS.DIMENSIONS.XS} />
  <IconDocumentSignVisa dimension={IconDocumentSignVisa.REFS.DIMENSIONS.S} />
  <IconDocumentSignVisa dimension={IconDocumentSignVisa.REFS.DIMENSIONS.M} />
  <IconDocumentSignVisa dimension={IconDocumentSignVisa.REFS.DIMENSIONS.L} />
  <IconDocumentSignVisa dimension={IconDocumentSignVisa.REFS.DIMENSIONS.XL} />
</div>
```