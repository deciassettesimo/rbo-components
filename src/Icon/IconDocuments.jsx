import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDocuments = props => (
  <IconPrimitive {...props} type={TYPES.DOCUMENTS} viewBox={REFS.DIMENSIONS.M}>
    <path d="M15,3 L7,3 L7,21 L21,21 L21,9 L15,9 L15,3 Z M17,3.91421356 L17,7 L20.0857864,7 L17,3.91421356 Z M5,1 L16.9142136,1 L23,7.08578644 L23,23 L5,23 L5,1 Z M9,18 L9,16 L19,16 L19,18 L9,18 Z M9,14 L9,12 L19,12 L19,14 L9,14 Z M1,1 L3,1 L3,23 L1,23 L1,1 Z" />
  </IconPrimitive>
);

IconDocuments.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDocuments.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDocuments.REFS = {
  ...REFS,
};

export default IconDocuments;
