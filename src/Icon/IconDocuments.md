```js
<div className="list">
  <IconDocuments dimension={IconDocuments.REFS.DIMENSIONS.XS} />
  <IconDocuments dimension={IconDocuments.REFS.DIMENSIONS.S} />
  <IconDocuments dimension={IconDocuments.REFS.DIMENSIONS.M} />
  <IconDocuments dimension={IconDocuments.REFS.DIMENSIONS.L} />
  <IconDocuments dimension={IconDocuments.REFS.DIMENSIONS.XL} />
</div>
```