import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDownload = props => (
  <IconPrimitive {...props} type={TYPES.DOWNLOAD} viewBox={REFS.DIMENSIONS.M}>
    <path d="M8,2 L16,2 L16,13 L22,13 L12,20.5 L2,13 L8,13 L8,2 Z M14,15 L14,4 L10,4 L10,15 L8,15 L12,18 L16,15 L14,15 Z M2,23 L2,21 L22,21 L22,23 L2,23 Z" />
  </IconPrimitive>
);

IconDownload.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDownload.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDownload.REFS = {
  ...REFS,
};

export default IconDownload;
