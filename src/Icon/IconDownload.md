```js
<div className="list">
  <IconDownload dimension={IconDownload.REFS.DIMENSIONS.XS} />
  <IconDownload dimension={IconDownload.REFS.DIMENSIONS.S} />
  <IconDownload dimension={IconDownload.REFS.DIMENSIONS.M} />
  <IconDownload dimension={IconDownload.REFS.DIMENSIONS.L} />
  <IconDownload dimension={IconDownload.REFS.DIMENSIONS.XL} />
</div>
```