import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconDraggable = props => (
  <IconPrimitive {...props} type={TYPES.DRAGGABLE} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M0,5.5 L0,4 L16,4 L16,5.5 L0,5.5 Z M0,8.5 L0,7 L16,7 L16,8.5 L0,8.5 Z M0,11.5 L0,10 L16,10 L16,11.5 L0,11.5 Z" />
  </IconPrimitive>
);

IconDraggable.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconDraggable.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconDraggable.REFS = {
  ...REFS,
};

export default IconDraggable;
