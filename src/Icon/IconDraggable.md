```js
<div className="list">
  <IconDraggable dimension={IconDraggable.REFS.DIMENSIONS.XS} />
  <IconDraggable dimension={IconDraggable.REFS.DIMENSIONS.S} />
  <IconDraggable dimension={IconDraggable.REFS.DIMENSIONS.M} />
  <IconDraggable dimension={IconDraggable.REFS.DIMENSIONS.L} />
  <IconDraggable dimension={IconDraggable.REFS.DIMENSIONS.XL} />
</div>
```