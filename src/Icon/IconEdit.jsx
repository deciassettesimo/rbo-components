import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconEdit = props => (
  <IconPrimitive {...props} type={TYPES.EDIT} viewBox={REFS.DIMENSIONS.S}>
    <path d="M15,6.58578644 L16.0857864,5.5 L14.5,3.91421356 L3,15.4142136 L3,17 L4.58578644,17 L13.5857864,8 L12.5428932,6.95710678 L13.9571068,5.54289322 L15,6.58578644 Z M7.41421356,17 L13,17 L13,19 L5.41421356,19 L1,19 L1,14.5857864 L14.5,1.08578644 L18.9142136,5.5 L7.41421356,17 Z" />
  </IconPrimitive>
);

IconEdit.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconEdit.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconEdit.REFS = {
  ...REFS,
};

export default IconEdit;
