```js
<div className="list">
  <IconEdit dimension={IconEdit.REFS.DIMENSIONS.XS} />
  <IconEdit dimension={IconEdit.REFS.DIMENSIONS.S} />
  <IconEdit dimension={IconEdit.REFS.DIMENSIONS.M} />
  <IconEdit dimension={IconEdit.REFS.DIMENSIONS.L} />
  <IconEdit dimension={IconEdit.REFS.DIMENSIONS.XL} />
</div>
```