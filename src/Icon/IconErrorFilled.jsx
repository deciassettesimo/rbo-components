import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconErrorFilled = props => (
  <IconPrimitive {...props} type={TYPES.ERROR_FILLED} viewBox={REFS.DIMENSIONS.S}>
    <path d="M10,8.58578644 L7.20710678,5.79289322 L5.79289322,7.20710678 L8.58578644,10 L5.79289322,12.7928932 L7.20710678,14.2071068 L10,11.4142136 L12.7928932,14.2071068 L14.2071068,12.7928932 L11.4142136,10 L14.2071068,7.20710678 L12.7928932,5.79289322 L10,8.58578644 Z M10,18 C5.581722,18 2,14.418278 2,10 C2,5.581722 5.581722,2 10,2 C14.418278,2 18,5.581722 18,10 C18,14.418278 14.418278,18 10,18 Z" />
  </IconPrimitive>
);

IconErrorFilled.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconErrorFilled.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconErrorFilled.REFS = {
  ...REFS,
};

export default IconErrorFilled;
