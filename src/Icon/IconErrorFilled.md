```js
<div className="list">
  <IconErrorFilled dimension={IconErrorFilled.REFS.DIMENSIONS.XS} />
  <IconErrorFilled dimension={IconErrorFilled.REFS.DIMENSIONS.S} />
  <IconErrorFilled dimension={IconErrorFilled.REFS.DIMENSIONS.M} />
  <IconErrorFilled dimension={IconErrorFilled.REFS.DIMENSIONS.L} />
  <IconErrorFilled dimension={IconErrorFilled.REFS.DIMENSIONS.XL} />
</div>
```