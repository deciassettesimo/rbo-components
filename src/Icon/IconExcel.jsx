import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconExcel = props => (
  <IconPrimitive {...props} type={TYPES.EXCEL} viewBox={REFS.DIMENSIONS.M}>
    <path d="M15.6642136,14.75 L18,17.0857864 L18,20 L13.0857864,20 L12,18.9142136 L10.9142136,20 L6,20 L6,17.5857864 L8.33578644,15.25 L6,12.9142136 L6,10 L10.9142136,10 L12,11.0857864 L13.0857864,10 L18,10 L18,12.4142136 L15.6642136,14.75 Z M14.25,13.3357864 L15.5857864,12 L13.9142136,12 L13.4142136,12.5 L14.25,13.3357864 Z M10.5857864,17.5 L9.75,16.6642136 L8.41421356,18 L10.0857864,18 L10.5857864,17.5 Z M13,3 L5,3 L5,21 L19,21 L19,9 L13,9 L13,3 Z M15,3.91421356 L15,7 L18.0857864,7 L15,3.91421356 Z M3,1 L14.9142136,1 L21,7.08578644 L21,23 L3,23 L3,1 Z M16,18 L16,17.9142136 L10.0857864,12 L8,12 L8,12.0857864 L13.9142136,18 L16,18 Z" />
  </IconPrimitive>
);

IconExcel.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconExcel.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconExcel.REFS = {
  ...REFS,
};

export default IconExcel;
