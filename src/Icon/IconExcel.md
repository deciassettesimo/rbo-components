```js
<div className="list">
  <IconExcel dimension={IconExcel.REFS.DIMENSIONS.XS} />
  <IconExcel dimension={IconExcel.REFS.DIMENSIONS.S} />
  <IconExcel dimension={IconExcel.REFS.DIMENSIONS.M} />
  <IconExcel dimension={IconExcel.REFS.DIMENSIONS.L} />
  <IconExcel dimension={IconExcel.REFS.DIMENSIONS.XL} />
</div>
```