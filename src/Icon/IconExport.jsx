import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconExportXS = props => (
  <IconPrimitive {...props} type={TYPES.EXPORT} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M8,3.06066017 L3.53033009,7.53033009 L2.46966991,6.46966991 L8,0.939339828 L13.5303301,6.46966991 L12.4696699,7.53033009 L8,3.06066017 Z M14.5,14.5 L14.5,11 L16,11 L16,16 L0,16 L0,11 L1.5,11 L1.5,14.5 L14.5,14.5 Z M7.25,3 L8.75,3 L8.75,13 L7.25,13 L7.25,3 Z" />
  </IconPrimitive>
);

const IconExportM = props => (
  <IconPrimitive {...props} type={TYPES.EXPORT} viewBox={REFS.DIMENSIONS.M}>
    <path d="M21,21 L21,16 L23,16 L23,23 L1,23 L1,16 L3,16 L3,21 L21,21 Z M12.0568757,3.86465468 L5.67717163,9.73582511 L4.32282837,8.26417489 L12.0691243,1.13534532 L19.6837487,8.2702824 L18.3162513,9.7297176 L12.0568757,3.86465468 Z M13,19 L11,19 L11,3.875 L13,3.875 L13,19 Z" />
  </IconPrimitive>
);

const IconExport = props => {
  if (props.dimension <= REFS.DIMENSIONS.XS) return <IconExportXS {...props} />;
  return <IconExportM {...props} />;
};

IconExport.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconExport.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconExport.REFS = {
  ...REFS,
};

export default IconExport;
