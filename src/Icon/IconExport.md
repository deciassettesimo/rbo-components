```js
<div className="list">
  <IconExport dimension={IconExport.REFS.DIMENSIONS.XS} />
  <IconExport dimension={IconExport.REFS.DIMENSIONS.S} />
  <IconExport dimension={IconExport.REFS.DIMENSIONS.M} />
  <IconExport dimension={IconExport.REFS.DIMENSIONS.L} />
  <IconExport dimension={IconExport.REFS.DIMENSIONS.XL} />
</div>
```