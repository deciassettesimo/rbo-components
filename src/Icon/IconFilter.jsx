import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconFilter = props => (
  <IconPrimitive {...props} type={TYPES.EXPORT} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M0,3.5 L0,2 L16,2 L16,3.5 L0,3.5 Z M2,8.5 L2,7 L14,7 L14,8.5 L2,8.5 Z M4,13.5 L4,12 L12,12 L12,13.5 L4,13.5 Z" />
  </IconPrimitive>
);

IconFilter.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconFilter.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconFilter.REFS = {
  ...REFS,
};

export default IconFilter;
