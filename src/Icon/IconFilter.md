```js
<div className="list">
  <IconFilter dimension={IconFilter.REFS.DIMENSIONS.XS} />
  <IconFilter dimension={IconFilter.REFS.DIMENSIONS.S} />
  <IconFilter dimension={IconFilter.REFS.DIMENSIONS.M} />
  <IconFilter dimension={IconFilter.REFS.DIMENSIONS.L} />
  <IconFilter dimension={IconFilter.REFS.DIMENSIONS.XL} />
</div>
```