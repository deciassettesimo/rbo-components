import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconFlag = props => (
  <IconPrimitive {...props} type={TYPES.FLAG} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M3.422 2H2v12h1.408l-.003-5.457h10.614L11.59 5.54l2.43-2.95H3.405z" />
  </IconPrimitive>
);

IconFlag.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconFlag.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconFlag.REFS = {
  ...REFS,
};

export default IconFlag;
