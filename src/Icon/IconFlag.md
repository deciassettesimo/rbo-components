```js
<div className="list">
  <IconFlag dimension={IconFlag.REFS.DIMENSIONS.XS} />
  <IconFlag dimension={IconFlag.REFS.DIMENSIONS.S} />
  <IconFlag dimension={IconFlag.REFS.DIMENSIONS.M} />
  <IconFlag dimension={IconFlag.REFS.DIMENSIONS.L} />
  <IconFlag dimension={IconFlag.REFS.DIMENSIONS.XL} />
</div>
```