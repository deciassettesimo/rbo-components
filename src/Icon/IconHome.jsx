import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconHome = props => (
  <IconPrimitive {...props} type={TYPES.HOME} viewBox={REFS.DIMENSIONS.M}>
    <path d="M3.5,11 L5.5,11 L5.5,19 L3.5,19 L3.5,11 Z M8.5,11 L10.5,11 L10.5,19 L8.5,19 L8.5,11 Z M13.5,11 L15.5,11 L15.5,19 L13.5,19 L13.5,11 Z M2,23 L2,21 L22,21 L22,23 L2,23 Z M18.5,11 L20.5,11 L20.5,19 L18.5,19 L18.5,11 Z M0,9 L12,1 L24,9 L0,9 Z M6.60555128,7 L17.3944487,7 L12,3.40370085 L6.60555128,7 Z" />
  </IconPrimitive>
);

IconHome.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconHome.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconHome.REFS = {
  ...REFS,
};

export default IconHome;
