```js
<div className="list">
  <IconHome dimension={IconHome.REFS.DIMENSIONS.XS} />
  <IconHome dimension={IconHome.REFS.DIMENSIONS.S} />
  <IconHome dimension={IconHome.REFS.DIMENSIONS.M} />
  <IconHome dimension={IconHome.REFS.DIMENSIONS.L} />
  <IconHome dimension={IconHome.REFS.DIMENSIONS.XL} />
</div>
```