import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconImportXS = props => (
  <IconPrimitive {...props} type={TYPES.IMPORT} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M14.5,14.5 L14.5,11 L16,11 L16,16 L0,16 L0,11 L1.5,11 L1.5,14.5 L14.5,14.5 Z M8.75,10.19 L12.47,6.47 L13.53,7.53 L8,13.06 L2.47,7.53 L3.53,6.47 L7.25,10.19 L7.25,0 L8.75,0 L8.75,10.19 Z" />
  </IconPrimitive>
);

const IconImportM = props => (
  <IconPrimitive {...props} type={TYPES.IMPORT} viewBox={REFS.DIMENSIONS.M}>
    <path d="M21,21 L21,16 L23,16 L23,23 L1,23 L1,16 L3,16 L3,21 L21,21 Z M18.3162513,10.2702824 L19.6837487,11.7297176 L12.0691243,18.8646547 L4.32282837,11.7358251 L5.67717163,10.2641749 L12.0568757,16.1353453 L18.3162513,10.2702824 Z M11,1 L13,1 L13,16.125 L11,16.125 L11,1 Z" />
  </IconPrimitive>
);

const IconImport = props => {
  if (props.dimension <= REFS.DIMENSIONS.XS) return <IconImportXS {...props} />;
  return <IconImportM {...props} />;
};

IconImport.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconImport.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconImport.REFS = {
  ...REFS,
};

export default IconImport;
