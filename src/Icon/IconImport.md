```js
<div className="list">
  <IconImport dimension={IconImport.REFS.DIMENSIONS.XS} />
  <IconImport dimension={IconImport.REFS.DIMENSIONS.S} />
  <IconImport dimension={IconImport.REFS.DIMENSIONS.M} />
  <IconImport dimension={IconImport.REFS.DIMENSIONS.L} />
  <IconImport dimension={IconImport.REFS.DIMENSIONS.XL} />
</div>
```