```js
<div className="list">
  <IconInfo dimension={IconInfo.REFS.DIMENSIONS.XS} />
  <IconInfo dimension={IconInfo.REFS.DIMENSIONS.S} />
  <IconInfo dimension={IconInfo.REFS.DIMENSIONS.M} />
  <IconInfo dimension={IconInfo.REFS.DIMENSIONS.L} />
  <IconInfo dimension={IconInfo.REFS.DIMENSIONS.XL} />
</div>
```