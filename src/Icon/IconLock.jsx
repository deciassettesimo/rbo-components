import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconLock = props => (
  <IconPrimitive {...props} type={TYPES.LOCK} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M4.25,4 C4.25,1.92893219 5.92893219,0.25 8,0.25 C10.0710678,0.25 11.75,1.92893219 11.75,4 L15,4 L15,16 L1,16 L1,4 L4.25,4 Z M5.75,4 L10.25,4 C10.25,2.75735931 9.24264069,1.75 8,1.75 C6.75735931,1.75 5.75,2.75735931 5.75,4 Z M2.5,5.5 L2.5,14.5 L13.5,14.5 L13.5,5.5 L2.5,5.5 Z M7.25,8 L8.75,8 L8.75,12 L7.25,12 L7.25,8 Z" />
  </IconPrimitive>
);

IconLock.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconLock.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconLock.REFS = {
  ...REFS,
};

export default IconLock;
