```js
<div className="list">
  <IconLock dimension={IconLock.REFS.DIMENSIONS.XS} />
  <IconLock dimension={IconLock.REFS.DIMENSIONS.S} />
  <IconLock dimension={IconLock.REFS.DIMENSIONS.M} />
  <IconLock dimension={IconLock.REFS.DIMENSIONS.L} />
  <IconLock dimension={IconLock.REFS.DIMENSIONS.XL} />
</div>
```