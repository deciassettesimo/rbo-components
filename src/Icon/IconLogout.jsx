import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconLogout = props => (
  <IconPrimitive {...props} type={TYPES.LOGOUT} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M12.3892236,7.25 L9.8452398,5.06944245 L10.8214269,3.93055755 L15.5691097,8 L10.8214269,12.0694425 L9.8452398,10.9305575 L12.3892236,8.75 L5,8.75 L5,7.25 L12.3892236,7.25 Z M2.5,14.5 L11,14.5 L11,16 L1,16 L1,-8.8817842e-16 L11,-8.8817842e-16 L11,1.5 L2.5,1.5 L2.5,14.5 Z" />
  </IconPrimitive>
);

IconLogout.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconLogout.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconLogout.REFS = {
  ...REFS,
};

export default IconLogout;
