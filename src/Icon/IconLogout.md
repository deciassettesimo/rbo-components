```js
<div className="list">
  <IconLogout dimension={IconLogout.REFS.DIMENSIONS.XS} />
  <IconLogout dimension={IconLogout.REFS.DIMENSIONS.S} />
  <IconLogout dimension={IconLogout.REFS.DIMENSIONS.M} />
  <IconLogout dimension={IconLogout.REFS.DIMENSIONS.L} />
  <IconLogout dimension={IconLogout.REFS.DIMENSIONS.XL} />
</div>
```