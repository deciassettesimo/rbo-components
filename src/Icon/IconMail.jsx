import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconMailS = props => (
  <IconPrimitive {...props} type={TYPES.MAIL} viewBox={REFS.DIMENSIONS.S}>
    <path d="M4.53659074,5 L10,9.68292222 L15.4634093,5 L4.53659074,5 Z M17,6.31707778 L10,12.3170778 L3,6.31707778 L3,15 L17,15 L17,6.31707778 Z M19,3 L19,17 L1,17 L1,3 L19,3 Z" />
  </IconPrimitive>
);

const IconMailM = props => (
  <IconPrimitive {...props} type={TYPES.MAIL} viewBox={REFS.DIMENSIONS.M}>
    <path d="M1,4 L23,4 L23,20 L1,20 L1,4 Z M4.72051821,6 L12,12.1875595 L19.2794818,6 L4.72051821,6 Z M21,7.16244047 L12,14.8124405 L3,7.16244047 L3,18 L21,18 L21,7.16244047 Z" />
  </IconPrimitive>
);

const IconMail = props => {
  if (props.dimension <= REFS.DIMENSIONS.S) return <IconMailS {...props} />;
  return <IconMailM {...props} />;
};

IconMail.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconMail.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconMail.REFS = {
  ...REFS,
};

export default IconMail;
