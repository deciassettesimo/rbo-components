```js
<div className="list">
  <IconMail dimension={IconMail.REFS.DIMENSIONS.XS} />
  <IconMail dimension={IconMail.REFS.DIMENSIONS.S} />
  <IconMail dimension={IconMail.REFS.DIMENSIONS.M} />
  <IconMail dimension={IconMail.REFS.DIMENSIONS.L} />
  <IconMail dimension={IconMail.REFS.DIMENSIONS.XL} />
</div>
```