import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconNotification = props => (
  <IconPrimitive {...props} type={TYPES.NOTIFICATION} viewBox={REFS.DIMENSIONS.S}>
    <path d="M5,15 L15,15 L15,12.9375 L8,12.9375 L8,11.0625 L15,11.0625 L15,9 C15,6.243 12.757,4 10,4 C7.243,4 5,6.243 5,9 L5,15 Z M9,17 C9,17.552 9.449,18 10,18 C10.551,18 11,17.552 11,17 L9,17 Z M17,15 L19,15 L19,17 L17,17 L13,17 C13,18.657 11.657,20 10,20 C8.343,20 7,18.657 7,17 L3,17 L1,17 L1,15 L3,15 L3,9 C3,5.47352142 5.607701,2.55611659 9,2.0708885 L9,0 L11,0 L11,2.0708885 C14.392299,2.55611659 17,5.47352142 17,9 L17,15 Z" />
  </IconPrimitive>
);

IconNotification.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconNotification.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconNotification.REFS = {
  ...REFS,
};

export default IconNotification;
