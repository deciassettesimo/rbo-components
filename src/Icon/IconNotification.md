```js
<div className="list">
  <IconNotification dimension={IconNotification.REFS.DIMENSIONS.XS} />
  <IconNotification dimension={IconNotification.REFS.DIMENSIONS.S} />
  <IconNotification dimension={IconNotification.REFS.DIMENSIONS.M} />
  <IconNotification dimension={IconNotification.REFS.DIMENSIONS.L} />
  <IconNotification dimension={IconNotification.REFS.DIMENSIONS.XL} />
</div>
```