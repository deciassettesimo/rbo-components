import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconPercent = props => (
  <IconPrimitive {...props} type={TYPES.PERCENT} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M8,16 C3.581722,16 5.41083001e-16,12.418278 0,8 C-5.41083001e-16,3.581722 3.581722,8.11624501e-16 8,0 C12.418278,2.705415e-16 16,3.581722 16,8 C16,12.418278 12.418278,16 8,16 Z M8,14.5 C11.5898509,14.5 14.5,11.5898509 14.5,8 C14.5,4.41014913 11.5898509,1.5 8,1.5 C4.41014913,1.5 1.5,4.41014913 1.5,8 C1.5,11.5898509 4.41014913,14.5 8,14.5 Z M4.53,12.53 L3.47,11.47 L11.47,3.47 L12.53,4.53 L4.53,12.53 Z M5.5,7 C4.67157288,7 4,6.32842712 4,5.5 C4,4.67157288 4.67157288,4 5.5,4 C6.03589839,3.99999999 6.53108893,4.28589837 6.79903813,4.74999999 C7.06698733,5.21410161 7.06698733,5.78589839 6.79903813,6.25000001 C6.53108893,6.71410163 6.03589839,7.00000001 5.5,7 Z M10.5,12 C9.67157288,12 9,11.3284271 9,10.5 C9,9.67157288 9.67157288,9 10.5,9 C11.3284271,9 12,9.67157288 12,10.5 C12,11.3284271 11.3284271,12 10.5,12 Z" />
  </IconPrimitive>
);

IconPercent.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconPercent.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconPercent.REFS = {
  ...REFS,
};

export default IconPercent;
