```js
<div className="list">
  <IconPercent dimension={IconPercent.REFS.DIMENSIONS.XS} />
  <IconPercent dimension={IconPercent.REFS.DIMENSIONS.S} />
  <IconPercent dimension={IconPercent.REFS.DIMENSIONS.M} />
  <IconPercent dimension={IconPercent.REFS.DIMENSIONS.L} />
  <IconPercent dimension={IconPercent.REFS.DIMENSIONS.XL} />
</div>
```