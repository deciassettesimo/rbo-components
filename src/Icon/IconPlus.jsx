import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconPlus = props => (
  <IconPrimitive {...props} type={TYPES.PLUS} viewBox={REFS.DIMENSIONS.M}>
    <path d="M14,10 L20,10 L20,14 L14,14 L14,20 L10,20 L10,14 L4,14 L4,10 L10,10 L10,4 L14,4 L14,10 Z" />
  </IconPrimitive>
);

IconPlus.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconPlus.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconPlus.REFS = {
  ...REFS,
};

export default IconPlus;
