```js
<div className="list">
  <IconPlus dimension={IconPlus.REFS.DIMENSIONS.XS} />
  <IconPlus dimension={IconPlus.REFS.DIMENSIONS.S} />
  <IconPlus dimension={IconPlus.REFS.DIMENSIONS.M} />
  <IconPlus dimension={IconPlus.REFS.DIMENSIONS.L} />
  <IconPlus dimension={IconPlus.REFS.DIMENSIONS.XL} />
</div>
```