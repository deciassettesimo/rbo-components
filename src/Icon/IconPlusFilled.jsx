import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconPlusFilled = props => (
  <IconPrimitive {...props} type={TYPES.PLUS_FILLED} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M9,7 L9,4 L7,4 L7,7 L4,7 L4,9 L7,9 L7,12 L9,12 L9,9 L12,9 L12,7 L9,7 Z M8,15 C4.13400675,15 1,11.8659932 1,8 C1,4.13400675 4.13400675,1 8,1 C11.8659932,1 15,4.13400675 15,8 C15,11.8659932 11.8659932,15 8,15 Z" />
  </IconPrimitive>
);

IconPlusFilled.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconPlusFilled.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconPlusFilled.REFS = {
  ...REFS,
};

export default IconPlusFilled;
