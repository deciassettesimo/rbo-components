```js
<div className="list">
  <IconPlusFilled dimension={IconPlusFilled.REFS.DIMENSIONS.XS} />
  <IconPlusFilled dimension={IconPlusFilled.REFS.DIMENSIONS.S} />
  <IconPlusFilled dimension={IconPlusFilled.REFS.DIMENSIONS.M} />
  <IconPlusFilled dimension={IconPlusFilled.REFS.DIMENSIONS.L} />
  <IconPlusFilled dimension={IconPlusFilled.REFS.DIMENSIONS.XL} />
</div>
```