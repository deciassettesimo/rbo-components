import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconPrint = props => (
  <IconPrimitive {...props} type={TYPES.PRINT} viewBox={REFS.DIMENSIONS.M}>
    <path d="M19,7 L19,11 L5,11 L5,7 L3,7 L3,17 L5,17 L5,19 L1,19 L1,5 L5,5 L5,1 L19,1 L19,5 L23,5 L23,19 L19,19 L19,17 L21,17 L21,7 L19,7 Z M7,3 L7,9 L17,9 L17,3 L7,3 Z M5,15 L19,15 L19,23 L5,23 L5,15 Z M7,17 L7,21 L17,21 L17,17 L7,17 Z M16,12 L18,12 L18,14 L16,14 L16,12 Z" />
  </IconPrimitive>
);

IconPrint.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconPrint.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconPrint.REFS = {
  ...REFS,
};

export default IconPrint;
