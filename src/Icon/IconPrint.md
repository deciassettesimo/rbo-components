```js
<div className="list">
  <IconPrint dimension={IconPrint.REFS.DIMENSIONS.XS} />
  <IconPrint dimension={IconPrint.REFS.DIMENSIONS.S} />
  <IconPrint dimension={IconPrint.REFS.DIMENSIONS.M} />
  <IconPrint dimension={IconPrint.REFS.DIMENSIONS.L} />
  <IconPrint dimension={IconPrint.REFS.DIMENSIONS.XL} />
</div>
```