import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconQuestion = props => (
  <IconPrimitive {...props} type={TYPES.QUESTION} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M7.17,9.53 L7.17,9.529 C7.17,8.983 7.236,8.547 7.368,8.223 C7.502,7.898 7.745,7.579 8.098,7.265 C8.452,6.95 8.687,6.694 8.804,6.497 C8.921,6.3 8.98,6.092 8.98,5.873 C8.98,5.213 8.676,4.883 8.066,4.883 C7.777,4.883 7.546,4.972 7.372,5.149 C7.198,5.327 7.107,5.572 7.1,5.885 L5.4,5.885 C5.408,5.139 5.649,4.555 6.124,4.133 C6.598,3.71 7.246,3.5 8.066,3.5 C8.894,3.5 9.537,3.701 9.994,4.101 C10.451,4.501 10.679,5.067 10.679,5.797 C10.679,6.129 10.605,6.442 10.457,6.737 C10.308,7.032 10.048,7.359 9.677,7.719 L9.203,8.17 C8.906,8.455 8.736,8.789 8.693,9.172 L8.67,9.53 L7.17,9.53 Z M7,11.327 C6.99260207,11.0833154 7.08934122,10.8480137 7.266,10.68 C7.45051429,10.5072596 7.69642619,10.4154477 7.949,10.425 C8.20123181,10.4157173 8.44673298,10.5075102 8.631,10.68 C8.80802699,10.8478371 8.9051395,11.0831622 8.898,11.327 C8.90529219,11.5673276 8.81046076,11.7995011 8.637,11.966 C8.463,12.134 8.234,12.218 7.949,12.218 C7.664,12.218 7.434,12.134 7.26,11.966 C7.087,11.797 7,11.585 7,11.327 Z" />
  </IconPrimitive>
);

IconQuestion.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconQuestion.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconQuestion.REFS = {
  ...REFS,
};

export default IconQuestion;
