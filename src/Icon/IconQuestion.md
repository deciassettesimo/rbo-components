```js
<div className="list">
  <IconQuestion dimension={IconQuestion.REFS.DIMENSIONS.XS} />
  <IconQuestion dimension={IconQuestion.REFS.DIMENSIONS.S} />
  <IconQuestion dimension={IconQuestion.REFS.DIMENSIONS.M} />
  <IconQuestion dimension={IconQuestion.REFS.DIMENSIONS.L} />
  <IconQuestion dimension={IconQuestion.REFS.DIMENSIONS.XL} />
</div>
```