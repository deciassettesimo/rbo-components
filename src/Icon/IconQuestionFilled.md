```js
<div className="list">
  <IconQuestionFilled dimension={IconQuestionFilled.REFS.DIMENSIONS.XS} />
  <IconQuestionFilled dimension={IconQuestionFilled.REFS.DIMENSIONS.S} />
  <IconQuestionFilled dimension={IconQuestionFilled.REFS.DIMENSIONS.M} />
  <IconQuestionFilled dimension={IconQuestionFilled.REFS.DIMENSIONS.L} />
  <IconQuestionFilled dimension={IconQuestionFilled.REFS.DIMENSIONS.XL} />
</div>
```