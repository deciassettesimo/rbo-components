import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconQueue = props => (
  <IconPrimitive {...props} type={TYPES.QUEUE} viewBox={REFS.DIMENSIONS.S}>
    <path d="M2,10 L18,10 L18,12 L2,12 L2,10 Z M8,4 L18,4 L18,6 L8,6 L8,4 Z M2,16 L12,16 L12,18 L2,18 L2,16 Z M6,5 L1.969,8 L1.969,2 L6,5 Z" />
  </IconPrimitive>
);

IconQueue.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconQueue.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconQueue.REFS = {
  ...REFS,
};

export default IconQueue;
