```js
<div className="list">
  <IconQueue dimension={IconQueue.REFS.DIMENSIONS.XS} />
  <IconQueue dimension={IconQueue.REFS.DIMENSIONS.S} />
  <IconQueue dimension={IconQueue.REFS.DIMENSIONS.M} />
  <IconQueue dimension={IconQueue.REFS.DIMENSIONS.L} />
  <IconQueue dimension={IconQueue.REFS.DIMENSIONS.XL} />
</div>
```