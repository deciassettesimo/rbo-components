import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconRecall = props => (
  <IconPrimitive {...props} type={TYPES.RECALL} viewBox={REFS.DIMENSIONS.M}>
    <path d="M4.50057614,12 L7.5,12 L3.75,16.5 L0,12 L2.50046631,12 C2.55413964,6.24706242 7.23438002,1.5999999 13,1.5999999 C18.7989899,1.5999999 23.5,6.30101003 23.5,12.0999999 C23.5,17.8989898 18.7989899,22.5999999 13,22.5999999 L13,20.5999999 C17.6944204,20.5999999 21.5,16.7944203 21.5,12.0999999 C21.5,7.40557953 17.6944204,3.5999999 13,3.5999999 C8.33895821,3.5999999 4.55416232,7.35165454 4.50057614,12 Z M14,12 L19,12 L19,14 L12,14 L12,6 L14,6 L14,12 Z" />
  </IconPrimitive>
);

IconRecall.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconRecall.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconRecall.REFS = {
  ...REFS,
};

export default IconRecall;
