```js
<div className="list">
  <IconRecall dimension={IconRecall.REFS.DIMENSIONS.XS} />
  <IconRecall dimension={IconRecall.REFS.DIMENSIONS.S} />
  <IconRecall dimension={IconRecall.REFS.DIMENSIONS.M} />
  <IconRecall dimension={IconRecall.REFS.DIMENSIONS.L} />
  <IconRecall dimension={IconRecall.REFS.DIMENSIONS.XL} />
</div>
```