```js
<div className="list">
  <IconRefresh dimension={IconRefresh.REFS.DIMENSIONS.XS} />
  <IconRefresh dimension={IconRefresh.REFS.DIMENSIONS.S} />
  <IconRefresh dimension={IconRefresh.REFS.DIMENSIONS.M} />
  <IconRefresh dimension={IconRefresh.REFS.DIMENSIONS.L} />
  <IconRefresh dimension={IconRefresh.REFS.DIMENSIONS.XL} />
</div>
```