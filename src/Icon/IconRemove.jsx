import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconRemove = props => (
  <IconPrimitive {...props} type={TYPES.REMOVE} viewBox={REFS.DIMENSIONS.M}>
    <path d="M6,6 L6,21 L18,21 L18,6 L6,6 Z M13.7324356,4 C13.3866262,3.40219863 12.7402824,3 12,3 C11.2597176,3 10.6133738,3.40219863 10.2675644,4 L13.7324356,4 Z M4,4 L8.12601749,4 C8.57006028,2.27477279 10.1361606,1 12,1 C13.8638394,1 15.4299397,2.27477279 15.8739825,4 L20,4 L22.0249844,4 L22.0249844,6 L20,6 L20,23 L4,23 L4,6 L2,6 L2,4 L4,4 Z M13,8 L15,8 L15,19.045361 L13,19.045361 L13,8 Z M9,8 L11,8 L11,19.045361 L9,19.045361 L9,8 Z" />
  </IconPrimitive>
);

IconRemove.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconRemove.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconRemove.REFS = {
  ...REFS,
};

export default IconRemove;
