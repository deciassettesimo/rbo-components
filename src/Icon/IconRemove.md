```js
<div className="list">
  <IconRemove dimension={IconRemove.REFS.DIMENSIONS.XS} />
  <IconRemove dimension={IconRemove.REFS.DIMENSIONS.S} />
  <IconRemove dimension={IconRemove.REFS.DIMENSIONS.M} />
  <IconRemove dimension={IconRemove.REFS.DIMENSIONS.L} />
  <IconRemove dimension={IconRemove.REFS.DIMENSIONS.XL} />
</div>
```