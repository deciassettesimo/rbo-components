import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconRepeat = props => (
  <IconPrimitive {...props} type={TYPES.REPEAT} viewBox={REFS.DIMENSIONS.M}>
    <path d="M19.6862294,16.6862294 L17,14 L23,14 L23,20 L21.1336507,18.1336507 C19.1280279,21.1184098 15.7376355,23 12,23 C7.64983725,23 3.76293216,20.4512486 1.98818971,16.5626016 L3.80765576,15.7322142 C5.26012743,18.9147321 8.44020947,21 12,21 C15.1947402,21 18.0792287,19.3201819 19.6862294,16.6862294 Z M4.31396095,7.31396095 L7,10 L1,10 L1,4 L2.86654262,5.86654262 C4.87616693,2.87553098 8.26828711,1 12,1 C16.406912,1 20.3349966,3.61574883 22.0762605,7.58135226 L20.2450178,8.38543585 C18.8200406,5.14015259 15.6061615,3 12,3 C8.81055255,3 5.9248256,4.67385596 4.31396095,7.31396095 Z" />
  </IconPrimitive>
);

IconRepeat.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconRepeat.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconRepeat.REFS = {
  ...REFS,
};

export default IconRepeat;
