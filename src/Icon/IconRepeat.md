```js
<div className="list">
  <IconRepeat dimension={IconRepeat.REFS.DIMENSIONS.XS} />
  <IconRepeat dimension={IconRepeat.REFS.DIMENSIONS.S} />
  <IconRepeat dimension={IconRepeat.REFS.DIMENSIONS.M} />
  <IconRepeat dimension={IconRepeat.REFS.DIMENSIONS.L} />
  <IconRepeat dimension={IconRepeat.REFS.DIMENSIONS.XL} />
</div>
```