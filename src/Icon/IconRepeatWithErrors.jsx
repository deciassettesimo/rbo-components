import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconRepeatWithErrors = props => (
  <IconPrimitive {...props} type={TYPES.REPEAT} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M2.68831456,4.43831456 L5,6.75 L8.8817842e-16,6.75 L8.8817842e-16,1.75 L1.60338175,3.35338175 C2.99664602,1.29661198 5.31778352,0 7.878,0 C11.330938,0 14.3231498,2.35696306 15.2240392,5.69137982 L13.7759608,6.08262018 C13.0496859,3.3944951 10.6445864,1.5 7.878,1.5 C5.72929976,1.5 3.78681688,2.64279986 2.68831456,4.43831456 Z M13.158902,10.908902 L11,8.75 L16,8.75 L16,13.75 L14.2501333,12.0001333 C12.8726642,14.1359083 10.5042031,15.5 7.878,15.5 C4.82264782,15.5 2.10101518,13.6520396 0.897162435,10.8566527 L2.27483757,10.2633473 C3.24501572,12.5161339 5.4304188,14 7.878,14 C10.0921889,14 12.0822636,12.7863309 13.158902,10.908902 Z M9.06066017,7.75 L10.5303301,9.21966991 L9.46966991,10.2803301 L8,8.81066017 L6.53033009,10.2803301 L5.46966991,9.21966991 L6.93933983,7.75 L5.46966991,6.28033009 L6.53033009,5.21966991 L8,6.68933983 L9.46966991,5.21966991 L10.5303301,6.28033009 L9.06066017,7.75 Z" />
  </IconPrimitive>
);

IconRepeatWithErrors.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconRepeatWithErrors.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconRepeatWithErrors.REFS = {
  ...REFS,
};

export default IconRepeatWithErrors;
