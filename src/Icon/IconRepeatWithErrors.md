```js
<div className="list">
  <IconRepeatWithErrors dimension={IconRepeatWithErrors.REFS.DIMENSIONS.XS} />
  <IconRepeatWithErrors dimension={IconRepeatWithErrors.REFS.DIMENSIONS.S} />
  <IconRepeatWithErrors dimension={IconRepeatWithErrors.REFS.DIMENSIONS.M} />
  <IconRepeatWithErrors dimension={IconRepeatWithErrors.REFS.DIMENSIONS.L} />
  <IconRepeatWithErrors dimension={IconRepeatWithErrors.REFS.DIMENSIONS.XL} />
</div>
```