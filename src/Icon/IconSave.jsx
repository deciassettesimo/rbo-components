import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconSave = props => (
  <IconPrimitive {...props} type={TYPES.SAVE} viewBox={REFS.DIMENSIONS.M}>
    <path d="M6,4 L4,4 L4,20 L20,20 L20,6.82842712 L18,4.82842712 L18,11 L6,11 L6,4 Z M2,2 L18,2 L22,6 L22,22 L2,22 L2,2 Z M8,4 L8,9 L16,9 L16,4 L8,4 Z M6,15 L6,13 L18,13 L18,15 L6,15 Z M6,18 L6,16 L18,16 L18,18 L6,18 Z" />
  </IconPrimitive>
);

IconSave.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconSave.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconSave.REFS = {
  ...REFS,
};

export default IconSave;
