```js
<div className="list">
  <IconSave dimension={IconSave.REFS.DIMENSIONS.XS} />
  <IconSave dimension={IconSave.REFS.DIMENSIONS.S} />
  <IconSave dimension={IconSave.REFS.DIMENSIONS.M} />
  <IconSave dimension={IconSave.REFS.DIMENSIONS.L} />
  <IconSave dimension={IconSave.REFS.DIMENSIONS.XL} />
</div>
```