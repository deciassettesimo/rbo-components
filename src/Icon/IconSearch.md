```js
<div className="list">
  <IconSearch dimension={IconSearch.REFS.DIMENSIONS.XS} />
  <IconSearch dimension={IconSearch.REFS.DIMENSIONS.S} />
  <IconSearch dimension={IconSearch.REFS.DIMENSIONS.M} />
  <IconSearch dimension={IconSearch.REFS.DIMENSIONS.L} />
  <IconSearch dimension={IconSearch.REFS.DIMENSIONS.XL} />
</div>
```