```js
<div className="list">
  <IconSettings dimension={IconSettings.REFS.DIMENSIONS.XS} />
  <IconSettings dimension={IconSettings.REFS.DIMENSIONS.S} />
  <IconSettings dimension={IconSettings.REFS.DIMENSIONS.M} />
  <IconSettings dimension={IconSettings.REFS.DIMENSIONS.L} />
  <IconSettings dimension={IconSettings.REFS.DIMENSIONS.XL} />
</div>
```