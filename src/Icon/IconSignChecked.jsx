import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconSignChecked = props => (
  <IconPrimitive {...props} type={TYPES.SIGN_CHECKED} viewBox={REFS.DIMENSIONS.M}>
    <path d="M10.7677656,10.6635905 L3,18.3284271 L3,21 L4.17157288,21 L6.17157288,19 L8,19 L8,17.1715729 L12.653429,12.4162793 C13.3531724,12.7888602 14.151929,13 15,13 C17.7614237,13 20,10.7614237 20,8 C20,5.23857625 17.7614237,3 15,3 C12.2385763,3 10,5.23857625 10,8 C10,8.97908188 10.2814128,9.89243628 10.7677656,10.6635905 Z M13.2265974,14.7734026 L10,18 L10,21 L7,21 L5,23 L1,23 L1,17.5 L8.33970233,10.1602977 C8.11917103,9.47990619 8,8.75387004 8,8 C8,4.13400675 11.1340068,1 15,1 C18.8659932,1 22,4.13400675 22,8 C22,11.8659932 18.8659932,15 15,15 C14.3872924,15 13.7929709,14.9212802 13.2265974,14.7734026 Z M15,11 C13.3431458,11 12,9.65685425 12,8 C12,6.34314575 13.3431458,5 15,5 C16.6568542,5 18,6.34314575 18,8 C18,9.65685425 16.6568542,11 15,11 Z M15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 C14.4477153,7 14,7.44771525 14,8 C14,8.55228475 14.4477153,9 15,9 Z M22.2928932,15.2928932 L23.7071068,16.7071068 L17,23.4142136 L13.7928932,20.2071068 L15.2071068,18.7928932 L17,20.5857864 L22.2928932,15.2928932 Z" />
  </IconPrimitive>
);

IconSignChecked.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconSignChecked.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconSignChecked.REFS = {
  ...REFS,
};

export default IconSignChecked;
