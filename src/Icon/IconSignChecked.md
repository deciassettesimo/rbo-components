```js
<div className="list">
  <IconSignChecked dimension={IconSignChecked.REFS.DIMENSIONS.XS} />
  <IconSignChecked dimension={IconSignChecked.REFS.DIMENSIONS.S} />
  <IconSignChecked dimension={IconSignChecked.REFS.DIMENSIONS.M} />
  <IconSignChecked dimension={IconSignChecked.REFS.DIMENSIONS.L} />
  <IconSignChecked dimension={IconSignChecked.REFS.DIMENSIONS.XL} />
</div>
```