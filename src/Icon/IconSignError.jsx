import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconSignError = props => (
  <IconPrimitive {...props} type={TYPES.SIGN_ERROR} viewBox={REFS.DIMENSIONS.S}>
    <path d="M16.7071068,7.29289322 L18.9142136,9.5 L15.2071068,13.2071068 L13.7928932,11.7928932 L16.0857864,9.5 L15.2928932,8.70710678 L5,19 L1,19 L1,15 L15,1 L19,5 L16.7071068,7.29289322 Z M15.2928932,5.87867966 L16.1715729,5 L15,3.82842712 L3,15.8284271 L3,17 L4.17157288,17 L13.8786797,7.29289322 L13.2928932,6.70710678 L14.7071068,5.29289322 L15.2928932,5.87867966 Z M15,15.0857864 L16.7928932,13.2928932 L18.2071068,14.7071068 L16.4142136,16.5 L18.2071068,18.2928932 L16.7928932,19.7071068 L15,17.9142136 L13.2071068,19.7071068 L11.7928932,18.2928932 L13.5857864,16.5 L11.7928932,14.7071068 L13.2071068,13.2928932 L15,15.0857864 Z" />
  </IconPrimitive>
);

IconSignError.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconSignError.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconSignError.REFS = {
  ...REFS,
};

export default IconSignError;
