```js
<div className="list">
  <IconSignError dimension={IconSignError.REFS.DIMENSIONS.XS} />
  <IconSignError dimension={IconSignError.REFS.DIMENSIONS.S} />
  <IconSignError dimension={IconSignError.REFS.DIMENSIONS.M} />
  <IconSignError dimension={IconSignError.REFS.DIMENSIONS.L} />
  <IconSignError dimension={IconSignError.REFS.DIMENSIONS.XL} />
</div>
```