import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconSignPartlySigned = props => (
  <IconPrimitive {...props} type={TYPES.SIGN_PARTLY_SIGNED} viewBox={REFS.DIMENSIONS.S}>
    <path d="M15.2928932,8.70710678 L7,17 L9,17 L9,19 L5,19 L1,19 L1,15 L15,1 L19,5 L16.7071068,7.29289322 L18.9142136,9.5 L15.2071068,13.2071068 L13.7928932,11.7928932 L16.0857864,9.5 L15.2928932,8.70710678 Z M13.8786797,7.29289322 L13.2928932,6.70710678 L14.7071068,5.29289322 L15.2928932,5.87867966 L16.1715729,5 L15,3.82842712 L3,15.8284271 L3,17 L4,17 L4.17157288,17 L13.8786797,7.29289322 Z M11,17 L14,17 L14,19 L11,19 L11,17 Z M16,17 L19,17 L19,19 L16,19 L16,17 Z" />
  </IconPrimitive>
);

IconSignPartlySigned.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconSignPartlySigned.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconSignPartlySigned.REFS = {
  ...REFS,
};

export default IconSignPartlySigned;
