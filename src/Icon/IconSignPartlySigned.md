```js
<div className="list">
  <IconSignPartlySigned dimension={IconSignPartlySigned.REFS.DIMENSIONS.XS} />
  <IconSignPartlySigned dimension={IconSignPartlySigned.REFS.DIMENSIONS.S} />
  <IconSignPartlySigned dimension={IconSignPartlySigned.REFS.DIMENSIONS.M} />
  <IconSignPartlySigned dimension={IconSignPartlySigned.REFS.DIMENSIONS.L} />
  <IconSignPartlySigned dimension={IconSignPartlySigned.REFS.DIMENSIONS.XL} />
</div>
```