```js
<div className="list">
  <IconSignRemove dimension={IconLock.REFS.DIMENSIONS.XS} />
  <IconSignRemove dimension={IconLock.REFS.DIMENSIONS.S} />
  <IconSignRemove dimension={IconLock.REFS.DIMENSIONS.M} />
  <IconSignRemove dimension={IconLock.REFS.DIMENSIONS.L} />
  <IconSignRemove dimension={IconLock.REFS.DIMENSIONS.XL} />
</div>
```
