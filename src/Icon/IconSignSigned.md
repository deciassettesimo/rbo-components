```js
<div className="list">
  <IconSignSigned dimension={IconSignSigned.REFS.DIMENSIONS.XS} />
  <IconSignSigned dimension={IconSignSigned.REFS.DIMENSIONS.S} />
  <IconSignSigned dimension={IconSignSigned.REFS.DIMENSIONS.M} />
  <IconSignSigned dimension={IconSignSigned.REFS.DIMENSIONS.L} />
  <IconSignSigned dimension={IconSignSigned.REFS.DIMENSIONS.XL} />
</div>
```