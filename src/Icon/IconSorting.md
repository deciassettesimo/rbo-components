```js
<div className="list">
  <IconSorting dimension={IconSorting.REFS.DIMENSIONS.XS} />
  <IconSorting dimension={IconSorting.REFS.DIMENSIONS.S} />
  <IconSorting dimension={IconSorting.REFS.DIMENSIONS.M} />
  <IconSorting dimension={IconSorting.REFS.DIMENSIONS.L} />
  <IconSorting dimension={IconSorting.REFS.DIMENSIONS.XL} />
</div>
```