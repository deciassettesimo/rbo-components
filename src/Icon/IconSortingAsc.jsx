import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconSortingAsc = props => (
  <IconPrimitive {...props} type={TYPES.SORTING_ASC} viewBox={REFS.DIMENSIONS.XS}>
    <path
      d="M3.24142136,7 C3.21489971,7 3.18946432,6.98946432 3.17071068,6.97071068 C3.13165825,6.93165825 3.13165825,6.86834175 3.17071068,6.82928932 L7.92928932,2.07071068 C7.96834175,2.03165825 8.03165825,2.03165825 8.07071068,2.07071068 L12.8292893,6.82928932 C12.848043,6.84804296 12.8585786,6.87347835 12.8585786,6.9 C12.8585786,6.95522847 12.8138071,7 12.7585786,7 L3.24142136,7 Z"
      id="icon-sorting"
    />
  </IconPrimitive>
);

IconSortingAsc.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconSortingAsc.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconSortingAsc.REFS = {
  ...REFS,
};

export default IconSortingAsc;
