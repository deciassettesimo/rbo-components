```js
<div className="list">
  <IconSortingAsc dimension={IconSortingAsc.REFS.DIMENSIONS.XS} />
  <IconSortingAsc dimension={IconSortingAsc.REFS.DIMENSIONS.S} />
  <IconSortingAsc dimension={IconSortingAsc.REFS.DIMENSIONS.M} />
  <IconSortingAsc dimension={IconSortingAsc.REFS.DIMENSIONS.L} />
  <IconSortingAsc dimension={IconSortingAsc.REFS.DIMENSIONS.XL} />
</div>
```