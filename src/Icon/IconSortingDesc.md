```js
<div className="list">
  <IconSortingDesc dimension={IconSortingDesc.REFS.DIMENSIONS.XS} />
  <IconSortingDesc dimension={IconSortingDesc.REFS.DIMENSIONS.S} />
  <IconSortingDesc dimension={IconSortingDesc.REFS.DIMENSIONS.M} />
  <IconSortingDesc dimension={IconSortingDesc.REFS.DIMENSIONS.L} />
  <IconSortingDesc dimension={IconSortingDesc.REFS.DIMENSIONS.XL} />
</div>
```