import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconStar = props => (
  <IconPrimitive {...props} type={TYPES.STAR} viewBox={REFS.DIMENSIONS.XS}>
    <path d="M8,12.0519898 L3.05572809,15.5 L4.79999995,9.72199708 L6.21724894e-15,6.07498167 L6.02229121,5.9519897 L8,0.25 L9.97770879,5.9519897 L16,6.07498167 L11.2,9.72199708 L12.9442719,15.5 L8,12.0519898 Z M8,10.1633705 L10.163119,11.671875 L9.40000002,9.14399872 L11.5,7.54842948 L8.8652476,7.49462049 L8,5 L7.1347524,7.49462049 L4.5,7.54842948 L6.59999998,9.14399872 L5.83688104,11.671875 L8,10.1633705 Z" />
  </IconPrimitive>
);

IconStar.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconStar.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconStar.REFS = {
  ...REFS,
};

export default IconStar;
