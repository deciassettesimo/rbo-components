```js
<div className="list">
  <IconStar dimension={IconStar.REFS.DIMENSIONS.XS} />
  <IconStar dimension={IconStar.REFS.DIMENSIONS.S} />
  <IconStar dimension={IconStar.REFS.DIMENSIONS.M} />
  <IconStar dimension={IconStar.REFS.DIMENSIONS.L} />
  <IconStar dimension={IconStar.REFS.DIMENSIONS.XL} />
</div>
```