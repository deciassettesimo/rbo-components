import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconStarFilled = props => (
  <IconPrimitive {...props} type={TYPES.STAR_FILLED} viewBox={REFS.DIMENSIONS.XS}>
    <polygon points="8 12.0519898 3.05572809 15.5 4.79999995 9.72199708 5.88418203e-15 6.07498167 6.02229121 5.9519897 8 0.25 9.97770879 5.9519897 16 6.07498167 11.2 9.72199708 12.9442719 15.5" />
  </IconPrimitive>
);

IconStarFilled.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconStarFilled.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconStarFilled.REFS = {
  ...REFS,
};

export default IconStarFilled;
