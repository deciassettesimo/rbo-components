```js
<div className="list">
  <IconStarFilled dimension={IconStarFilled.REFS.DIMENSIONS.XS} />
  <IconStarFilled dimension={IconStarFilled.REFS.DIMENSIONS.S} />
  <IconStarFilled dimension={IconStarFilled.REFS.DIMENSIONS.M} />
  <IconStarFilled dimension={IconStarFilled.REFS.DIMENSIONS.L} />
  <IconStarFilled dimension={IconStarFilled.REFS.DIMENSIONS.XL} />
</div>
```