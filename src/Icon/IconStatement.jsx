import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconStatement = props => (
  <IconPrimitive {...props} type={TYPES.STATEMENT} viewBox={REFS.DIMENSIONS.M}>
    <path d="M3,1 L21,1 L21,23 L3,23 L3,1 Z M5,21 L19,21 L19,3 L5,3 L5,21 Z M7,7 L7,5 L17,5 L17,7 L7,7 Z M7,11 L7,9 L13,9 L13,11 L7,11 Z" />
  </IconPrimitive>
);

IconStatement.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconStatement.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconStatement.REFS = {
  ...REFS,
};

export default IconStatement;
