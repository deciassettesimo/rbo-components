```js
<div className="list">
  <IconStatement dimension={IconStatement.REFS.DIMENSIONS.XS} />
  <IconStatement dimension={IconStatement.REFS.DIMENSIONS.S} />
  <IconStatement dimension={IconStatement.REFS.DIMENSIONS.M} />
  <IconStatement dimension={IconStatement.REFS.DIMENSIONS.L} />
  <IconStatement dimension={IconStatement.REFS.DIMENSIONS.XL} />
</div>
```