import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconSupport = props => (
  <IconPrimitive {...props} type={TYPES.SUPPORT} viewBox={REFS.DIMENSIONS.M}>
    <path d="M9.94054253,10.1032719 L13.9268931,14.0896224 L16.8422762,13.1674146 L22.400651,16.9580943 C22.890855,17.3428061 22.9641136,17.9717188 22.6213967,18.4048049 L19.743479,22.0415844 C19.2873228,22.6180221 18.5517138,22.8977655 17.8277942,22.7700969 C14.4425836,22.1731039 10.9827824,20.1074036 7.44838831,16.5730095 C3.91332306,13.0379443 1.84983861,9.57982934 1.25793497,6.19866475 C1.13155574,5.47674153 1.41016486,4.74351621 1.98406489,4.28768335 L5.61652359,1.40252757 C5.63542775,1.38751252 5.65486948,1.37318693 5.67481044,1.35957907 C6.13099789,1.04827296 6.75317431,1.1657225 7.06448038,1.62190994 L10.8627504,7.18788881 L9.94054253,10.1032719 Z M6.01852081,3.6373409 L3.22797616,5.85379109 C3.7407812,8.78311658 5.5959809,11.892175 8.86260187,15.158796 C12.1290467,18.4252408 15.2405029,20.282959 18.1751101,20.8004985 L20.3868697,18.0055627 L16.5185831,15.3674829 L13.3717482,16.3629047 L7.6672603,10.6584168 L8.66257216,7.51192945 L6.01852081,3.6373409 Z" />
  </IconPrimitive>
);

IconSupport.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconSupport.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconSupport.REFS = {
  ...REFS,
};

export default IconSupport;
