```js
<div className="list">
  <IconSupport dimension={IconSupport.REFS.DIMENSIONS.XS} />
  <IconSupport dimension={IconSupport.REFS.DIMENSIONS.S} />
  <IconSupport dimension={IconSupport.REFS.DIMENSIONS.M} />
  <IconSupport dimension={IconSupport.REFS.DIMENSIONS.L} />
  <IconSupport dimension={IconSupport.REFS.DIMENSIONS.XL} />
</div>
```