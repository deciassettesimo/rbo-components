import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconSwap = props => (
  <IconPrimitive {...props} type={TYPES.SWAP} viewBox={REFS.DIMENSIONS.S}>
    <path d="M4.414,7 L7.707,10.293 L6.293,11.707 L0.586,6 L6.293,0.293 L7.707,1.707 L4.414,5 L12,5 L12,7 L4.414,7 Z M15.586,13 L12.293,9.707 L13.707,8.293 L19.414,14 L13.707,19.707 L12.293,18.293 L15.586,15 L8,15 L8,13 L15.586,13 Z" />
  </IconPrimitive>
);

IconSwap.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconSwap.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconSwap.REFS = {
  ...REFS,
};

export default IconSwap;
