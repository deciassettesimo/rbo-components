```js
<div className="list">
  <IconSwap dimension={IconSwap.REFS.DIMENSIONS.XS} />
  <IconSwap dimension={IconSwap.REFS.DIMENSIONS.S} />
  <IconSwap dimension={IconSwap.REFS.DIMENSIONS.M} />
  <IconSwap dimension={IconSwap.REFS.DIMENSIONS.L} />
  <IconSwap dimension={IconSwap.REFS.DIMENSIONS.XL} />
</div>
```