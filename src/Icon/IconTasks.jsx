import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconTasks = props => (
  <IconPrimitive {...props} type={TYPES.TASKS} viewBox={REFS.DIMENSIONS.M}>
    <path d="M2,2 L22,2 L22,22 L2,22 L2,2 Z M4,4 L4,20 L20,20 L20,4 L4,4 Z M13,4 L17,4 L17,11 L15,9 L13,11 L13,4 Z" />
  </IconPrimitive>
);

IconTasks.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconTasks.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconTasks.REFS = {
  ...REFS,
};

export default IconTasks;
