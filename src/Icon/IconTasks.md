```js
<div className="list">
  <IconTasks dimension={IconTasks.REFS.DIMENSIONS.XS} />
  <IconTasks dimension={IconTasks.REFS.DIMENSIONS.S} />
  <IconTasks dimension={IconTasks.REFS.DIMENSIONS.M} />
  <IconTasks dimension={IconTasks.REFS.DIMENSIONS.L} />
  <IconTasks dimension={IconTasks.REFS.DIMENSIONS.XL} />
</div>
```