import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconTuning = props => (
  <IconPrimitive {...props} type={TYPES.TUNING} viewBox={REFS.DIMENSIONS.M}>
    <path d="M14,18 L14,17 L18,17 L18,18 L22,18 L22,20 L18,20 L18,21 L14,21 L14,20 L2,20 L2,18 L14,18 Z M6,11 L6,10 L10,10 L10,11 L22,11 L22,13 L10,13 L10,14 L6,14 L6,13 L2,13 L2,11 L6,11 Z M14,4 L14,3 L18,3 L18,4 L22,4 L22,6 L18,6 L18,7 L14,7 L14,6 L2,6 L2,4 L14,4 Z" />
  </IconPrimitive>
);

IconTuning.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconTuning.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconTuning.REFS = {
  ...REFS,
};

export default IconTuning;
