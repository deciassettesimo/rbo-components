```js
<div className="list">
  <IconTuning dimension={IconTuning.REFS.DIMENSIONS.XS} />
  <IconTuning dimension={IconTuning.REFS.DIMENSIONS.S} />
  <IconTuning dimension={IconTuning.REFS.DIMENSIONS.M} />
  <IconTuning dimension={IconTuning.REFS.DIMENSIONS.L} />
  <IconTuning dimension={IconTuning.REFS.DIMENSIONS.XL} />
</div>
```