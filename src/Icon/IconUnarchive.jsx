import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconUnarchive = props => (
  <IconPrimitive {...props} type={TYPES.UNARCHIVE} viewBox={REFS.DIMENSIONS.M}>
    <path d="M13,9.39093388 L13,14 L11,14 L11,9.43696076 L9.20258388,11.2116009 L7.79741612,9.78839907 L12.0539951,5.58575732 L16.2116588,9.79747472 L14.7883412,11.2025253 L13,9.39093388 Z M8,15 L10,15 C10,16.1045695 10.8954305,17 12,17 C13.1045695,17 14,16.1045695 14,15 L16,15 L20,15 L20,4 L4,4 L4,15 L8,15 Z M8.53512878,17 L4,17 L4,20 L20,20 L20,17 L15.4648712,17 C14.7732524,18.1956027 13.4805647,19 12,19 C10.5194353,19 9.22674762,18.1956027 8.53512878,17 Z M2,2 L22,2 L22,22 L2,22 L2,2 Z" />
  </IconPrimitive>
);

IconUnarchive.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconUnarchive.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconUnarchive.REFS = {
  ...REFS,
};

export default IconUnarchive;
