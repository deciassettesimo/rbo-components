```js
<div className="list">
  <IconUnarchive dimension={IconUnarchive.REFS.DIMENSIONS.XS} />
  <IconUnarchive dimension={IconUnarchive.REFS.DIMENSIONS.S} />
  <IconUnarchive dimension={IconUnarchive.REFS.DIMENSIONS.M} />
  <IconUnarchive dimension={IconUnarchive.REFS.DIMENSIONS.L} />
  <IconUnarchive dimension={IconUnarchive.REFS.DIMENSIONS.XL} />
</div>
```