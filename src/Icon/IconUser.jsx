import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconUser = props => (
  <IconPrimitive {...props} type={TYPES.USER} viewBox={REFS.DIMENSIONS.S}>
    <path d="M18.430418,17.656422 L18.96958,19.0251691 L1.02226102,18.997743 L1.57189972,17.6276697 C2.92244805,14.2611851 6.27217631,12 10,12 C13.7508232,12 17.0946187,14.2652865 18.430418,17.656422 Z M10,11 C7.23857625,11 5,8.76142375 5,6 C5,3.23857625 7.23857625,1 10,1 C12.7614237,1 15,3.23857625 15,6 C15,8.76142375 12.7614237,11 10,11 Z M10,14 C7.63359967,14 5.46343864,15.165893 4.17213976,17.0025541 L15.8379965,17.0203812 C14.5558315,15.1693784 12.3860883,14 10,14 Z M10,9 C11.6568542,9 13,7.65685425 13,6 C13,4.34314575 11.6568542,3 10,3 C8.34314575,3 7,4.34314575 7,6 C7,7.65685425 8.34314575,9 10,9 Z" />
  </IconPrimitive>
);

IconUser.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconUser.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconUser.REFS = {
  ...REFS,
};

export default IconUser;
