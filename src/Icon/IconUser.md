```js
<div className="list">
  <IconUser dimension={IconUser.REFS.DIMENSIONS.XS} />
  <IconUser dimension={IconUser.REFS.DIMENSIONS.S} />
  <IconUser dimension={IconUser.REFS.DIMENSIONS.M} />
  <IconUser dimension={IconUser.REFS.DIMENSIONS.L} />
  <IconUser dimension={IconUser.REFS.DIMENSIONS.XL} />
</div>
```