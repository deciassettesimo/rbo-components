import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconViewCards = props => (
  <IconPrimitive {...props} type={TYPES.VIEW_CARDS} viewBox={REFS.DIMENSIONS.S}>
    <path d="M9,11 L9,19 L1,19 L1,11 L9,11 Z M19,11 L19,19 L11,19 L11,11 L19,11 Z M19,1 L19,9 L11,9 L11,1 L19,1 Z M9,1 L9,9 L1,9 L1,1 L9,1 Z M13,13 L13,17 L17,17 L17,13 L13,13 Z M3,13 L3,17 L7,17 L7,13 L3,13 Z M13,3 L13,7 L17,7 L17,3 L13,3 Z M3,3 L3,7 L7,7 L7,3 L3,3 Z" />
  </IconPrimitive>
);

IconViewCards.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconViewCards.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconViewCards.REFS = {
  ...REFS,
};

export default IconViewCards;
