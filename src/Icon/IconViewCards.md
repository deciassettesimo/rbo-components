```js
<div className="list">
  <IconViewCards dimension={IconViewCards.REFS.DIMENSIONS.XS} />
  <IconViewCards dimension={IconViewCards.REFS.DIMENSIONS.S} />
  <IconViewCards dimension={IconViewCards.REFS.DIMENSIONS.M} />
  <IconViewCards dimension={IconViewCards.REFS.DIMENSIONS.L} />
  <IconViewCards dimension={IconViewCards.REFS.DIMENSIONS.XL} />
</div>
```