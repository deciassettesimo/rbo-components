import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconViewList = props => (
  <IconPrimitive {...props} type={TYPES.VIEW_LIST} viewBox={REFS.DIMENSIONS.S}>
    <path d="M1,5 L1,3 L19,3 L19,5 L1,5 Z M1,11 L1,9 L19,9 L19,11 L1,11 Z M1,17 L1,15 L19,15 L19,17 L1,17 Z" />
  </IconPrimitive>
);

IconViewList.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconViewList.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconViewList.REFS = {
  ...REFS,
};

export default IconViewList;
