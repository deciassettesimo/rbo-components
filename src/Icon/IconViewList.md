```js
<div className="list">
  <IconViewList dimension={IconViewList.REFS.DIMENSIONS.XS} />
  <IconViewList dimension={IconViewList.REFS.DIMENSIONS.S} />
  <IconViewList dimension={IconViewList.REFS.DIMENSIONS.M} />
  <IconViewList dimension={IconViewList.REFS.DIMENSIONS.L} />
  <IconViewList dimension={IconViewList.REFS.DIMENSIONS.XL} />
</div>
```