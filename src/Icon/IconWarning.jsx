import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconWarning = props => (
  <IconPrimitive {...props} type={TYPES.WARNING} viewBox={REFS.DIMENSIONS.S}>
    <path d="M0,18.0590532 L10,1 L20,18.0590532 L0,18.0590532 Z M16.5934602,16.1068814 L10,4.85944363 L3.40653978,16.1068814 L16.5934602,16.1068814 Z M9.0239141,8.29819424 L10.9760859,8.29819424 L10.9760859,12.2025378 L9.0239141,12.2025378 L9.0239141,8.29819424 Z M9.0239141,13.1786237 L10.9760859,13.1786237 L10.9760859,15.1307955 L9.0239141,15.1307955 L9.0239141,13.1786237 Z" />
  </IconPrimitive>
);

IconWarning.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconWarning.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconWarning.REFS = {
  ...REFS,
};

export default IconWarning;
