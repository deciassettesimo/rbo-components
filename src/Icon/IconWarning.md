```js
<div className="list">
  <IconWarning dimension={IconWarning.REFS.DIMENSIONS.XS} />
  <IconWarning dimension={IconWarning.REFS.DIMENSIONS.S} />
  <IconWarning dimension={IconWarning.REFS.DIMENSIONS.M} />
  <IconWarning dimension={IconWarning.REFS.DIMENSIONS.L} />
  <IconWarning dimension={IconWarning.REFS.DIMENSIONS.XL} />
</div>
```