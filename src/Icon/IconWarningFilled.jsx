import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import IconPrimitive from './_internal/IconPrimitive';

const IconWarningFilled = props => (
  <IconPrimitive {...props} type={TYPES.WARNING_FILLED} viewBox={REFS.DIMENSIONS.S}>
    <path d="M10,1 L19,17.875 L1,17.875 L10,1 Z M10,16.75 C10.6213203,16.75 11.125,16.2463203 11.125,15.625 C11.125,15.0036797 10.6213203,14.5 10,14.5 C9.37867966,14.5 8.875,15.0036797 8.875,15.625 C8.875,16.2463203 9.37867966,16.75 10,16.75 Z M9.15625,7.1875 L9.15625,13.375 L10.84375,13.375 L10.84375,7.1875 L9.15625,7.1875 Z" />
  </IconPrimitive>
);

IconWarningFilled.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for svg fill (e.g. '#3C3C3C') */
  color: PropTypes.string,
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

IconWarningFilled.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

IconWarningFilled.REFS = {
  ...REFS,
};

export default IconWarningFilled;
