```js
<div className="list">
  <IconWarningFilled dimension={IconWarningFilled.REFS.DIMENSIONS.XS} />
  <IconWarningFilled dimension={IconWarningFilled.REFS.DIMENSIONS.S} />
  <IconWarningFilled dimension={IconWarningFilled.REFS.DIMENSIONS.M} />
  <IconWarningFilled dimension={IconWarningFilled.REFS.DIMENSIONS.L} />
  <IconWarningFilled dimension={IconWarningFilled.REFS.DIMENSIONS.XL} />
</div>
```