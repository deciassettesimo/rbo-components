### REFS

```js static
{
  DISPLAY: {
    BLOCK: 'block',
    INLINE_BLOCK: 'inline-block',
  },
  DIMENSIONS: {
    XXS: 12,
    XS: 16,
    S: 20,
    M: 24,
    L: 32,
    XL: 40,
    XXL: 48,
  },
  COLORS: STYLES.COLORS,
}
```

### Icon SET

```js
<div className="list">
  <div className="inline"><IconAccountChecked title="IconAccountChecked" /></div>
  <div className="inline"><IconArchive title="IconArchive" /></div>
  <div className="inline"><IconArrowDown title="IconArrowDown" /></div>
  <div className="inline"><IconArrowDownFilled title="IconArrowDownFilled" /></div>
  <div className="inline"><IconArrowDownTailed title="IconArrowDownTailed" /></div>
  <div className="inline"><IconArrowLeft title="IconArrowLeft" /></div>
  <div className="inline"><IconArrowLeftDouble title="IconArrowLeftDouble" /></div>
  <div className="inline"><IconArrowLeftFilled title="IconArrowLeftFilled" /></div>
  <div className="inline"><IconArrowLeftTailed title="IconArrowLeftTailed" /></div>
  <div className="inline"><IconArrowRight title="IconArrowRight" /></div>
  <div className="inline"><IconArrowRightDouble title="IconArrowRightDouble" /></div>
  <div className="inline"><IconArrowRightFilled title="IconArrowRightFilled" /></div>
  <div className="inline"><IconArrowUp title="IconArrowUp" /></div>
  <div className="inline"><IconArrowUpFilled title="IconArrowUpFilled" /></div>
  <div className="inline"><IconBlock title="IconBlock" /></div>
  <div className="inline"><IconBlockFilled title="IconBlockFilled" /></div>
  <div className="inline"><IconBurger title="IconBurger" /></div>
  <div className="inline"><IconCalendar title="IconCalendar" /></div>
  <div className="inline"><IconCards title="IconCards" /></div>
  <div className="inline"><IconCase title="IconCase" /></div>
  <div className="inline"><IconCaseBack title="IconCaseBack" /></div>
  <div className="inline"><IconChart title="IconChart" /></div>
  <div className="inline"><IconChat title="IconChat" /></div>
  <div className="inline"><IconChecked title="IconChecked" /></div>
  <div className="inline"><IconCheckedCheckbox title="IconCheckedCheckbox" /></div>
  <div className="inline"><IconCheckedRadio title="IconCheckedRadio" /></div>
  <div className="inline"><IconCircle title="IconCircle" /></div>
  <div className="inline"><IconCircleChecked title="IconCircleChecked" /></div>
  <div className="inline"><IconCircleCheckedDashed title="IconCircleCheckedDashed" /></div>
  <div className="inline"><IconCircleCheckedFilled title="IconCircleCheckedFilled" /></div>
  <div className="inline"><IconClip title="IconClip" /></div>
  <div className="inline"><IconClock title="IconClock" /></div>
  <div className="inline"><IconClose title="IconClose" /></div>
  <div className="inline"><IconContextHorizontal title="IconContextHorizontal" /></div>
  <div className="inline"><IconContextHorizontalRound title="IconContextHorizontalRound" /></div>
  <div className="inline"><IconContextVertical title="IconContextVertical" /></div>
  <div className="inline"><IconContextVerticalRound title="IconContextVerticalRound" /></div>
  <div className="inline"><IconCopy title="IconCopy" /></div>
  <div className="inline"><IconCurrencyEur title="IconCurrencyEur" /></div>
  <div className="inline"><IconCurrencyRur title="IconCurrencyRur" /></div>
  <div className="inline"><IconDictionary title="IconDictionary" /></div>
  <div className="inline"><IconDictionaryAdd title="IconDictionaryAdd" /></div>
  <div className="inline"><IconDocument title="IconDocument" /></div>
  <div className="inline"><IconDocumentCopy title="IconDocumentCopy" /></div>
  <div className="inline"><IconDocumentCreateFromTemplate title="IconDocumentCreateFromTemplate" /></div>
  <div className="inline"><IconDocumentEdit title="IconDocumentEdit" /></div>
  <div className="inline"><IconDocumentHistory title="IconDocumentHistory" /></div>
  <div className="inline"><IconDocumentReissue title="IconDocumentReissue" /></div>
  <div className="inline"><IconDocuments title="IconDocuments" /></div>
  <div className="inline"><IconDocumentSaveAsTemplate title="IconDocumentSaveAsTemplate" /></div>
  <div className="inline"><IconDocumentSend title="IconDocumentSend" /></div>
  <div className="inline"><IconDocumentSign title="IconDocumentSign" /></div>
  <div className="inline"><IconDocumentSignAndSend title="IconDocumentSignAndSend" /></div>
  <div className="inline"><IconDocumentSignVisa title="IconDocumentSignVisa" /></div>
  <div className="inline"><IconDownload title="IconDownload" /></div>
  <div className="inline"><IconDraggable title="IconDraggable" /></div>
  <div className="inline"><IconEdit title="IconEdit" /></div>
  <div className="inline"><IconErrorFilled title="IconErrorFilled" /></div>
  <div className="inline"><IconExcel title="IconExcel" /></div>
  <div className="inline"><IconExport title="IconExport" /></div>
  <div className="inline"><IconFilter title="IconFilter" /></div>
  <div className="inline"><IconFlag title="IconFlag" /></div>
  <div className="inline"><IconHome title="IconHome" /></div>
  <div className="inline"><IconImport title="IconImport" /></div>
  <div className="inline"><IconInfo title="IconInfo" /></div>
  <div className="inline"><IconLock title="IconLock" /></div>
  <div className="inline"><IconLogout title="IconLogout" /></div>
  <div className="inline"><IconMail title="IconMail" /></div>
  <div className="inline"><IconNotification title="IconNotification" /></div>
  <div className="inline"><IconPercent title="IconPercent" /></div>
  <div className="inline"><IconPlus title="IconPlus" /></div>
  <div className="inline"><IconPlusFilled title="IconPlusFilled" /></div>
  <div className="inline"><IconPrint title="IconPrint" /></div>
  <div className="inline"><IconQuestion title="IconQuestion" /></div>
  <div className="inline"><IconQuestionFilled title="IconQuestionFilled" /></div>
  <div className="inline"><IconQueue title="IconQueue" /></div>
  <div className="inline"><IconRecall title="IconRecall" /></div>
  <div className="inline"><IconRefresh title="IconRefresh" /></div>
  <div className="inline"><IconRemove title="IconRemove" /></div>
  <div className="inline"><IconRepeat title="IconRepeat" /></div>
  <div className="inline"><IconRepeatWithErrors title="IconRepeatWithErrors" /></div>
  <div className="inline"><IconSave title="IconSave" /></div>
  <div className="inline"><IconSearch title="IconSearch" /></div>
  <div className="inline"><IconSettings title="IconSettings" /></div>
  <div className="inline"><IconSignChecked title="IconSignChecked" /></div>
  <div className="inline"><IconSignError title="IconSignError" /></div>
  <div className="inline"><IconSignPartlySigned title="IconSignPartlySigned" /></div>
  <div className="inline"><IconSignRemove title="IconSignRemove" /></div>
  <div className="inline"><IconSignSigned title="IconSignSigned" /></div>
  <div className="inline"><IconSorting title="IconSorting" /></div>
  <div className="inline"><IconSortingAsc title="IconSortingAsc" /></div>
  <div className="inline"><IconSortingDesc title="IconSortingDesc" /></div>
  <div className="inline"><IconStar title="IconStar" /></div>
  <div className="inline"><IconStarFilled title="IconStarFilled" /></div>
  <div className="inline"><IconStatement title="IconStatement" /></div>
  <div className="inline"><IconSupport title="IconSupport" /></div>
  <div className="inline"><IconSwap title="IconSwap" /></div>
  <div className="inline"><IconTasks title="IconTasks" /></div>
  <div className="inline"><IconTuning title="IconTuning" /></div>
  <div className="inline"><IconUnarchive title="IconUnarchive" /></div>
  <div className="inline"><IconUser title="IconUser" /></div>
  <div className="inline"><IconViewCards title="IconViewCards" /></div>
  <div className="inline"><IconViewList title="IconViewList" /></div>
  <div className="inline"><IconWarning title="IconWarning" /></div>
  <div className="inline"><IconWarningFilled title="IconWarningFilled" /></div>
</div>
```