import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import Icon from '../Icon';

describe('Icons', () => {
  const typeKeys = Object.keys(Icon.REFS.TYPES);

  // loop by Icon.REFS.TYPES (all icons)
  for (let i = 0; i < typeKeys.length; i += 1) {
    const iconType = Icon.REFS.TYPES[typeKeys[i]];

    describe(`${typeKeys[i]} (${iconType})`, () => {
      const dimensions = Object.values(Icon.REFS.DIMENSIONS);

      // loop by dimensions of icon
      for (let j = 0; j < dimensions.length; j += 1) {
        const iconDimension = dimensions[j];

        it(`dimensions ${iconDimension}`, () => {
          const wrapper = renderer.create(<Icon type={iconType} dimension={iconDimension} title={iconType} />).toJSON();
          // expect with snapshot
          expect(wrapper).toMatchSnapshot();
        });
      }
    });
  }
});
