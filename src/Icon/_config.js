import { DISPLAY, DIMENSIONS, COLORS } from './_constants';

const REFS = {
  DISPLAY,
  DIMENSIONS,
  COLORS,
};

export default REFS;
