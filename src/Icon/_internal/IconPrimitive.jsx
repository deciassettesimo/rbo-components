import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import REFS from '../_config';
import { COMPONENT, TYPES } from '../_constants';
import { StyledIcon, StyledIconSvg } from '../_style';

const IconPrimitive = props => (
  <StyledIcon
    {...addDataAttributes({ component: COMPONENT, type: props.type })}
    title={props.title}
    sDisplay={props.display}
    sDimension={props.dimension}
  >
    <StyledIconSvg viewBox={`0 0 ${props.viewBox} ${props.viewBox}`} focusable="false" sColor={props.color}>
      {props.children}
    </StyledIconSvg>
  </StyledIcon>
);

IconPrimitive.propTypes = {
  type: PropTypes.oneOf(Object.values(TYPES)),
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  color: PropTypes.string,
  title: PropTypes.string,
  viewBox: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)).isRequired,
  children: PropTypes.node.isRequired,
};

IconPrimitive.defaultProps = {
  type: null,
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.GRAYISH_BROWN,
  title: null,
};

export default IconPrimitive;
