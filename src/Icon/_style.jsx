import styled from 'styled-components';

export const StyledIcon = styled.div`
  position: relative;
  box-sizing: border-box;
  vertical-align: middle;
  display: ${props => props.sDisplay};
  height: ${props => props.sDimension}px;
  width: ${props => props.sDimension}px;
`;

export const StyledIconSvg = styled.svg`
  position: relative;
  display: block;
  width: 100%;
  height: 100%;
  fill: ${props => props.sColor};
`;
