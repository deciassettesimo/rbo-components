### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputAccount Dimensions

```js
<div className="list">
  <div>
    <InputAccount
      id="InputAccountXS"
      dimension={InputAccount.REFS.DIMENSIONS.XS}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputAccount
      id="InputAccountS"
      dimension={InputAccount.REFS.DIMENSIONS.S}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputAccount
      id="InputAccountM"
      dimension={InputAccount.REFS.DIMENSIONS.M}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputAccount
      id="InputAccountL"
      dimension={InputAccount.REFS.DIMENSIONS.L}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputAccount
      id="InputAccountXL"
      dimension={InputAccount.REFS.DIMENSIONS.XL}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
</div>
```

### InputAccount with placeholder

```js
<div className="list">
  <div>
    <InputAccount id="InputAccountWithPlaceholder" placeholder="00000.000.0.00000000000" />
  </div>
</div>
```

### InputAccount with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputAccount id="InputAccountWithWarning" value="40702810800002409387" isWarning />
  </div>
  <div>
    <InputAccount id="InputAccountWithError" value="40702810800002409387" isError />
  </div>
</div>
```

### InputAccount disabled

```js
<div className="list">
  <div>
    <InputAccount id="InputAccountDisabled" disabled />
  </div>
  <div>
    <InputAccount id="InputAccountDisabledWithPlaceholder" placeholder="00000.000.0.00000000000" disabled />
  </div>
  <div>
    <InputAccount id="InputAccountDisabledWithValue" value="40702810800002409387" disabled />
  </div>
  <div>
    <InputAccount id="InputAccountDisabledWithWarning" value="40702810800002409387" disabled isWarning />
  </div>
  <div>
    <InputAccount id="InputAccountDisabledWithError" value="40702810800002409387" disabled isError />
  </div>
</div>
```
