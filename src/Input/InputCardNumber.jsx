import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { TYPES } from './_constants';
import { INPUT_REFS as REFS } from './_config';
import { getChar, getCaretPosition, formatCardNumberValue, deformatDigitalValue } from './_utils';
import { StyledInputText } from './_style';

export default class InputCardNumber extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.PHONE;

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component value (e.g '+79281234567') */
    value: PropTypes.string,
    /** Placeholder */
    placeholder: PropTypes.string,
    /** Max-length */
    maxLength: PropTypes.number,
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Align text */
    textAlign: PropTypes.oneOf(Object.values(REFS.TEXT_ALIGN)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onBlur: PropTypes.func,
    /** Change handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onChange: PropTypes.func,
    /** @ignore */
    isFocused: PropTypes.bool,
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    placeholder: null,
    maxLength: 16,
    dimension: REFS.DIMENSIONS.M,
    textAlign: REFS.TEXT_ALIGN.LEFT,
    width: '100%',
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    isFocused: null,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: this.props.isFocused,
      value: this.props.value,
      formattedValue: '',
      caretPosition: null,
      maxLength: this.props.maxLength + 3,
    };

    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    const propsValue = props.value || null;
    if (
      !state.isInitialized ||
      propsValue !== state.value ||
      (props.isFocused !== null && props.isFocused !== state.isFocused)
    ) {
      const formattedValue = formatCardNumberValue(props.value, props.maxLength);
      const value = deformatDigitalValue(formattedValue);
      return {
        isInitialized: true,
        value,
        formattedValue,
        isFocused: props.isFocused !== null ? props.isFocused : state.isFocused,
      };
    }
    return null;
  }

  componentDidMount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  componentDidUpdate() {
    if (this.props.disabled && this.state.isFocused) this.handleBlur();

    if (this.state.caretPosition !== null) {
      this.inputNode.current.setSelectionRange(this.state.caretPosition, this.state.caretPosition);
    }
  }

  componentWillUnmount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  handleFocus = () => {
    if (this.props.isFocused === null) this.setState({ isFocused: true });

    this.setState({ caretPosition: null });

    this.props.onFocus({ id: this.props.id, value: this.state.value, formattedValue: this.state.formattedValue });
  };

  handleBlur = () => {
    this.setState({ isFocused: false, caretPosition: null });

    this.props.onBlur({ id: this.props.id, value: this.state.value, formattedValue: this.state.formattedValue });
  };

  handleChange = e => {
    const { value } = e.target;
    const stateValue = this.state.value;
    const beforeCaretPosValue = value.substring(0, e.target.selectionEnd).replace(/\D/g, '');
    const formattedValue = formatCardNumberValue(value, this.props.maxLength);
    const deformattedValue = deformatDigitalValue(formattedValue);
    const caretPosition = getCaretPosition(formattedValue, beforeCaretPosValue, 1);

    this.setState({
      value: deformattedValue,
      formattedValue,
      caretPosition,
    });

    if (deformattedValue !== stateValue) {
      this.props.onChange({ id: this.props.id, value: deformattedValue, formattedValue });
    }
  };

  handleKeyDown = e => {
    this.setState({ caretPosition: null });
    if ((e.ctrlKey || e.metaKey) && e.keyCode === 90) e.preventDefault(); // disable ctrl-z
  };

  handleKeyPress = e => {
    const char = getChar(e);
    if (!e.ctrlKey && !e.altKey && !e.metaKey && char !== null && (char < '0' || char > '9')) e.preventDefault();
  };

  render() {
    return (
      <StyledInputText
        {...addDataAttributes({ component: TYPES.PHONE })}
        innerRef={ref => {
          this.inputNode.current = ref;
        }}
        sDimension={this.props.dimension}
        sTextAlign={this.props.textAlign}
        sWidth={this.props.width}
        isDisabled={this.props.disabled}
        isFocused={this.state.isFocused}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
        id={this.props.id}
        value={this.state.formattedValue}
        placeholder={this.props.placeholder}
        maxLength={this.state.maxLength}
        disabled={this.props.disabled}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onKeyDown={this.handleKeyDown}
        onKeyPress={this.handleKeyPress}
      />
    );
  }
}
