### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputCardNumber Dimensions

```js
<div className="list">
  <div>
    <InputCardNumber
      id="InputCardNumberXS"
      dimension={InputCardNumber.REFS.DIMENSIONS.XS}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputCardNumber
      id="InputCardNumberS"
      dimension={InputCardNumber.REFS.DIMENSIONS.S}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputCardNumber
      id="InputCardNumberM"
      dimension={InputCardNumber.REFS.DIMENSIONS.M}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputCardNumber
      id="InputCardNumberL"
      dimension={InputCardNumber.REFS.DIMENSIONS.L}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputCardNumber
      id="InputCardNumberXL"
      dimension={InputCardNumber.REFS.DIMENSIONS.XL}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
</div>
```

### InputCardNumber with different maxLengths (16, 18, 19)

```js
<div className="list">
  <div>
    <InputCardNumber id="InputCardNumber16" maxLength={16} value="1234567890123456" />
  </div>
  <div>
    <InputCardNumber id="InputCardNumber18" maxLength={18} value="123456789012345678" />
  </div>
  <div>
    <InputCardNumber id="InputCardNumber19" maxLength={19} value="1234567890123456789" />
  </div>
</div>
```

### InputCardNumber with placeholder

```js
<div className="list">
  <div>
    <InputCardNumber id="InputCardNumberWithPlaceholder" placeholder="1234 5678 1234 5678" />
  </div>
</div>
```

### InputCardNumber with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputCardNumber id="InputCardNumberWithWarning" value="1234567812345678" isWarning />
  </div>
  <div>
    <InputCardNumber id="InputCardNumberWithError" value="1234567812345678" isError />
  </div>
</div>
```

### InputCardNumber disabled

```js
<div className="list">
  <div>
    <InputCardNumber id="InputCardNumberDisabled" disabled />
  </div>
  <div>
    <InputCardNumber id="InputCardNumberDisabledWithPlaceholder" placeholder="1234 5678 1234 567890" maxLength={18} disabled />
  </div>
  <div>
    <InputCardNumber id="InputCardNumberDisabledWithValue" value="1234567812345678" disabled />
  </div>
  <div>
    <InputCardNumber id="InputCardNumberDisabledWithWarning" value="1234567812345678" disabled isWarning />
  </div>
  <div>
    <InputCardNumber id="InputCardNumberDisabledWithError" value="1234567812345678" disabled isError />
  </div>
</div>
```
