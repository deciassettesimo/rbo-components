import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import IconCheckedCheckbox from '../Icon/IconCheckedCheckbox';

import { TYPES } from './_constants';
import { INPUT_REFS as REFS } from './_config';

import {
  StyledInputSwitch,
  StyledInputCheckboxHtmlInput,
  StyledInputCheckboxIcon,
  StyledInputSwitchTitle,
} from './_style';

export default class InputCheckbox extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.CHECKBOX;

  static propTypes = {
    /** id of form input element */
    id: PropTypes.string.isRequired,
    /** checked flag of form input element */
    checked: PropTypes.bool,
    /** title for checkbox */
    children: PropTypes.node,
    /** dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** warning flag */
    isWarning: PropTypes.bool,
    /** error flag */
    isError: PropTypes.bool,
    /** disabled flag of form input element */
    disabled: PropTypes.bool,
    /** focus handler, params { id, value } */
    onFocus: PropTypes.func,
    /** blur handler, params { id, value } */
    onBlur: PropTypes.func,
    /** change handler, params { id, value } */
    onChange: PropTypes.func,
    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    checked: false,
    children: null,
    dimension: REFS.DIMENSIONS.M,
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      disallowBlurFlag: false,
      checked: this.props.checked,
    };

    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.isInitialized || props.checked !== state.checked) {
      return { isInitialized: true, checked: props.checked };
    }
    return null;
  }

  componentDidMount() {
    document.addEventListener('mouseup', this.allowBlur);

    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  componentDidUpdate() {
    if (this.props.disabled && this.state.isFocused) this.handleBlur();
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.allowBlur);

    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  disallowBlur = () => {
    this.setState({ disallowBlurFlag: true });
  };

  allowBlur = () => {
    if (this.state.disallowBlurFlag) {
      this.inputNode.current.focus();
      this.setState({ disallowBlurFlag: false });
    }
  };

  handleFocus = () => {
    if (!this.state.isFocused) {
      this.setState({ isFocused: true });
      this.props.onFocus({ id: this.props.id, value: this.state.checked });
    }
  };

  handleBlur = () => {
    if (!this.state.disallowBlurFlag) {
      this.setState({ isFocused: false });
      this.props.onBlur({ id: this.props.id, value: this.state.checked });
    }
  };

  handleChange = e => {
    const { checked } = e.target;
    const stateChecked = this.state.checked;
    this.setState({ checked });

    if (checked !== stateChecked) {
      this.props.onChange({ id: this.props.id, value: checked });
    }
  };

  render() {
    return (
      <StyledInputSwitch
        {...addDataAttributes({ component: TYPES.CHECKBOX })}
        sDimension={this.props.dimension}
        isDisabled={this.props.disabled}
        onMouseDown={this.disallowBlur}
        onMouseUp={this.allowBlur}
      >
        <StyledInputCheckboxHtmlInput
          innerRef={ref => {
            this.inputNode.current = ref;
          }}
          sDimension={this.props.dimension}
          id={this.props.id}
          checked={this.state.checked}
          disabled={this.props.disabled}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          onChange={this.handleChange}
        />
        <StyledInputCheckboxIcon
          sDimension={this.props.dimension}
          isChecked={this.state.checked}
          isDisabled={this.props.disabled}
          isFocused={this.state.isFocused}
          isWarning={this.props.isWarning}
          isError={this.props.isError}
        >
          {this.state.checked && (
            <IconCheckedCheckbox
              color={
                this.props.disabled
                  ? IconCheckedCheckbox.REFS.COLORS.BLACK_48
                  : IconCheckedCheckbox.REFS.COLORS.CORPORATE_YELLOW
              }
              dimension={
                this.props.dimension === REFS.DIMENSIONS.XS
                  ? IconCheckedCheckbox.REFS.DIMENSIONS.XS
                  : IconCheckedCheckbox.REFS.DIMENSIONS.S
              }
              display={IconCheckedCheckbox.REFS.DISPLAY.BLOCK}
            />
          )}
        </StyledInputCheckboxIcon>
        {this.props.children && (
          <StyledInputSwitchTitle sDimension={this.props.dimension}>{this.props.children}</StyledInputSwitchTitle>
        )}
      </StyledInputSwitch>
    );
  }
}
