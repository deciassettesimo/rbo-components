### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputCheckbox Dimensions

```js
<div className="list">
  <div>
    <InputCheckbox
      id="InputCheckboxXS"
      dimension={InputCheckbox.REFS.DIMENSIONS.XS}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet
    </InputCheckbox>
  </div>
  <div>
    <InputCheckbox
      id="InputCheckboxS"
      dimension={InputCheckbox.REFS.DIMENSIONS.S}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet
    </InputCheckbox>
  </div>
  <div>
    <InputCheckbox
      id="InputCheckboxM"
      dimension={InputCheckbox.REFS.DIMENSIONS.M}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet
    </InputCheckbox>
  </div>
  <div>
    <InputCheckbox
      id="InputCheckboxL"
      dimension={InputCheckbox.REFS.DIMENSIONS.L}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet
    </InputCheckbox>
  </div>
  <div>
    <InputCheckbox
      id="InputCheckboxXL"
      dimension={InputCheckbox.REFS.DIMENSIONS.XL}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet
    </InputCheckbox>
  </div>
</div>
```

### InputCheckbox with long text

```js
<div className="list">
  <div>
    <InputCheckbox
      id="InputCheckboxLongXS"
      dimension={InputCheckbox.REFS.DIMENSIONS.XS}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
      fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
      sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
      ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
      venenatis egestas scelerisque. In hac habitasse platea dictumst.
      Mauris ut ex a elit auctor suscipit in ut nibh.
    </InputCheckbox>
  </div>
  <div>
    <InputCheckbox
      id="InputCheckboxLongS"
      dimension={InputCheckbox.REFS.DIMENSIONS.S}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
      fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
      sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
      ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
      venenatis egestas scelerisque. In hac habitasse platea dictumst.
      Mauris ut ex a elit auctor suscipit in ut nibh.
    </InputCheckbox>
  </div>
  <div>
    <InputCheckbox
      id="InputCheckboxLongM"
      dimension={InputCheckbox.REFS.DIMENSIONS.M}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
      fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
      sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
      ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
      venenatis egestas scelerisque. In hac habitasse platea dictumst.
      Mauris ut ex a elit auctor suscipit in ut nibh.
    </InputCheckbox>
  </div>
  <div>
    <InputCheckbox
      id="InputCheckboxLongL"
      dimension={InputCheckbox.REFS.DIMENSIONS.L}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
      fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
      sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
      ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
      venenatis egestas scelerisque. In hac habitasse platea dictumst.
      Mauris ut ex a elit auctor suscipit in ut nibh.
    </InputCheckbox>
  </div>
  <div>
    <InputCheckbox
      id="InputCheckboxLongXL"
      dimension={InputCheckbox.REFS.DIMENSIONS.XL}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    >
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
      fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
      sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
      ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
      venenatis egestas scelerisque. In hac habitasse platea dictumst.
      Mauris ut ex a elit auctor suscipit in ut nibh.
    </InputCheckbox>
  </div>
</div>
```

### InputCheckbox with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputCheckbox id="InputCheckboxWithWarning1" isWarning>InputCheckboxWithWarning1</InputCheckbox>
  </div>
  <div>
    <InputCheckbox id="InputCheckboxWithWarning2" checked isWarning>InputCheckboxWithWarning2</InputCheckbox>
  </div>
  <div>
    <InputCheckbox id="InputCheckboxWithError1" isError>InputCheckboxWithError1</InputCheckbox>
  </div>
  <div>
    <InputCheckbox id="InputCheckboxWithError2" checked isError>InputCheckboxWithError2</InputCheckbox>
  </div>
</div>
```

### InputCheckbox disabled

```js
<div className="list">
  <div>
    <InputCheckbox id="InputCheckboxDisabled" disabled>InputCheckboxDisabled</InputCheckbox>
  </div>
  <div>
    <InputCheckbox id="InputCheckboxDisabledWithValue" checked disabled>InputCheckboxDisabledWithValue</InputCheckbox>
  </div>
  <div>
    <InputCheckbox id="InputCheckboxDisabledWithWarning" disabled isWarning>InputCheckboxDisabledWithWarning</InputCheckbox>
  </div>
  <div>
    <InputCheckbox id="InputCheckboxDisabledWithWarningAndValue" checked disabled isWarning>InputCheckboxDisabledWithWarningAndValue</InputCheckbox>
  </div>
  <div>
    <InputCheckbox id="InputCheckboxDisabledWithError" disabled isError>InputCheckboxDisabledWithError</InputCheckbox>
  </div>
  <div>
    <InputCheckbox id="InputCheckboxDisabledWithErrorAndValue" checked disabled isError>InputCheckboxDisabledWithErrorAndValue</InputCheckbox>
  </div>
</div>
```
