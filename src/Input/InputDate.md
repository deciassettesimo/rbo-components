### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputDate Dimensions

```js
<div className="list">
  <div>
    <InputDate
      id="InputDateXS"
      dimension={InputDate.REFS.DIMENSIONS.XS}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputDate
      id="InputDateS"
      dimension={InputDate.REFS.DIMENSIONS.S}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputDate
      id="InputDateM"
      dimension={InputDate.REFS.DIMENSIONS.M}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputDate
      id="InputDateL"
      dimension={InputDate.REFS.DIMENSIONS.L}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputDate
      id="InputDateXL"
      dimension={InputDate.REFS.DIMENSIONS.XL}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
</div>
```

### InputDate with placeholder

```js
<div className="list">
  <div>
    <InputDate id="InputDateWithPlaceholder" placeholder="Введите дату" />
  </div>
</div>
```

### InputDate with value

```js
<div className="list">
  <div>
    <InputDate id="InputDateWithValue1" value={'2013-02-08T09:30:26.123+03:00'} />
  </div>
</div>
```

### InputDate with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputDate id="InputDateWithWarning" value={'2013-02-08T09:30:26.123+04:00'} isWarning />
  </div>
  <div>
    <InputDate id="InputDateWithError" value={'2013-02-08T09:30:26.123+05:00'} isError />
  </div>
</div>
```

### InputDate disabled

```js
<div className="list">
  <div>
    <InputDate id="InputDateDisabled" disabled />
  </div>
  <div>
    <InputDate id="InputDateDisabledWithPlaceholder" placeholder="Введите дату" disabled />
  </div>
  <div>
    <InputDate id="InputDateDisabledWithValue" value={'2013-02-08T09:30:26.123+06:00'} disabled />
  </div>
  <div>
    <InputDate id="InputDateDisabledWithWarning" value={'2013-02-08T09:30:26.123+07:00'} disabled isWarning />
  </div>
  <div>
    <InputDate id="InputDateDisabledWithError" value={'2013-02-08T00:30:26.123+08:00'} disabled isError />
  </div>
</div>
```
