### REFS
```js static
{
  LOCALES = {
    RU: 'ru',
    EN: 'en',
  },
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputDateCalendar Dimensions

```js
<div className="list">
  <div>
    <InputDateCalendar
      id="InputDateCalendarXS"
      dimension={InputDateCalendar.REFS.DIMENSIONS.XS}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputDateCalendar
      id="InputDateCalendarS"
      dimension={InputDateCalendar.REFS.DIMENSIONS.S}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputDateCalendar
      id="InputDateCalendarM"
      dimension={InputDateCalendar.REFS.DIMENSIONS.M}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      locale={InputDateCalendar.REFS.LOCALES.EN}
    />
  </div>
  <div>
    <InputDateCalendar
      id="InputDateCalendarL"
      dimension={InputDateCalendar.REFS.DIMENSIONS.L}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      locale={InputDateCalendar.REFS.LOCALES.RU}
    />
  </div>
  <div>
    <InputDateCalendar
      id="InputDateCalendarXL"
      dimension={InputDateCalendar.REFS.DIMENSIONS.XL}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      locale={InputDateCalendar.REFS.LOCALES.RU}
    />
  </div>
</div>
```

### InputDateCalendar with placeholder

```js
<div className="list">
  <div>
    <InputDateCalendar id="InputDateCalendarWithPlaceholder" placeholder="Введите дату" />
  </div>
</div>
```

### InputDateCalendar with value

```js
<div className="list">
  <div>
    <InputDateCalendar id="InputDateCalendarWithValue1" value={'2013-02-08T09:30:26.123+03:00'} />
  </div>
</div>
```

### InputDateCalendar with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputDateCalendar id="InputDateCalendarWithWarning" value={'2013-02-08T09:30:26.123+04:00'} isWarning />
  </div>
  <div>
    <InputDateCalendar id="InputDateCalendarWithError" value={'2013-02-08T09:30:26.123+05:00'} isError />
  </div>
</div>
```

### InputDateCalendar disabled

```js
<div className="list">
  <div>
    <InputDateCalendar id="InputDateCalendarDisabled" disabled />
  </div>
  <div>
    <InputDateCalendar id="InputDateCalendarDisabledWithPlaceholder" placeholder="Введите дату" disabled />
  </div>
  <div>
    <InputDateCalendar id="InputDateCalendarDisabledWithValue" value={'2013-02-08T09:30:26.123+06:00'} disabled />
  </div>
  <div>
    <InputDateCalendar id="InputDateCalendarDisabledWithWarning" value={'2013-02-08T09:30:26.123+07:00'} disabled isWarning />
  </div>
  <div>
    <InputDateCalendar id="InputDateCalendarDisabledWithError" value={'2013-02-08T00:30:26.123+08:00'} disabled isError />
  </div>
</div>
```
