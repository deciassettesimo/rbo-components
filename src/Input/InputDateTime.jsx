import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { TYPES } from './_constants';
import { INPUT_REFS as REFS } from './_config';
import {
  getChar,
  getCaretPosition,
  getStringDateTimeFromMomentValue,
  formatDateTimeValue,
  deformatDateTimeValue,
} from './_utils';
import { StyledInputText } from './_style';

export default class InputDateTime extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.DATE_TIME;

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component value: date in ISO 8601 String format (e.g. '2018-07-25T00:00:00+03:00') */
    value: PropTypes.string,
    /** Placeholder */
    placeholder: PropTypes.string,
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onBlur: PropTypes.func,
    /** Change handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onChange: PropTypes.func,
    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    placeholder: null,
    dimension: REFS.DIMENSIONS.M,
    width: '100%',
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      value: this.props.value,
      formattedValue: '',
      caretPosition: null,
    };
    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    const propsValue = props.value || null;
    if (!state.isInitialized || propsValue !== state.value) {
      const formattedValue = formatDateTimeValue(getStringDateTimeFromMomentValue(props.value));
      const value = deformatDateTimeValue(formattedValue);
      return { isInitialized: true, value, formattedValue };
    }
    return null;
  }

  componentDidMount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  componentDidUpdate() {
    if (this.props.disabled && this.state.isFocused) this.handleBlur();

    if (this.state.caretPosition !== null) {
      this.inputNode.current.setSelectionRange(this.state.caretPosition, this.state.caretPosition);
    }
  }

  componentWillUnmount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  handleFocus = () => {
    const formattedValue = formatDateTimeValue(getStringDateTimeFromMomentValue(this.state.value));
    this.setState({ isFocused: true, caretPosition: null, formattedValue });

    this.props.onFocus({ id: this.props.id, value: this.state.value, formattedValue });
  };

  handleBlur = () => {
    const formattedValue = formatDateTimeValue(getStringDateTimeFromMomentValue(this.state.value));
    this.setState({ isFocused: false, caretPosition: null, formattedValue });

    this.props.onBlur({ id: this.props.id, value: this.state.value, formattedValue });
  };

  handleChange = e => {
    const { value } = e.target;
    const stateValue = this.state.value;
    const formattedValue = formatDateTimeValue(value);
    const deformattedValue = deformatDateTimeValue(formattedValue);
    const beforeCaretPosValue = value.substring(0, e.target.selectionEnd).replace(/[^\d\\.]/g, '');
    const caretPosition = getCaretPosition(formattedValue, beforeCaretPosValue);

    this.setState({
      value: deformattedValue,
      formattedValue,
      caretPosition,
    });

    if (deformattedValue !== stateValue) {
      this.props.onChange({ id: this.props.id, value: deformattedValue, formattedValue });
    }
  };

  handleKeyDown = e => {
    this.setState({
      caretPosition: null,
    });
    if ((e.ctrlKey || e.metaKey) && e.keyCode === 90) e.preventDefault(); // disable ctrl-z
  };

  handleKeyPress = e => {
    const char = getChar(e);
    if (!e.ctrlKey && !e.altKey && !e.metaKey && (char !== null && (char < '0' || char > '9') && char !== '.')) {
      e.preventDefault();
    }
  };

  render() {
    return (
      <StyledInputText
        {...addDataAttributes({ component: TYPES.DATE_TIME })}
        innerRef={ref => {
          this.inputNode.current = ref;
        }}
        sDimension={this.props.dimension}
        sWidth={this.props.width}
        isDisabled={this.props.disabled}
        isFocused={this.state.isFocused}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
        id={this.props.id}
        value={this.state.formattedValue}
        placeholder={this.props.placeholder}
        maxLength={16}
        disabled={this.props.disabled}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onKeyDown={this.handleKeyDown}
        onKeyPress={this.handleKeyPress}
      />
    );
  }
}
