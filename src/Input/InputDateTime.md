### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputDateTime Dimensions

```js
<div className="list">
  <div>
    <InputDateTime
      id="InputDateXS"
      dimension={InputDateTime.REFS.DIMENSIONS.XS}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputDateTime
      id="InputDateS"
      dimension={InputDateTime.REFS.DIMENSIONS.S}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputDateTime
      id="InputDateM"
      dimension={InputDateTime.REFS.DIMENSIONS.M}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputDateTime
      id="InputDateL"
      dimension={InputDateTime.REFS.DIMENSIONS.L}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputDateTime
      id="InputDateXL"
      dimension={InputDateTime.REFS.DIMENSIONS.XL}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
</div>
```

### InputDateTime with placeholder

```js
<div className="list">
  <div>
    <InputDateTime id="InputDateWithPlaceholder" placeholder="Введите дату" />
  </div>
</div>
```

### InputDateTime with value

```js
<div className="list">
  <div>
    <InputDateTime id="InputDateWithValue1" value={'2013-02-08T09:30:26.123+03:00'} />
  </div>
</div>
```

### InputDateTime with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputDateTime id="InputDateWithWarning" value={'2013-02-08T09:30:26.123+04:00'} isWarning />
  </div>
  <div>
    <InputDateTime id="InputDateWithError" value={'2013-02-08T09:30:26.123+05:00'} isError />
  </div>
</div>
```

### InputDateTime disabled

```js
<div className="list">
  <div>
    <InputDateTime id="InputDateDisabled" disabled />
  </div>
  <div>
    <InputDateTime id="InputDateDisabledWithPlaceholder" placeholder="Введите дату" disabled />
  </div>
  <div>
    <InputDateTime id="InputDateDisabledWithValue" value={'2013-02-08T09:30:26.123+06:00'} disabled />
  </div>
  <div>
    <InputDateTime id="InputDateDisabledWithWarning" value={'2013-02-08T09:30:26.123+07:00'} disabled isWarning />
  </div>
  <div>
    <InputDateTime id="InputDateDisabledWithError" value={'2013-02-08T00:30:26.123+08:00'} disabled isError />
  </div>
</div>
```
