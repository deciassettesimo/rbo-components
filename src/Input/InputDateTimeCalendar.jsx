import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Popup, { Opener, Box } from '../Popup';
import InputDateTimeCalendarField from './_internal/InputDateTimeCalendarField';
import InputCalendarWithTimePicker from './_internal/InputCalendarWithTimePicker';
import { TYPES } from './_constants';
import { INPUT_DATE_CALENDAR_REFS as REFS } from './_config';

export default class InputDateTimeCalendar extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.DATE__TIME_CALENDAR;

  static propTypes = {
    /** id of form input element */
    id: PropTypes.string.isRequired,
    /** date in ISO 8601 String format */
    value: PropTypes.string,
    /** placeholder of form input element */
    placeholder: PropTypes.string,
    /** dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** warning flag */
    isWarning: PropTypes.bool,
    /** error flag */
    isError: PropTypes.bool,
    /** disabled flag of form input element */
    disabled: PropTypes.bool,
    /** focus handler, params { id, value, formattedValue } */
    onFocus: PropTypes.func,
    /** blur handler, params { id, value, formattedValue } */
    onBlur: PropTypes.func,
    /** change handler, params { id, value, formattedValue } */
    onChange: PropTypes.func,
    /** language for calendar */
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    placeholder: null,
    dimension: REFS.DIMENSIONS.M,
    width: '100%',
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    locale: REFS.LOCALES.RU,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      isOpened: false,
      disallowBlurFlag: false,
      value: this.props.value,
    };

    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    const propsValue = props.value || null;
    if (!state.isInitialized || propsValue !== state.value) {
      return {
        isInitialized: true,
        value: props.value,
      };
    }
    return null;
  }

  componentDidMount() {
    document.addEventListener('mouseup', this.allowBlur);
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.allowBlur);
  }

  onInputElementFocus = () => {
    if (!this.state.isFocused && !this.props.disabled) this.handleFocus();
  };

  onInputElementBlur = () => {
    if (!this.state.disallowBlurFlag) this.handleBlur();
  };

  onInputElementChange = ({ value }) => {
    this.setState({ value });
    this.handleChange(value);
  };

  onIconElementClick = () => {
    if (!this.props.disabled) this.setState({ isOpened: !this.state.isOpened });
  };

  onCalendarSelect = value => {
    const stateValue = this.state.value;
    this.setState({ value });

    if (value !== stateValue) {
      this.props.onChange({ id: this.props.id, value });
    }
  };

  onPopupClose = () => {
    this.setState({ isOpened: false });
  };

  disallowBlur = () => {
    this.setState({ disallowBlurFlag: true });
  };

  allowBlur = () => {
    if (this.state.disallowBlurFlag) {
      this.setState({ disallowBlurFlag: false });
      this.inputNode.current.inputNode.current.focus();
    }
  };

  handleFocus = () => {
    this.setState({ isFocused: true });

    this.props.onFocus({ id: this.props.id, value: this.state.value });
  };

  handleBlur = () => {
    this.onPopupClose();
    this.setState({ isFocused: false });

    this.props.onBlur({ id: this.props.id, value: this.state.value });
  };

  handleChange = value => {
    const stateValue = this.state.value;
    this.setState({ value, isOpened: false });

    if (value !== stateValue) {
      this.props.onChange({ id: this.props.id, value });
    }
  };

  render() {
    const inputDateTimeCalendarFieldProps = {
      id: this.props.id,
      value: this.state.value,
      placeholder: this.props.placeholder,
      dimension: this.props.dimension,
      width: this.props.width,
      isFocused: this.state.isFocused,
      isWarning: this.props.isWarning,
      isError: this.props.isError,
      disabled: this.props.disabled,
      onInputElementFocus: this.onInputElementFocus,
      onInputElementBlur: this.onInputElementBlur,
      onInputElementChange: this.onInputElementChange,
      onIconElementClick: this.onIconElementClick,
      onIconElementMouseDown: this.disallowBlur,
      onIconElementMouseUo: this.allowBlur,
      setInputNode: this.props.setInputNode,
    };

    const inputCalendarWithTimePickerProps = {
      id: this.props.id,
      value: this.state.value,
      onPopupElementMouseDown: this.disallowBlur,
      onPopupElementMouseUp: this.allowBlur,
      onSelect: this.onCalendarSelect,
      onClickOkay: this.onPopupClose,
      locale: this.props.locale,
    };

    return (
      <Popup isOpened={!this.props.disabled && this.state.isOpened} onClose={this.onPopupClose}>
        <Opener display={Opener.REFS.DISPLAY.BLOCK} isNotAutoOpen>
          <InputDateTimeCalendarField {...inputDateTimeCalendarFieldProps} ref={this.inputNode} />
        </Opener>
        <Box>
          <InputCalendarWithTimePicker {...inputCalendarWithTimePickerProps} />
        </Box>
      </Popup>
    );
  }
}
