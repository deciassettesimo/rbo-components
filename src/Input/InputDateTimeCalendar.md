### REFS
```js static
{
  LOCALES = {
    RU: 'ru',
    EN: 'en',
  },
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputDateTimeCalendar Dimensions

```js
<div className="list">
  <div>
    <InputDateTimeCalendar
      id="InputDateTimeCalendarXS"
      dimension={InputDateTimeCalendar.REFS.DIMENSIONS.XS}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputDateTimeCalendar
      id="InputDateTimeCalendarS"
      dimension={InputDateCalendar.REFS.DIMENSIONS.S}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputDateTimeCalendar
      id="InputDateTimeCalendarM"
      dimension={InputDateTimeCalendar.REFS.DIMENSIONS.M}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      locale={InputDateTimeCalendar.REFS.LOCALES.EN}
    />
  </div>
  <div>
    <InputDateTimeCalendar
      id="InputDateTimeCalendarL"
      dimension={InputDateTimeCalendar.REFS.DIMENSIONS.L}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      locale={InputDateTimeCalendar.REFS.LOCALES.RU}
    />
  </div>
  <div>
    <InputDateTimeCalendar
      id="InputDateTimeCalendarXL"
      dimension={InputDateTimeCalendar.REFS.DIMENSIONS.XL}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      locale={InputDateTimeCalendar.REFS.LOCALES.RU}
    />
  </div>
</div>
```

### InputDateTimeCalendar with placeholder

```js
<div className="list">
  <div>
    <InputDateTimeCalendar id="InputDateTimeCalendarWithPlaceholder" placeholder="Введите дату" />
  </div>
</div>
```

### InputDateTimeCalendar with value

```js
<div className="list">
  <div>
    <InputDateTimeCalendar id="InputDateTimeCalendarWithValue1" value={'2013-02-08T09:30:26.123+03:00'} />
  </div>
</div>
```

### InputDateTimeCalendar with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputDateTimeCalendar id="InputDateTimeCalendarWithWarning" value={'2013-02-08T09:30:26.123+04:00'} isWarning />
  </div>
  <div>
    <InputDateTimeCalendar id="InputDateTimeCalendarWithError" value={'2013-02-08T09:30:26.123+05:00'} isError />
  </div>
</div>
```

### InputDateTimeCalendar disabled

```js
<div className="list">
  <div>
    <InputDateTimeCalendar id="InputDateTimeCalendarDisabled" disabled />
  </div>
  <div>
    <InputDateTimeCalendar id="InputDateTimeCalendarDisabledWithPlaceholder" placeholder="Введите дату" disabled />
  </div>
  <div>
    <InputDateTimeCalendar id="InputDateTimeCalendarDisabledWithValue" value={'2013-02-08T09:30:26.123+06:00'} disabled />
  </div>
  <div>
    <InputDateTimeCalendar id="InputDateTimeCalendarDisabledWithWarning" value={'2013-02-08T09:30:26.123+07:00'} disabled isWarning />
  </div>
  <div>
    <InputDateTimeCalendar id="InputDateTimeCalendarDisabledWithError" value={'2013-02-08T00:30:26.123+08:00'} disabled isError />
  </div>
</div>
```
