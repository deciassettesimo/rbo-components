### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputDigital Dimensions

```js
<div className="list">
  <div>
    <InputDigital
      id="InputDigitalXS"
      dimension={InputDigital.REFS.DIMENSIONS.XS}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputDigital
      id="InputDigitalS"
      dimension={InputDigital.REFS.DIMENSIONS.S}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputDigital
      id="InputDigitalM"
      dimension={InputDigital.REFS.DIMENSIONS.M}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputDigital
      id="InputDigitalL"
      dimension={InputDigital.REFS.DIMENSIONS.L}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputDigital
      id="InputDigitalXL"
      dimension={InputDigital.REFS.DIMENSIONS.XL}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
</div>
```

### InputDigital with placeholder

```js
<div className="list">
  <div>
    <InputDigital id="InputDigitalWithPlaceholder" placeholder="1234567890" />
  </div>
</div>
```

### InputDigital with maxLength (for example 10)

```js
<div className="list">
  <div>
    <InputDigital id="InputDigitalWithMaxLength" maxLength={10} value="1234567890" />
  </div>
</div>
```

### InputDigital with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputDigital id="InputDigitalWithWarning" value="1234567890" isWarning />
  </div>
  <div>
    <InputDigital id="InputDigitalWithError" value="1234567890" isError />
  </div>
</div>
```

### InputDigital disabled

```js
<div className="list">
  <div>
    <InputDigital id="InputDigitalDisabled" disabled />
  </div>
  <div>
    <InputDigital id="InputDigitalDisabledWithPlaceholder" placeholder="1234567890" disabled />
  </div>
  <div>
    <InputDigital id="InputDigitalDisabledWithValue" value="1234567890" disabled />
  </div>
  <div>
    <InputDigital id="InputDigitalDisabledWithWarning" value="1234567890" disabled isWarning />
  </div>
  <div>
    <InputDigital id="InputDigitalDisabledWithError" value="1234567890" disabled isError />
  </div>
</div>
```
