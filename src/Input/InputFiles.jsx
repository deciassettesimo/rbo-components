import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import FilesManager from '../FilesManager';
import { TYPES } from './_constants';
import { INPUT_FILES_REFS as REFS } from './_config';
import { StyledInputFiles, StyledInputHiddenHtmlInput } from './_style';

export default class InputFiles extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.FILES;

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component value */
    value: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        type: PropTypes.string,
        name: PropTypes.string,
        size: PropTypes.number,
        date: PropTypes.string,
        comment: PropTypes.string,
        isDownloadable: PropTypes.bool,
        file: PropTypes.shape(),
      }),
    ),
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** ViewMode flag */
    isViewMode: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onBlur: PropTypes.func,
    /** Change handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onChange: PropTypes.func,

    /** FilesManager Locale */
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    /** FilesManager isLoading */
    isLoading: PropTypes.bool,
    /** FilesManager minHeight */
    minHeight: PropTypes.number,
    /** FilesManager maxFilesQty */
    maxFilesQty: PropTypes.number,
    /** FilesManager maxFilesTotalSize */
    maxFilesTotalSize: PropTypes.number,
    /** FilesManager maxFileSize */
    maxFileSize: PropTypes.number,
    /** FilesManager isWithComment */
    isWithComment: PropTypes.bool,
    /** FilesManager commentMaxLength */
    commentMaxLength: PropTypes.number,
    /** FilesManager addLabel */
    addLabel: PropTypes.node,
    /** FilesManager addError */
    addError: PropTypes.node,
    /** FilesManager maxFilesAddedLabel */
    maxFilesAddedLabel: PropTypes.string,
    /** FilesManager fileDateLabel */
    fileDateLabel: PropTypes.string,
    /** FilesManager onDownloadClick */
    onDownloadClick: PropTypes.func,

    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    dimension: REFS.DIMENSIONS.M,
    width: '100%',
    isWarning: false,
    isError: false,
    disabled: false,
    isViewMode: false,
    onBlur: () => null,
    onFocus: () => null,
    onChange: () => null,

    locale: REFS.LOCALES.RU,
    isLoading: false,
    minHeight: 64,
    maxFilesQty: Infinity,
    maxFilesTotalSize: Infinity,
    maxFileSize: Infinity,
    isWithComment: true,
    commentMaxLength: null,
    addLabel: null,
    addError: null,
    maxFilesAddedLabel: null,
    fileDateLabel: null,
    onDownloadClick: () => null,

    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      value: this.props.value,
    };

    this.node = React.createRef();
    this.inputNode = React.createRef();
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleDocumentClick, true);
    document.addEventListener('touchstart', this.handleDocumentClick, true);

    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  static getDerivedStateFromProps(props, state) {
    if (JSON.stringify(props.value) !== JSON.stringify(state.value)) return { value: props.value };
    return null;
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleDocumentClick, true);
    document.removeEventListener('touchstart', this.handleDocumentClick, true);

    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  handleDocumentClick = e => {
    if (this.node.current.contains(e.target)) {
      if (!this.state.isFocused) setTimeout(this.handleFocus, 0);
    } else if (this.state.isFocused) {
      this.handleBlur();
    }
  };

  handleFocus = () => {
    this.setState({ isFocused: true });
    this.props.onFocus({ id: this.props.id, value: this.state.value });
  };

  handleBlur = () => {
    this.setState({ isFocused: false });
    this.props.onBlur({ id: this.props.id, value: this.state.value });
  };

  handleChange = value => {
    const stateValue = this.state.value;
    this.setState({ value });
    if (value !== stateValue) {
      this.props.onChange({ id: this.props.id, value });
    }
  };

  render() {
    return (
      <StyledInputFiles
        {...addDataAttributes({ component: TYPES.FILES })}
        innerRef={ref => {
          this.node.current = ref;
        }}
        sDimension={this.props.dimension}
        sWidth={this.props.width}
        isDisabled={this.props.disabled}
        isViewMode={this.props.isViewMode}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
      >
        <StyledInputHiddenHtmlInput
          innerRef={ref => {
            this.inputNode.current = ref;
          }}
          id={this.props.id}
          disabled={this.props.disabled}
          onFocus={this.handleFocus}
        />
        <FilesManager
          locale={this.props.locale}
          items={this.state.value}
          isLoading={this.props.isLoading}
          isEditable={!this.props.disabled && !this.props.isViewMode}
          minHeight={this.props.minHeight}
          maxFilesQty={this.props.maxFilesQty}
          maxFilesTotalSize={this.props.maxFilesTotalSize}
          maxFileSize={this.props.maxFileSize}
          isWithComment={this.props.isWithComment}
          commentInputDimension={this.props.dimension}
          commentMaxLength={this.props.commentMaxLength}
          addLabel={this.props.addLabel}
          addError={this.props.addError}
          maxFilesAddedLabel={this.props.maxFilesAddedLabel}
          fileDateLabel={this.props.fileDateLabel}
          onChange={this.handleChange}
          onDownloadClick={this.props.onDownloadClick}
        />
      </StyledInputFiles>
    );
  }
}
