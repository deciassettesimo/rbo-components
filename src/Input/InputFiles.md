### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputFiles Dimensions

```js
<div className="list">
  <div>
    <InputFiles
      id="InputFilesXS"
      dimension={InputFiles.REFS.DIMENSIONS.XS}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputFiles
      id="InputFilesS-XL"
    />
  </div>
</div>
```

### InputFiles with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputFiles id="InputFilesWithWarning" isWarning />
  </div>
  <div>
    <InputFiles id="InputFilesWithError" isError />
  </div>
</div>
```

### InputFiles disabled

```js
const items = [
  {
    id: 'kljklj1',
    name: 'файл1.txt',
    size: 1201922,
    date: '2013-02-08T09:30:26.123+03:00',
    isDownloadable: true,
  },
  {
    id: 'kljklj2',
    name: 'файл2.txt',
    size: 1201922,
    date: '2013-02-08T09:30:26.123+03:00',
    isDownloadable: true,
  },
  {
    id: 'kljklj3',
    name: 'файл3.txt',
    size: 1201922,
    date: '2013-02-08T09:30:26.123+03:00',
    isDownloadable: false,
    comment: 'Комментарий к файлу 3',
  },
];
<div className="list">
  <div>
    <InputFiles id="InputFilesDisabledWithoutValue" disabled />
  </div>
  <div>
    <InputFiles id="InputFilesDisabledWithValue" disabled value={items} />
  </div>
</div>
```

### InputFiles view mode

```js
const items = [
  {
    id: 'kljklj1',
    name: 'файл1.txt',
    size: 1201922,
    date: '2013-02-08T09:30:26.123+03:00',
    isDownloadable: true,
  },
  {
    id: 'kljklj2',
    name: 'файл2.txt',
    size: 1201922,
    date: '2013-02-08T09:30:26.123+03:00',
    isDownloadable: true,
  },
  {
    id: 'kljklj3',
    name: 'файл3.txt',
    size: 1201922,
    date: '2013-02-08T09:30:26.123+03:00',
    isDownloadable: false,
    comment: 'Комментарий к файлу 3',
  },
];
<div className="list">
  <div>
    <InputFiles id="InputFilesViewModeWithoutValue" isViewMode />
  </div>
  <div>
    <InputFiles id="InputFilesViewModeWithValue" isViewMode value={items} />
  </div>
</div>
```
