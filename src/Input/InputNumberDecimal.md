### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputNumberDecimal Dimensions

```js
<div className="list">
  <div>
    <InputNumberDecimal
      id="InputNumberDecimalXS"
      dimension={InputNumberDecimal.REFS.DIMENSIONS.XS}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputNumberDecimal
      id="InputNumberDecimalS"
      dimension={InputNumberDecimal.REFS.DIMENSIONS.S}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputNumberDecimal
      id="InputNumberDecimalM"
      dimension={InputNumberDecimal.REFS.DIMENSIONS.M}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputNumberDecimal
      id="InputNumberDecimalL"
      dimension={InputNumberDecimal.REFS.DIMENSIONS.L}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputNumberDecimal
      id="InputNumberDecimalXL"
      dimension={InputNumberDecimal.REFS.DIMENSIONS.XL}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
</div>
```

### InputNumberDecimal with frictionLength

```js
<div className="list">
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithFrictionLengthr" isFormatted frictionLength={4} />
  </div>
</div>
```

### InputNumberDecimal with placeholder

```js
<div className="list">
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithPlaceholder" placeholder="0.00" isNegative isFormatted />
  </div>
</div>
```

### InputNumberDecimal isNegative, isFormatted flags

```js
<div className="list">
  <div>
    <InputNumberDecimal id="InputNumberDecimalDefault" value={123456789} />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithIsNegative" value={-123456789} isNegative />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithIsFormatted" value={123456789} isFormatted />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithIsFormattedAndIsNegative" value={-123456789} isFormatted isNegative />
  </div>
</div>
```

### InputNumberDecimal with value

```js
<div className="list">
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithValue1" value={0} isFormatted isNegative />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithValue2" value={.80} isFormatted isNegative />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithValue3" value={-.80} isFormatted isNegative />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithValue4" value={1234} isFormatted isNegative />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithValue5" value={-1234} isFormatted isNegative />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithValue6" value={12345678901234.56} isFormatted isNegative />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithValue7" value={-1234.56} isFormatted isNegative />
  </div>
</div>
```

### InputNumberDecimal with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithWarning" value={1000000} isWarning />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalWithError" value={1000000} isError />
  </div>
</div>
```

### InputNumberDecimal disabled

```js
<div className="list">
  <div>
    <InputNumberDecimal id="InputNumberDecimalDisabled" disabled />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalDisabledWithPlaceholder" placeholder="0.00" disabled />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalDisabledWithValue" value={1000000} disabled />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalDisabledWithWarning" value={1000000} disabled isWarning />
  </div>
  <div>
    <InputNumberDecimal id="InputNumberDecimalDisabledWithError" value={1000000} disabled isError />
  </div>
</div>
```
