import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { TYPES } from './_constants';
import { INPUT_REFS as REFS } from './_config';
import { getChar, getCaretPosition, formatNumberIntegerValue, deformatNumberIntegerValue } from './_utils';
import { StyledInputText } from './_style';

export default class InputNumberInteger extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.NUMBER_INTEGER;

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component value: number integer (e.g. 100023) */
    value: PropTypes.number,
    /** Placeholder */
    placeholder: PropTypes.string,
    /** Max-length */
    maxLength: PropTypes.number,
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Align text */
    textAlign: PropTypes.oneOf(Object.values(REFS.TEXT_ALIGN)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Possibility of entering a negative number flag */
    isNegative: PropTypes.bool,
    /** Format digits flag (e.g. '100 023') */
    isFormatted: PropTypes.bool,
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onBlur: PropTypes.func,
    /** Change handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onChange: PropTypes.func,
    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    placeholder: null,
    maxLength: 14,
    dimension: REFS.DIMENSIONS.M,
    textAlign: REFS.TEXT_ALIGN.RIGHT,
    width: '100%',
    isNegative: false,
    isFormatted: false,
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      value: this.props.value,
      formattedValue: '',
      caretPosition: null,
      maxLength:
        this.props.maxLength +
        (this.props.isFormatted ? Math.floor(this.props.maxLength / 3) : 0) +
        (this.props.isNegative ? 1 : 0),
    };

    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.isInitialized || props.value !== state.value) {
      const formattedValue = formatNumberIntegerValue(
        props.value,
        props.isNegative,
        props.isFormatted,
        props.maxLength,
        !state.isFocused,
      );
      const value = deformatNumberIntegerValue(formattedValue);
      return { isInitialized: true, value, formattedValue };
    }
    return null;
  }

  componentDidMount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  componentDidUpdate() {
    if (this.props.disabled && this.state.isFocused) this.handleBlur();

    if (this.state.caretPosition !== null) {
      this.inputNode.current.setSelectionRange(this.state.caretPosition, this.state.caretPosition);
    }
  }

  componentWillUnmount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  handleFocus = () => {
    const formattedValue = formatNumberIntegerValue(
      this.state.value,
      this.props.isNegative,
      this.props.isFormatted,
      this.props.maxLength,
      false,
    );
    this.setState({ isFocused: true, caretPosition: null, formattedValue });

    this.props.onFocus({ id: this.props.id, value: this.state.value, formattedValue });
  };

  handleBlur = () => {
    const formattedValue = formatNumberIntegerValue(
      this.state.value,
      this.props.isNegative,
      this.props.isFormatted,
      this.props.maxLength,
      true,
    );
    this.setState({ isFocused: false, caretPosition: null, formattedValue });

    this.props.onBlur({ id: this.props.id, value: this.state.value, formattedValue });
  };

  handleChange = e => {
    const { value } = e.target;
    const stateValue = this.state.value;
    const beforeCaretPosValue = value
      .substring(0, e.target.selectionEnd)
      .replace(',', '.')
      .replace(/[^-\d\\.]/g, '');
    const formattedValue = formatNumberIntegerValue(
      value,
      this.props.isNegative,
      this.props.isFormatted,
      this.props.maxLength,
      false,
    );
    const deformattedValue = deformatNumberIntegerValue(formattedValue);
    const caretPosition = getCaretPosition(formattedValue, beforeCaretPosValue);

    this.setState({
      value: deformattedValue,
      formattedValue,
      caretPosition,
    });

    if (deformattedValue !== stateValue) {
      this.props.onChange({ id: this.props.id, value: deformattedValue, formattedValue });
    }
  };

  handleKeyDown = e => {
    this.setState({
      caretPosition: null,
    });
    if ((e.ctrlKey || e.metaKey) && e.keyCode === 90) e.preventDefault(); // disable ctrl-z
  };

  handleKeyPress = e => {
    const char = getChar(e);
    if (
      !e.ctrlKey &&
      !e.altKey &&
      !e.metaKey &&
      ((char !== null && (char < '0' || char > '9') && char !== '-') ||
        (char === '-' && e.target.value.indexOf('-') > -1))
    ) {
      e.preventDefault();
    }
  };

  render() {
    return (
      <StyledInputText
        {...addDataAttributes({ component: TYPES.NUMBER_INTEGER })}
        innerRef={ref => {
          this.inputNode.current = ref;
        }}
        sDimension={this.props.dimension}
        sTextAlign={this.props.textAlign}
        sWidth={this.props.width}
        isDisabled={this.props.disabled}
        isFocused={this.state.isFocused}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
        id={this.props.id}
        value={this.state.formattedValue}
        placeholder={this.props.placeholder}
        maxLength={this.state.maxLength}
        disabled={this.props.disabled}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onKeyDown={this.handleKeyDown}
        onKeyPress={this.handleKeyPress}
      />
    );
  }
}
