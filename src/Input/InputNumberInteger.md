### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputNumberInteger Dimensions

```js
<div className="list">
  <div>
    <InputNumberInteger
      id="InputNumberIntegerXS"
      dimension={InputNumberInteger.REFS.DIMENSIONS.XS}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputNumberInteger
      id="InputNumberIntegerS"
      dimension={InputNumberInteger.REFS.DIMENSIONS.S}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputNumberInteger
      id="InputNumberIntegerM"
      dimension={InputNumberInteger.REFS.DIMENSIONS.M}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputNumberInteger
      id="InputNumberIntegerL"
      dimension={InputNumberInteger.REFS.DIMENSIONS.L}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputNumberInteger
      id="InputNumberIntegerXL"
      dimension={InputNumberInteger.REFS.DIMENSIONS.XL}
      isNegative
      isFormatted
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
</div>
```

### InputNumberInteger with placeholder

```js
<div className="list">
  <div>
    <InputNumberInteger id="InputNumberIntegerWithPlaceholder" placeholder="123" isNegative isFormatted />
  </div>
</div>
```

### InputNumberInteger isNegative, isFormatted flags

```js
<div className="list">
  <div>
    <InputNumberInteger id="InputNumberIntegerDefault" value={123456789} />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerWithIsNegative" value={-123456789} isNegative />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerWithIsFormatted" value={123456789} isFormatted />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerWithIsFormattedAndIsNegative" value={-123456789} isFormatted isNegative />
  </div>
</div>
```

### InputNumberInteger with value

```js
<div className="list">
  <div>
    <InputNumberInteger id="InputNumberIntegerWithValue1" value={0} isFormatted isNegative />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerWithValue4" value={1234} isFormatted isNegative />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerWithValue5" value={-1234} isFormatted isNegative />
  </div>
</div>
```

### InputNumberInteger with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputNumberInteger id="InputNumberIntegerWithWarning" value={1000000} isWarning />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerWithError" value={1000000} isError />
  </div>
</div>
```

### InputNumberInteger disabled

```js
<div className="list">
  <div>
    <InputNumberInteger id="InputNumberIntegerDisabled" disabled />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerDisabledWithPlaceholder" placeholder="123" disabled />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerDisabledWithValue" value={1000000} disabled />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerDisabledWithWarning" value={1000000} disabled isWarning />
  </div>
  <div>
    <InputNumberInteger id="InputNumberIntegerDisabledWithError" value={1000000} disabled isError />
  </div>
</div>
```
