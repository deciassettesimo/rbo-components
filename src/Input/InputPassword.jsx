import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { TYPES } from './_constants';
import { INPUT_REFS as REFS } from './_config';
import { StyledInputPassword } from './_style';
import { deformatTextValue, formatTextValue } from './_utils';

export default class InputPassword extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.PASSWORD;

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component value */
    value: PropTypes.string,
    /** Placeholder */
    placeholder: PropTypes.string,
    /** Max-length */
    maxLength: PropTypes.number,
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Align text */
    textAlign: PropTypes.oneOf(Object.values(REFS.TEXT_ALIGN)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onBlur: PropTypes.func,
    /** Change handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onChange: PropTypes.func,
    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    placeholder: null,
    maxLength: null,
    dimension: REFS.DIMENSIONS.M,
    textAlign: REFS.TEXT_ALIGN.LEFT,
    width: '100%',
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      value: this.props.value,
      formattedValue: '',
    };

    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    const propsValue = props.value || null;
    if (!state.isInitialized || propsValue !== state.value) {
      const formattedValue = formatTextValue(props.value, props.maxLength);
      const value = deformatTextValue(formattedValue);
      return { isInitialized: true, value, formattedValue };
    }
    return null;
  }

  componentDidMount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  componentDidUpdate() {
    if (this.props.disabled && this.state.isFocused) this.handleBlur();
  }

  componentWillUnmount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  handleFocus = () => {
    this.setState({ isFocused: true });

    this.props.onFocus({ id: this.props.id, value: this.state.value, formattedValue: this.state.formattedValue });
  };

  handleBlur = () => {
    this.setState({ isFocused: false });

    this.props.onBlur({ id: this.props.id, value: this.state.value, formattedValue: this.state.formattedValue });
  };

  handleChange = e => {
    const { value } = e.target;
    const stateValue = this.state.value;
    const formattedValue = formatTextValue(value, this.props.maxLength);
    const deformattedValue = deformatTextValue(formattedValue);
    this.setState({
      value: deformattedValue,
      formattedValue,
    });
    if (deformattedValue !== stateValue) {
      this.props.onChange({ id: this.props.id, value: deformattedValue, formattedValue });
    }
  };

  render() {
    return (
      <StyledInputPassword
        {...addDataAttributes({ component: TYPES.PASSWORD })}
        innerRef={ref => {
          this.inputNode.current = ref;
        }}
        sDimension={this.props.dimension}
        sTextAlign={this.props.textAlign}
        sWidth={this.props.width}
        isDisabled={this.props.disabled}
        isFocused={this.state.isFocused}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
        id={this.props.id}
        value={this.state.formattedValue}
        placeholder={this.props.placeholder}
        maxLength={this.props.maxLength}
        disabled={this.props.disabled}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
      />
    );
  }
}
