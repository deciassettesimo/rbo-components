### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputPassword Dimensions

```js
<div className="list">
  <div>
    <InputPassword
      id="InputPasswordXS"
      dimension={InputPassword.REFS.DIMENSIONS.XS}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputPassword
      id="InputPasswordS"
      dimension={InputPassword.REFS.DIMENSIONS.S}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputPassword
      id="InputPasswordM"
      dimension={InputPassword.REFS.DIMENSIONS.M}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputPassword
      id="InputPasswordL"
      dimension={InputPassword.REFS.DIMENSIONS.L}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputPassword
      id="InputPasswordXL"
      dimension={InputPassword.REFS.DIMENSIONS.XL}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
</div>
```

### InputPassword with placeholder

```js
<div className="list">
  <div>
    <InputPassword id="InputPasswordWithPlaceholder" placeholder="Placeholder" />
  </div>
</div>
```

### InputPassword with maxLength (for example 10)

```js
<div className="list">
  <div>
    <InputPassword id="InputPasswordWithMaxLength" maxLength={10} value="1234567890" />
  </div>
</div>
```

### InputPassword with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputPassword id="InputPasswordWithWarning" value="Поле с предупреждением" isWarning />
  </div>
  <div>
    <InputPassword id="InputPasswordWithError" value="Поле с ошибкой" isError />
  </div>
</div>
```

### InputPassword disabled

```js
<div className="list">
  <div>
    <InputPassword id="InputPasswordDisabled" disabled />
  </div>
  <div>
    <InputPassword id="InputPasswordDisabledWithPlaceholder" placeholder="Placeholder" disabled />
  </div>
  <div>
    <InputPassword id="InputPasswordDisabledWithValue" value="Поле недоступно для редактирования" disabled />
  </div>
  <div>
    <InputPassword id="InputPasswordDisabledWithWarning" value="Поле с предупреждением недоступно для редактирования" disabled isWarning />
  </div>
  <div>
    <InputPassword id="InputPasswordDisabledWithError" value="Поле с ошибкой недоступно для редактирования" disabled isError />
  </div>
</div>
```
