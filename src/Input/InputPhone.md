### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputPhone Dimensions

```js
<div className="list">
  <div>
    <InputPhone
      id="InputPhoneXS"
      dimension={InputPhone.REFS.DIMENSIONS.XS}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputPhone
      id="InputPhoneS"
      dimension={InputPhone.REFS.DIMENSIONS.S}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputPhone
      id="InputPhoneM"
      dimension={InputPhone.REFS.DIMENSIONS.M}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputPhone
      id="InputPhoneL"
      dimension={InputPhone.REFS.DIMENSIONS.L}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
  <div>
    <InputPhone
      id="InputPhoneXL"
      dimension={InputPhone.REFS.DIMENSIONS.XL}
      onFocus={({ id, value, formattedValue })=>{ console.log('focus:', id, value, formattedValue); }}
      onBlur={({ id, value, formattedValue })=>{ console.log('blur:', id, value, formattedValue); }}
      onChange={({ id, value, formattedValue })=>{ console.log('change:', id, value, formattedValue); }}
    />
  </div>
</div>
```

### InputPhone with placeholder

```js
<div className="list">
  <div>
    <InputPhone id="InputPhoneWithPlaceholder" placeholder="+7(926)123-45-67" />
  </div>
</div>
```

### InputPhone with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputPhone id="InputPhoneWithWarning" value="+79261234567" isWarning />
  </div>
  <div>
    <InputPhone id="InputPhoneWithError" value="+79261234567" isError />
  </div>
</div>
```

### InputPhone disabled

```js
<div className="list">
  <div>
    <InputPhone id="InputPhoneDisabled" disabled />
  </div>
  <div>
    <InputPhone id="InputPhoneDisabledWithPlaceholder" placeholder="+7(926)123-45-67" disabled />
  </div>
  <div>
    <InputPhone id="InputPhoneDisabledWithValue" value="+79261234567" disabled />
  </div>
  <div>
    <InputPhone id="InputPhoneDisabledWithWarning" value="+79261234567" disabled isWarning />
  </div>
  <div>
    <InputPhone id="InputPhoneDisabledWithError" value="+79261234567" disabled isError />
  </div>
</div>
```
