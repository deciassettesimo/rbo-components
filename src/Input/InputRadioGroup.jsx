import React, { PureComponent, createContext } from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import { INPUT_REFS as REFS } from './_config';
import InputRadioGroupOption from './_internal/InputRadioGroupOption';

export const InputRadioGroupContext = createContext({});

export default class InputRadioGroup extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.RADIO_GROUP;

  static propTypes = {
    /** children */
    children: PropTypes.node.isRequired,
    /** id of radio group */
    id: PropTypes.string.isRequired,
    /** value of radio group */
    value: PropTypes.string,
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** focus handler, params { id, value } */
    onFocus: PropTypes.func,
    /** blur handler, params { id, value } */
    onBlur: PropTypes.func,
    /** change handler, params { id, value } */
    onChange: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    dimension: REFS.DIMENSIONS.M,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
  };

  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      value: this.props.value,
      dimension: this.props.dimension,
      isFocused: false,
      onFocus: this.handleFocus,
      onBlur: this.handleBlur,
      onChange: this.handleChange,
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.isInitialized || props.value !== state.value) {
      return { isInitialized: true, value: props.value };
    }
    return null;
  }

  handleFocus = () => {
    this.setState({ isFocused: true });
    this.props.onFocus({ id: this.props.id, value: this.state.value });
  };

  handleBlur = () => {
    this.setState({ isFocused: false });
    this.props.onBlur({ id: this.props.id, value: this.state.value });
  };

  handleChange = ({ value }) => {
    const stateValue = this.state.value;
    this.setState({ value });
    if (value !== stateValue) this.props.onChange({ id: this.props.id, value });
  };

  render() {
    return (
      <InputRadioGroupContext.Provider value={{ ...this.state }}>{this.props.children}</InputRadioGroupContext.Provider>
    );
  }
}

InputRadioGroup.Option = InputRadioGroupOption;
