### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

#### InputRadioGroup.Option props & methods
```js static
    value: string isRequired,
    children: node,
    dimension: Object.values(REFS.DIMENSIONS),
    isWarning: bool,
    isError: bool,
    disabled: bool,
```

### InputRadioGroup Dimensions

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupXS"
    dimension={InputRadioGroup.REFS.DIMENSIONS.XS}
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1">
        InputRadioGroupXS Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        InputRadioGroupXS Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupS"
    dimension={InputRadioGroup.REFS.DIMENSIONS.S}
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1">
        InputRadioGroupS Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        InputRadioGroupS Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupM"
    dimension={InputRadioGroup.REFS.DIMENSIONS.M}
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1">
        InputRadioGroupM Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        InputRadioGroupM Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupL"
    dimension={InputRadioGroup.REFS.DIMENSIONS.L}
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1">
        InputRadioGroupL Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        InputRadioGroupL Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupXL"
    dimension={InputRadioGroup.REFS.DIMENSIONS.XL}
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1">
        InputRadioGroupXL Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        InputRadioGroupXL Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

### InputRadioGroup with Long text

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupLongXS"
    dimension={InputRadioGroup.REFS.DIMENSIONS.XS}
    value="1"
  >
    <div>
      <InputRadioGroup.Option value="1">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupLongS"
    dimension={InputRadioGroup.REFS.DIMENSIONS.S}
    value="1"
  >
    <div>
      <InputRadioGroup.Option value="1">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupLongM"
    dimension={InputRadioGroup.REFS.DIMENSIONS.M}
    value="1"
  >
    <div>
      <InputRadioGroup.Option value="1">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupLongL"
    dimension={InputRadioGroup.REFS.DIMENSIONS.L}
    value="1"
  >
    <div>
      <InputRadioGroup.Option value="1">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupLongXL"
    dimension={InputRadioGroup.REFS.DIMENSIONS.XL}
    value="1"
  >
    <div>
      <InputRadioGroup.Option value="1">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
        fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies
        sem, eu pretium risus. Integer eu lectus venenatis purus lobortis
        ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce
        venenatis egestas scelerisque. In hac habitasse platea dictumst.
        Mauris ut ex a elit auctor suscipit in ut nibh.
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

### InputRadioGroup inline

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupInline"
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div className="inline">
      <InputRadioGroup.Option value="1">
        InputRadioGroupS Option 1
      </InputRadioGroup.Option>
    </div>
    <div className="inline">
      <InputRadioGroup.Option value="2">
        InputRadioGroupS Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

### InputRadioGroup with isWarning and isError flags

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupWithWarning"
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1" isWarning>
        InputRadioGroupS Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2" isWarning>
        InputRadioGroupS Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupWithError"
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1" isError>
        InputRadioGroupS Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2" isError>
        InputRadioGroupS Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

### InputRadioGroup disabled

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupDisabled"
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1" disabled>
        InputRadioGroupS Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2" disabled>
        InputRadioGroupS Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupDisabledWithWarning"
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1" disabled isWarning>
        InputRadioGroupS Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2" disabled isWarning>
        InputRadioGroupS Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

```js
<div className="list">
  <InputRadioGroup
    id="InputRadioGroupDisabledWithError"
    value="1"
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
  >
    <div>
      <InputRadioGroup.Option value="1" disabled isError>
        InputRadioGroupS Option 1
      </InputRadioGroup.Option>
    </div>
    <div>
      <InputRadioGroup.Option value="2" disabled isError>
        InputRadioGroupS Option 2
      </InputRadioGroup.Option>
    </div>
  </InputRadioGroup>
</div>
```

