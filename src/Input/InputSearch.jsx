import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import InputText from './InputText';
import Popup, { Opener, Box } from '../Popup';
import InputSearchField from './_internal/InputSearchField';
import InputOptions from './_internal/InputOptions';
import { TYPES } from './_constants';
import { INPUT_SELECT_REFS as REFS } from './_config';
import { getOptionTitle } from './_utils';

export default class InputSearch extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.SEARCH;

  static propTypes = {
    /** InputElement Component (you can use only components, were value is string
     * (one of InputAccount, InputDigital, InputPhone, InputText, InputTextArea)
     */
    inputElement: PropTypes.func,
    /** Input Locale */
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component value (= value of selected option) */
    value: PropTypes.string,
    /** Component search Value (for inputElement) */
    searchValue: PropTypes.string,
    /** Array of options (ATTENTION: values must be unique) */
    options: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
      }),
    ).isRequired,
    /** Placeholder */
    placeholder: PropTypes.string,
    /** Max-length */
    maxLength: PropTypes.number,
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Align text */
    textAlign: PropTypes.oneOf(Object.values(REFS.TEXT_ALIGN)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Text for not found state (if empty options array) */
    notFoundLabel: PropTypes.string,
    /** Align of PopupBox */
    popupBoxAlign: PropTypes.oneOf(Object.values(REFS.POPUP_BOX_ALIGN)),
    /** Style of PopupBox (e.g. { width: 'auto', minWidth: '100%', height: 300 }) */
    popupBoxStyle: PropTypes.shape(),
    /** Cleanable value flag */
    isCleanable: PropTypes.bool,
    /** Searching process flag */
    isSearching: PropTypes.bool,
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onBlur: PropTypes.func,
    /** Change handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onChange: PropTypes.func,
    /** Search handler
     * @param {string} id Component Id
     * @param {string} value Search value
     * */
    onSearch: PropTypes.func.isRequired,
    /** Function for custom option renderer
     * @param {string} id Component Id
     * @param {object} option Option to render
     * */
    optionRenderer: PropTypes.func,
    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    inputElement: InputText,
    locale: REFS.LOCALES.RU,
    value: null,
    searchValue: null,
    placeholder: null,
    maxLength: null,
    dimension: REFS.DIMENSIONS.M,
    textAlign: REFS.TEXT_ALIGN.LEFT,
    width: '100%',
    notFoundLabel: null,
    popupBoxAlign: REFS.POPUP_BOX_ALIGN.START,
    popupBoxStyle: null,
    isCleanable: false,
    isSearching: false,
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    optionRenderer: ({ option }) => option && option.title,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      isOpened: false,
      disallowBlurFlag: false,
      defaultValue: this.props.value,
      value: this.props.value,
      active: null,
      inputElementValue: this.props.searchValue || getOptionTitle(this.props.value, this.props.options),
      inputElementFormattedValue: '',
      inputNodeWidth: null,
    };

    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.isInitialized || props.value !== state.defaultValue) {
      return {
        isInitialized: true,
        defaultValue: props.value,
        value: props.value,
        active: props.value || (props.options.length && props.options[0].value) || null,
        inputElementValue: props.searchValue || getOptionTitle(props.value, props.options),
      };
    }
    if (!props.options.find(option => option.value === state.active)) {
      return {
        active: (props.options.length && props.options[0].value) || null,
      };
    }
    return null;
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown);
    document.addEventListener('mouseup', this.allowBlur);
    window.addEventListener('resize', this.setOffset);
    setTimeout(this.setOffset, 0);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
    document.removeEventListener('mouseup', this.allowBlur);
    window.removeEventListener('resize', this.setOffset);
  }

  onInputElementFocus = ({ value }) => {
    if (!this.state.isFocused && !this.props.disabled) {
      this.handleSearch(value);
      this.handleFocus();
    }
  };

  onInputElementBlur = () => {
    if (!this.state.disallowBlurFlag) {
      this.handleBlur();
    }
  };

  onInputElementChange = ({ value, formattedValue }) => {
    this.setState({
      isOpened: true,
      value: null,
      active: this.props.options.length ? this.props.options[0].value : null,
      inputElementValue: value,
      inputElementFormattedValue: formattedValue,
    });
    this.handleSearch(value);
  };

  onInputElementClick = () => {
    if (!this.state.isOpened && !this.props.disabled) this.setState({ isOpened: true });
  };

  onCleanElementClick = e => {
    e.preventDefault();
    e.stopPropagation();
    this.handleChange(null);
  };

  onPopupClose = () => {
    this.setState({ isOpened: false });
  };

  onOptionClick = value => {
    this.createCustomEvent();
    this.handleChange(value);
    this.onPopupClose();
  };

  onOptionMouseEnter = value => {
    this.setState({ active: value });
  };

  onOptionMouseLeave = () => {
    this.setState({ active: null });
  };

  setOffset = () => {
    if (!this.inputNode || !this.inputNode.current) return;
    const inputNodeBCRect = this.inputNode.current.inputNode.current.getBoundingClientRect();

    this.setState({
      inputNodeWidth: inputNodeBCRect.width,
    });
  };

  getOptions = () =>
    this.props.options.map(option => ({
      ...option,
      isSelected: option.value === this.state.value,
      isActive: option.value === this.state.active,
      searchValue: this.state.inputElementFormattedValue,
    }));

  createCustomEvent = () => {
    const event = document.createEvent('Event');
    event.initEvent('rboComponentsInputChange', true, true);
    document.body.dispatchEvent(event);
    this.inputNode.current.inputNode.current.dispatchEvent(event);
  };

  disallowBlur = () => {
    this.setState({ disallowBlurFlag: true });
  };

  allowBlur = () => {
    if (this.state.disallowBlurFlag) {
      this.setState({ disallowBlurFlag: false });
      this.inputNode.current.inputNode.current.focus();
    }
  };

  handleKeyDown = e => {
    if (this.state.isFocused) {
      switch (e.which) {
        case 38: // arrow up
          if (this.state.isOpened) {
            e.preventDefault();
            this.activatePrevOption();
          }
          break;
        case 40: // arrow down
          if (this.state.isOpened) {
            e.preventDefault();
            this.activateNextOption();
          }
          break;
        case 13: // enter
          if (this.state.isOpened) {
            e.preventDefault();
            this.selectActiveOption();
          } else {
            this.setState({ isOpened: true });
          }
          break;
        default:
          break;
      }
    }
  };

  activatePrevOption = () => {
    const active = this.state.active || this.state.value;
    const activeOptionIndex = this.props.options.findIndex(option => option.value === active);
    const prevOptionIndex = activeOptionIndex <= 0 ? this.props.options.length - 1 : activeOptionIndex - 1;
    const prevOptionValue = this.props.options[prevOptionIndex] && this.props.options[prevOptionIndex].value;
    this.setState({ active: prevOptionValue });
  };

  activateNextOption = () => {
    const active = this.state.active || this.state.value;
    const activeOptionIndex = this.props.options.findIndex(option => option.value === active);
    const nextOptionIndex = activeOptionIndex >= this.props.options.length - 1 ? 0 : activeOptionIndex + 1;
    const nextOptionValue = this.props.options[nextOptionIndex] && this.props.options[nextOptionIndex].value;
    this.setState({ active: nextOptionValue });
  };

  selectActiveOption = () => {
    this.createCustomEvent();
    this.handleChange(this.state.active || this.state.value);
    this.onPopupClose();
  };

  handlePopupCloseByOptionRendererClick = () => {
    this.setState({ disallowBlurFlag: false, value: this.state.defaultValue });
    this.onPopupClose();
  };

  handleFocus = () => {
    this.setState({ isFocused: true, isOpened: true });

    if (!this.state.active) {
      this.setState({
        active: this.state.value || (this.props.options.length && this.props.options[0].value) || null,
      });
    }

    this.props.onFocus({ id: this.props.id, value: this.state.value });
  };

  handleBlur = () => {
    this.setState({ isFocused: false, isOpened: false });
    const blurValue = this.checkOnBlurValue();

    this.props.onBlur({ id: this.props.id, value: blurValue });
  };

  handleChange = (value, isOnBlur) => {
    const { defaultValue } = this.state;
    const inputElementValue = getOptionTitle(value, this.props.options);
    this.setState({
      defaultValue: value,
      value,
      active: value,
      inputElementValue,
      inputElementFormattedValue: '',
    });

    this.handleSearch(inputElementValue);

    if (value !== defaultValue || isOnBlur) {
      this.props.onChange({ id: this.props.id, value });
    }
  };

  handleSearch = value => {
    this.props.onSearch({ id: this.props.id, value: value || '' });
  };

  checkOnBlurValue = () => {
    const optionByTitle = this.props.options.find(option => option.title === this.state.inputElementValue);
    const blurValue = optionByTitle ? optionByTitle.value : this.state.value;
    this.handleChange(blurValue, true);
    return blurValue;
  };

  render() {
    const inputSearchFieldProps = {
      type: 'search',
      inputElement: this.props.inputElement,
      id: this.props.id,
      value: this.state.inputElementValue,
      placeholder: this.props.placeholder,
      maxLength: this.props.maxLength,
      dimension: this.props.dimension,
      textAlign: this.props.textAlign,
      width: this.props.width,
      isFocused: this.state.isFocused,
      isCleanable: this.props.isCleanable,
      isSearching: this.props.isSearching,
      isWarning: this.props.isWarning,
      isError: this.props.isError,
      disabled: this.props.disabled,
      onInputElementFocus: this.onInputElementFocus,
      onInputElementBlur: this.onInputElementBlur,
      onInputElementChange: this.onInputElementChange,
      onInputElementClick: this.onInputElementClick,
      onCleanElementClick: this.onCleanElementClick,
      onIconElementMouseDown: this.disallowBlur,
      onIconElementMouseUp: this.allowBlur,
      setInputNode: this.props.setInputNode,
    };

    const inputOptionsProps = {
      locale: this.props.locale,
      id: this.props.id,
      dimension: this.props.dimension,
      popupBoxStyle: this.props.popupBoxStyle,
      inputNodeWidth: this.state.inputNodeWidth,
      onPopupElementMouseDown: this.disallowBlur,
      onPopupElementMouseUp: this.allowBlur,
      items: this.getOptions(),
      emptyLabel: this.props.notFoundLabel,
      optionRenderer: this.props.optionRenderer,
      onOptionClick: this.onOptionClick,
      onOptionMouseEnter: this.onOptionMouseEnter,
      onOptionMouseLeave: this.onOptionMouseLeave,
      handlePopupClose: this.handlePopupCloseByOptionRendererClick,
    };

    return (
      <Popup isOpened={!this.props.disabled && this.state.isOpened} onClose={this.onPopupClose}>
        <Opener display={Opener.REFS.DISPLAY.BLOCK} isNotAutoOpen>
          <InputSearchField {...inputSearchFieldProps} ref={this.inputNode} />
        </Opener>
        <Box align={this.props.popupBoxAlign}>
          <InputOptions {...inputOptionsProps} />
        </Box>
      </Popup>
    );
  }
}
