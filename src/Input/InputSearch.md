### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
  POPUP_BOX_ALIGN = {
    START: 'start',
    END: 'end',
  };
}
```

### InputSearch Example
```jsx
const InputSearchExample = require('./__examples__/InputSearch.example').default;

<div className="list">
  <InputSearchExample
    locale={InputSearch.REFS.LOCALES.EN}
    id="InputSearchS"
    value={'1'}
    options={[
      { value: '1', title: 'Title 1'},
      { value: '2', title: 'Title 2'},
      { value: '3', title: 'Title 3'},
      { value: '4', title: 'Title 4'},
      { value: '5', title: 'Title 5'},
      { value: '6', title: 'Title 6'},
      { value: '7', title: 'Title 7'},
      { value: '8', title: 'Title 8'},
      { value: '9', title: 'Title 9'},
      { value: '10', title: 'Title 10'},
      { value: '11', title: 'Title 11'},
      { value: '12', title: 'Title 12'},
      { value: '13', title: 'Title 13'},
      { value: '14', title: 'Title 14'},
      { value: '15', title: 'Title 15'},
    ]}
    isCleanable
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value })=>{ console.log('change:', id, value); }}
    onSearch={({ id, value })=>{ console.log('search:', id, value); }}
  />
</div>
```
#### InputSearch Example Source Code
```js { "file": "./__examples__/InputSearch.example.jsx" }
```

### InputSearch Dimensions
```js
<div className="list">
  <div>
    <InputSearch
      id="InputSearchXS"
      value={'1'}
      dimension={InputSearch.REFS.DIMENSIONS.XS}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSearch
      id="InputSearchS"
      value={'1'}
      dimension={InputSearch.REFS.DIMENSIONS.S}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSearch
      id="InputSearchM"
      value={'2'}
      dimension={InputSearch.REFS.DIMENSIONS.M}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSearch
      id="InputSearchL"
      value={'14'}
      dimension={InputSearch.REFS.DIMENSIONS.L}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSearch
      id="InputSearchXL"
      value={'14'}
      dimension={InputSearch.REFS.DIMENSIONS.XL}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
</div>
```

### InputSearch with placeholder
```js
<div className="list">
  <div>
    <InputSearch
      id="InputSearchWithPlaceholder"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      placeholder="Placeholder"
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
</div>
```

### InputSearch with isWarning and isError flags
```js
<div className="list">
  <div>
    <InputSearch
      id="InputSearchWithWarning"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      isWarning
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSearch
      id="InputSearchWithError"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      isError
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
     />
  </div>
</div>
```

### InputSearch with isCleanable
```js
<div className="list">
  <div>
    <InputSearch
      id="InputSearchWithIsCleanable"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      isCleanable
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
</div>
```

### InputSearch disabled
```js
<div className="list">
  <div>
    <InputSearch
      id="InputSearchDisabled"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      disabled
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSearch
      id="InputSearchDisabledWithPlaceholder"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      placeholder="Placeholder"
      disabled
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSearch
      id="InputSearchDisabledWithValue"
      value={'1'}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      disabled
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
</div>
```

### InputSearch Account
```js
<InputSearch
  id="InputSearchAccount"
  inputElement={InputAccount}
  options={[
    { value: '1', title: '11111111111111111111'},
    { value: '2', title: '22222222222222222222'},
    { value: '3', title: '33333333333333333333'},
  ]}
  onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
  onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
  onChange={({ id, value })=>{ console.log('change:', id, value); }}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```

### InputSearch Digital
```js
<InputSearch
  id="InputSearchDigital"
  inputElement={InputDigital}
  options={[
    { value: '1', title: '111111'},
    { value: '2', title: '222222'},
    { value: '3', title: '333333'},
  ]}
  onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
  onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
  onChange={({ id, value })=>{ console.log('change:', id, value); }}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```

### InputSearch Phone
```js
<InputSearch
  id="InputSearchPhone"
  inputElement={InputPhone}
  options={[
    { value: '1', title: '+11111111111'},
    { value: '2', title: '+22222222222'},
    { value: '3', title: '+33333333333'},
  ]}
  onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
  onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
  onChange={({ id, value })=>{ console.log('change:', id, value); }}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```

### InputSearch TextArea
```js
<InputSearch
  id="InputSearchTextArea"
  inputElement={InputTextArea}
  options={[
    { value: '1', title: '11111111111'},
    { value: '2', title: '22222222222'},
    { value: '3', title: '33333333333'},
  ]}
  onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
  onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
  onChange={({ id, value })=>{ console.log('change:', id, value); }}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```

### InputSearch Empty
```js
<InputSearch
  id="InputSearchEmpty"
  options={[]}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```
