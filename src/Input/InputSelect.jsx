import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Popup, { Opener, Box } from '../Popup';
import InputSelectField from './_internal/InputSelectField';
import InputOptions from './_internal/InputOptions';
import { TYPES } from './_constants';
import { INPUT_SELECT_REFS as REFS } from './_config';

export default class InputSelect extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.SELECT;

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component value (= value of selected option) */
    value: PropTypes.string,
    /** Array of options (ATTENTION: values must be unique) */
    options: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
      }),
    ).isRequired,
    /** Placeholder */
    placeholder: PropTypes.string,
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Align of PopupBox */
    popupBoxAlign: PropTypes.oneOf(Object.values(REFS.POPUP_BOX_ALIGN)),
    /** Style of PopupBox (e.g. { width: 'auto', minWidth: '100%', height: 300 }) */
    popupBoxStyle: PropTypes.shape(),
    /** Cleanable value flag */
    isCleanable: PropTypes.bool,
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onBlur: PropTypes.func,
    /** Change handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onChange: PropTypes.func,
    /** Function for custom value renderer
     * @param {string} id Component Id
     * @param {object} option Option to render
     * */
    valueRenderer: PropTypes.func,
    /** Function for custom option renderer
     * @param {string} id Component Id
     * @param {object} option Option to render
     * */
    optionRenderer: PropTypes.func,
    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    placeholder: null,
    dimension: REFS.DIMENSIONS.M,
    width: '100%',
    popupBoxAlign: REFS.POPUP_BOX_ALIGN.START,
    popupBoxStyle: null,
    isCleanable: false,
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    valueRenderer: ({ option }) => option && option.title,
    optionRenderer: ({ option }) => option && option.title,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      isOpened: false,
      disallowBlurFlag: false,
      value: this.props.value,
      active: null,
      inputNodeWidth: null,
    };

    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.isInitialized || props.value !== state.value) {
      return {
        isInitialized: true,
        value: props.value,
        active: props.value,
      };
    }
    return null;
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown);
    document.addEventListener('mouseup', this.allowBlur);
    window.addEventListener('resize', this.setOffset);
    setTimeout(this.setOffset, 0);

    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  componentDidUpdate() {
    if (this.props.disabled && this.state.isFocused) this.handleBlur();
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
    document.removeEventListener('mouseup', this.allowBlur);
    window.removeEventListener('resize', this.setOffset);

    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  onElementClick = () => {
    if (this.state.isOpened) this.onPopupClose();
    else this.onPopupOpen();
  };

  onInputElementClick = e => {
    e.preventDefault();
  };

  onInputElementFocus = () => {
    if (!this.state.isFocused && !this.props.disabled) this.handleFocus();
  };

  onInputElementBlur = () => {
    if (!this.state.disallowBlurFlag) this.handleBlur();
  };

  onCleanElementClick = e => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({ isOpened: false });
    this.handleChange(null);
  };

  onPopupOpen = () => {
    if (!this.props.disabled) this.setState({ isOpened: true });
  };

  onPopupClose = () => {
    this.setState({ isOpened: false });
  };

  onOptionClick = value => {
    this.createCustomEvent();
    this.handleChange(value);
    this.onPopupClose();
  };

  onOptionMouseEnter = value => {
    this.setState({ active: value });
  };

  onOptionMouseLeave = () => {
    this.setState({ active: null });
  };

  setOffset = () => {
    if (!this.inputNode || !this.inputNode.current) return;
    const inputNodeBCRect = this.inputNode.current.getBoundingClientRect();

    this.setState({
      inputNodeWidth: inputNodeBCRect.width,
    });
  };

  getOptions = () =>
    this.props.options.map(option => ({
      ...option,
      isSelected: option.value === this.state.value,
      isActive: option.value === this.state.active,
    }));

  createCustomEvent = () => {
    const event = document.createEvent('Event');
    event.initEvent('rboComponentsInputChange', true, true);
    document.body.dispatchEvent(event);
    this.inputNode.current.dispatchEvent(event);
  };

  disallowBlur = () => {
    this.setState({ disallowBlurFlag: true });
  };

  allowBlur = () => {
    if (this.state.disallowBlurFlag) {
      this.setState({ disallowBlurFlag: false });
      this.inputNode.current.focus();
    }
  };

  handleKeyDown = e => {
    if (this.state.isFocused) {
      switch (e.which) {
        case 38: // arrow up
          e.preventDefault();
          this.activatePrevOption();
          break;
        case 40: // arrow down
          e.preventDefault();
          this.activateNextOption();
          break;
        case 13: // enter
          if (this.state.isOpened) {
            e.preventDefault();
            this.selectActiveOption();
          }
          break;
        case 8: // backspace
          if (this.props.isCleanable && !this.state.isOpened && this.state.value) {
            this.handleChange(null);
          }
          break;
        default:
          break;
      }
    }
  };

  activatePrevOption = () => {
    const active = this.state.active || this.state.value;
    const activeOptionIndex = this.props.options.findIndex(option => option.value === active);
    const prevOptionIndex = activeOptionIndex <= 0 ? this.props.options.length - 1 : activeOptionIndex - 1;
    const prevOptionValue = this.props.options[prevOptionIndex].value;
    if (!this.state.isOpened) this.handleChange(prevOptionValue);
    else this.setState({ active: prevOptionValue });
  };

  activateNextOption = () => {
    const active = this.state.active || this.state.value;
    const activeOptionIndex = this.props.options.findIndex(option => option.value === active);
    const nextOptionIndex = activeOptionIndex >= this.props.options.length - 1 ? 0 : activeOptionIndex + 1;
    const nextOptionValue = this.props.options[nextOptionIndex].value;
    if (!this.state.isOpened) this.handleChange(nextOptionValue);
    else this.setState({ active: nextOptionValue });
  };

  selectActiveOption = () => {
    this.createCustomEvent();
    this.handleChange(this.state.active);
    this.onPopupClose();
  };

  handlePopupCloseByOptionRendererClick = () => {
    this.setState({ disallowBlurFlag: false });
    this.onPopupClose();
  };

  handleFocus = () => {
    this.setState({ isFocused: true });

    this.props.onFocus({ id: this.props.id, value: this.state.value });
  };

  handleBlur = () => {
    this.setState({ isFocused: false, isOpened: false });

    this.props.onBlur({ id: this.props.id, value: this.state.value });
  };

  handleChange = value => {
    const stateValue = this.state.value;
    this.setState({
      value,
      active: value,
    });

    if (value !== stateValue) {
      this.props.onChange({ id: this.props.id, value });
    }
  };

  render() {
    const inputSelectFieldProps = {
      id: this.props.id,
      value: this.state.value,
      selectedOption: this.props.options.find(option => option.value === this.state.value),
      placeholder: this.props.placeholder,
      dimension: this.props.dimension,
      width: this.props.width,
      isFocused: this.state.isFocused,
      isCleanable: this.props.isCleanable,
      isWarning: this.props.isWarning,
      isError: this.props.isError,
      disabled: this.props.disabled,
      valueRenderer: this.props.valueRenderer,
      onElementClick: this.onElementClick,
      onInputElementFocus: this.onInputElementFocus,
      onInputElementBlur: this.onInputElementBlur,
      onInputElementClick: this.onInputElementClick,
      onCleanElementClick: this.onCleanElementClick,
      onIconElementMouseDown: this.disallowBlur,
      onIconElementMouseUp: this.allowBlur,
      setInputNode: this.props.setInputNode,
    };

    const inputOptionsProps = {
      id: this.props.id,
      dimension: this.props.dimension,
      popupBoxStyle: this.props.popupBoxStyle,
      inputNodeWidth: this.state.inputNodeWidth,
      onPopupElementMouseDown: this.disallowBlur,
      onPopupElementMouseUp: this.allowBlur,
      items: this.getOptions(),
      optionRenderer: this.props.optionRenderer,
      onOptionClick: this.onOptionClick,
      onOptionMouseEnter: this.onOptionMouseEnter,
      onOptionMouseLeave: this.onOptionMouseLeave,
      handlePopupClose: this.handlePopupCloseByOptionRendererClick,
    };

    return (
      <Popup isOpened={!this.props.disabled && this.state.isOpened} onClose={this.onPopupClose}>
        <Opener display={Opener.REFS.DISPLAY.BLOCK} isNotAutoOpen>
          <InputSelectField {...inputSelectFieldProps} ref={this.inputNode} />
        </Opener>
        <Box align={this.props.popupBoxAlign}>
          <InputOptions {...inputOptionsProps} />
        </Box>
      </Popup>
    );
  }
}
