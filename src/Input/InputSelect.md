### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
  POPUP_BOX_ALIGN = {
    START: 'start',
    END: 'end',
  };
}
```

### InputSelect Dimensions
```js
<div className="list">
  <div>
    <InputSelect
      id="InputSelectXS"
      dimension={InputSelect.REFS.DIMENSIONS.XS}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputSelect
      id="InputSelectS"
      dimension={InputSelect.REFS.DIMENSIONS.S}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputSelect
      id="InputSelectM"
      dimension={InputSelect.REFS.DIMENSIONS.M}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      popupBoxStyle={{ width: 320 }}
    />
  </div>
  <div>
    <InputSelect
      id="InputSelectL"
      dimension={InputSelect.REFS.DIMENSIONS.L}
      value={'14'}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputSelect
      id="InputSelectXL"
      dimension={InputSelect.REFS.DIMENSIONS.XL}
      value={'14'}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
</div>
```

### InputSelect with popupBoxStyle & popupBoxAlign end
```js
<div className="list">
  <div>
    <InputSelect
      id="InputSelectWithStyleAndAlign1"
      value='1'
      options={[
        { value: '1', title: 'InputSelect with popupBoxStyle And popupBoxAlign Right Title 1'},
        { value: '2', title: 'InputSelect with popupBoxStyle And popupBoxAlign Right Title 2'},
      ]}
      popupBoxAlign={InputSelect.REFS.POPUP_BOX_ALIGN.END}
      popupBoxStyle={{ width: 'auto' }}
    />
  </div>
  <div>
    <InputSelect
      id="InputSelectWithStyleAndAlign2"
      value='1'
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
        { value: '3', title: 'Title 3'},
        { value: '4', title: 'Title 4'},
        { value: '5', title: 'Title 5'},
        { value: '6', title: 'Title 6'},
        { value: '7', title: 'Title 7'},
        { value: '8', title: 'Title 8'},
        { value: '9', title: 'Title 9'},
      ]}
      popupBoxStyle={{ width: 320, height: 200 }}
    />
  </div>
</div>
```

### InputSelect with value & valueRenderer
```js
<div className="list">
  <div>
    <InputSelect
      id="InputSelectWithValue"
      value='1'
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
    />
  </div>
  <div>
    <InputSelect
      id="InputSelectWithValueRenderer"
      value='2'
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      valueRenderer={({ option }) => (<div>Выбранное значение: <strong>{option.title}</strong></div>)}
    />
  </div>
</div>
```

### InputSelect with isCleanable
```js
<div className="list">
  <div>
    <InputSelect
      id="InputSelectWithIsCleanable"
      value='1'
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      isCleanable
    />
  </div>
</div>
```

### InputSelect with placeholder
```js
<div className="list">
  <div>
    <InputSelect
      id="InputSelectWithPlaceholder"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      placeholder="Placeholder"
    />
  </div>
</div>
```

### InputSelect with isWarning & isError flags
```js
<div className="list">
  <div>
    <InputSelect
      id="InputSelectWithWarning"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      isWarning
    />
  </div>
  <div>
    <InputSelect
      id="InputSelectWithError"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      isError
     />
  </div>
</div>
```

### InputSelect disabled
```js
<div className="list">
  <div>
    <InputSelect
      id="InputSelectDisabled"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      disabled
    />
  </div>
  <div>
    <InputSelect
      id="InputSelectDisabledWithPlaceholder"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      placeholder="Placeholder"
      disabled
    />
  </div>
  <div>
    <InputSelect
      id="InputSelectDisabledWithValue"
      value='1'
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      disabled
    />
  </div>
</div>
```
