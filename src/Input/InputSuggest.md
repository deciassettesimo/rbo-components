### REFS
```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
  POPUP_BOX_ALIGN = {
    START: 'start',
    END: 'end',
  };
}
```

### InputSuggest Example
```jsx
const InputSuggestExample = require('./__examples__/InputSuggest.example').default;

<div className="list">
  <InputSuggestExample
    locale={InputSuggest.REFS.LOCALES.EN}
    id="InputSearchS"
    value={'Title 1'}
    options={[
      { value: '1', title: 'Title 1'},
      { value: '2', title: 'Title 2'},
      { value: '3', title: 'Title 3'},
      { value: '4', title: 'Title 4'},
      { value: '5', title: 'Title 5'},
      { value: '6', title: 'Title 6'},
      { value: '7', title: 'Title 7'},
      { value: '8', title: 'Title 8'},
      { value: '9', title: 'Title 9'},
      { value: '10', title: 'Title 10'},
      { value: '11', title: 'Title 11'},
      { value: '12', title: 'Title 12'},
      { value: '13', title: 'Title 13'},
      { value: '14', title: 'Title 14'},
      { value: '15', title: 'Title 15'},
    ]}
    onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
    onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
    onChange={({ id, value, optionValue })=>{ console.log('change:', id, value, optionValue); }}
    onSearch={({ id, value })=>{ console.log('search:', id, value); }}
  />
</div>
```
#### InputSuggest Example Source Code
```js { "file": "./__examples__/InputSuggest.example.jsx" }
```

### InputSuggest Dimensions
```js
<div className="list">
  <div>
    <InputSuggest
      id="InputSuggestXS"
      value={'Title 1'}
      dimension={InputSuggest.REFS.DIMENSIONS.XS}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSuggest
      id="InputSuggestS"
      value={'Title 1'}
      dimension={InputSuggest.REFS.DIMENSIONS.S}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSuggest
      id="InputSuggestM"
      value={'Title 2'}
      dimension={InputSuggest.REFS.DIMENSIONS.M}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      popupBoxStyle={{ width: 320 }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSuggest
      id="InputSuggestL"
      value={'Title 14'}
      dimension={InputSuggest.REFS.DIMENSIONS.L}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSuggest
      id="InputSuggestXL"
      value={'Title 14'}
      dimension={InputSuggest.REFS.DIMENSIONS.XL}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
</div>
```

### InputSuggest with placeholder
```js
<div className="list">
  <div>
    <InputSuggest
      id="InputSuggestWithPlaceholder"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      placeholder="Placeholder"
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
</div>
```

### InputSuggest with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputSuggest
      id="InputSuggestWithWarning"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      isWarning
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSuggest
      id="InputSuggestWithError"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      isError
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
     />
  </div>
</div>
```

### InputSuggest disabled
```js
<div className="list">
  <div>
    <InputSuggest
      id="InputSuggestDisabled"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      disabled
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSuggest
      id="InputSuggestDisabledWithPlaceholder"
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      placeholder="Placeholder"
      disabled
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
  <div>
    <InputSuggest
      id="InputSuggestDisabledWithValue"
      value={'Title 1'}
      options={[
        { value: '1', title: 'Title 1'},
        { value: '2', title: 'Title 2'},
      ]}
      disabled
      onSearch={({ id, value })=>{ console.log('search:', id, value); }}
    />
  </div>
</div>
```

### InputSuggest Account
```js
<InputSuggest
  id="InputSuggestAccount"
  inputElement={InputAccount}
  options={[
    { value: '1', title: '11111111111111111111'},
    { value: '2', title: '22222222222222222222'},
    { value: '3', title: '33333333333333333333'},
  ]}
  onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
  onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
  onChange={({ id, value })=>{ console.log('change:', id, value); }}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```

### InputSuggest Digital
```js
<InputSuggest
  id="InputSuggestDigital"
  inputElement={InputDigital}
  options={[
    { value: '1', title: '111111'},
    { value: '2', title: '222222'},
    { value: '3', title: '333333'},
  ]}
  onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
  onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
  onChange={({ id, value })=>{ console.log('change:', id, value); }}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```

### InputSuggest Phone
```js
<InputSuggest
  id="InputSuggestPhone"
  inputElement={InputPhone}
  options={[
    { value: '1', title: '+11111111111'},
    { value: '2', title: '+22222222222'},
    { value: '3', title: '+33333333333'},
  ]}
  onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
  onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
  onChange={({ id, value })=>{ console.log('change:', id, value); }}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```

### InputSuggest TextArea
```js
<InputSuggest
  id="InputSuggestTextArea"
  inputElement={InputTextArea}
  options={[
    { value: '1', title: '11111111111'},
    { value: '2', title: '22222222222'},
    { value: '3', title: '33333333333'},
  ]}
  onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
  onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
  onChange={({ id, value })=>{ console.log('change:', id, value); }}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```

### InputSuggest Empty
```js
<InputSuggest
  id="InputSuggestEmpty"
  options={[]}
  onSearch={({ id, value })=>{ console.log('search:', id, value); }}
/>
```
