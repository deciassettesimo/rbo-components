import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import RboDocTable from '../RboDocTable';
import Button from '../Button';
import IconPlusFilled from '../Icon/IconPlusFilled';
import { INPUT_TABLE_ADD_BUTTON_ID_PREFIX, TYPES, LABELS, INPUT_TABLE_LOCALES as LOCALES } from './_constants';
import { INPUT_TABLE_REFS as REFS } from './_config';
import { StyledInputTable, StyledInputHiddenHtmlInput, StyledInputTableAdd } from './_style';

export default class InputTable extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.TABLE;

  static propTypes = {
    /** RboDocTable locale */
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component value */
    value: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        isWarning: PropTypes.bool,
        isError: PropTypes.bool,
      }),
    ),
    /** Add Button Title flag */
    addButtonTitle: PropTypes.string,
    /** Add Button click handler */
    onAddItemButtonClick: PropTypes.func,
    /** operations */
    operations: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        icon: PropTypes.string,
        viewMode: PropTypes.bool,
        editMode: PropTypes.bool,
      }),
    ),
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** ViewMode flag */
    isViewMode: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * */
    onBlur: PropTypes.func,

    /** RboDocTable columns */
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        width: PropTypes.number.isRequired,
        align: PropTypes.oneOf(Object.values(RboDocTable.REFS.COLUMNS_ALIGN)),
        label: PropTypes.string,
      }),
    ).isRequired,
    /** RboDocTable isMultiLine */
    isMultiLine: PropTypes.bool,
    /** RboDocTable emptyLabel */
    emptyLabel: PropTypes.string,
    /** RboDocTable onItemClick */
    onItemClick: PropTypes.func,
    /** RboDocTable onOperationClick */
    onOperationClick: PropTypes.func,

    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    locale: REFS.LOCALES.RU,
    value: [],
    addButtonTitle: null,
    isWarning: false,
    isError: false,
    disabled: false,
    isViewMode: false,
    onBlur: () => null,
    onFocus: () => null,

    operations: [],
    onAddItemButtonClick: null,
    isMultiLine: false,
    emptyLabel: null,
    onItemClick: null,
    onOperationClick: null,

    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      value: this.props.value,
    };

    this.node = React.createRef();
    this.inputNode = React.createRef();
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleDocumentClick, true);
    document.addEventListener('touchstart', this.handleDocumentClick, true);

    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  static getDerivedStateFromProps(props, state) {
    if (JSON.stringify(props.value) !== JSON.stringify(state.value)) return { value: props.value };
    return null;
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleDocumentClick, true);
    document.removeEventListener('touchstart', this.handleDocumentClick, true);

    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  getOperations = () => {
    if (this.props.disabled) return [];
    if (this.props.isViewMode) return this.props.operations.filter(operation => operation.viewMode);
    return this.props.operations.filter(operation => operation.editMode);
  };

  getAddButtonTitle = locale => {
    switch (locale) {
      case LOCALES.EN:
        return LABELS.EN.TABLE_ADD_BUTTON;
      case LOCALES.RU:
      default:
        return LABELS.RU.TABLE_ADD_BUTTON;
    }
  };

  handleDocumentClick = e => {
    if (this.node.current.contains(e.target)) {
      if (!this.state.isFocused) setTimeout(this.handleFocus, 0);
    } else if (this.state.isFocused) {
      this.handleBlur();
    }
  };

  handleFocus = () => {
    this.setState({ isFocused: true });
    this.props.onFocus({ id: this.props.id, value: this.state.value });
  };

  handleBlur = () => {
    this.setState({ isFocused: false });
    this.props.onBlur({ id: this.props.id, value: this.state.value });
  };

  render() {
    return (
      <StyledInputTable
        {...addDataAttributes({ component: TYPES.TABLE })}
        innerRef={ref => {
          this.node.current = ref;
        }}
        isDisabled={this.props.disabled}
        isViewMode={this.props.isViewMode}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
      >
        <StyledInputHiddenHtmlInput
          innerRef={ref => {
            this.inputNode.current = ref;
          }}
          id={this.props.id}
          disabled={this.props.disabled}
          onFocus={this.handleFocus}
        />
        <RboDocTable
          locale={this.props.locale}
          items={this.state.value}
          columns={this.props.columns}
          operations={this.getOperations()}
          isMultiLine={this.props.isMultiLine}
          emptyLabel={this.props.emptyLabel}
          onItemClick={this.props.disabled ? null : this.props.onItemClick}
          onOperationClick={this.props.disabled ? null : this.props.onOperationClick}
        />
        {!this.props.disabled &&
          !this.props.isViewMode &&
          this.props.onAddItemButtonClick && (
            <StyledInputTableAdd>
              <Button
                id={`${INPUT_TABLE_ADD_BUTTON_ID_PREFIX}${this.props.id}`}
                dimension={Button.REFS.DIMENSIONS.XS}
                type={Button.REFS.TYPES.LINK}
                onClick={this.props.onAddItemButtonClick}
                isWithIcon
              >
                <IconPlusFilled dimension={IconPlusFilled.REFS.DIMENSIONS.XS} />
                <span>
                  {!this.props.addButtonTitle && this.getAddButtonTitle(this.props.locale)}
                  {this.props.addButtonTitle}
                </span>
              </Button>
            </StyledInputTableAdd>
          )}
      </StyledInputTable>
    );
  }
}
