### InputTable

```js
const columns = [
    {
      id: 'nonResidentName',
      label: 'Наименование нерезидента',
      align: 'left',
      width: 300,
    },
    {
      id: 'countryName',
      label: 'Страна',
      align: 'left',
      width: 200,
    },
    {
      id: 'countryCode',
      label: 'Код',
      align: 'right',
      width: 100,
    },
];

const operations=[
    {
      id: 'preview',
      icon: 'document',
      title: 'Просмотр',
      viewMode: true,
      editMode: false,
    },
    {
      id: 'edit',
      icon: 'edit',
      title: 'Редактировать',
      viewMode: false,
      editMode: true,
    },
    {
      id: 'copy',
      icon: 'document-copy',
      title: 'Копировать',
      viewMode: false,
      editMode: true,
    },
    {
      id: 'remove',
      icon: 'remove',
      title: 'Удалить',
      viewMode: false,
      editMode: true,
    },
];

const value = [
    {
      countryCode: "KR / 091",
      countryISOCode: "KR",
      countryName: "Республика Корея",
      id: "1",
      isError: false,
      isWarning: false,
      nonResidentName: "Корея",
    },
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан",
      id: "2",
      isError: false,
      isWarning: false,
      nonResidentName: "Азербайджан",
    },
    {
      countryCode: "RU / 007",
      countryISOCode: "RU",
      countryName: "Российская Федерация",
      id: "3",
      isError: false,
      isWarning: false,
      nonResidentName: "Россия",
    },
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан",
      id: "4",
      isError: false,
      isWarning: false,
      nonResidentName: "Азербайджан",
    },
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан",
      id: "5",
      isError: false,
      isWarning: false,
      nonResidentName: "Азербайджан",
    },
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан",
      id: "6",
      isError: false,
      isWarning: false,
      nonResidentName: "Азербайджан",
    },
];

<div className="list">
  <div>
    <InputTable
      id="InputTable"
      columns={columns}
      operations={operations}
      emptyLabel={'Нет документов'}
      onItemClick={() => {}}
      onOperationClick={() => {}}
      onAddItemButtonClick={() => {}}
      addButtonTitle={'Добавить документ'}
    />
  </div>
  <div>
    <InputTable
      id="InputTable"
      value={value}
      columns={columns}
      operations={operations}
      emptyLabel={'Нет документов'}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onItemClick={() => {}}
      onOperationClick={() => {}}
      onAddItemButtonClick={() => {}}
      addButtonTitle={'Добавить документ'}
    />
  </div>
</div>
```

### InputTable with isWarning and isError flags

```js
const columns = [
    {
      id: 'nonResidentName',
      label: 'Наименование нерезидента',
      align: 'left',
      width: 300,
    },
    {
      id: 'countryName',
      label: 'Страна',
      align: 'left',
      width: 200,
    },
    {
      id: 'countryCode',
      label: 'Код',
      align: 'right',
      width: 100,
    },
];

const operations=[
    {
      id: 'preview',
      icon: 'document',
      title: 'Просмотр',
      viewMode: true,
      editMode: false,
    },
    {
      id: 'edit',
      icon: 'edit',
      title: 'Редактировать',
      viewMode: false,
      editMode: true,
    },
    {
      id: 'copy',
      icon: 'document-copy',
      title: 'Копировать',
      viewMode: false,
      editMode: true,
    },
    {
      id: 'remove',
      icon: 'remove',
      title: 'Удалить',
      viewMode: false,
      editMode: true,
    },
];

<div className="list">
  <div>
    <InputTable
      id="InputTableWithWarning"
      columns={columns}
      operations={operations}
      onItemClick={() => {}}
      onOperationClick={() => {}}
      onAddItemButtonClick={() => {}}
      isWarning
    />
  </div>
  <div>
    <InputTable
      id="InputTableWithError"
      columns={columns}
      operations={operations}
      onItemClick={() => {}}
      onOperationClick={() => {}}
      onAddItemButtonClick={() => {}}
      isError
    />
  </div>
</div>
```

### InputTable disabled

```js
const items = [
    {
      countryCode: "KR / 091",
      countryISOCode: "KR",
      countryName: "Республика Корея",
      id: "1",
      isError: false,
      isWarning: false,
      nonResidentName: "Корея",
    },
];

const operations=[
    {
      id: 'preview',
      icon: 'document',
      title: 'Просмотр',
      viewMode: true,
      editMode: false,
    },
    {
      id: 'edit',
      icon: 'edit',
      title: 'Редактировать',
      viewMode: false,
      editMode: true,
    },
    {
      id: 'copy',
      icon: 'document-copy',
      title: 'Копировать',
      viewMode: false,
      editMode: true,
    },
    {
      id: 'remove',
      icon: 'remove',
      title: 'Удалить',
      viewMode: false,
      editMode: true,
    },
];

const columns = [
    {
      id: 'nonResidentName',
      label: 'Наименование нерезидента',
      align: 'left',
      width: 300,
    },
    {
      id: 'countryName',
      label: 'Страна',
      align: 'left',
      width: 200,
    },
    {
      id: 'countryCode',
      label: 'Код',
      align: 'right',
      width: 100,
    },
];

<div className="list">
  <div>
    <InputTable
      id="InputTableDisabledWithoutValue"
      columns={columns}
      operations={operations}
      onItemClick={() => {}}
      onOperationClick={() => {}}
      onAddItemButtonClick={() => {}}
      disabled
    />
  </div>
  <div>
    <InputTable
      id="InputTableDisabledWithValue"
      columns={columns}
      operations={operations}
      disabled
      value={items}
      onItemClick={() => {}}
      onOperationClick={() => {}}
      onAddItemButtonClick={() => {}}
    />
  </div>
</div>
```

### InputTable view mode

```js
const items = [
    {
      countryCode: "KR / 091",
      countryISOCode: "KR",
      countryName: "Республика Корея",
      id: "1",
      isError: false,
      isWarning: false,
      nonResidentName: "Корея",
    },
];

const operations=[
    {
      id: 'preview',
      icon: 'document',
      title: 'Просмотр',
      viewMode: true,
      editMode: false,
    },
    {
      id: 'edit',
      icon: 'edit',
      title: 'Редактировать',
      viewMode: false,
      editMode: true,
    },
    {
      id: 'copy',
      icon: 'document-copy',
      title: 'Копировать',
      viewMode: false,
      editMode: true,
    },
    {
      id: 'remove',
      icon: 'remove',
      title: 'Удалить',
      viewMode: false,
      editMode: true,
    },
];

const columns = [
    {
      id: 'nonResidentName',
      label: 'Наименование нерезидента',
      align: 'left',
      width: 300,
    },
    {
      id: 'countryName',
      label: 'Страна',
      align: 'left',
      width: 200,
    },
    {
      id: 'countryCode',
      label: 'Код',
      align: 'right',
      width: 100,
    },
];

<div className="list">
  <div>
    <InputTable
      id="InputTableViewModeWithoutValue"
      columns={columns}
      operations={operations}
      onOperationClick={() => {}}
      onAddItemButtonClick={() => {}}
      isViewMode
    />
  </div>
  <div>
    <InputTable
      id="InputTableViewModeWithValue"
      columns={columns}
      operations={operations}
      onOperationClick={() => {}}
      onAddItemButtonClick={() => {}}
      isViewMode
      value={items}
    />
  </div>
</div>
```
