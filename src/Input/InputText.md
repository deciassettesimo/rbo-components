### REFS

```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputText Dimensions

```js
<div className="list">
  <div>
    <InputText
      id="InputTextXS"
      dimension={InputText.REFS.DIMENSIONS.XS}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputText
      id="InputTextS"
      dimension={InputText.REFS.DIMENSIONS.S}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputText
      id="InputTextM"
      dimension={InputText.REFS.DIMENSIONS.M}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputText
      id="InputTextL"
      dimension={InputText.REFS.DIMENSIONS.L}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
  <div>
    <InputText
      id="InputTextXL"
      dimension={InputText.REFS.DIMENSIONS.XL}
      onFocus={({ id, value })=>{ console.log('focus:', id, value); }}
      onBlur={({ id, value })=>{ console.log('blur:', id, value); }}
      onChange={({ id, value })=>{ console.log('change:', id, value); }}
    />
  </div>
</div>
```

### InputText with placeholder

```js
<div className="list">
  <div>
    <InputText id="InputTextWithPlaceholder" placeholder="Placeholder" />
  </div>
</div>
```

### InputText with maxLength (for example 10)

```js
<div className="list">
  <div>
    <InputText id="InputTextWithMaxLength" maxLength={10} value="1234567890" />
  </div>
</div>
```

### InputText with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputText id="InputTextWithWarning" value="Поле с предупреждением" isWarning />
  </div>
  <div>
    <InputText id="InputTextWithError" value="Поле с ошибкой" isError />
  </div>
</div>
```

### InputText disabled

```js
<div className="list">
  <div>
    <InputText id="InputTextDisabled" disabled />
  </div>
  <div>
    <InputText id="InputTextDisabledWithPlaceholder" placeholder="Placeholder" disabled />
  </div>
  <div>
    <InputText id="InputTextDisabledWithValue" value="Поле недоступно для редактирования" disabled />
  </div>
  <div>
    <InputText id="InputTextDisabledWithWarning" value="Поле с предупреждением недоступно для редактирования" disabled isWarning />
  </div>
  <div>
    <InputText id="InputTextDisabledWithError" value="Поле с ошибкой недоступно для редактирования" disabled isError />
  </div>
</div>
```
