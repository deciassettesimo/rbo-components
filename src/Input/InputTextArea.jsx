import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { TYPES } from './_constants';
import { INPUT_REFS as REFS } from './_config';
import { formatTextValue, deformatTextValue } from './_utils';

import { StyledInputTextArea } from './_style';

export default class InputTextArea extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.TEXT_AREA;

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component value */
    value: PropTypes.string,
    /** Placeholder */
    placeholder: PropTypes.string,
    /** Max-length */
    maxLength: PropTypes.number,
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Align text */
    textAlign: PropTypes.oneOf(Object.values(REFS.TEXT_ALIGN)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Number of rows */
    rows: PropTypes.number,
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Disabled flag */
    disabled: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onBlur: PropTypes.func,
    /** Change handler
     * @param {string} id Component Id
     * @param {string} value Current value
     * @param {string} formattedValue Current formatted value
     * */
    onChange: PropTypes.func,
    /** @ignore */
    isFocused: PropTypes.bool,
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    placeholder: null,
    maxLength: null,
    dimension: REFS.DIMENSIONS.M,
    textAlign: REFS.TEXT_ALIGN.LEFT,
    width: '100%',
    rows: 3,
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    isFocused: null,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: this.props.isFocused,
      value: this.props.value,
      formattedValue: '',
    };

    this.inputNode = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    const propsValue = props.value || null;
    if (
      !state.isInitialized ||
      propsValue !== state.value ||
      (props.isFocused !== null && props.isFocused !== state.isFocused)
    ) {
      const formattedValue = formatTextValue(props.value, props.maxLength);
      const value = deformatTextValue(formattedValue);
      return {
        isInitialized: true,
        value,
        formattedValue,
        isFocused: props.isFocused !== null ? props.isFocused : state.isFocused,
      };
    }
    return null;
  }

  componentDidMount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  componentDidUpdate() {
    if (this.props.disabled && this.state.isFocused) this.handleBlur();
  }

  componentWillUnmount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  handleFocus = () => {
    if (this.props.isFocused === null) this.setState({ isFocused: true });

    this.props.onFocus({ id: this.props.id, value: this.state.value, formattedValue: this.state.formattedValue });
  };

  handleBlur = () => {
    this.setState({ isFocused: false });

    this.props.onBlur({ id: this.props.id, value: this.state.value, formattedValue: this.state.formattedValue });
  };

  handleChange = e => {
    const { value } = e.target;
    const stateValue = this.state.value;
    const formattedValue = formatTextValue(value, this.props.maxLength);
    const deformattedValue = deformatTextValue(formattedValue);
    this.setState({
      value: deformattedValue,
      formattedValue,
    });

    if (deformattedValue !== stateValue) {
      this.props.onChange({ id: this.props.id, value: deformattedValue, formattedValue });
    }
  };

  render() {
    return (
      <StyledInputTextArea
        {...addDataAttributes({ component: TYPES.TEXT_AREA })}
        innerRef={ref => {
          this.inputNode.current = ref;
        }}
        sDimension={this.props.dimension}
        sTextAlign={this.props.textAlign}
        sWidth={this.props.width}
        sRows={this.props.rows}
        isDisabled={this.props.disabled}
        isFocused={this.state.isFocused}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
        id={this.props.id}
        value={this.state.formattedValue}
        placeholder={this.props.placeholder}
        maxLength={this.props.maxLength}
        disabled={this.props.disabled}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
      />
    );
  }
}
