### REFS

```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputTextArea Dimension XS

```js
<div className="list">
  <div>
    <InputTextArea
      id="InputTextAreaXSRows1"
      dimension={InputTextArea.REFS.DIMENSIONS.XS}
      rows={1}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaXSRows2"
      dimension={InputTextArea.REFS.DIMENSIONS.XS}
      rows={2}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaXSRows3"
      dimension={InputTextArea.REFS.DIMENSIONS.XS}
      rows={3}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaXSRows4"
      dimension={InputTextArea.REFS.DIMENSIONS.XS}
      rows={4}
    />
  </div>
</div>
```

### InputTextArea Dimension S

```js
<div className="list">
  <div>
    <InputTextArea
      id="InputTextAreaSRows1"
      dimension={InputTextArea.REFS.DIMENSIONS.S}
      rows={1}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaSRows2"
      dimension={InputTextArea.REFS.DIMENSIONS.S}
      rows={2}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaSRows3"
      dimension={InputTextArea.REFS.DIMENSIONS.S}
      rows={3}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaSRows4"
      dimension={InputTextArea.REFS.DIMENSIONS.S}
      rows={4}
    />
  </div>
</div>
```

### InputTextArea Dimension M

```js
<div className="list">
  <div>
    <InputTextArea
      id="InputTextAreaMRows1"
      dimension={InputTextArea.REFS.DIMENSIONS.M}
      rows={1}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaMRows2"
      dimension={InputTextArea.REFS.DIMENSIONS.M}
      rows={2}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaMRows3"
      dimension={InputTextArea.REFS.DIMENSIONS.M}
      rows={3}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaMRows4"
      dimension={InputTextArea.REFS.DIMENSIONS.M}
      rows={4}
    />
  </div>
</div>
```

### InputTextArea Dimension L

```js
<div className="list">
  <div>
    <InputTextArea
      id="InputTextAreaLRows1"
      dimension={InputTextArea.REFS.DIMENSIONS.L}
      rows={1}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaLRows2"
      dimension={InputTextArea.REFS.DIMENSIONS.L}
      rows={2}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaLRows3"
      dimension={InputTextArea.REFS.DIMENSIONS.L}
      rows={3}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaLRows4"
      dimension={InputTextArea.REFS.DIMENSIONS.L}
      rows={4}
    />
  </div>
</div>
```

### InputTextArea Dimension XL

```js
<div className="list">
  <div>
    <InputTextArea
      id="InputTextAreaXLRows1"
      dimension={InputTextArea.REFS.DIMENSIONS.XL}
      rows={1}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaXLRows2"
      dimension={InputTextArea.REFS.DIMENSIONS.XL}
      rows={2}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaXLRows3"
      dimension={InputTextArea.REFS.DIMENSIONS.XL}
      rows={3}
    />
  </div>
  <div>
    <InputTextArea
      id="InputTextAreaXLRows4"
      dimension={InputTextArea.REFS.DIMENSIONS.XL}
      rows={4}
    />
  </div>
</div>
```

### InputTextArea with placeholder

```js
<div className="list">
  <div>
    <InputTextArea id="InputTextAreaWithPlaceholder" placeholder="Placeholder" />
  </div>
</div>
```

### InputTextArea with maxLength (for example 10)

```js
<div className="list">
  <div>
    <InputTextArea id="InputTextAreaWithMaxLength" maxLength={10} value="1234567890" />
  </div>
</div>
```

### InputTextArea with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputTextArea id="InputTextAreaWithWarning" value="Поле с предупреждением" isWarning />
  </div>
  <div>
    <InputTextArea id="InputTextAreaWithError" value="Поле с ошибкой" isError />
  </div>
</div>
```

### InputTextArea disabled

```js
<div className="list">
  <div>
    <InputTextArea id="InputTextAreaDisabled" disabled />
  </div>
  <div>
    <InputTextArea id="InputTextAreaDisabledWithPlaceholder" placeholder="Placeholder" disabled />
  </div>
  <div>
    <InputTextArea id="InputTextAreaDisabledWithValue" value="Поле недоступно для редактирования" disabled />
  </div>
  <div>
    <InputTextArea id="InputTextAreaDisabledWithWarning" value="Поле с предупреждением недоступно для редактирования" disabled isWarning />
  </div>
  <div>
    <InputTextArea id="InputTextAreaDisabledWithError" value="Поле с ошибкой недоступно для редактирования" disabled isError />
  </div>
</div>
```
