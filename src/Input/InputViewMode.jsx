import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import IconCheckedCheckbox from '../Icon/IconCheckedCheckbox';
import IconCheckedRadio from '../Icon/IconCheckedRadio';
import { TYPES } from './_constants';
import { INPUT_VIEW_MODE_REFS as REFS } from './_config';
import {
  StyledInputViewMode,
  StyledInputViewModePlaceholder,
  StyledInputViewModeValue,
  StyledInputViewModeSwitch,
  StyledInputCheckboxIcon,
  StyledInputRadioIcon,
  StyledInputSwitchTitle,
} from './_style';

export default class InputViewMode extends PureComponent {
  static REFS = { ...REFS };

  static inputType = TYPES.VIEW_MODE;

  static propTypes = {
    /** Component Id */
    id: PropTypes.string.isRequired,
    /** Component Type */
    type: PropTypes.oneOf(Object.values(REFS.TYPES)),
    /** Component value (if type is text (default)) */
    value: PropTypes.string,
    /** checked flag (if type is checkbox or radio) */
    checked: PropTypes.bool,
    /** Placeholder */
    placeholder: PropTypes.string,
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Align text */
    textAlign: PropTypes.oneOf(Object.values(REFS.TEXT_ALIGN)),
    /** Width in px (number), % or one of WIDTH_MAP key (string) */
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Number of rows */
    rows: PropTypes.number,
    /** Warning flag */
    isWarning: PropTypes.bool,
    /** Error flag */
    isError: PropTypes.bool,
    /** Focus handler
     * @param {string} id Component Id
     * */
    onFocus: PropTypes.func,
    /** Blur handler
     * @param {string} id Component Id
     * */
    onBlur: PropTypes.func,
    /** title for checkbox or radio */
    children: PropTypes.node,
    /** @ignore */
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    type: REFS.TYPES.TEXT,
    value: null,
    checked: false,
    placeholder: null,
    dimension: REFS.DIMENSIONS.M,
    textAlign: REFS.TEXT_ALIGN.LEFT,
    width: '100%',
    rows: null,
    isWarning: false,
    isError: false,
    onFocus: () => null,
    onBlur: () => null,
    children: null,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
    };

    this.inputNode = React.createRef();
  }

  componentDidMount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode);
  }

  componentWillUnmount() {
    if (this.props.setInputNode) this.props.setInputNode(this.props.id, this.inputNode, true);
  }

  onClick = e => {
    e.preventDefault();
  };

  handleFocus = () => {
    this.setState({ isFocused: true });

    this.props.onFocus({ id: this.props.id });
  };

  handleBlur = () => {
    this.setState({ isFocused: false });

    this.props.onBlur({ id: this.props.id });
  };

  render() {
    if ([REFS.TYPES.CHECKBOX, REFS.TYPES.RADIO].includes(this.props.type)) {
      return (
        <StyledInputViewModeSwitch
          {...addDataAttributes({ component: TYPES.VIEW_MODE, type: this.props.type })}
          innerRef={ref => {
            this.inputNode.current = ref;
          }}
          sDimension={this.props.dimension}
          onMouseDown={this.disallowBlur}
          onMouseUp={this.allowBlur}
          id={this.props.id}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          onClick={this.onClick}
        >
          {this.props.type === REFS.TYPES.CHECKBOX && (
            <StyledInputCheckboxIcon
              isViewMode
              sDimension={this.props.dimension}
              isChecked={this.props.checked}
              isFocused={this.state.isFocused}
              isWarning={this.props.isWarning}
              isError={this.props.isError}
            >
              {this.props.checked && (
                <IconCheckedCheckbox
                  color={IconCheckedCheckbox.REFS.COLORS.BLACK}
                  dimension={
                    this.props.dimension === REFS.DIMENSIONS.XS
                      ? IconCheckedCheckbox.REFS.DIMENSIONS.XS
                      : IconCheckedCheckbox.REFS.DIMENSIONS.S
                  }
                  display={IconCheckedCheckbox.REFS.DISPLAY.BLOCK}
                />
              )}
            </StyledInputCheckboxIcon>
          )}
          {this.props.type === REFS.TYPES.RADIO && (
            <StyledInputRadioIcon
              isViewMode
              sDimension={this.props.dimension}
              isChecked={this.props.checked}
              isFocused={this.state.isFocused}
              isWarning={this.props.isWarning}
              isError={this.props.isError}
            >
              {this.props.checked && (
                <IconCheckedRadio
                  color={IconCheckedRadio.REFS.COLORS.BLACK}
                  dimension={
                    this.props.dimension === REFS.DIMENSIONS.XS
                      ? IconCheckedRadio.REFS.DIMENSIONS.XS
                      : IconCheckedRadio.REFS.DIMENSIONS.S
                  }
                  display={IconCheckedRadio.REFS.DISPLAY.BLOCK}
                />
              )}
            </StyledInputRadioIcon>
          )}
          {this.props.children && (
            <StyledInputSwitchTitle sDimension={this.props.dimension}>{this.props.children}</StyledInputSwitchTitle>
          )}
        </StyledInputViewModeSwitch>
      );
    }

    return (
      <StyledInputViewMode
        {...addDataAttributes({ component: TYPES.VIEW_MODE, type: this.props.type })}
        innerRef={ref => {
          this.inputNode.current = ref;
        }}
        isViewMode
        sDimension={this.props.dimension}
        sWidth={this.props.width}
        sRows={this.props.rows}
        isFocused={this.state.isFocused}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
        id={this.props.id}
        value={this.props.value}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onClick={this.onClick}
      >
        {!this.props.value &&
          this.props.placeholder && (
            <StyledInputViewModePlaceholder sRows={this.props.rows}>
              {this.props.placeholder}
            </StyledInputViewModePlaceholder>
          )}
        {this.props.value && (
          <StyledInputViewModeValue sRows={this.props.rows} sTextAlign={this.props.textAlign}>
            {this.props.value}
          </StyledInputViewModeValue>
        )}
      </StyledInputViewMode>
    );
  }
}
