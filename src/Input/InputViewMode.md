### REFS

```js static
{
  TYPES: {
    TEXT: 'text',
    CHECKBOX: 'checkbox',
    RADIO: 'radio',
  },
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
}
```

### InputViewMode Dimension XS

```js
const value = 'LoremipsumdolorsitametconsecteturadipiscingelitAliquamfermentumliberoaauctordignissim. Vestibulum sit amet ultricies sem, eu pretium risus. Integer eu lectus venenatis purus lobortis ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce venenatis egestas scelerisque. In hac habitasse platea dictumst. Mauris ut ex a elit auctor suscipit in ut nibh.';

<div className="list">
  <div>
    <InputViewMode
      id="InputViewModeXSRows1"
      dimension={InputViewMode.REFS.DIMENSIONS.XS}
      rows={1}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeXSRows2"
      dimension={InputViewMode.REFS.DIMENSIONS.XS}
      rows={2}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeXSRows3"
      dimension={InputViewMode.REFS.DIMENSIONS.XS}
      rows={3}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeXSRows4"
      dimension={InputViewMode.REFS.DIMENSIONS.XS}
      rows={4}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeXSCheckbox"
      dimension={InputViewMode.REFS.DIMENSIONS.XS}
    >
      Input View Mode Checkbox XS
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeXSCheckboxChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.XS}
      checked
    >
      Input View Mode Checkbox XS
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeXSRadio"
      dimension={InputViewMode.REFS.DIMENSIONS.XS}
    >
      Input View Mode Radio XS
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeXSRadioChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.XS}
      checked
    >
      Input View Mode Radio XS
    </InputViewMode>
  </div>
</div>
```

### InputViewMode Dimension S

```js
const value = 'LoremipsumdolorsitametconsecteturadipiscingelitAliquamfermentumliberoaauctordignissim. Vestibulum sit amet ultricies sem, eu pretium risus. Integer eu lectus venenatis purus lobortis ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce venenatis egestas scelerisque. In hac habitasse platea dictumst. Mauris ut ex a elit auctor suscipit in ut nibh.';

<div className="list">
  <div>
    <InputViewMode
      id="InputViewModeSRows1"
      dimension={InputViewMode.REFS.DIMENSIONS.S}
      rows={1}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeSRows2"
      dimension={InputViewMode.REFS.DIMENSIONS.S}
      rows={2}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeSRows3"
      dimension={InputViewMode.REFS.DIMENSIONS.S}
      rows={3}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeSRows4"
      dimension={InputViewMode.REFS.DIMENSIONS.S}
      rows={4}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeSCheckbox"
      dimension={InputViewMode.REFS.DIMENSIONS.S}
    >
      Input View Mode Checkbox S
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeSCheckboxChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.S}
      checked
    >
      Input View Mode Checkbox S
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeSRadio"
      dimension={InputViewMode.REFS.DIMENSIONS.S}
    >
      Input View Mode Radio S
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeSRadioChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.S}
      checked
    >
      Input View Mode Radio S
    </InputViewMode>
  </div>
</div>
```

### InputViewMode Dimension M

```js
const value = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies sem, eu pretium risus. Integer eu lectus venenatis purus lobortis ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce venenatis egestas scelerisque. In hac habitasse platea dictumst. Mauris ut ex a elit auctor suscipit in ut nibh.';

<div className="list">
  <div>
    <InputViewMode
      id="InputViewModeMRows1"
      dimension={InputViewMode.REFS.DIMENSIONS.M}
      rows={1}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeMRows2"
      dimension={InputViewMode.REFS.DIMENSIONS.M}
      rows={2}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeMRows3"
      dimension={InputViewMode.REFS.DIMENSIONS.M}
      rows={3}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeMRows4"
      dimension={InputViewMode.REFS.DIMENSIONS.M}
      rows={4}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeMCheckbox"
      dimension={InputViewMode.REFS.DIMENSIONS.M}
    >
      Input View Mode Checkbox M
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeMCheckboxChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.M}
      checked
    >
      Input View Mode Checkbox M
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeMRadio"
      dimension={InputViewMode.REFS.DIMENSIONS.M}
    >
      Input View Mode Radio M
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeMRadioChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.M}
      checked
    >
      Input View Mode Radio M
    </InputViewMode>
  </div>
</div>
```

### InputViewMode Dimension L
```js
const value = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies sem, eu pretium risus. Integer eu lectus venenatis purus lobortis ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce venenatis egestas scelerisque. In hac habitasse platea dictumst. Mauris ut ex a elit auctor suscipit in ut nibh.';

<div className="list">
  <div>
    <InputViewMode
      id="InputViewModeLRows1"
      dimension={InputViewMode.REFS.DIMENSIONS.L}
      rows={1}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeLRows2"
      dimension={InputViewMode.REFS.DIMENSIONS.L}
      rows={2}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeLRows3"
      dimension={InputViewMode.REFS.DIMENSIONS.L}
      rows={3}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeLRows4"
      dimension={InputViewMode.REFS.DIMENSIONS.L}
      rows={4}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeLCheckbox"
      dimension={InputViewMode.REFS.DIMENSIONS.L}
    >
      Input View Mode Checkbox L
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeLCheckboxChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.L}
      checked
    >
      Input View Mode Checkbox L
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeLRadio"
      dimension={InputViewMode.REFS.DIMENSIONS.L}
    >
      Input View Mode Radio L
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeLRadioChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.L}
      checked
    >
      Input View Mode Radio L
    </InputViewMode>
  </div>
</div>
```

### InputViewMode Dimension XL
```js
const value = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies sem, eu pretium risus. Integer eu lectus venenatis purus lobortis ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce venenatis egestas scelerisque. In hac habitasse platea dictumst. Mauris ut ex a elit auctor suscipit in ut nibh.';

<div className="list">
  <div>
    <InputViewMode
      id="InputViewModeXLRows1"
      dimension={InputViewMode.REFS.DIMENSIONS.XL}
      rows={1}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeXLRows2"
      dimension={InputViewMode.REFS.DIMENSIONS.XL}
      rows={2}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeXLRows3"
      dimension={InputViewMode.REFS.DIMENSIONS.XL}
      rows={3}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeXLRows4"
      dimension={InputViewMode.REFS.DIMENSIONS.XL}
      rows={4}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeXLCheckbox"
      dimension={InputViewMode.REFS.DIMENSIONS.XL}
    >
      Input View Mode Checkbox XL
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeXLCheckboxChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.XL}
      checked
    >
      Input View Mode Checkbox XL
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeXLRadio"
      dimension={InputViewMode.REFS.DIMENSIONS.XL}
    >
      Input View Mode Radio XL
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeXLRadioChecked"
      dimension={InputViewMode.REFS.DIMENSIONS.XL}
      checked
    >
      Input View Mode Radio XL
    </InputViewMode>
  </div>
</div>
```

### InputViewMode text align Center
```js
const value = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies sem, eu pretium risus. Integer eu lectus venenatis purus lobortis ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce venenatis egestas scelerisque. In hac habitasse platea dictumst. Mauris ut ex a elit auctor suscipit in ut nibh.';

<div className="list">
  <div>
    <InputViewMode
      id="InputViewModeCenterRows1"
      textAlign={InputViewMode.REFS.TEXT_ALIGN.CENTER}
      rows={1}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeCenterRows2"
      textAlign={InputViewMode.REFS.TEXT_ALIGN.CENTER}
      rows={2}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeCenterRows3"
      textAlign={InputViewMode.REFS.TEXT_ALIGN.CENTER}
      rows={3}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeCenterRows4"
      textAlign={InputViewMode.REFS.TEXT_ALIGN.CENTER}
      rows={4}
      value={value}
    />
  </div>
</div>
```

### InputViewMode text align Right
```js
const value = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies sem, eu pretium risus. Integer eu lectus venenatis purus lobortis ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce venenatis egestas scelerisque. In hac habitasse platea dictumst. Mauris ut ex a elit auctor suscipit in ut nibh.';

<div className="list">
  <div>
    <InputViewMode
      id="InputViewModeRightRows1"
      textAlign={InputViewMode.REFS.TEXT_ALIGN.RIGHT}
      rows={1}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeRightRows2"
      textAlign={InputViewMode.REFS.TEXT_ALIGN.RIGHT}
      rows={2}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeRightRows3"
      textAlign={InputViewMode.REFS.TEXT_ALIGN.RIGHT}
      rows={3}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeRightRows4"
      textAlign={InputViewMode.REFS.TEXT_ALIGN.RIGHT}
      rows={4}
      value={value}
    />
  </div>
</div>
```

### InputViewMode without rows
```js
const value = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fermentum libero ac auctor dignissim. Vestibulum sit amet ultricies sem, eu pretium risus. Integer eu lectus venenatis purus lobortis ullamcorper. Morbi placerat augue in nibh euismod accumsan. Fusce venenatis egestas scelerisque. In hac habitasse platea dictumst. Mauris ut ex a elit auctor suscipit in ut nibh.';

<div className="list">
  <div>
    <InputViewMode
      id="InputViewModeXSWide"
      dimension={InputViewMode.REFS.DIMENSIONS.XS}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeSWide"
      dimension={InputViewMode.REFS.DIMENSIONS.S}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeMWide"
      dimension={InputViewMode.REFS.DIMENSIONS.M}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeLWide"
      dimension={InputViewMode.REFS.DIMENSIONS.L}
      value={value}
    />
  </div>
  <div>
    <InputViewMode
      id="InputViewModeXLWide"
      dimension={InputViewMode.REFS.DIMENSIONS.XL}
      value={value}
    />
  </div>
</div>
```

### InputViewMode with placeholder

```js
<div className="list">
  <div>
    <InputViewMode id="InputViewModeWithPlaceholder" placeholder="Placeholder" />
  </div>
</div>
```

### InputViewMode with isWarning and isError flags

```js
<div className="list">
  <div>
    <InputViewMode id="InputViewModeWithWarning" value="Поле с предупреждением" isWarning />
  </div>
  <div>
    <InputViewMode id="InputViewModeWithError" value="Поле с ошибкой" isError />
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeCheckboxWhitWarning"
      isWarning
    >
      Input View Mode Checkbox With Warning
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.CHECKBOX}
      id="InputViewModeCheckboxWhitError"
      isError
      checked
    >
      Input View Mode Checkbox With Error
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeRadioWithWarning"
      isWarning
    >
      Input View Mode Radio With Warning
    </InputViewMode>
  </div>
  <div>
    <InputViewMode
      type={InputViewMode.REFS.TYPES.RADIO}
      id="InputViewModeRadioWithError"
      isError
      checked
    >
      Input View Mode Radio With Error
    </InputViewMode>
  </div>
</div>
```
