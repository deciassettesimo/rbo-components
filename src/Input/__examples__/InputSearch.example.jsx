import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import InputSearch from '../InputSearch';

export default class InputSearchExample extends PureComponent {
  static propTypes = {
    value: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.shape).isRequired,
    onSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    value: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
      searchValue: null,
      options: this.props.options,
      isSearching: false,
    };
  }

  onSearch = params => {
    this.setState({ isSearching: true });
    if (this.searchTimer) clearTimeout(this.searchTimer);
    this.searchTimer = setTimeout(() => {
      this.search(params.value);
    }, 200);
    this.props.onSearch(params);
  };

  onChange = params => {
    this.setState({ value: params.value });
    this.props.onChange(params);
  };

  search = value => {
    const searchRegExp = new RegExp(value, 'ig');
    const options = this.props.options.filter(option => option.title.search(searchRegExp) >= 0);
    this.setState({ options, isSearching: false, searchValue: value });
  };

  render() {
    return (
      <InputSearch
        {...this.props}
        value={this.state.value}
        searchValue={this.state.searchValue}
        options={this.state.options}
        isSearching={this.state.isSearching}
        onChange={this.onChange}
        onSearch={this.onSearch}
      />
    );
  }
}
