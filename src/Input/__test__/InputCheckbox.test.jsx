import React from 'react';
import { mount } from 'enzyme';
import InputCheckbox from '../InputCheckbox';

const getWrapper = props => mount(<InputCheckbox {...props} />);

describe('InputCheckbox Component', () => {
  it('test Component with normal props', () => {
    const id = 'ComponentId';
    const value = false;
    const props = {
      id,
      value,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.find('input').prop('checked')).toEqual(value);
    expect(wrapper.prop('onFocus')()).toEqual(null);
    expect(wrapper.prop('onBlur')()).toEqual(null);
    expect(wrapper.prop('onChange')()).toEqual(null);
  });

  it('test Component with disabled prop', () => {
    const id = 'ComponentId';
    const props = {
      id,
      disabled: true,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.find('input').prop('disabled')).toEqual(true);
    wrapper.setProps({ disabled: false });
    expect(wrapper.find('input').prop('disabled')).toEqual(false);
    wrapper.setProps({ disabled: true });
    expect(wrapper.find('input').prop('disabled')).toEqual(true);
  });

  it('test Component with handlers', () => {
    const spy = jest.fn();
    const id = 'ComponentId';
    const props = {
      id,
      onChange: spy,
      onFocus: spy,
      onBlur: spy,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.find('input').prop('checked')).toEqual(false);
    wrapper.find('input').simulate('focus');
    expect(wrapper.state('isFocused')).toEqual(true);
    expect(spy.mock.calls.length).toEqual(1);
    expect(spy.mock.calls[0][0]).toEqual({ id, value: false });
    wrapper.find('input').simulate('change', { target: { checked: true } });
    expect(wrapper.find('input').prop('checked')).toEqual(true);
    expect(spy.mock.calls.length).toEqual(2);
    expect(spy.mock.calls[1][0]).toEqual({ id, value: true });
    wrapper.find('input').simulate('blur');
    expect(wrapper.state('isFocused')).toEqual(false);
    expect(spy.mock.calls.length).toEqual(3);
    expect(spy.mock.calls[2][0]).toEqual({ id, value: true });
  });

  it('test Component disable', () => {
    const id = 'ComponentId';
    const props = { id };
    const wrapper = getWrapper(props);
    const instance = wrapper.instance();
    const spyHandleBlur = jest.spyOn(instance, 'handleBlur');

    wrapper.setProps({ disabled: true });
    expect(spyHandleBlur).not.toHaveBeenCalled();
    wrapper.setProps({ disabled: false });
    wrapper.find('input').simulate('focus');
    wrapper.setProps({ disabled: true });
    expect(spyHandleBlur).toHaveBeenCalled();
  });
});
