import React from 'react';
import { mount } from 'enzyme';
import InputNumberDecimal from '../InputNumberDecimal';

const getWrapper = props => mount(<InputNumberDecimal {...props} />);

describe('InputNumberDecimal Component', () => {
  it('test Component with normal props', () => {
    const id = 'ComponentId';
    const value = 1234567890;
    const formattedValue = '1234567890.00';
    const props = {
      id,
      value,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.state('value')).toEqual(value);
    expect(wrapper.state('formattedValue')).toEqual(formattedValue);
    expect(wrapper.find('input').prop('value')).toEqual(formattedValue);
    expect(wrapper.prop('onFocus')()).toEqual(null);
    expect(wrapper.prop('onBlur')()).toEqual(null);
    expect(wrapper.prop('onChange')()).toEqual(null);
  });

  it('test Component with disabled prop', () => {
    const id = 'ComponentId';
    const props = {
      id,
      disabled: true,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.find('input').prop('disabled')).toEqual(true);
    wrapper.setProps({ disabled: false });
    expect(wrapper.find('input').prop('disabled')).toEqual(false);
    wrapper.setProps({ disabled: true });
    expect(wrapper.find('input').prop('disabled')).toEqual(true);
  });

  it('test Component with handlers', () => {
    const spy = jest.fn();
    const id = 'ComponentId';
    const value = 1234567890;
    const formattedValueFocus = '1 234 567 890';
    const formattedValue = '1 234 567 890.00';
    const props = {
      id,
      isFormatted: true,
      isNegative: true,
      onChange: spy,
      onFocus: spy,
      onBlur: spy,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.state('value')).toEqual(null);
    expect(wrapper.state('formattedValue')).toEqual('');
    expect(wrapper.find('input').prop('value')).toEqual('');
    wrapper.simulate('focus');
    expect(wrapper.state('isFocused')).toEqual(true);
    expect(spy.mock.calls.length).toEqual(1);
    expect(spy.mock.calls[0][0]).toEqual({ id, value: null, formattedValue: '' });
    wrapper.simulate('change', { target: { value: value.toString() } });
    expect(wrapper.state('value')).toEqual(value);
    expect(wrapper.state('formattedValue')).toEqual(formattedValueFocus);
    expect(wrapper.find('input').prop('value')).toEqual(formattedValueFocus);
    expect(spy.mock.calls.length).toEqual(2);
    expect(spy.mock.calls[1][0]).toEqual({ id, value, formattedValue: formattedValueFocus });
    wrapper.simulate('blur');
    expect(wrapper.state('isFocused')).toEqual(false);
    expect(spy.mock.calls.length).toEqual(3);
    expect(spy.mock.calls[2][0]).toEqual({ id, value, formattedValue });
  });

  it('test Component disable', () => {
    const id = 'ComponentId';
    const props = { id };
    const wrapper = getWrapper(props);
    const instance = wrapper.instance();
    const spyHandleBlur = jest.spyOn(instance, 'handleBlur');

    wrapper.setProps({ disabled: true });
    expect(spyHandleBlur).not.toHaveBeenCalled();
    wrapper.setProps({ disabled: false });
    wrapper.simulate('focus');
    wrapper.setProps({ disabled: true });
    expect(spyHandleBlur).toHaveBeenCalled();
  });

  it('test Component handleKeyDown and handleKeyPress', () => {
    const id = 'ComponentId';
    const props = { id };
    const wrapper = getWrapper(props);
    const instance = wrapper.instance();
    const spyPreventDefault = jest.fn();
    const spyHandleKeyDown = jest.spyOn(instance, 'handleKeyDown');
    const spyHandleKeyPress = jest.spyOn(instance, 'handleKeyPress');

    wrapper.simulate('focus');
    wrapper.simulate('keydown', { keyCode: 40, ctrlKey: false, preventDefault: spyPreventDefault });
    expect(spyHandleKeyDown).toHaveBeenCalled();
    expect(spyPreventDefault).not.toHaveBeenCalled();

    wrapper.simulate('keydown', { keyCode: 90, ctrlKey: true, preventDefault: spyPreventDefault });
    expect(spyPreventDefault).toHaveBeenCalled();

    spyPreventDefault.mockReset();
    spyPreventDefault.mockRestore();

    wrapper.simulate('keypress', { keyCode: 0, which: 49, preventDefault: spyPreventDefault });
    expect(spyHandleKeyPress).toHaveBeenCalled();
    expect(spyPreventDefault).not.toHaveBeenCalled();

    wrapper.simulate('keypress', { keyCode: 0, which: 103, preventDefault: spyPreventDefault });
    expect(spyHandleKeyPress).toHaveBeenCalled();
    expect(spyPreventDefault).toHaveBeenCalled();
  });

  it('test Component formatValue', () => {
    const id = 'ComponentId';
    const props = {
      id,
      isFormatted: true,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.state('value')).toEqual(null);
    expect(wrapper.state('formattedValue')).toEqual('');
    wrapper.simulate('focus');
    wrapper.simulate('change', { target: { value: '.8' } });
    expect(wrapper.state('value')).toEqual(0.8);
    expect(wrapper.state('formattedValue')).toEqual('0.8');
    wrapper.simulate('change', { target: { value: '-' } });
    expect(wrapper.state('value')).toEqual(null);
    expect(wrapper.state('formattedValue')).toEqual('');
    wrapper.simulate('change', { target: { value: '10.10' } });
    expect(wrapper.state('value')).toEqual(10.1);
    expect(wrapper.state('formattedValue')).toEqual('10.10');
    wrapper.simulate('change', { target: { value: '.1' } });
    wrapper.simulate('blur');
    expect(wrapper.state('value')).toEqual(0.1);
    expect(wrapper.state('formattedValue')).toEqual('0.10');
  });

  it('test Component with isNegative props', () => {
    const id = 'ComponentId';
    const props = {
      id,
      isNegative: true,
    };
    const wrapper = getWrapper(props);
    wrapper.simulate('change', { target: { value: '--' } });
    expect(wrapper.state('value')).toEqual(null);
    expect(wrapper.state('formattedValue')).toEqual('-');
    expect(wrapper.find('input').prop('value')).toEqual('-');
  });
});
