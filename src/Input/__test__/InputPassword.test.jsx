import React from 'react';
import { mount } from 'enzyme';
import InputPassword from '../InputPassword';

const getWrapper = props => mount(<InputPassword {...props} />);

describe('InputPassword Component', () => {
  it('test Component with normal props', () => {
    const id = 'ComponentId';
    const value = 'ComponentValue';
    const props = {
      id,
      value,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.state('value')).toEqual(value);
    expect(wrapper.find('input').prop('value')).toEqual(value);
    expect(wrapper.prop('onFocus')()).toEqual(null);
    expect(wrapper.prop('onBlur')()).toEqual(null);
    expect(wrapper.prop('onChange')()).toEqual(null);
  });

  it('test Component with disabled prop', () => {
    const id = 'ComponentId';
    const value = 'ComponentValue';
    const props = {
      id,
      value,
      disabled: true,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.find('input').prop('disabled')).toEqual(true);
    wrapper.setProps({ disabled: false });
    expect(wrapper.find('input').prop('disabled')).toEqual(false);
    wrapper.setProps({ disabled: true });
    expect(wrapper.find('input').prop('disabled')).toEqual(true);
  });

  it('test Component with handlers', () => {
    const spy = jest.fn();
    const id = 'ComponentId';
    const value = 'ComponentValue';
    const props = {
      id,
      onChange: spy,
      onFocus: spy,
      onBlur: spy,
    };
    const wrapper = getWrapper(props);
    expect(wrapper.state('value')).toEqual(null);
    expect(wrapper.find('input').prop('value')).toEqual('');
    wrapper.simulate('focus');
    expect(wrapper.state('isFocused')).toEqual(true);
    expect(spy.mock.calls.length).toEqual(1);
    expect(spy.mock.calls[0][0]).toEqual({ id, value: null, formattedValue: '' });
    wrapper.simulate('change', { target: { value } });
    expect(wrapper.state('value')).toEqual(value);
    expect(wrapper.find('input').prop('value')).toEqual(value);
    expect(spy.mock.calls.length).toEqual(2);
    expect(spy.mock.calls[1][0]).toEqual({ id, value, formattedValue: value });
    wrapper.simulate('blur');
    expect(wrapper.state('isFocused')).toEqual(false);
    expect(spy.mock.calls.length).toEqual(3);
    expect(spy.mock.calls[2][0]).toEqual({ id, value, formattedValue: value });
  });

  it('test Component disable', () => {
    const id = 'ComponentId';
    const props = { id };
    const wrapper = getWrapper(props);
    const instance = wrapper.instance();
    const spyHandleBlur = jest.spyOn(instance, 'handleBlur');

    wrapper.setProps({ disabled: true });
    expect(spyHandleBlur).not.toHaveBeenCalled();
    wrapper.setProps({ disabled: false });
    wrapper.simulate('focus');
    wrapper.setProps({ disabled: true });
    expect(spyHandleBlur).toHaveBeenCalled();
  });
});
