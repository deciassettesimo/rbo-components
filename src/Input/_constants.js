import { GLOBAL } from '../_constants';
import { BOX_ALIGN as POPUP_BOX_ALIGN } from '../Popup/_constants';

export const TYPES = {
  ACCOUNT: 'InputAccount',
  CHECKBOX: 'InputCheckbox',
  DATE: 'InputDate',
  DATE_CALENDAR: 'InputDateCalendar',
  DATE_TIME: 'InputDateTime',
  DATE_TIME_CALENDAR: 'InputDateTimeCalendar',
  DIGITAL: 'InputDigital',
  FILES: 'InputFiles',
  NUMBER_DECIMAL: 'InputNumberDecimal',
  NUMBER_INTEGER: 'InputNumberInteger',
  PASSWORD: 'InputPassword',
  PHONE: 'InputPhone',
  RADIO_GROUP: 'InputRadioGroup',
  RADIO_GROUP_OPTION: 'InputRadioOption',
  SEARCH: 'InputSearch',
  SELECT: 'InputSelect',
  SUGGEST: 'InputSuggest',
  TABLE: 'InputTable',
  TEXT: 'InputText',
  TEXT_AREA: 'InputTextArea',
  VIEW_MODE: 'InputViewMode',
  BUTTON: 'Button',
};

export const COMPONENTS = {
  CALENDAR: 'InputCalendar',
  CALENDAR_WITH_TIME_PICKER: 'InputCalendarWithTimePicker',
  OPTIONS: 'Options',
  OPTIONS_ITEM: 'OptionsItem',
};

export const DIMENSIONS = {
  XS: 24,
  S: 32,
  M: 36,
  L: 40,
  XL: 48,
};

export const TEXT_ALIGN = {
  LEFT: 'left',
  CENTER: 'center',
  RIGHT: 'right',
};

export const INPUT_SELECT_LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const INPUT_SELECT_POPUP_BOX_ALIGN = {
  START: POPUP_BOX_ALIGN.START,
  END: POPUP_BOX_ALIGN.END,
};

export const CALENDAR_LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const INPUT_TABLE_ADD_BUTTON_ID_PREFIX = 'InputTableAddButton_';

export const INPUT_TABLE_LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const INPUT_VIEW_MODE_TYPES = {
  TEXT: 'text',
  CHECKBOX: 'checkbox',
  RADIO: 'radio',
};

export const WIDTH_MAP = {
  ...GLOBAL.WIDTH_MAP,
};

export const LABELS = {
  ...GLOBAL.LABELS.INPUT,
};
