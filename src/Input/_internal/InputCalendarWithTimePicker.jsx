import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import Calendar from 'rc-calendar';
import TimePickerPanel from 'rc-time-picker/lib/Panel';
import RU from 'rc-calendar/lib/locale/ru_RU';
import EN from 'rc-calendar/lib/locale/en_GB';

import { addDataAttributes } from '../../_utils';

import Card from '../../Card';
import { COMPONENTS } from '../_constants';
import { INPUT_DATE_CALENDAR_REFS as REFS } from '../_config';
import { convertMomentValue } from '../_utils';
import { StyledInputCalendar } from '../_style';

export default class InputCalendarWithTimePicker extends PureComponent {
  static propTypes = {
    value: PropTypes.string,
    onPopupElementMouseDown: PropTypes.func.isRequired,
    onPopupElementMouseUp: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    onClickOkay: PropTypes.func,
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
  };

  static defaultProps = {
    value: null,
    locale: REFS.LOCALES.RU,
    onClickOkay: () => {},
  };

  state = {};

  static getDerivedStateFromProps(props, state) {
    if (!state.isInitialized) {
      return { isInitialized: true };
    }
    return null;
  }

  getLocale = () => {
    switch (this.props.locale) {
      case REFS.LOCALES.EN:
        return EN;
      case REFS.LOCALES.RU:
      default:
        return RU;
    }
  };

  handleSelect = params => {
    this.props.onSelect(convertMomentValue(params));
  };

  render() {
    const timePickerElement = (
      <TimePickerPanel defaultOpenValue={convertMomentValue(this.props.value) || moment('00:00:00', 'HH:mm:ss')} />
    );
    const extraFooter = () => (
      <button onClick={this.props.onClickOkay} className="rbo-components-rc-calendar-extra-btn">
        Ok
      </button>
    );

    return (
      <Card isShadowed>
        <StyledInputCalendar
          {...addDataAttributes({ component: COMPONENTS.CALENDAR_WITH_TIME_PICKER })}
          onMouseDown={this.props.onPopupElementMouseDown}
          onMouseUp={this.props.onPopupElementMouseUp}
        >
          <Calendar
            locale={this.getLocale()}
            showDateInput={false}
            prefixCls="rbo-components-rc-calendar"
            defaultValue={convertMomentValue(this.props.value)}
            onSelect={this.handleSelect}
            timePicker={timePickerElement}
            showOk={false}
            showToday={false}
            renderFooter={extraFooter}
          />
        </StyledInputCalendar>
      </Card>
    );
  }
}
