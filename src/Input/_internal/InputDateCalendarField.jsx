import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import ClickableIcon from '../../ClickableIcon';
import InputDate from '../InputDate';
import { StyledInputWithIcon, StyledInputIcon } from '../_style';
import { TYPES } from '../_constants';
import { INPUT_REFS as REFS } from '../_config';

const InputDateCalendarField = forwardRef((props, ref) => (
  <StyledInputWithIcon {...addDataAttributes({ component: TYPES.DATE_CALENDAR })} sWidth={props.width}>
    <InputDate
      ref={ref}
      id={props.id}
      value={props.value}
      placeholder={props.placeholder}
      dimension={props.dimension}
      isWarning={props.isWarning}
      isError={props.isError}
      disabled={props.disabled}
      onFocus={props.onInputElementFocus}
      onBlur={props.onInputElementBlur}
      onChange={props.onInputElementChange}
      isFocused={props.isFocused}
      disabledDate={props.disabledDate}
      setInputNode={props.setInputNode}
    />
    <StyledInputIcon
      sDimension={props.dimension}
      onMouseDown={props.onIconElementMouseDown}
      onMouseUp={props.onIconElementMouseUp}
    >
      <ClickableIcon
        type={ClickableIcon.REFS.TYPES.CALENDAR}
        dimension={ClickableIcon.REFS.DIMENSIONS.XS}
        display={ClickableIcon.REFS.DISPLAY.BLOCK}
        onClick={props.onIconElementClick}
        isDisabled={props.disabled}
      />
    </StyledInputIcon>
  </StyledInputWithIcon>
));

InputDateCalendarField.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)).isRequired,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  isFocused: PropTypes.bool,
  isWarning: PropTypes.bool,
  isError: PropTypes.bool,
  disabled: PropTypes.bool,
  disabledDate: PropTypes.func,
  onInputElementFocus: PropTypes.func.isRequired,
  onInputElementBlur: PropTypes.func.isRequired,
  onInputElementChange: PropTypes.func.isRequired,
  onIconElementClick: PropTypes.func.isRequired,
  setInputNode: PropTypes.func,
};

InputDateCalendarField.defaultProps = {
  value: '',
  placeholder: null,
  isFocused: false,
  isWarning: false,
  isError: false,
  disabled: false,
  disabledDate: () => false,
  setInputNode: null,
};

export default InputDateCalendarField;
