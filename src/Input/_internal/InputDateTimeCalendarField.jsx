import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';
import ClickableIcon from '../../ClickableIcon';
import InputDateTime from '../InputDateTime';
import { StyledInputWithIcon, StyledInputIcon } from '../_style';
import { TYPES } from '../_constants';
import { INPUT_REFS as REFS } from '../_config';

const InputDateTimeCalendarField = forwardRef((props, ref) => (
  <StyledInputWithIcon {...addDataAttributes({ component: TYPES.DATE_TIME_CALENDAR })} sWidth={props.width}>
    <InputDateTime
      ref={ref}
      id={props.id}
      value={props.value}
      placeholder={props.placeholder}
      dimension={props.dimension}
      isWarning={props.isWarning}
      isError={props.isError}
      disabled={props.disabled}
      onFocus={props.onInputElementFocus}
      onBlur={props.onInputElementBlur}
      onChange={props.onInputElementChange}
      isFocused={props.isFocused}
      setInputNode={props.setInputNode}
    />
    <StyledInputIcon
      sDimension={props.dimension}
      onMouseDown={props.onIconElementMouseDown}
      onMouseUp={props.onIconElementMouseUp}
    >
      <ClickableIcon
        type={ClickableIcon.REFS.TYPES.CALENDAR}
        dimension={ClickableIcon.REFS.DIMENSIONS.XS}
        display={ClickableIcon.REFS.DISPLAY.BLOCK}
        onClick={props.onIconElementClick}
        isDisabled={props.disabled}
      />
    </StyledInputIcon>
  </StyledInputWithIcon>
));

InputDateTimeCalendarField.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)).isRequired,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  isFocused: PropTypes.bool,
  isWarning: PropTypes.bool,
  isError: PropTypes.bool,
  disabled: PropTypes.bool,
  onInputElementFocus: PropTypes.func.isRequired,
  onInputElementBlur: PropTypes.func.isRequired,
  onInputElementChange: PropTypes.func.isRequired,
  onIconElementClick: PropTypes.func.isRequired,
  setInputNode: PropTypes.func,
};

InputDateTimeCalendarField.defaultProps = {
  value: '',
  placeholder: null,
  isFocused: false,
  isWarning: false,
  isError: false,
  disabled: false,
  setInputNode: null,
};

export default InputDateTimeCalendarField;
