import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Card from '../../Card';
import InputOptionsItem from './InputOptionsItem';
import { StyledInputOptions, StyledInputOptionsEmpty } from '../_style';
import { COMPONENTS, DIMENSIONS, INPUT_SELECT_LOCALES as LOCALES, LABELS } from '../_constants';

export default class InputOptions extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    id: PropTypes.string.isRequired,
    dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
    popupBoxStyle: PropTypes.shape(),
    inputNodeWidth: PropTypes.number,
    onPopupElementMouseDown: PropTypes.func.isRequired,
    onPopupElementMouseUp: PropTypes.func.isRequired,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
      }),
    ).isRequired,
    emptyLabel: PropTypes.string,
    optionRenderer: PropTypes.func.isRequired,
    onOptionClick: PropTypes.func.isRequired,
    onOptionMouseEnter: PropTypes.func.isRequired,
    onOptionMouseLeave: PropTypes.func.isRequired,
    handlePopupClose: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    dimension: DIMENSIONS.M,
    popupBoxStyle: {},
    inputNodeWidth: null,
    emptyLabel: null,
  };

  constructor(props) {
    super(props);

    this.node = React.createRef();
    this.itemsNodes = {};
    this.props.items.forEach(item => {
      this.itemsNodes[item.value] = React.createRef();
    });
  }

  componentDidMount() {
    this.scrollToActiveItem();
  }

  componentDidUpdate() {
    this.scrollToActiveItem();
  }

  getEmptyLabel = locale => {
    switch (locale) {
      case LOCALES.EN:
        return LABELS.EN.OPTIONS_EMPTY;
      case LOCALES.RU:
      default:
        return LABELS.RU.OPTIONS_EMPTY;
    }
  };

  getActiveItemNode = () => {
    const activeItem = this.props.items.find(item => item.isActive);
    if (activeItem && this.itemsNodes[activeItem.value]) return this.itemsNodes[activeItem.value].current;
    return null;
  };

  scrollToActiveItem = () => {
    const node = this.node.current;
    const activeItemNode = this.getActiveItemNode();
    if (node && activeItemNode) {
      const nodeBCR = node.getBoundingClientRect();
      const activeItemNodeBCR = activeItemNode.getBoundingClientRect();
      if (nodeBCR.top > activeItemNodeBCR.top) node.scrollTop -= nodeBCR.top - activeItemNodeBCR.top;
      if (nodeBCR.bottom < activeItemNodeBCR.bottom) node.scrollTop += activeItemNodeBCR.bottom - nodeBCR.bottom;
    }
  };

  render() {
    return (
      <Card isShadowed>
        <StyledInputOptions
          {...addDataAttributes({ component: COMPONENTS.OPTIONS })}
          innerRef={ref => {
            this.node.current = ref;
          }}
          sStyle={this.props.popupBoxStyle}
          sDefaultWidth={this.props.inputNodeWidth}
          onMouseDown={this.props.onPopupElementMouseDown}
          onMouseUp={this.props.onPopupElementMouseUp}
        >
          {!!this.props.items.length &&
            this.props.items.map(item => (
              <InputOptionsItem
                key={item.value}
                ref={this.itemsNodes[item.value]}
                dimension={this.props.dimension}
                value={item.value}
                isSelected={item.isSelected}
                isActive={item.isActive}
                onClick={this.props.onOptionClick}
                onMouseEnter={this.props.onOptionMouseEnter}
                onMouseLeave={this.props.onOptionMouseLeave}
              >
                {this.props.optionRenderer({
                  id: this.props.id,
                  option: item,
                  handlePopupClose: this.props.handlePopupClose,
                })}
              </InputOptionsItem>
            ))}
          {!this.props.items.length && (
            <StyledInputOptionsEmpty sDimension={this.props.dimension}>
              {!this.props.emptyLabel && this.getEmptyLabel(this.props.locale)}
              {this.props.emptyLabel}
            </StyledInputOptionsEmpty>
          )}
        </StyledInputOptions>
      </Card>
    );
  }
}
