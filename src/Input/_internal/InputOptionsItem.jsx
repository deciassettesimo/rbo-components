import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledInputOptionsItem } from '../_style';
import { INPUT_REFS as REFS } from '../_config';

const InputOptionsItem = forwardRef((props, ref) => {
  const handleOnClick = () => {
    props.onClick(props.value);
  };

  const handleOnMouseEnter = () => {
    props.onMouseEnter(props.value);
  };

  const handleOnMouseLeave = () => {
    props.onMouseLeave(props.value);
  };

  return (
    <StyledInputOptionsItem
      {...addDataAttributes({ component: COMPONENTS.OPTIONS_ITEM, value: props.value })}
      innerRef={ref}
      sDimension={props.dimension}
      isSelected={props.isSelected}
      isActive={props.isActive}
      onClick={handleOnClick}
      onMouseEnter={handleOnMouseEnter}
      onMouseLeave={handleOnMouseLeave}
    >
      {props.children}
    </StyledInputOptionsItem>
  );
});

InputOptionsItem.propTypes = {
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  value: PropTypes.string.isRequired,
  isSelected: PropTypes.bool,
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  onMouseEnter: PropTypes.func.isRequired,
  onMouseLeave: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

InputOptionsItem.defaultProps = {
  dimension: REFS.DIMENSIONS.M,
  isSelected: false,
  isActive: false,
};

export default InputOptionsItem;
