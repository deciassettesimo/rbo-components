import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import IconCheckedRadio from '../../Icon/IconCheckedRadio';
import { InputRadioGroupContext } from '../InputRadioGroup';
import { INPUT_REFS as REFS } from '../_config';
import { TYPES } from '../_constants';

import { StyledInputSwitch, StyledInputRadioHtmlInput, StyledInputRadioIcon, StyledInputSwitchTitle } from '../_style';

class InputRadioGroupOptionWithContext extends PureComponent {
  static propTypes = {
    group: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    checked: PropTypes.bool,
    children: PropTypes.node,
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    isWarning: PropTypes.bool,
    isError: PropTypes.bool,
    disabled: PropTypes.bool,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    setInputNode: PropTypes.func,
  };

  static defaultProps = {
    checked: false,
    children: null,
    dimension: REFS.DIMENSIONS.M,
    isWarning: false,
    isError: false,
    disabled: false,
    onFocus: () => null,
    onBlur: () => null,
    onChange: () => null,
    setInputNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      disallowBlurFlag: false,
    };

    this.inputNode = React.createRef();
  }

  componentDidMount() {
    document.addEventListener('mouseup', this.allowBlur);

    if (this.props.setInputNode) this.props.setInputNode(this.props.group, this.inputNode);
  }

  componentDidUpdate() {
    if (this.props.disabled && this.state.isFocused) this.handleBlur();
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.allowBlur);

    if (this.props.setInputNode) this.props.setInputNode(this.props.group, this.inputNode, true);
  }

  disallowBlur = () => {
    this.setState({ disallowBlurFlag: true });
  };

  allowBlur = () => {
    if (this.state.disallowBlurFlag) {
      this.inputNode.current.focus();
      this.setState({ disallowBlurFlag: false });
    }
  };

  handleFocus = () => {
    if (!this.state.isFocused) {
      this.setState({ isFocused: true });
      this.props.onFocus();
    }
  };

  handleBlur = () => {
    if (!this.state.disallowBlurFlag) {
      this.setState({ isFocused: false });
      this.props.onBlur();
    }
  };

  handleChange = e => {
    this.props.onChange({ value: this.props.value, checked: e.target.checked });
  };

  render() {
    return (
      <StyledInputSwitch
        {...addDataAttributes({ component: TYPES.RADIO_GROUP_OPTION })}
        sDimension={this.props.dimension}
        isDisabled={this.props.disabled}
        onMouseDown={this.disallowBlur}
        onMouseUp={this.allowBlur}
      >
        <StyledInputRadioHtmlInput
          innerRef={ref => {
            this.inputNode.current = ref;
          }}
          sDimension={this.props.dimension}
          value={this.props.value}
          name={this.props.group}
          checked={this.props.checked}
          disabled={this.props.disabled}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          onChange={this.handleChange}
        />
        <StyledInputRadioIcon
          sDimension={this.props.dimension}
          isChecked={this.props.checked}
          isDisabled={this.props.disabled}
          isFocused={this.state.isFocused}
          isWarning={this.props.isWarning}
          isError={this.props.isError}
        >
          {this.props.checked && (
            <IconCheckedRadio
              color={
                this.props.disabled
                  ? IconCheckedRadio.REFS.COLORS.BLACK_48
                  : IconCheckedRadio.REFS.COLORS.CORPORATE_YELLOW
              }
              dimension={
                this.props.dimension === REFS.DIMENSIONS.XS
                  ? IconCheckedRadio.REFS.DIMENSIONS.XS
                  : IconCheckedRadio.REFS.DIMENSIONS.S
              }
              display={IconCheckedRadio.REFS.DISPLAY.BLOCK}
            />
          )}
        </StyledInputRadioIcon>
        {this.props.children && (
          <StyledInputSwitchTitle sDimension={this.props.dimension}>{this.props.children}</StyledInputSwitchTitle>
        )}
      </StyledInputSwitch>
    );
  }
}

const InputRadioGroupOption = props => (
  <InputRadioGroupContext.Consumer>
    {({ id, value, dimension, onFocus, onBlur, onChange }) => (
      <InputRadioGroupOptionWithContext
        {...props}
        group={id}
        dimension={props.dimension || dimension}
        checked={props.value === value}
        onFocus={onFocus}
        onBlur={onBlur}
        onChange={onChange}
        setInputNode={props.setInputNode}
      />
    )}
  </InputRadioGroupContext.Consumer>
);

InputRadioGroupOption.inputType = TYPES.RADIO_GROUP_OPTION;

InputRadioGroupOption.propTypes = {
  value: PropTypes.string.isRequired,
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  setInputNode: PropTypes.func,
};

InputRadioGroupOption.defaultProps = {
  dimension: null,
  setInputNode: null,
};

InputRadioGroupOption.REFS = {
  ...REFS,
};

export default InputRadioGroupOption;
