import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import IconSearch from '../../Icon/IconSearch';
import ClickableIcon from '../../ClickableIcon';
import Loader from '../../Loader/Loader';
import { TYPES } from '../_constants';
import { StyledInputWithIcon, StyledInputIcon } from '../_style';
import { INPUT_REFS as REFS } from '../_config';

const InputSearchField = forwardRef((props, ref) => (
  <StyledInputWithIcon
    {...addDataAttributes({ component: TYPES.SEARCH })}
    sWidth={props.width}
    onClick={props.onInputElementClick}
  >
    <props.inputElement
      ref={ref}
      id={props.id}
      value={props.value}
      placeholder={props.placeholder}
      maxLength={props.maxLength}
      dimension={props.dimension}
      textAlign={props.textAlign}
      isWarning={props.isWarning}
      isError={props.isError}
      disabled={props.disabled}
      onFocus={props.onInputElementFocus}
      onBlur={props.onInputElementBlur}
      onChange={props.onInputElementChange}
      isFocused={props.isFocused}
      setInputNode={props.setInputNode}
    />
    <StyledInputIcon
      sDimension={props.dimension}
      onMouseDown={props.onIconElementMouseDown}
      onMouseUp={props.onIconElementMouseUp}
    >
      {!props.disabled &&
        props.isSearching && <Loader dimension={Loader.REFS.DIMENSIONS.XS} display={Loader.REFS.DISPLAY.BLOCK} />}
      {(props.disabled || !props.isCleanable || !props.value) &&
        !props.isSearching && (
          <IconSearch
            dimension={IconSearch.REFS.DIMENSIONS.XS}
            display={IconSearch.REFS.DISPLAY.BLOCK}
            color={props.disabled ? IconSearch.REFS.COLORS.BLACK_48 : IconSearch.REFS.COLORS.GRAYISH_BROWN}
          />
        )}
      {!props.disabled &&
        !props.isSearching &&
        props.isCleanable &&
        props.value && (
          <ClickableIcon
            type={ClickableIcon.REFS.TYPES.CLOSE}
            dimension={ClickableIcon.REFS.DIMENSIONS.XS}
            display={ClickableIcon.REFS.DISPLAY.BLOCK}
            onClick={props.onCleanElementClick}
          />
        )}
    </StyledInputIcon>
  </StyledInputWithIcon>
));

InputSearchField.propTypes = {
  inputElement: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  maxLength: PropTypes.number,
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)).isRequired,
  textAlign: PropTypes.oneOf(Object.values(REFS.TEXT_ALIGN)).isRequired,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  isFocused: PropTypes.bool,
  isSearching: PropTypes.bool,
  isCleanable: PropTypes.bool,
  isWarning: PropTypes.bool,
  isError: PropTypes.bool,
  disabled: PropTypes.bool,
  onInputElementFocus: PropTypes.func.isRequired,
  onInputElementBlur: PropTypes.func.isRequired,
  onInputElementChange: PropTypes.func.isRequired,
  onIconElementMouseDown: PropTypes.func.isRequired,
  onIconElementMouseUp: PropTypes.func.isRequired,
  setInputNode: PropTypes.func,
};

InputSearchField.defaultProps = {
  value: '',
  placeholder: null,
  maxLength: null,
  isFocused: false,
  isSearching: false,
  isCleanable: false,
  isWarning: false,
  isError: false,
  disabled: false,
  setInputNode: null,
};

export default InputSearchField;
