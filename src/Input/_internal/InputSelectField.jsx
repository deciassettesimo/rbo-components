import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import IconArrowDownFilled from '../../Icon/IconArrowDownFilled';
import ClickableIcon from '../../ClickableIcon';
import { TYPES } from '../_constants';
import {
  StyledInputWithIcon,
  StyledInputSelect,
  StyledInputSelectValue,
  StyledInputSelectPlaceholder,
  StyledInputIcon,
} from '../_style';
import { INPUT_REFS as REFS } from '../_config';

const InputSelectField = forwardRef((props, ref) => (
  <StyledInputWithIcon
    {...addDataAttributes({ component: TYPES.SELECT })}
    sWidth={props.width}
    onClick={props.onElementClick}
  >
    <StyledInputSelect
      innerRef={ref}
      sDimension={props.dimension}
      isDisabled={props.disabled}
      isFocused={props.isFocused}
      isWarning={props.isWarning}
      isError={props.isError}
      id={props.id}
      onFocus={props.onInputElementFocus}
      onBlur={props.onInputElementBlur}
      onClick={props.onInputElementClick}
    >
      {!props.value &&
        props.placeholder && <StyledInputSelectPlaceholder>{props.placeholder}</StyledInputSelectPlaceholder>}
      {props.value && (
        <StyledInputSelectValue>
          {props.valueRenderer({ id: props.id, option: props.selectedOption })}
        </StyledInputSelectValue>
      )}
    </StyledInputSelect>
    <StyledInputIcon
      sDimension={props.dimension}
      onMouseDown={props.onIconElementMouseDown}
      onMouseUp={props.onIconElementMouseUp}
    >
      {(props.disabled || !props.isCleanable || !props.value) && (
        <IconArrowDownFilled
          dimension={IconArrowDownFilled.REFS.DIMENSIONS.XS}
          display={IconArrowDownFilled.REFS.DISPLAY.BLOCK}
          color={
            props.disabled ? IconArrowDownFilled.REFS.COLORS.BLACK_48 : IconArrowDownFilled.REFS.COLORS.GRAYISH_BROWN
          }
        />
      )}
      {!props.disabled &&
        props.isCleanable &&
        props.value && (
          <ClickableIcon
            type={ClickableIcon.REFS.TYPES.CLOSE}
            dimension={ClickableIcon.REFS.DIMENSIONS.XS}
            display={ClickableIcon.REFS.DISPLAY.BLOCK}
            onClick={props.onCleanElementClick}
          />
        )}
    </StyledInputIcon>
  </StyledInputWithIcon>
));

InputSelectField.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.string,
  selectedOption: PropTypes.shape({
    value: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }),
  placeholder: PropTypes.string,
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)).isRequired,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  isFocused: PropTypes.bool,
  isCleanable: PropTypes.bool,
  isWarning: PropTypes.bool,
  isError: PropTypes.bool,
  disabled: PropTypes.bool,
  valueRenderer: PropTypes.func.isRequired,
  onElementClick: PropTypes.func.isRequired,
  onInputElementFocus: PropTypes.func.isRequired,
  onInputElementBlur: PropTypes.func.isRequired,
  onInputElementClick: PropTypes.func.isRequired,
  onCleanElementClick: PropTypes.func.isRequired,
  onIconElementMouseDown: PropTypes.func.isRequired,
  onIconElementMouseUp: PropTypes.func.isRequired,
};

InputSelectField.defaultProps = {
  value: null,
  selectedOption: null,
  placeholder: null,
  isFocused: false,
  isCleanable: false,
  isWarning: false,
  isError: false,
  disabled: false,
};

export default InputSelectField;
