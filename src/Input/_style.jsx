import styled from 'styled-components';
import './_style-rc-calendar.css';
import './_style-rc-time-picker.css';
import { STYLES } from '../_constants';
import { StyledIcon } from '../Icon/_style';
import { DIMENSIONS as ICON_DIMENSIONS } from '../Icon/_constants';
import { DIMENSIONS, WIDTH_MAP } from './_constants';

const inputFontSize = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 12;
  return 14;
};

const inputLineHeight = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 16;
  if (sDimension === DIMENSIONS.XL) return 24;
  return 20;
};

const inputPaddingV = sDimension => (sDimension - inputLineHeight(sDimension)) / 2 - 1;

const inputBackgroundColor = (isDisabled, isViewMode) => {
  if (isDisabled) return STYLES.COLORS.DISABLED_GRAY;
  if (isViewMode) return STYLES.COLORS.VIEW_MODE_GRAY;
  return STYLES.COLORS.WHITE;
};

const inputBorderColor = (isWarning, isError, isDisabled, isFocused, isViewMode, isHovered) => {
  if (isFocused && !isViewMode) return STYLES.COLORS.PERRY_WINKLE;
  if (isHovered && !isDisabled && !isViewMode) return STYLES.COLORS.BLACK;
  if (isError) return STYLES.COLORS.TOMATO_RED;
  if (isWarning) return STYLES.COLORS.MARIGOLD;
  if (isDisabled) return STYLES.COLORS.BLACK_20;
  return STYLES.COLORS.BLACK_24;
};

const inputColor = isDisabled => {
  if (isDisabled) return STYLES.COLORS.BLACK_48;
  return STYLES.COLORS.BLACK;
};

const inputWidth = sWidth => {
  if (typeof sWidth === 'number') return `${sWidth}px`;
  return WIDTH_MAP[sWidth] || sWidth || '100%';
};

const inputWithRowsHeight = (sDimension, sRows) => {
  if (sRows) return sRows * inputLineHeight(sDimension) + inputPaddingV(sDimension) * 2 + 2;
  return 'auto';
};

const inputIconTop = sDimension => (sDimension - ICON_DIMENSIONS.XS) / 2;

const inputOptionsStyle = (sStyle, sDefaultWidth) => {
  const width = sDefaultWidth - 2; // border of Card component
  let minWidth = (sStyle && sStyle.minWidth) || null;
  if (sStyle && sStyle.minWidth && sStyle.minWidth === '100%') minWidth = width;
  let maxWidth = (sStyle && sStyle.maxWidth) || null;
  if (sStyle && sStyle.maxWidth && sStyle.maxWidth === '100%') maxWidth = width;
  return {
    maxHeight: 400,
    width,
    ...sStyle,
    minWidth,
    maxWidth,
  };
};

const inputOptionsItemBackgroundColor = (isSelected, isActive) => {
  if (isSelected) return STYLES.COLORS.PARCHMENT;
  if (isActive) return STYLES.COLORS.ICE_BLUE;
  return STYLES.COLORS.TRANSPARENT;
};

const inputSwitchIconTop = sDimension => {
  if (sDimension === DIMENSIONS.XS) return (sDimension - ICON_DIMENSIONS.XS) / 2 - 2;
  return (sDimension - ICON_DIMENSIONS.S) / 2 - 2;
};

const inputSwitchIconDimension = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 16;
  return 20;
};

const inputSwitchBackgroundColor = (isChecked, isDisabled, isViewMode) => {
  if (isDisabled) return STYLES.COLORS.DISABLED_GRAY;
  if (isViewMode) return STYLES.COLORS.VIEW_MODE_GRAY;
  if (isChecked) return STYLES.COLORS.GRAYISH_BROWN;
  return STYLES.COLORS.WHITE;
};

const inputSwitchTitleMarginLeft = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 24;
  return 28;
};

const StyledInput = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  outline: 0 none;
  width: ${props => inputWidth(props.sWidth)};
  max-width: 100%;
  line-height: ${props => inputLineHeight(props.sDimension)}px;
  padding: ${props => inputPaddingV(props.sDimension)}px 8px;
  font-family: inherit;
  font-size: ${props => inputFontSize(props.sDimension)}px;
  text-align: ${props => (props.sTextAlign ? props.sTextAlign : 'left')};
  background: ${props => inputBackgroundColor(props.isDisabled, props.isViewMode)};
  border: 1px ${props => (props.isViewMode ? 'dotted' : 'solid')}
    ${props => inputBorderColor(props.isWarning, props.isError, props.isDisabled, props.isFocused, props.isViewMode)};
  color: ${props => inputColor(props.isDisabled)};
  transition: border-color 0.32s ease-out;

  :hover {
    border-color: ${props =>
      inputBorderColor(props.isWarning, props.isError, props.isDisabled, props.isFocused, props.isViewMode, true)};
  }

  :-ms-input-placeholder {
    color: ${STYLES.COLORS.BLACK_24};
    opacity: 1;
  }
  :-moz-placeholder {
    color: ${STYLES.COLORS.BLACK_24};
    opacity: 1;
  }
  ::-moz-placeholder {
    color: ${STYLES.COLORS.BLACK_24};
    opacity: 1;
  }
  ::-webkit-input-placeholder {
    color: ${STYLES.COLORS.BLACK_24};
    opacity: 1;
  }
`;

export const StyledInputText = StyledInput.withComponent('input').extend.attrs({
  type: 'text',
  spellCheck: 'false',
  autoComplete: 'off',
  autoCorrect: 'off',
})`
  height: ${props => props.sDimension}px;
`;

export const StyledInputPassword = StyledInputText.extend.attrs({ type: 'password' })``;

export const StyledInputTextArea = StyledInput.withComponent('textarea').extend.attrs({
  spellCheck: 'false',
  autoComplete: 'off',
  autoCorrect: 'off',
})`
  resize: vertical;
  height: ${props => inputWithRowsHeight(props.sDimension, props.sRows)}px;
  min-height: ${props => inputWithRowsHeight(props.sDimension, props.sRows)}px;
`;

export const StyledInputSelect = StyledInput.withComponent('a').extend.attrs({
  href: '#',
  tabIndex: ({ isDisabled }) => (isDisabled ? '-1' : null),
})`
  cursor: default;
  text-decoration: none;
  height: ${props => props.sDimension}px;
  
  :hover, :active, :visited {
    color: ${props => inputColor(props.isDisabled)};
  }
`;

export const StyledInputSelectValue = styled.div`
  position: relative;
  box-sizing: border-box;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const StyledInputSelectPlaceholder = StyledInputSelectValue.extend`
  color: ${STYLES.COLORS.BLACK_24};
`;

export const StyledInputViewMode = StyledInput.withComponent('a').extend.attrs({
  href: '#',
  tabIndex: -1,
})`
  cursor: default;
  text-decoration: none;
  min-height: ${props => props.sDimension}px;
  height: ${props => inputWithRowsHeight(props.sDimension, props.sRows)}px;
  overflow: hidden;
  
  :hover, :active, :visited {
    color: ${props => inputColor(props.isDisabled)};
  }
`;

export const StyledInputViewModeValue = styled.div`
  position: relative;
  box-sizing: border-box;
  height: 100%;
  white-space: ${props => (props.sRows === 1 ? 'nowrap' : 'pre-line')};
  word-wrap: break-word;
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: ${props => (props.sTextAlign ? props.sTextAlign : 'left')};
`;

export const StyledInputViewModePlaceholder = StyledInputViewModeValue.extend`
  color: ${STYLES.COLORS.BLACK_24};
`;

export const StyledInputWithIcon = styled.div`
  position: relative;
  box-sizing: border-box;
  width: ${props => inputWidth(props.sWidth)};
  max-width: 100%;

  ${StyledInputText}, ${StyledInputTextArea}, ${StyledInputSelect} {
    padding-right: ${ICON_DIMENSIONS.XS + 8}px;
  }
`;

export const StyledInputIcon = styled.div`
  position: absolute;
  box-sizing: border-box;
  top: ${props => inputIconTop(props.sDimension)}px;
  right: 5px;
`;

export const StyledInputOptions = styled.div.attrs({
  style: ({ sStyle, sDefaultWidth }) => ({
    ...inputOptionsStyle(sStyle, sDefaultWidth),
  }),
})`
  position: relative;
  box-sizing: border-box;
  overflow-y: auto;
  padding: 8px 0;
`;

const StyledInputOptionsItemPrimitive = styled.div`
  position: relative;
  box-sizing: border-box;
  min-height: ${props => props.sDimension}px;
  width: 100%;
  line-height: ${props => inputLineHeight(props.sDimension)}px;
  padding: ${props => inputPaddingV(props.sDimension)}px 8px;
  border-top: 1px solid ${STYLES.COLORS.TRANSPARENT};
  border-bottom: 1px solid ${STYLES.COLORS.TRANSPARENT};
  font-family: inherit;
  font-size: ${props => inputFontSize(props.sDimension)}px;
  word-wrap: break-word;
  background-color: ${props => inputOptionsItemBackgroundColor(props.isSelected, props.isActive)};
`;

export const StyledInputOptionsItem = StyledInputOptionsItemPrimitive.extend`
  cursor: pointer;
`;

export const StyledInputOptionsEmpty = StyledInputOptionsItemPrimitive.extend`
  font-style: italic;
  cursor: default;
  color: ${STYLES.COLORS.BLACK_48};
`;

export const StyledInputCalendar = styled.div`
  position: relative;
  box-sizing: border-box;
`;

export const StyledInputSwitch = styled.label`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  min-height: ${props => props.sDimension}px;
  min-width: 22px;
  cursor: default;
  border-top: 1px solid ${STYLES.COLORS.TRANSPARENT};
  border-bottom: 1px solid ${STYLES.COLORS.TRANSPARENT};
  padding: ${props => inputPaddingV(props.sDimension)}px 0;
  line-height: ${props => inputLineHeight(props.sDimension)}px;
  font-family: inherit;
  font-size: ${props => inputFontSize(props.sDimension)}px;
  color: ${props => inputColor(props.isDisabled)};
`;

export const StyledInputViewModeSwitch = StyledInputSwitch.withComponent('a').extend.attrs({
  href: '#',
  tabIndex: -1,
})`
  outline: 0 none;
  text-decoration: none;
  
  :hover, :active, :visited {
    color: ${props => inputColor(props.isDisabled)};
  }
`;

export const StyledInputSwitchHtmlInput = styled.input`
  position: absolute;
  top: ${props => inputSwitchIconTop(props.sDimension) + 1}px;
  left: 1px;
  height: 0;
  width: 0;
  opacity: 0;
  border: none;
  outline: none;
`;

export const StyledInputCheckboxHtmlInput = StyledInputSwitchHtmlInput.extend.attrs({ type: 'checkbox' })``;

export const StyledInputRadioHtmlInput = StyledInputSwitchHtmlInput.extend.attrs({ type: 'radio' })``;

export const StyledInputSwitchIcon = styled.div`
  position: absolute;
  box-sizing: border-box;
  z-index: 1;
  top: ${props => inputSwitchIconTop(props.sDimension)}px;
  left: 0;
  height: ${props => inputSwitchIconDimension(props.sDimension)}px;
  width: ${props => inputSwitchIconDimension(props.sDimension)}px;
  background: ${props => inputSwitchBackgroundColor(props.isChecked, props.isDisabled, props.isViewMode)};
  border: 1px ${props => (props.isViewMode ? 'dotted' : 'solid')}
    ${props => inputBorderColor(props.isWarning, props.isError, props.isDisabled, props.isFocused, props.isViewMode)};
  transition: border-color 0.32s ease-out;
  overflow: hidden;

  ${StyledInputSwitch}:hover & {
    border-color: ${props =>
      inputBorderColor(props.isWarning, props.isError, props.isDisabled, props.isFocused, props.isViewMode, true)};
  }

  ${StyledIcon} {
    top: -1px;
    left: -1px;
  }
`;

export const StyledInputCheckboxIcon = StyledInputSwitchIcon.extend``;

export const StyledInputRadioIcon = StyledInputSwitchIcon.extend`
  border-radius: 50%;
`;

export const StyledInputSwitchTitle = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  margin-left: ${props => inputSwitchTitleMarginLeft(props.sDimension)}px;
`;

export const StyledInputFiles = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  font-family: inherit;
  font-size: ${props => inputFontSize(props.sDimension)}px;
  background: ${props => inputBackgroundColor(props.isDisabled, props.isViewMode)};
  border: 1px ${props => (props.isViewMode ? 'dotted' : 'solid')}
    ${props => inputBorderColor(props.isWarning, props.isError, props.isDisabled, false, props.isViewMode)};
  transition: border-color 0.32s ease-out;
  width: ${props => inputWidth(props.sWidth)};
  max-width: 100%;
`;

export const StyledInputTable = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  font-family: inherit;
  border-top: 1px solid
    ${props => inputBorderColor(props.isWarning, props.isError, props.isDisabled, false, props.isViewMode)};
  border-bottom: 1px solid
    ${props => inputBorderColor(props.isWarning, props.isError, props.isDisabled, false, props.isViewMode)};
  transition: border-color 0.32s ease-out;
`;

export const StyledInputTableAdd = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px 40px;
`;

export const StyledInputHiddenHtmlInput = styled.input.attrs({
  type: 'text',
  tabIndex: -1,
})`
  position: absolute;
  top: 0;
  left: 0;
  height: 0;
  width: 0;
  padding: 0;
  margin: 0;
  border: 0;
  opacity: 0;
`;
