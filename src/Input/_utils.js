import moment from 'moment-timezone';

export const getChar = e => {
  if (e.which === null) {
    if (e.keyCode < 32) return null;
    return String.fromCharCode(e.keyCode);
  }
  if (e.which !== 0 && e.charCode !== 0) {
    if (e.which < 32) return null;
    return String.fromCharCode(e.which);
  }
  return null;
};

export const getCaretPosition = (value, beforeCaretPosValue, firstPosition) => {
  let pos = 0;
  let conj = 0;
  if (beforeCaretPosValue) {
    value.split('').some(symbol => {
      pos += 1;
      if (symbol === beforeCaretPosValue[conj]) {
        conj += 1;
        if (conj === beforeCaretPosValue.length) return true;
      }
      return false;
    });
  }
  if (firstPosition && pos < firstPosition) return firstPosition;
  return pos;
};

export const getStringFromMomentValue = value => {
  if (value === null) return value;
  const momentValue = moment(value);
  return momentValue.isValid() ? momentValue.format('DD.MM.YYYY') : '';
};

export const getStringDateTimeFromMomentValue = value => {
  if (value === null) return value;
  const momentValue = moment(value);
  return momentValue.isValid() ? momentValue.format('DD.MM.YYYY HH:mm') : '';
};

export const convertMomentValue = value => {
  if (value === null) return null;
  if (typeof value === 'string') {
    const momentValue = moment(value);
    return momentValue.isValid() ? momentValue : null;
  }
  return value.isValid && value.isValid() ? value.format() : null;
};

export const getStringFromNumberValue = value => (typeof value === 'number' ? value.toString() : value);

export const getOptionTitle = (value, options) => {
  if (!value || !options) return '';
  const selectedOption = options.find(option => option.value === value);
  return selectedOption ? selectedOption.title : '';
};

export const formatTextValue = (value, maxLength) => {
  if (value === null) return '';
  return maxLength ? value.substring(0, maxLength) : value;
};

export const deformatTextValue = value => {
  if (value === '') return null;
  return value;
};

export const formatAccountValue = value => {
  if (value === null) return '';
  const preparedValue = value.replace(/\D/g, '').substring(0, 20);

  const resultValue = [];
  preparedValue.split('').forEach((symbol, index) => {
    resultValue.push(symbol);
    if (index !== preparedValue.length - 1 && [4, 7, 8].includes(index)) resultValue.push('.');
  });
  return resultValue.join('');
};

export const formatDateValue = value => {
  if (value === null) return '';
  const dottedValue = value
    .replace(/[^\d\\.]/g, '')
    .replace(/\.+/g, '.')
    .substring(0, 10);
  const preparedValue = value.replace(/\D/g, '').substring(0, 8);

  const resultValue = [];
  preparedValue.split('').forEach((symbol, index) => {
    resultValue.push(symbol);
    if (index !== preparedValue.length - 1 && [1, 3].includes(index)) resultValue.push('.');
  });
  if ([3, 6].includes(dottedValue.length) && dottedValue[dottedValue.length - 1] === '.') resultValue.push('.');
  return resultValue.join('');
};

export const formatDateTimeValue = value => {
  if (value === null) return '';
  const dottedValue = value
    .replace(/[^\d\\.]/g, '')
    .replace(/\.+/g, '.')
    .substring(0, 14);
  const preparedValue = value.replace(/\D/g, '').substring(0, 12);

  const resultValue = [];
  preparedValue.split('').forEach((symbol, index) => {
    resultValue.push(symbol);
    if (index !== preparedValue.length - 1 && [1, 3].includes(index)) resultValue.push('.');
    if (index !== preparedValue.length - 1 && [7].includes(index)) resultValue.push(' ');
    if (index !== preparedValue.length - 1 && [9].includes(index)) resultValue.push(':');
  });
  if ([3, 6].includes(dottedValue.length) && dottedValue[dottedValue.length - 1] === '.') resultValue.push('.');
  if ([10].includes(dottedValue.length) && dottedValue[dottedValue.length - 1] === ' ') resultValue.push(' ');
  if ([13].includes(dottedValue.length) && dottedValue[dottedValue.length - 1] === ':') resultValue.push(':');
  return resultValue.join('');
};

export const deformatDateValue = (value, disabledDate = () => false) => {
  if (value.length < 10) return null;
  const momentValue = moment(value, 'DD.MM.YYYY');
  const isValid = /^\d{2}\.\d{2}\.\d{4}$/gi.test(value) && momentValue.isValid() && !disabledDate(momentValue);
  return isValid ? momentValue.format() : null;
};

export const deformatDateTimeValue = value => {
  if (value.length < 16) return null;
  const momentValue = moment(value, 'DD.MM.YYYY HH:mm');
  const isValid = /^\d{2}\.\d{2}\.\d{4}\s+\d{2}[,-:]\d{2}$/gi.test(value) && momentValue.isValid();
  return isValid ? momentValue.format() : null;
};

export const formatDigitalValue = (value, maxLength) => {
  if (value === null) return '';
  const result = value.replace(/\D/g, '');
  return maxLength ? result.substring(0, maxLength) : value;
};

export const deformatDigitalValue = value => {
  if (value === '') return null;
  return value.replace(/\D/g, '');
};

export const formatNumberDecimalValue = (value, isNegative, isFormatted, maxLength, frictionLength, isFinal) => {
  const stringValue = getStringFromNumberValue(value);
  if (stringValue === null) return '';

  const minus = !isNegative || stringValue.indexOf('-') < 0 ? '' : '-';
  const preparedValue = stringValue
    .replace(',', '.')
    .replace(/[^\d\\.]/g, '')
    .replace(/^0+/, '0')
    .replace(/^0([\d\\.]+)$/, '$1');
  const dotIndex = preparedValue.indexOf('.');
  const integerValue = preparedValue
    .substring(0, dotIndex < 0 ? preparedValue.length : dotIndex)
    .substring(0, maxLength);
  const decimalValue = dotIndex < 0 ? '' : preparedValue.substring(dotIndex + 1).substring(0, frictionLength);

  let integerResultValue = integerValue;
  if (isFormatted) {
    const integerResultValueArray = [];
    integerValue
      .split('')
      .reverse()
      .forEach((symbol, index) => {
        if (index && !(index % 3)) integerResultValueArray.unshift(' ');
        integerResultValueArray.unshift(symbol);
      });
    integerResultValue = integerResultValueArray.join('');
  }

  const emptyDecimal = Array(frictionLength)
    .fill('0')
    .join('');
  const decimalResultValue = isFinal ? `${decimalValue}${emptyDecimal}`.substring(0, frictionLength) : decimalValue;

  if (isFinal && !integerResultValue.length) return `${minus}0.${decimalResultValue}`;
  if (isFinal) return `${minus}${integerResultValue}.${decimalResultValue}`;
  if (!integerResultValue.length && dotIndex > -1) return `${minus}0.${decimalResultValue}`;
  if (!integerResultValue.length) return minus;
  if (integerResultValue.length && dotIndex > -1) return `${minus}${integerResultValue}.${decimalResultValue}`;
  return `${minus}${integerResultValue}`;
};

export const deformatNumberDecimalValue = value => {
  if (value === '' || value === '-') return null;
  return parseFloat(value.replace(/[^-\d\\.]/g, ''));
};

export const formatNumberIntegerValue = (value, isNegative, isFormatted, maxLength, isFinal) => {
  const stringValue = getStringFromNumberValue(value);
  if (stringValue === null) return '';

  const minus = !isNegative || stringValue.indexOf('-') < 0 ? '' : '-';
  const preparedValue = stringValue
    .replace(/[^\d]/g, '')
    .replace(/^0+/, '0')
    .replace(/^0([\d]+)$/, '$1');
  const integerValue = preparedValue.substring(0, maxLength);

  let integerResultValue = integerValue;
  if (isFormatted) {
    const integerResultValueArray = [];
    integerValue
      .split('')
      .reverse()
      .forEach((symbol, index) => {
        if (index && !(index % 3)) integerResultValueArray.unshift(' ');
        integerResultValueArray.unshift(symbol);
      });
    integerResultValue = integerResultValueArray.join('');
  }

  if (isFinal) return `${minus}${integerResultValue}`;
  if (!integerResultValue.length) return minus;
  return `${minus}${integerResultValue}`;
};

export const deformatNumberIntegerValue = value => {
  if (value === '' || value === '-') return null;
  return parseInt(value.replace(/[^-\d]/g, ''), 10);
};

export const formatCardNumberValue = (value, maxLength) => {
  if (value === null) return '';
  const preparedValue = value.replace(/\D/g, '').substring(0, maxLength);

  const resultValue = [];
  preparedValue.split('').forEach((symbol, index) => {
    resultValue.push(symbol);
    if (index !== preparedValue.length - 1 && [3, 7, 11].includes(index)) resultValue.push(' ');
  });
  return resultValue.join('');
};

export const formatPhoneValue = value => {
  if (value === null) return '';
  const preparedValue = value
    .replace('+', '')
    .replace(/\D/g, '')
    .substring(0, 11);

  const resultValue = [];
  if (preparedValue.length) resultValue.push('+');
  preparedValue.split('').forEach((symbol, index) => {
    resultValue.push(symbol);
    if (index !== preparedValue.length - 1 && index === 0) resultValue.push('(');
    if (index !== preparedValue.length - 1 && index === 3) resultValue.push(')');
    if (index !== preparedValue.length - 1 && [6, 8].includes(index)) resultValue.push('-');
  });
  return resultValue.join('');
};

export const deformatPhoneValue = value => {
  if (value === '') return null;
  return `${value.replace(/[^+\d]/g, '')}`;
};
