import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENT } from './_constants';
import REFS from './_config';
import { StyledLoader, StyledLoaderIcon, StyledLoaderContent } from './_style';

const Loader = props => (
  <StyledLoader
    {...addDataAttributes({ component: COMPONENT })}
    sDisplay={props.display}
    sDimension={props.dimension}
    isCentered={props.isCentered}
    sWithChildren={!!props.children}
  >
    <StyledLoaderIcon sDimension={props.dimension} />
    {props.children && <StyledLoaderContent>{props.children}</StyledLoaderContent>}
  </StyledLoader>
);

Loader.propTypes = {
  children: PropTypes.node,
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** dimension size (width & height) in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** place to center of container flag */
  isCentered: PropTypes.bool,
};

Loader.defaultProps = {
  children: null,
  display: REFS.DISPLAY.INLINE_BLOCK,
  dimension: REFS.DIMENSIONS.M,
  isCentered: false,
};

Loader.REFS = {
  ...REFS,
};

export default Loader;
