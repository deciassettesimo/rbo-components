### REFS
```js static
{
  DISPLAY: {
    BLOCK: 'block',
    INLINE_BLOCK: 'inline-block',
  },
  DIMENSIONS: {
    XS: 16,
    S: 20,
    M: 24,
    L: 32,
    XL: 48,
  },
}
```

```js
<div className="list">
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.XS} />
  </div>
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.S} />
  </div>
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.M} />
  </div>
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.L} />
  </div>
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.XL} />
  </div>
</div>
```

```js
<div className="list">
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.XS}>Loading in progress</Loader>
  </div>
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.S}>Loading in progress</Loader>
  </div>
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.M}>Loading in progress</Loader>
  </div>
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.L}>Loading in progress</Loader>
  </div>
  <div className="inline">
    <Loader dimension={Loader.REFS.DIMENSIONS.XL}>Loading in progress</Loader>
  </div>
</div>
```

```js
<div className="list">
  <div style={{ border: '1px solid #ccc', height: 200 }}>
    <Loader isCentered/>
  </div>
  <div style={{ border: '1px solid #ccc', height: 200 }}>
    <Loader dimension={Loader.REFS.DIMENSIONS.XL} isCentered>Loading in progress</Loader>
  </div>
</div>
```

