import { DISPLAY, DIMENSIONS } from './_constants';

const REFS = {
  DISPLAY,
  DIMENSIONS,
};

export default REFS;
