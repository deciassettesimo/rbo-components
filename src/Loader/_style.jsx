import styled, { keyframes } from 'styled-components';

import { STYLES } from '../_constants';
import { DIMENSIONS } from './_constants';

const loaderFontSize = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 12;
  return 14;
};

const loaderPositioning = isCentered => {
  if (isCentered) {
    return {
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translateX(-50%) translateY(-50%)',
    };
  }
  return {
    position: 'relative',
  };
};

const rotate360 = keyframes`from { transform: rotate(0deg); } to { transform: rotate(360deg); }`;

export const StyledLoader = styled.div.attrs({
  style: ({ isCentered }) => loaderPositioning(isCentered),
})`
  box-sizing: border-box;
  vertical-align: middle;
  display: ${props => props.sDisplay};
  padding-top: ${props => props.sDimension}px;
  min-width: ${props => props.sDimension}px;
  width: ${props => (props.sWithChildren ? 'auto' : props.sDimension)};
  font-size: ${props => loaderFontSize(props.sDimension)}px;
`;

export const StyledLoaderIcon = styled.div`
  position: absolute;
  box-sizing: border-box;
  top: 0;
  left: 50%;
  height: ${props => props.sDimension}px;
  width: ${props => props.sDimension}px;
  margin-left: -${props => props.sDimension / 2}px;
  border: ${props => props.sDimension / 8}px solid ${STYLES.COLORS.BLACK};
  border-left-color: ${STYLES.COLORS.CORPORATE_YELLOW};
  border-top-color: ${STYLES.COLORS.CORPORATE_YELLOW};
  border-radius: 50%;
  animation: ${rotate360} 1.2s linear infinite;
`;

export const StyledLoaderContent = styled.div`
  position: relative;
  box-sizing: border-box;
  margin-top: 4px;
`;
