import React, { Component, createContext, Fragment } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import ClickableIcon from '../ClickableIcon';
import Card from '../Card';
import { COMPONENTS } from './_constants';
import { StyledModalWindowFade, StyledModalWindow, StyledModalWindowClose, StyledModalWindowInner } from './_style';
import REFS from './_config';

export const ModalWindowContext = createContext({});

class ModalWindowWithRef extends Component {
  static REFS = { ...REFS };

  static propTypes = {
    children: PropTypes.node.isRequired,
    vAlign: PropTypes.oneOf(Object.values(REFS.V_ALIGN)),
    align: PropTypes.oneOf(Object.values(REFS.ALIGN)),
    isClosingOnEscPress: PropTypes.bool,
    isClosingOnOutClick: PropTypes.bool,
    isWithoutClosingIcon: PropTypes.bool,
    isWithoutShade: PropTypes.bool,
    onClose: PropTypes.func,
    forwardedRef: PropTypes.shape(),
  };

  static defaultProps = {
    vAlign: REFS.V_ALIGN.CENTER,
    align: REFS.ALIGN.CENTER,
    isClosingOnEscPress: true,
    isClosingOnOutClick: true,
    isWithoutClosingIcon: false,
    isWithoutShade: false,
    onClose: () => null,
    forwardedRef: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isMounted: false,
      innerNodeHeight: null,
      innerNodeWidth: null,
    };

    this.innerNode = React.createRef();
    this.bodyNode = document.body;
    this.bodyNodeStyleOverflow = '';
  }

  componentDidMount() {
    this.bodyNodeStyleOverflow = window.getComputedStyle(this.bodyNode).overflow;
    this.bodyNode.style.overflow = 'hidden';
    window.addEventListener('resize', this.setOffset);
    document.addEventListener('scroll', this.setOffset);
    document.addEventListener('click', this.handleClick, true);
    document.addEventListener('touchstart', this.handleClick, true);
    document.addEventListener('keydown', this.handleKeyPress);
    this.setOffset();
  }

  componentDidUpdate() {
    const innerNodeBCRect = this.innerNode.getBoundingClientRect();
    if (this.state.innerNodeHeight !== innerNodeBCRect.height || this.state.innerNodeWidth !== innerNodeBCRect.width) {
      this.setOffset();
    }
  }

  componentWillUnmount() {
    this.bodyNode.style.overflow = this.bodyNodeStyleOverflow;
    window.removeEventListener('resize', this.setOffset);
    document.removeEventListener('scroll', this.setOffset);
    document.removeEventListener('click', this.handleClick, true);
    document.removeEventListener('touchstart', this.handleClick, true);
    document.removeEventListener('keypress', this.handleKeyPress);
  }

  setOffset = () => {
    if (!this.innerNode) return;
    const innerNodeBCRect = this.innerNode.getBoundingClientRect();

    this.setState({
      isMounted: true,
      innerNodeHeight: innerNodeBCRect.height,
      innerNodeWidth: innerNodeBCRect.width,
    });
  };

  handleClick = e => {
    if (this.props.isClosingOnOutClick && !this.innerNode.contains(e.target)) {
      e.preventDefault();
      this.props.onClose();
    }
  };

  handleKeyPress = e => {
    if (this.props.isClosingOnEscPress && e.which === 27) this.props.onClose();
  };

  render() {
    return createPortal(
      <Fragment>
        {!this.props.isWithoutShade && (
          <StyledModalWindowFade {...addDataAttributes({ component: COMPONENTS.MODAL_WINDOW_FADE })} />
        )}
        <StyledModalWindow
          {...addDataAttributes({ component: COMPONENTS.MODAL_WINDOW })}
          innerRef={ref => {
            if (this.props.forwardedRef) this.props.forwardedRef.current = ref;
          }}
          isMounted={this.state.isMounted}
          sVAlign={this.props.vAlign}
          sAlign={this.props.align}
        >
          <StyledModalWindowInner
            innerRef={ref => {
              this.innerNode = ref;
            }}
          >
            <ModalWindowContext.Provider value={{ popupBoxZIndex: 14 }}>
              <Card isBordered={false} isShadowed>
                {this.props.children}
              </Card>
            </ModalWindowContext.Provider>
            {!this.props.isWithoutClosingIcon && (
              <StyledModalWindowClose>
                <ClickableIcon
                  type={ClickableIcon.REFS.TYPES.CLOSE}
                  dimension={ClickableIcon.REFS.DIMENSIONS.S}
                  isExpanded
                  onClick={this.props.onClose}
                />
              </StyledModalWindowClose>
            )}
          </StyledModalWindowInner>
        </StyledModalWindow>
      </Fragment>,
      this.bodyNode,
    );
  }
}

const ModalWindow = React.forwardRef((props, ref) => <ModalWindowWithRef {...props} forwardedRef={ref} />);

ModalWindow.REFS = { ...REFS };

ModalWindow.propTypes = {
  children: PropTypes.node.isRequired,
  vAlign: PropTypes.oneOf(Object.values(REFS.V_ALIGN)),
  align: PropTypes.oneOf(Object.values(REFS.ALIGN)),
  isClosingOnEscPress: PropTypes.bool,
  isClosingOnOutClick: PropTypes.bool,
  isWithoutClosingIcon: PropTypes.bool,
  isWithoutShade: PropTypes.bool,
  onClose: PropTypes.func,
};

ModalWindow.defaultProps = {
  vAlign: REFS.V_ALIGN.CENTER,
  align: REFS.ALIGN.CENTER,
  isClosingOnEscPress: true,
  isClosingOnOutClick: true,
  isWithoutClosingIcon: false,
  isWithoutShade: false,
  onClose: () => null,
};

export default ModalWindow;
