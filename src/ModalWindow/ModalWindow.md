### ModalWindow vAlign TOP align LEFT

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  vAlign={ModalWindow.REFS.V_ALIGN.TOP}
  align={ModalWindow.REFS.ALIGN.LEFT}
/>
```

### ModalWindow vAlign CENTER align LEFT

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  vAlign={ModalWindow.REFS.V_ALIGN.CENTER}
  align={ModalWindow.REFS.ALIGN.LEFT}
/>
```

### ModalWindow vAlign BOTTOM align LEFT

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  vAlign={ModalWindow.REFS.V_ALIGN.BOTTOM}
  align={ModalWindow.REFS.ALIGN.LEFT}
/>
```

### ModalWindow vAlign TOP align CENTER

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  vAlign={ModalWindow.REFS.V_ALIGN.TOP}
  align={ModalWindow.REFS.ALIGN.CENTER}
/>
```

### ModalWindow vAlign CENTER align CENTER

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  vAlign={ModalWindow.REFS.V_ALIGN.CENTER}
  align={ModalWindow.REFS.ALIGN.CENTER}
/>
```

### ModalWindow vAlign BOTTOM align CENTER

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  vAlign={ModalWindow.REFS.V_ALIGN.BOTTOM}
  align={ModalWindow.REFS.ALIGN.CENTER}
/>
```

### ModalWindow vAlign TOP align RIGHT

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  vAlign={ModalWindow.REFS.V_ALIGN.TOP}
  align={ModalWindow.REFS.ALIGN.RIGHT}
/>
```

### ModalWindow vAlign CENTER align RIGHT

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  vAlign={ModalWindow.REFS.V_ALIGN.CENTER}
  align={ModalWindow.REFS.ALIGN.RIGHT}
/>
```

### ModalWindow vAlign BOTTOM align RIGHT

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  vAlign={ModalWindow.REFS.V_ALIGN.BOTTOM}
  align={ModalWindow.REFS.ALIGN.RIGHT}
/>
```

#### ModalWindow Example Source Code
```js { "file": "./__examples__/ModalWindow.example.jsx" }
```

### ModalWindow isWithoutClosingIcon and isWithoutShade

```jsx
const ModalWindowExample = require('./__examples__/ModalWindow.example').default;
<ModalWindowExample
  isWithoutClosingIcon
  isWithoutShade
/>
```

