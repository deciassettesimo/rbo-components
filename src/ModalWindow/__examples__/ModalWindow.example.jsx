import React, { Fragment, PureComponent } from 'react';
import Button from '../../Button';
import ModalWindow from '../ModalWindow';

export default class ModalWindowExample extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = { isOpen: false };
  }

  handleModalOpen = () => {
    this.setState({ isOpen: true });
  };

  handleModalClose = () => {
    this.setState({ isOpen: false });
  };

  render() {
    return (
      <Fragment>
        <Button id="ModalWindowOpener" onClick={this.handleModalOpen}>
          Открыть модальное окно
        </Button>
        {this.state.isOpen && (
          <ModalWindow {...this.props} onClose={this.handleModalClose}>
            <div style={{ height: 400, width: 640 }}>Модальное окно</div>
          </ModalWindow>
        )}
      </Fragment>
    );
  }
}
