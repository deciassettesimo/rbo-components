import { V_ALIGN, ALIGN } from './_constants';

const REFS = {
  V_ALIGN,
  ALIGN,
};

export default REFS;
