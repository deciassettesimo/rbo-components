export const COMPONENTS = {
  MODAL_WINDOW: 'ModalWindow',
  MODAL_WINDOW_FADE: 'ModalWindowFade',
};

export const V_ALIGN = {
  TOP: 'top',
  CENTER: 'center',
  BOTTOM: 'bottom',
};

export const ALIGN = {
  LEFT: 'left',
  CENTER: 'center',
  RIGHT: 'right',
};
