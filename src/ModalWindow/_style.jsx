import styled from 'styled-components';

import { STYLES } from '../_constants';
import { V_ALIGN, ALIGN } from './_constants';

const modalWindowPositioning = (vAlign, align) => {
  const viewportHeight = document.documentElement.clientHeight || window.innerHeight;
  const viewportWidth = document.documentElement.clientWidth || window.innerWidth;

  let top = 'auto';
  let bottom = 'auto';
  let left = 'auto';
  let right = 'auto';
  let transform = 'none';
  let translateX = 0;
  let translateY = 0;
  const maxHeight = viewportHeight;
  const maxWidth = viewportWidth;

  switch (vAlign) {
    case V_ALIGN.TOP:
      top = 0;
      break;
    case V_ALIGN.BOTTOM:
      bottom = 0;
      break;
    case V_ALIGN.CENTER:
    default:
      top = '50%';
      translateY = '-50%';
  }

  switch (align) {
    case ALIGN.LEFT:
      left = 0;
      break;
    case ALIGN.RIGHT:
      right = 0;
      break;
    case ALIGN.CENTER:
    default:
      left = '50%';
      translateX = '-50%';
  }

  if (translateX || translateY) transform = `translate(${translateX}, ${translateY})`;

  return { top, bottom, left, right, transform, maxHeight, maxWidth };
};

export const StyledModalWindowFade = styled.div`
  position: fixed;
  box-sizing: border-box;
  display: block;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: 12;
  background: ${STYLES.COLORS.BLACK_72};
`;

export const StyledModalWindow = styled.div.attrs({
  style: ({ sVAlign, sAlign }) => ({
    ...modalWindowPositioning(sVAlign, sAlign),
  }),
})`
  position: fixed;
  box-sizing: border-box;
  display: block;
  z-index: 13;
  opacity: ${props => (props.isMounted ? 1 : 0)};
  overflow: auto;
`;

export const StyledModalWindowClose = styled.div`
  position: absolute;
  top: 0;
  right: 0;
`;

export const StyledModalWindowInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
`;
