import React, { Component, createContext } from 'react';
import PropTypes from 'prop-types';
import { v4 } from 'uuid';

import { addDataAttributes } from '../_utils';

import OperationsPanelMain from './_internal/OperationsPanelMain';
import OperationsPanelExtra from './_internal/OperationsPanelExtra';
import { StyledOperationPanel } from './_style';
import { COMPONENTS, MORE_BUTTON_WIDTH } from './_constants';
import REFS from './_config';

export const OperationsPanelContext = createContext({});

export default class OperationsPanel extends Component {
  static REFS = { ...REFS };

  static propTypes = {
    /** language for text */
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    /** Dimension size (height) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    align: PropTypes.oneOf(Object.values(REFS.ALIGN)),
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        icon: PropTypes.string,
        disabled: PropTypes.bool,
        progress: PropTypes.bool,
      }),
    ).isRequired,
    onItemClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: REFS.LOCALES.RU,
    dimension: REFS.DIMENSIONS.M,
    align: REFS.ALIGN.LEFT,
  };

  constructor(props) {
    super(props);

    this.state = {
      visibleItemsQty: 0,
      visibleItemsWidth: 0,
    };

    this.iframe = v4();
    this.node = React.createRef();
    this.itemsNodes = {};
  }

  componentDidMount() {
    window[this.iframe].addEventListener('resize', this.handleUpdate);
    this.handleUpdate();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.items !== this.props.items) {
      this.handleUpdate();
    }
  }

  componentWillUnmount() {
    if (window[this.iframe]) window[this.iframe].removeEventListener('resize', this.handleUpdate);
  }

  setItemNode = (id, node) => {
    this.itemsNodes[id] = node;
  };

  handleUpdate = () => {
    let availableWidth = this.node.current.getBoundingClientRect().width - 24 - MORE_BUTTON_WIDTH[this.props.dimension];

    const itemsNodes = this.props.items.map(item => this.itemsNodes[item.id]);

    let visibleItemsWidth = 0;
    let visibleItemsQty = itemsNodes.length;
    itemsNodes.every((item, index) => {
      const childOffsetWidth = item && item.current ? item.current.getBoundingClientRect().width : 0;
      if (index === itemsNodes.length - 1) availableWidth += MORE_BUTTON_WIDTH[this.props.dimension];
      if (availableWidth < visibleItemsWidth + childOffsetWidth) {
        visibleItemsQty = index;
        return false;
      }
      visibleItemsWidth += childOffsetWidth;
      return true;
    });
    this.setState({
      visibleItemsQty,
      visibleItemsWidth,
    });
  };

  render() {
    const extraItems =
      this.props.items.length > this.state.visibleItemsQty ? this.props.items.slice(this.state.visibleItemsQty) : null;

    return (
      <OperationsPanelContext.Provider value={{ setItemNode: this.setItemNode }}>
        <StyledOperationPanel
          {...addDataAttributes({ component: COMPONENTS.GENERAL })}
          innerRef={ref => {
            this.node.current = ref;
          }}
          sDimension={this.props.dimension}
          sAlign={this.props.align}
        >
          <iframe
            name={this.iframe}
            title="operationPanel"
            width="100%"
            height="0"
            style={{ position: 'absolute', left: 0, top: 0, opacity: 0, zIndex: '-1' }}
          />
          <OperationsPanelMain
            dimension={this.props.dimension}
            width={this.state.visibleItemsWidth}
            items={this.props.items}
            onItemClick={this.props.onItemClick}
          />
          {extraItems && (
            <OperationsPanelExtra
              dimension={this.props.dimension}
              items={extraItems}
              onItemClick={this.props.onItemClick}
              locale={this.props.locale}
            />
          )}
        </StyledOperationPanel>
      </OperationsPanelContext.Provider>
    );
  }
}
