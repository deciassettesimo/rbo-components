### REFS
```js static
{
  LOCALES = {
    RU: 'ru',
    EN: 'en',
  },
  DIMENSIONS: {
    XS: 32,
    S: 40,
    M: 48,
    L: 56,
    XL: 64,
  },
}
```

### OperationsPanel Example

```jsx
const OperationsPanelExample = require('./__examples__/OperationsPanel.example').default;
<OperationsPanelExample />
```
#### OperationsPanel Example Source Code
```js { "file": "./__examples__/OperationsPanel.example.jsx" }
```

### OperationsPanel Dimensions

```js
const items = [
     {
       id: 'back',
       icon: 'arrow-left-tailed',
       disabled: false,
       progress: false,
     },
     {
       id: 'save',
       icon: 'save',
       title: 'Сохранить',
       disabled: false,
       progress: false,
     },
     {
       id: 'print',
       icon: 'print',
       title: 'Распечатать',
       disabled: false,
       progress: false,
     },
     {
       id: 'remove',
       icon: 'remove',
       title: 'Удалить',
       disabled: false,
       progress: false,
     },
     {
       id: 'repeat',
       icon: 'repeat',
       title: 'Повторить',
       disabled: false,
       progress: false,
     },
     {
       id: 'document-sign-and-send',
       icon: 'document-sign-and-send',
       title: 'Подписать и отправить',
       disabled: false,
       progress: false,
     },
     {
       id: 'document-sign',
       icon: 'document-sign',
       title: 'Подписать',
       disabled: false,
       progress: false,
     }
];
<div className="list">
  <div>
    <OperationsPanel
      dimension={OperationsPanel.REFS.DIMENSIONS.XS}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <OperationsPanel
      dimension={OperationsPanel.REFS.DIMENSIONS.S}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <OperationsPanel
      dimension={OperationsPanel.REFS.DIMENSIONS.M}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <OperationsPanel
      dimension={OperationsPanel.REFS.DIMENSIONS.L}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <OperationsPanel
      dimension={OperationsPanel.REFS.DIMENSIONS.XL}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
</div>
```

```js
<div>
  <OperationsPanel
    onItemClick={(id) => {console.log(id);}}
    items={[
     {
       id: 'back1',
       icon: 'arrow-left-tailed',
       disabled: true,
       progress: false,
     },
     {
       id: 'save1',
       icon: 'save',
       title: 'Сохранить',
       disabled: true,
       progress: false,
     },
     {
       id: 'print',
       icon: 'print',
       title: 'Распечатать',
       disabled: true,
       progress: true,
     },
     {
       id: 'remove',
       icon: 'remove',
       title: 'Удалить',
       disabled: true,
       progress: false,
     },
     {
       id: 'repeat',
       icon: 'repeat',
       title: 'Повторить',
       disabled: true,
       progress: false,
     },
     {
       id: 'document-sign-and-send',
       icon: 'document-sign-and-send',
       title: 'Подписать и отправить',
       disabled: true,
       progress: false,
     },
     {
       id: 'document-sign',
       icon: 'document-sign',
       title: 'Подписать',
       disabled: true,
       progress: false,
     },
     {
       id: 'document-send',
       icon: 'document-send',
       title: 'Отправить',
       disabled: true,
       progress: false,
     },
     {
       id: 'document-sign-visa',
       icon: 'document-sign-visa',
       title: 'Виза',
       disabled: true,
       progress: false,
     },
     {
       id: 'archive',
       icon: 'archive',
       title: 'Переместить в архив',
       disabled: true,
       progress: false,
     },
     {
       id: 'document-save-as-template',
       icon: 'document-save-as-template',
       title: 'Сохранить как шаблон',
       disabled: true,
       progress: false,
     }
   ]}
  />
</div>
```

```js
<div>
  <OperationsPanel
    locale={OperationsPanel.REFS.LOCALES.EN}
    onItemClick={(id) => {console.log(id);}}
    items={[
     {
       id: 'back1',
       icon: 'arrow-left-tailed',
       disabled: false,
       progress: false,
     },
     {
       id: 'save1',
       icon: 'save',
       title: 'Save',
       disabled: false,
       progress: false,
     },
     {
       id: 'print',
       icon: 'print',
       title: 'Print',
       disabled: false,
       progress: false,
     },
     {
       id: 'remove',
       icon: 'remove',
       title: 'Remove',
       disabled: false,
       progress: false,
     },
     {
       id: 'repeat',
       icon: 'repeat',
       title: 'Repeat',
       disabled: false,
       progress: false,
     },
     {
       id: 'document-sign-and-send',
       icon: 'document-sign-and-send',
       title: 'Sign and Send',
       disabled: false,
       progress: false,
     },
     {
       id: 'document-sign',
       icon: 'document-sign',
       title: 'Sign',
       disabled: false,
       progress: false,
     },
     {
       id: 'document-send',
       icon: 'document-send',
       title: 'Send',
       disabled: false,
       progress: false,
     },
     {
       id: 'archive',
       icon: 'archive',
       title: 'Archive',
       disabled: false,
       progress: false,
     },
     {
       id: 'document-save-as-template',
       icon: 'document-save-as-template',
       title: 'Save as template',
       disabled: false,
       progress: false,
     }
   ]}
  />
</div>
```
