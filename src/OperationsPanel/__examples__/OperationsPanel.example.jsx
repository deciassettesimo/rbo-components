import React, { PureComponent } from 'react';
import OperationsPanel from '../OperationsPanel';

const operations = [
  {
    id: 'back',
    icon: 'arrow-left-tailed',
    disabled: false,
    progress: false,
  },
  {
    id: 'save',
    icon: 'save',
    title: 'Сохранить',
    disabled: false,
    progress: false,
  },
  {
    id: 'print',
    icon: 'print',
    title: 'Распечатать',
    disabled: false,
    progress: false,
  },
  {
    id: 'remove',
    icon: 'remove',
    title: 'Удалить',
    disabled: false,
    progress: false,
  },
  {
    id: 'repeat',
    icon: 'repeat',
    title: 'Повторить',
    disabled: false,
    progress: false,
  },
  {
    id: 'document-sign-and-send',
    icon: 'document-sign-and-send',
    title: 'Подписать и отправить',
    disabled: false,
    progress: false,
  },
  {
    id: 'document-sign',
    icon: 'document-sign',
    title: 'Подписать',
    disabled: false,
    progress: false,
  },
];

export default class OperationsPanelExample extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = { operations: operations.filter(item => ['back', 'save', 'print', 'remove'].includes(item.id)) };
  }

  handleItemClick = id => {
    this.setState({
      operations: operations
        .filter(item => {
          switch (id) {
            case 'back':
              return ['back', 'save', 'print', 'remove'].includes(item.id);
            default:
              return true;
          }
        })
        .map(item => ({
          ...item,
          progress: item.id === id,
        })),
    });
  };

  render() {
    return <OperationsPanel items={this.state.operations} onItemClick={this.handleItemClick} />;
  }
}
