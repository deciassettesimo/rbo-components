import { LOCALES, DIMENSIONS, ALIGN } from './_constants';

const REFS = {
  LOCALES,
  DIMENSIONS,
  ALIGN,
};

export default REFS;
