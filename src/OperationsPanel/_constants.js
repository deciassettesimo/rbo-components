import { GLOBAL } from '../_constants';

export const COMPONENTS = {
  GENERAL: 'OperationPanel',
  EXTRA: 'OperationPanelExtra',
  ITEM: 'OperationPanelItem',
  MAIN: 'OperationPanelMain',
};

export const EXTRA_OPENER_ID = 'OperationPanelExtraOpener';

export const LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const LABELS = {
  ...GLOBAL.LABELS.OPERATIONS_PANEL,
};

export const DIMENSIONS = {
  XS: 32,
  S: 40,
  M: 48,
  L: 56,
  XL: 64,
};

export const ALIGN = {
  LEFT: 'left',
  RIGHT: 'right',
};

export const MORE_BUTTON_WIDTH = {
  [DIMENSIONS.XS]: 64,
  [DIMENSIONS.S]: 74,
  [DIMENSIONS.M]: 88,
  [DIMENSIONS.L]: 92,
  [DIMENSIONS.XL]: 112,
};
