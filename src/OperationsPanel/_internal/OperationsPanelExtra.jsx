import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Card from '../../Card';
import Popup, { Opener, Box } from '../../Popup';
import { TYPES as ICON_TYPES } from '../../Icon/_constants';
import { COMPONENTS, EXTRA_OPENER_ID, LOCALES, LABELS, DIMENSIONS } from '../_constants';
import { StyledOperationsPanelExtra, StyledOperationsPanelExtraPopup } from '../_style';
import OperationsPanelItem from './OperationsPanelItem';

export default class OperationPanelExtra extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)).isRequired,
    dimension: PropTypes.oneOf(Object.values(DIMENSIONS)).isRequired,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        icon: PropTypes.string,
        disabled: PropTypes.bool,
        progress: PropTypes.bool,
      }),
    ).isRequired,
    onItemClick: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isPopupOpen: false,
    };
  }

  getOpenerTitle = locale => {
    switch (locale) {
      case LOCALES.EN:
        return LABELS.EN.EXTRA_OPENER;
      case LOCALES.RU:
      default:
        return LABELS.RU.EXTRA_OPENER;
    }
  };

  handlePopupOpen = () => {
    this.setState({ isPopupOpen: true });
  };

  handlePopupClose = () => {
    this.setState({ isPopupOpen: false });
  };

  handleItemClick = id => {
    this.handlePopupClose();
    this.props.onItemClick(id);
  };

  render() {
    return (
      <StyledOperationsPanelExtra {...addDataAttributes({ component: COMPONENTS.EXTRA })}>
        <Popup isOpened={this.state.isPopupOpen} onOpen={this.handlePopupOpen} onClose={this.handlePopupClose}>
          <Opener display={Opener.REFS.DISPLAY.BLOCK}>
            <OperationsPanelItem
              id={EXTRA_OPENER_ID}
              dimension={this.props.dimension}
              title={this.getOpenerTitle(this.props.locale)}
              icon={ICON_TYPES.CONTEXT_HORIZONTAL}
              onClick={() => {}}
            />
          </Opener>
          <Box placement={Box.REFS.PLACEMENT.BOTTOM} align={Box.REFS.ALIGN.END}>
            <Card isShadowed>
              <StyledOperationsPanelExtraPopup>
                {this.props.items.map(item => (
                  <OperationsPanelItem
                    key={item.id}
                    id={item.id}
                    dimension={this.props.dimension}
                    title={item.title}
                    icon={item.icon}
                    disabled={item.disabled}
                    progress={item.progress}
                    isExtra
                    onClick={this.handleItemClick}
                  />
                ))}
              </StyledOperationsPanelExtraPopup>
            </Card>
          </Box>
        </Popup>
      </StyledOperationsPanelExtra>
    );
  }
}
