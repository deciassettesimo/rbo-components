import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Icon from '../../Icon';
import { COMPONENTS, DIMENSIONS } from '../_constants';
import { OperationsPanelContext } from '../OperationsPanel';
import { StyledOperationsPanelItem, StyledOperationsPanelItemIcon, StyledOperationsPanelItemTitle } from '../_style';

class OperationsPanelItemWithContext extends PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    dimension: PropTypes.oneOf(Object.values(DIMENSIONS)).isRequired,
    title: PropTypes.string,
    icon: PropTypes.oneOf(Object.values(Icon.REFS.TYPES)),
    isMain: PropTypes.bool,
    isExtra: PropTypes.bool,
    disabled: PropTypes.bool,
    progress: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
    setItemNode: PropTypes.func.isRequired,
  };

  static defaultProps = {
    title: null,
    icon: null,
    isMain: false,
    isExtra: false,
    disabled: false,
    progress: false,
  };

  constructor(props) {
    super(props);

    this.node = React.createRef();
  }

  componentDidMount() {
    if (this.props.isMain) this.props.setItemNode(this.props.id, this.node);
  }

  getIconDimension = () => {
    switch (this.props.dimension) {
      case DIMENSIONS.XS:
      case DIMENSIONS.S:
      case DIMENSIONS.M:
        return Icon.REFS.DIMENSIONS.XS;
      case DIMENSIONS.XL:
        return Icon.REFS.DIMENSIONS.M;
      default:
        return Icon.REFS.DIMENSIONS.S;
    }
  };

  handleOnClick = () => {
    if (!this.props.disabled && !this.props.progress) {
      this.props.onClick(this.props.id);
    }
  };

  render() {
    return (
      <StyledOperationsPanelItem
        {...addDataAttributes({ component: COMPONENTS.ITEM, id: this.props.id })}
        innerRef={ref => {
          this.node.current = ref;
        }}
        sDimension={this.props.dimension}
        onClick={this.handleOnClick}
        isExtra={this.props.isExtra}
        isDisabled={this.props.disabled}
        isProgress={this.props.progress}
      >
        {this.props.icon && (
          <StyledOperationsPanelItemIcon>
            <Icon
              type={this.props.icon}
              display={Icon.REFS.DISPLAY.BLOCK}
              dimension={this.getIconDimension()}
              color={this.props.disabled || this.props.progress ? Icon.REFS.COLORS.BLACK_48 : Icon.REFS.COLORS.BLACK}
            />
          </StyledOperationsPanelItemIcon>
        )}
        {this.props.title && (
          <StyledOperationsPanelItemTitle sDimension={this.props.dimension}>
            {this.props.title}
          </StyledOperationsPanelItemTitle>
        )}
      </StyledOperationsPanelItem>
    );
  }
}

const OperationsPanelItem = props => (
  <OperationsPanelContext.Consumer>
    {({ setItemNode }) => <OperationsPanelItemWithContext {...props} setItemNode={setItemNode} />}
  </OperationsPanelContext.Consumer>
);

export default OperationsPanelItem;
