import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS, DIMENSIONS } from '../_constants';
import { StyledOperationsPanelMain } from '../_style';
import OperationsPanelItem from './OperationsPanelItem';

const OperationsPanelMain = props => (
  <StyledOperationsPanelMain
    {...addDataAttributes({ component: COMPONENTS.MAIN })}
    sDimension={props.dimension}
    sWidth={props.width}
  >
    {props.items.map(item => (
      <OperationsPanelItem
        key={item.id}
        id={item.id}
        dimension={props.dimension}
        title={item.title}
        icon={item.icon}
        disabled={item.disabled}
        progress={item.progress}
        isMain
        onClick={props.onItemClick}
      />
    ))}
  </StyledOperationsPanelMain>
);

OperationsPanelMain.propTypes = {
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)).isRequired,
  width: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string,
      icon: PropTypes.string,
      disabled: PropTypes.bool,
      progress: PropTypes.bool,
    }),
  ).isRequired,
  onItemClick: PropTypes.func.isRequired,
};

export default OperationsPanelMain;
