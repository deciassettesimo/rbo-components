import styled from 'styled-components';

import { STYLES } from '../_constants';
import { DIMENSIONS, ALIGN } from './_constants';

const operationsPanelJustifyContent = sAlign => {
  switch (sAlign) {
    case ALIGN.RIGHT:
      return 'flex-end';
    case ALIGN.LEFT:
    default:
      return 'flex-start';
  }
};

const operationsPanelFontSize = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 12;
  if (sDimension === DIMENSIONS.S) return 12;
  if (sDimension === DIMENSIONS.XL) return 16;
  return 14;
};

const operationsPanelLineHeight = sDimension => {
  if (sDimension === DIMENSIONS.XS) return 14;
  if (sDimension === DIMENSIONS.S) return 14;
  if (sDimension === DIMENSIONS.L) return 20;
  if (sDimension === DIMENSIONS.XL) return 24;
  return 16;
};

const operationsPanelItemPadding = sDimension => (sDimension - operationsPanelLineHeight(sDimension)) / 2;

const operationsPanelItemCursor = (isDisabled, isProgress) => {
  if (isProgress) return 'progress';
  if (isDisabled) return 'not-allowed';
  return 'pointer';
};

const operationsPanelItemColor = (isDisabled, isProgress) => {
  if (isDisabled || isProgress) return STYLES.COLORS.BLACK_48;
  return STYLES.COLORS.BLACK;
};

const operationsPanelItemBackgroundColor = (isDisabled, isProgress, isHovered) => {
  if (isProgress) return STYLES.COLORS.PARCHMENT;
  if (isDisabled) return STYLES.COLORS.WHITE;
  if (isHovered) return STYLES.COLORS.ICE_BLUE;
  return STYLES.COLORS.TRANSPARENT;
};

const operationsPanelItemTitleMarginLeft = sDimension => operationsPanelItemPadding(sDimension) / 2;

export const StyledOperationPanel = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  height: ${props => props.sDimension}px;
  justify-content: ${props => operationsPanelJustifyContent(props.sAlign)};
`;

export const StyledOperationsPanelMain = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  height: ${props => props.sDimension}px;
  width: ${props => props.sWidth}px;
  white-space: nowrap;
  overflow: hidden;
`;

export const StyledOperationsPanelExtra = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  height: ${props => props.sDimension}px;
`;

export const StyledOperationsPanelExtraPopup = styled.div`
  padding: 8px 1px 8px 0;
`;

export const StyledOperationsPanelItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: ${props => (props.isExtra ? 'block' : 'inline-block')};
  text-align: ${props => (props.isExtra ? 'left' : 'center')};
  height: ${props => props.sDimension}px;
  min-width: ${props => props.sDimension}px;
  padding: ${props => operationsPanelItemPadding(props.sDimension)}px;
  line-height: ${props => operationsPanelLineHeight(props.sDimension)}px;
  font-size: ${props => operationsPanelFontSize(props.sDimension)}px;
  white-space: nowrap;
  cursor: ${props => operationsPanelItemCursor(props.isDisabled, props.isProgress)};
  color: ${props => operationsPanelItemColor(props.isDisabled, props.isProgress)};
  background-color: ${props => operationsPanelItemBackgroundColor(props.isDisabled, props.isProgress)};
  transition: background-color 0.32s ease-out;

  &:hover {
    background-color: ${props => operationsPanelItemBackgroundColor(props.isDisabled, props.isProgress, true)};
  }
`;

export const StyledOperationsPanelItemIcon = styled.div`
  display: inline-block;
  vertical-align: middle;
`;

export const StyledOperationsPanelItemTitle = styled.div`
  display: inline-block;
  vertical-align: middle;
  margin-left: ${props => operationsPanelItemTitleMarginLeft(props.sDimension)}px;
`;
