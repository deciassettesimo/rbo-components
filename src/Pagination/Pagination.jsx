import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import IconArrowLeftDouble from '../Icon/IconArrowLeftDouble';
import IconArrowLeft from '../Icon/IconArrowLeft';
import IconArrowRight from '../Icon/IconArrowRight';
import { COMPONENTS } from './_constants';
import REFS from './_config';
import { getLabel, getItems } from './_utils';
import { StyledPagination, StyledPaginationDescribe, StyledPaginationItems } from './_style';
import PaginationItem from './_internal/PaginationItem';
import PaginationSettings from './_internal/PaginationSettings';

const Pagination = props => (
  <StyledPagination {...addDataAttributes({ component: COMPONENTS.GENERAL })}>
    {props.settings && (
      <PaginationSettings
        locale={props.locale}
        value={props.settings.value}
        options={props.settings.options}
        onChange={props.onSettingsChange}
      />
    )}
    {props.describe && <StyledPaginationDescribe>{props.describe}</StyledPaginationDescribe>}
    <StyledPaginationItems>
      {props.showToStart &&
        props.selected !== 1 && (
          <PaginationItem
            dimension={props.dimension}
            index={1}
            icon={IconArrowLeftDouble}
            title={getLabel('TO_START', props.locale)}
            onClick={props.onItemClick}
          />
        )}
      {props.selected !== 1 && (
        <PaginationItem
          dimension={props.dimension}
          index={props.selected - 1}
          icon={IconArrowLeft}
          title={getLabel('PREV', props.locale)}
          onClick={props.onItemClick}
        />
      )}
      {!props.hidePages &&
        getItems(props.selected, props.visibleQty, props.totalQty).map(item => (
          <PaginationItem
            dimension={props.dimension}
            key={item}
            index={item}
            label={item.toString()}
            isSelected={item === props.selected}
            onClick={props.onItemClick}
          />
        ))}
      {props.selected !== props.totalQty && (
        <PaginationItem
          dimension={props.dimension}
          index={props.selected + 1}
          icon={IconArrowRight}
          title={getLabel('NEXT', props.locale)}
          onClick={props.onItemClick}
        />
      )}
    </StyledPaginationItems>
  </StyledPagination>
);

Pagination.propTypes = {
  /** language for text */
  locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  selected: PropTypes.number.isRequired,
  visibleQty: PropTypes.number.isRequired,
  totalQty: PropTypes.number.isRequired,
  describe: PropTypes.string,
  showToStart: PropTypes.bool,
  hidePages: PropTypes.bool,
  onItemClick: PropTypes.func.isRequired,
  settings: PropTypes.shape({
    value: PropTypes.number,
    options: PropTypes.arrayOf(PropTypes.number).isRequired,
  }),
  onSettingsChange: PropTypes.func,
};

Pagination.defaultProps = {
  locale: REFS.LOCALES.RU,
  dimension: REFS.DIMENSIONS.M,
  describe: null,
  showToStart: false,
  hidePages: false,
  settings: null,
  onSettingsChange: null,
};

Pagination.REFS = { ...REFS };

export default Pagination;
