```js
const pages = {
  selected: 1,
  visibleQty: 1,
  totalQty: 2,
};
<Pagination {...pages} onItemClick={index => {console.log(index)}} />
```

```js
const pages = {
  selected: 3,
  visibleQty: 1,
  totalQty: 3,
  settings: { options: [30, 60, 120, 240], value: 30 },
};
<Pagination {...pages} onItemClick={index => {console.log(index)}} onSettingsChange={value => {console.log(value)}} />
```

```js
const pages = {
  selected: 3,
  visibleQty: 1,
  totalQty: 3,
  settings: { options: [30, 60, 120, 240], value: 30 },
};
<Pagination {...pages} locale={Pagination.REFS.LOCALES.EN} onItemClick={index => {console.log(index)}} onSettingsChange={value => {console.log(value)}} />
```

```js
const pages = {
  selected: 3,
  visibleQty: 1,
  totalQty: 5,
};
<Pagination {...pages} onItemClick={index => {console.log(index)}} />
```

```js
const pages = {
  selected: 3,
  visibleQty: 1,
  totalQty: 5,
};
<Pagination {...pages} onItemClick={index => {console.log(index)}} showToStart />
```

```js
const pages = {
  selected: 8,
  visibleQty: 5,
  totalQty: 19,
};
<Pagination {...pages} onItemClick={index => {console.log(index)}} />
```

```js
const pages = {
  selected: 8,
  visibleQty: 5,
  totalQty: 19,
};
<Pagination {...pages} dimension={Pagination.REFS.DIMENSIONS.S} onItemClick={index => {console.log(index)}} />
```

```js
const pages = {
  selected: 2,
  visibleQty: 1,
  totalQty: 19,
};
<Pagination {...pages} onItemClick={index => {console.log(index)}} describe="1-30 из 900" hidePages />
```

```js
const pages = {
  selected: 2,
  visibleQty: 1,
  totalQty: 19,
};
<Pagination {...pages} dimension={Pagination.REFS.DIMENSIONS.S} onItemClick={index => {console.log(index)}} describe="1-30 из 900" hidePages />
```

```js
const pages = {
  selected: 8,
  visibleQty: 5,
  totalQty: 19,
  settings: { options: [30, 60, 120, 240], value: 30 },
};
<Pagination {...pages} dimension={Pagination.REFS.DIMENSIONS.S} onItemClick={index => {console.log(index)}} onSettingsChange={value => {console.log(value)}} />
```

