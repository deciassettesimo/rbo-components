import { LOCALES, DIMENSIONS } from './_constants';

const REFS = {
  LOCALES,
  DIMENSIONS,
};

export default REFS;
