import { GLOBAL } from '../_constants';

export const COMPONENTS = {
  GENERAL: 'Pagination',
  SETTINGS: 'PaginationSettings',
  ITEM: 'PaginationItem',
};

export const SELECT_ID = 'PaginationSettingsSelect';

export const DIMENSIONS = {
  S: 24,
  M: 32,
};

export const LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const LABELS = {
  ...GLOBAL.LABELS.PAGINATION,
};
