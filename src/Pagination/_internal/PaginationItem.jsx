import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS, DIMENSIONS } from '../_constants';
import { StyledPaginationItem } from '../_style';

const PaginationItem = props => {
  const handleClick = e => {
    e.stopPropagation();
    if (!props.isSelected) props.onClick(props.index);
  };

  return (
    <StyledPaginationItem
      {...addDataAttributes({ component: COMPONENTS.ITEM })}
      sDimension={props.dimension}
      isSelected={props.isSelected}
      title={props.title}
      onClick={handleClick}
    >
      {props.icon && <props.icon dimension={props.icon.REFS.DIMENSIONS.XS} display={props.icon.REFS.DISPLAY.BLOCK} />}
      {props.label}
    </StyledPaginationItem>
  );
};

PaginationItem.propTypes = {
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  index: PropTypes.number.isRequired,
  icon: PropTypes.func,
  label: PropTypes.string,
  title: PropTypes.string,
  isSelected: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

PaginationItem.defaultProps = {
  dimension: DIMENSIONS.M,
  icon: null,
  label: null,
  title: null,
  isSelected: false,
};

export default PaginationItem;
