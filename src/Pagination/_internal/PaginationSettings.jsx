import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import InputSelect from '../../Input/InputSelect';
import Label from '../../Styles/Label';
import { COMPONENTS, LOCALES, SELECT_ID } from '../_constants';
import { getLabel, getSettingOptions } from '../_utils';
import { StyledPaginationSettings, StyledPaginationSettingsText, StyledPaginationSettingsField } from '../_style';

const PaginationSettings = props => {
  const handleChange = ({ value }) => {
    props.onChange(parseInt(value, 10));
  };

  return (
    <StyledPaginationSettings {...addDataAttributes({ component: COMPONENTS.SETTINGS })}>
      <StyledPaginationSettingsText>
        <Label>{getLabel('SETTINGS_PREFIX', props.locale)}</Label>
      </StyledPaginationSettingsText>
      <StyledPaginationSettingsField>
        <InputSelect
          id={SELECT_ID}
          width={64}
          dimension={InputSelect.REFS.DIMENSIONS.XS}
          value={props.value && props.value.toString()}
          options={getSettingOptions(props.options)}
          onChange={handleChange}
        />
      </StyledPaginationSettingsField>
      <StyledPaginationSettingsText>
        <Label>{getLabel('SETTINGS_POSTFIX', props.locale)}</Label>
      </StyledPaginationSettingsText>
    </StyledPaginationSettings>
  );
};

PaginationSettings.propTypes = {
  locale: PropTypes.oneOf(Object.values(LOCALES)),
  value: PropTypes.number,
  options: PropTypes.arrayOf(PropTypes.number).isRequired,
  onChange: PropTypes.func.isRequired,
};

PaginationSettings.defaultProps = {
  locale: LOCALES.RU,
  value: null,
};

export default PaginationSettings;
