import styled from 'styled-components';

import { STYLES } from '../_constants';

export const StyledPagination = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  white-space: nowrap;
`;

export const StyledPaginationDescribe = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  font-size: 14px;
  line-height: 16px;
  margin-right: 8px;
`;

export const StyledPaginationSettings = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  margin-right: 16px;
  font-size: 12px;
`;

export const StyledPaginationSettingsText = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
`;

export const StyledPaginationSettingsField = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  margin: 0 4px;
`;

export const StyledPaginationItems = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  white-space: nowrap;
  margin-right: 0 -4px;
`;

export const StyledPaginationItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  white-space: nowrap;
  vertical-align: middle;
  margin: 0 4px;
  height: ${props => props.sDimension}px;
  min-width: ${props => props.sDimension}px;
  background: ${STYLES.COLORS.WHITE};
  color: ${props => (props.isSelected ? STYLES.COLORS.BLACK : STYLES.COLORS.GRAYISH_BROWN)};
  border: 1px solid ${props => (props.isSelected ? STYLES.COLORS.BLACK : STYLES.COLORS.BLACK_12)};
  font-size: 12px;
  line-height: 16px;
  padding: ${props => (props.sDimension - 16) / 2 - 1}px;
  text-align: center;
  transition: border-color 0.32s ease-out;
  cursor: ${props => (props.isSelected ? 'default' : 'pointer')};

  :hover {
    border-color: ${STYLES.COLORS.BLACK};
  }
`;
