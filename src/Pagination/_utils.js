import { LABELS, LOCALES } from './_constants';

export const getLabel = (key, locale) => {
  switch (locale) {
    case LOCALES.EN:
      return LABELS.EN[key];
    case LOCALES.RU:
    default:
      return LABELS.RU[key];
  }
};

export const getItems = (selected, visibleQty, totalQty) => {
  let endIndex = parseInt(totalQty - selected < visibleQty / 2 ? totalQty : selected + visibleQty / 2, 10);
  let startIndex = endIndex - visibleQty + 1;
  if (startIndex < 1) {
    startIndex = 1;
    endIndex = visibleQty;
  }
  const list = [];
  for (let i = startIndex; i <= endIndex; i += 1) list.push(i);
  return list;
};

export const getSettingOptions = options =>
  options.map(option => ({ value: option.toString(), title: option.toString() }));
