import PropTypes from 'prop-types';

import REFS from './_config';
import { TYPES } from './_constants';
import * as Items from './_items';

const PaymentSystemLogoMap = {
  [TYPES.MASTERCARD]: Items.PaymentSystemLogoMastercard,
  [TYPES.VISA]: Items.PaymentSystemLogoVisa,
};

const PaymentSystemLogo = props =>
  PaymentSystemLogoMap[props.type]({
    display: props.display,
    dimension: props.dimension,
    title: props.title,
  });

PaymentSystemLogo.propTypes = {
  /** type of payment system */
  type: PropTypes.oneOf(Object.values(TYPES)).isRequired,
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** height in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

PaymentSystemLogo.REFS = {
  ...REFS,
  TYPES,
};

export default PaymentSystemLogo;
