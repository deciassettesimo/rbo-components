import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import PaymentSystemLogoPrimitive from './_internal/PaymentSystemLogoPrimitive';

const PaymentSystemLogoMastercard = props => (
  <PaymentSystemLogoPrimitive {...props} type={TYPES.MASTERCARD}>
    <g transform="translate(9.000000, 3.000000)">
      <path
        d="M29.25,9 C29.25,13.97025 25.22025,18 20.25,18 C15.27975,18 11.25,13.97025 11.25,9 C11.25,4.02975 15.27975,0 20.25,0 C25.22025,0 29.25,4.02975 29.25,9"
        fill="#F79E1B"
      />
      <path
        d="M11.25,9 C11.25,6.159375 12.569625,3.630375 14.625,1.98 C13.08375,0.743625 11.129625,0 9,0 C4.02975,0 0,4.02975 0,9 C0,13.97025 4.02975,18 9,18 C11.129625,18 13.08375,17.256375 14.625,16.02 C12.569625,14.369625 11.25,11.840625 11.25,9"
        fill="#EB001B"
      />
      <path
        d="M18,9 C18,6.159375 16.680375,3.630375 14.625,1.98 C12.569625,3.630375 11.25,6.159375 11.25,9 C11.25,11.840625 12.569625,14.369625 14.625,16.02 C16.680375,14.369625 18,11.840625 18,9"
        fill="#FF5A00"
      />
    </g>
  </PaymentSystemLogoPrimitive>
);

PaymentSystemLogoMastercard.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** height in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

PaymentSystemLogoMastercard.defaultProps = {
  display: REFS.DISPLAY.BLOCK,
  dimension: REFS.DIMENSIONS.M,
  title: null,
};

PaymentSystemLogoMastercard.REFS = {
  ...REFS,
};

export default PaymentSystemLogoMastercard;
