```js
<div className="list">
  <div className="inline">
    <PaymentSystemLogoMastercard dimension={PaymentSystemLogoMastercard.REFS.DIMENSIONS.XS} />
  </div>
  <div className="inline">
    <PaymentSystemLogoMastercard dimension={PaymentSystemLogoMastercard.REFS.DIMENSIONS.S} />
  </div>
  <div className="inline">
    <PaymentSystemLogoMastercard dimension={PaymentSystemLogoMastercard.REFS.DIMENSIONS.M} />
  </div>
  <div className="inline">
    <PaymentSystemLogoMastercard dimension={PaymentSystemLogoMastercard.REFS.DIMENSIONS.L} />
  </div>
</div>
```