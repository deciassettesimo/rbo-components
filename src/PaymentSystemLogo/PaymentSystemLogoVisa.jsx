import React from 'react';
import PropTypes from 'prop-types';

import { TYPES } from './_constants';
import REFS from './_config';
import PaymentSystemLogoPrimitive from './_internal/PaymentSystemLogoPrimitive';

const PaymentSystemLogoVisa = props => (
  <PaymentSystemLogoPrimitive {...props} type={TYPES.VISA}>
    <path
      d="M22.794,5.287 L19.988,18.405 L16.594,18.405 L19.4,5.287 L22.793,5.287 L22.794,5.287 Z M37.071,13.757 L38.858,8.831 L39.886,13.757 L37.071,13.757 Z M40.858,18.405 L43.997,18.405 L41.257,5.287 L38.36,5.287 C37.709,5.287 37.16,5.666 36.916,6.249 L31.824,18.405 L35.386,18.405 L36.094,16.445 L40.448,16.445 L40.858,18.405 Z M27.246,8.922 C27.256,8.452 27.705,7.952 28.686,7.823 C29.171,7.76 30.512,7.711 32.031,8.41 L32.627,5.629 C31.6116901,5.24776192 30.5365198,5.05067558 29.452,5.047 C26.098,5.047 23.737,6.83 23.718,9.383 C23.696,11.273 25.403,12.325 26.69,12.953 C28.011,13.596 28.455,14.009 28.45,14.583 C28.44,15.464 27.395,15.853 26.418,15.867 C24.712,15.894 23.722,15.407 22.933,15.039 L22.319,17.913 C23.111,18.277 24.575,18.594 26.092,18.61 C29.658,18.61 31.989,16.85 32,14.122 C32.015,10.66 27.213,10.469 27.246,8.922 Z M12.446,18.405 L8.86,18.405 L6.154,7.935 C5.99,7.291 5.847,7.055 5.348,6.784 C4.532,6.34 3.185,5.925 2,5.667 L2.08,5.287 L7.855,5.287 C8.59,5.287 9.252,5.777 9.419,6.624 L10.849,14.215 L14.38,5.287 L17.945,5.287 L12.447,18.405 L12.446,18.405 Z"
      fill="#1B226F"
    />
  </PaymentSystemLogoPrimitive>
);

PaymentSystemLogoVisa.propTypes = {
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** height in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** html-attr title (e.g. 'Icon Title') */
  title: PropTypes.string,
};

PaymentSystemLogoVisa.defaultProps = {
  display: REFS.DISPLAY.BLOCK,
  dimension: REFS.DIMENSIONS.M,
  title: null,
};

PaymentSystemLogoVisa.REFS = {
  ...REFS,
};

export default PaymentSystemLogoVisa;
