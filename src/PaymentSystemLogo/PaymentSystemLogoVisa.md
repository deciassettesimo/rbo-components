```js
<div className="list">
  <div className="inline">
    <PaymentSystemLogoVisa dimension={PaymentSystemLogoVisa.REFS.DIMENSIONS.XS} />
  </div>
  <div className="inline">
    <PaymentSystemLogoVisa dimension={PaymentSystemLogoVisa.REFS.DIMENSIONS.S} />
  </div>
  <div className="inline">
    <PaymentSystemLogoVisa dimension={PaymentSystemLogoVisa.REFS.DIMENSIONS.M} />
  </div>
  <div className="inline">
    <PaymentSystemLogoVisa dimension={PaymentSystemLogoVisa.REFS.DIMENSIONS.L} />
  </div>
</div>
```