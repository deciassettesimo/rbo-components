### REFS

```js static
{
  DISPLAY: {
    BLOCK: 'block',
    INLINE_BLOCK: 'inline-block',
  },
  DIMENSIONS: {
    XS: 16,
    S: 20,
    M: 24,
    L: 32,
  },
}
```

### PaymentSystemLogo SET

```js
<div className="list">
  <div className="inline"><PaymentSystemLogoMastercard title="PaymentSystemLogoMastercard" /></div>
  <div className="inline"><PaymentSystemLogoVisa title="PaymentSystemLogoVisa" /></div>
</div>
```