import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import PaymentSystemLogo from '../PaymentSystemLogo';

describe('PaymentSystemLogo', () => {
  const typeKeys = Object.keys(PaymentSystemLogo.REFS.TYPES);

  // loop by PaymentSystemLogo.REFS.TYPES (all icons)
  for (let i = 0; i < typeKeys.length; i += 1) {
    const iconType = PaymentSystemLogo.REFS.TYPES[typeKeys[i]];

    describe(`${typeKeys[i]} (${iconType})`, () => {
      const dimensions = Object.values(PaymentSystemLogo.REFS.DIMENSIONS);

      // loop by dimensions of PaymentSystemLogo
      for (let j = 0; j < dimensions.length; j += 1) {
        const iconDimension = dimensions[j];

        it(`dimensions ${iconDimension}`, () => {
          const wrapper = renderer.create(<PaymentSystemLogo type={iconType} dimension={iconDimension} />).toJSON();
          // expect with snapshot
          expect(wrapper).toMatchSnapshot();
        });
      }
    });
  }
});
