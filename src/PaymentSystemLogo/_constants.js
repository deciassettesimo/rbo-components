import { STYLES } from '../_constants';

export const COMPONENT = 'PaymentSystemLogo';

export const TYPES = {
  MASTERCARD: 'mastercard',
  VISA: 'visa',
};

export const DISPLAY = {
  BLOCK: STYLES.DISPLAY.BLOCK,
  INLINE_BLOCK: STYLES.DISPLAY.INLINE_BLOCK,
};

export const DIMENSIONS = {
  XS: 16,
  S: 20,
  M: 24,
  L: 32,
};
