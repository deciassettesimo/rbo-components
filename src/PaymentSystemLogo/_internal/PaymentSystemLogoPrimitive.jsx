import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENT, TYPES } from '../_constants';
import REFS from '../_config';
import { StyledPaymentSystemLogo, StyledPaymentSystemLogoSvg } from '../_style';

const PaymentSystemLogoPrimitive = props => (
  <StyledPaymentSystemLogo
    {...addDataAttributes({ component: COMPONENT, type: props.type })}
    title={props.title}
    sDisplay={props.display}
    sDimension={props.dimension}
  >
    <StyledPaymentSystemLogoSvg viewBox="0 0 48 24" focusable="false">
      {props.children}
    </StyledPaymentSystemLogoSvg>
  </StyledPaymentSystemLogo>
);

PaymentSystemLogoPrimitive.propTypes = {
  type: PropTypes.oneOf(Object.values(TYPES)),
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
};

PaymentSystemLogoPrimitive.defaultProps = {
  type: null,
  display: REFS.DISPLAY.BLOCK,
  dimension: REFS.DIMENSIONS.M,
  title: null,
};

export default PaymentSystemLogoPrimitive;
