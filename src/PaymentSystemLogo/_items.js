export { default as PaymentSystemLogoMastercard } from './PaymentSystemLogoMastercard';
export { default as PaymentSystemLogoVisa } from './PaymentSystemLogoVisa';
