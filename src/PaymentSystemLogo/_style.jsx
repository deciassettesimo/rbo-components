import styled from 'styled-components';

export const StyledPaymentSystemLogo = styled.div`
  position: relative;
  box-sizing: border-box;
  vertical-align: middle;
  display: ${props => props.sDisplay};
  height: ${props => props.sDimension}px;
  width: ${props => props.sDimension * 2}px;
`;

export const StyledPaymentSystemLogoSvg = styled.svg`
  position: relative;
  display: block;
  width: 100%;
  height: 100%;
`;
