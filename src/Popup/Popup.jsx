import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import PopupContext from './_context';
import PopupOpener from './_internal/PopupOpener';
import PopupBox from './_internal/PopupBox';

export default class Popup extends PureComponent {
  static propTypes = {
    /** children must be Popup.Opener and Popup.Box components */
    children: PropTypes.node.isRequired,
    /** opened box flag */
    isOpened: PropTypes.bool,
    /** open popup handler */
    onOpen: PropTypes.func,
    /** close popup handler */
    onClose: PropTypes.func,
    /** Stop Propagation on open */
    isStopPropagation: PropTypes.bool,
  };

  static defaultProps = {
    isOpened: false,
    isStopPropagation: false,
    onOpen: () => null,
    onClose: () => null,
  };

  constructor(props) {
    super(props);

    this.state = {
      openerNode: null,
      setOpenerNode: this.setOpenerNode,
      isOpened: this.props.isOpened,
      isStopPropagation: this.props.isStopPropagation,
      onOpen: this.onOpen,
      onClose: this.onClose,
      onToggle: this.onToggle,
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.isOpened !== state.isOpened) {
      return { isOpened: props.isOpened };
    }
    return null;
  }

  onOpen = () => {
    this.setState({ isOpened: true });
    this.props.onOpen();
  };

  onClose = () => {
    this.setState({ isOpened: false });
    this.props.onClose();
  };

  onToggle = () => {
    if (this.state.isOpened) this.onClose();
    else this.onOpen();
  };

  setOpenerNode = node => {
    this.setState({ openerNode: node });
  };

  render() {
    return <PopupContext.Provider value={{ ...this.state }}>{this.props.children}</PopupContext.Provider>;
  }
}

Popup.Opener = PopupOpener;
Popup.Box = PopupBox;
