#### Popup.Opener REFS
```js static
{
  DISPLAY = {
    INLINE: STYLES.DISPLAY.INLINE,
    INLINE_BLOCK: STYLES.DISPLAY.INLINE_BLOCK,
    BLOCK: STYLES.DISPLAY.BLOCK,
  },
}
```

#### Popup.Opener props & methods
```js static
  display: Object.values(REFS.DISPLAY),
  isNotAutoOpen: bool,
```

#### Popup.Box REFS
```js static
{
  PLACEMENT = {
    TOP: 'top',
    RIGHT: 'right',
    BOTTOM: 'bottom',
    LEFT: 'left',
  },
  ALIGN = {
    START: 'start',
    CENTER: 'center',
    END: 'end',
  },
}
```

#### Popup.Box props & methods
```js static
  placement: Object.values(REFS.PLACEMENT)
  align: Object.values(REFS.ALIGN)
  isClosingOnEscPress: bool
  isClosingOnMouseLeave: bool
```

### Popup placement BOTTOM
```js
<div className="list m-r-64">
  <div className="inline">
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">bottom start</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.BOTTOM} align={Popup.Box.REFS.ALIGN.START}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
  <div className="inline">
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">bottom center</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.BOTTOM} align={Popup.Box.REFS.ALIGN.CENTER}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
  <div className="inline">
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">bottom end</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.BOTTOM} align={Popup.Box.REFS.ALIGN.END}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
</div>
```

### Popup placement TOP
```js
<div className="list m-r-64">
  <div className="inline">
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">top start</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.TOP} align={Popup.Box.REFS.ALIGN.START}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
  <div className="inline">
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">top center</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.TOP} align={Popup.Box.REFS.ALIGN.CENTER}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
  <div className="inline">
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">top end</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.TOP} align={Popup.Box.REFS.ALIGN.END}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
</div>
```

### Popup placement RIGHT
```js
<div className="list m-b-64">
  <div>
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">right start</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.RIGHT} align={Popup.Box.REFS.ALIGN.START}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
  <div>
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">right center</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.RIGHT} align={Popup.Box.REFS.ALIGN.CENTER}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
  <div>
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">right end</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.RIGHT} align={Popup.Box.REFS.ALIGN.END}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
</div>
```

### Popup placement LEFT
```js
<div className="list m-b-64">
  <div>
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">left start</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.LEFT} align={Popup.Box.REFS.ALIGN.START}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
  <div>
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">left center</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.LEFT} align={Popup.Box.REFS.ALIGN.CENTER}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
  <div>
    <Popup>
      <Popup.Opener>
        <div className="popup-opener">left end</div>
      </Popup.Opener>
      <Popup.Box placement={Popup.Box.REFS.PLACEMENT.LEFT} align={Popup.Box.REFS.ALIGN.END}>
        <div className="popup-box">popup-box content</div>
      </Popup.Box>
    </Popup>
  </div>
</div>
```

### Popup placement with isClosingOnMouseLeave and without isClosingOnEscPress
```js
<Popup>
  <Popup.Opener>
    <div className="popup-opener">popup-opener</div>
  </Popup.Opener>
  <Popup.Box isClosingOnEscPress={false} isClosingOnMouseLeave >
    <div className="popup-box">popup-box</div>
  </Popup.Box>
</Popup>
```
