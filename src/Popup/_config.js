import { OPENER_DISPLAY, BOX_PLACEMENT, BOX_ALIGN } from './_constants';

export const OPENER_REFS = {
  DISPLAY: OPENER_DISPLAY,
};

export const BOX_REFS = {
  PLACEMENT: BOX_PLACEMENT,
  ALIGN: BOX_ALIGN,
};
