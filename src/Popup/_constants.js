import { STYLES } from '../_constants';

export const COMPONENTS = {
  BOX: 'PopupBox',
  OPENER: 'PopupOpener',
};

export const OPENER_DISPLAY = {
  INLINE: STYLES.DISPLAY.INLINE,
  INLINE_BLOCK: STYLES.DISPLAY.INLINE_BLOCK,
  BLOCK: STYLES.DISPLAY.BLOCK,
};

export const BOX_PLACEMENT = {
  TOP: 'top',
  RIGHT: 'right',
  BOTTOM: 'bottom',
  LEFT: 'left',
};

export const BOX_ALIGN = {
  START: 'start',
  CENTER: 'center',
  END: 'end',
};
