import React, { Component } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { ModalWindowContext } from '../../ModalWindow/ModalWindow';
import { RboDocLayoutPanelContext } from '../../RboDocLayout/RboDocLayout';
import PopupContext from '../_context';
import { COMPONENTS } from '../_constants';
import { StyledPopupBox, StyledPopupBoxInner } from '../_style';
import { BOX_REFS as REFS } from '../_config';

class PopupBoxWithContext extends Component {
  static propTypes = {
    openerNode: PropTypes.shape(),
    children: PropTypes.element.isRequired,
    placement: PropTypes.oneOf(Object.values(REFS.PLACEMENT)),
    align: PropTypes.oneOf(Object.values(REFS.ALIGN)),
    zIndex: PropTypes.number,
    isClosingOnEscPress: PropTypes.bool,
    isClosingOnMouseLeave: PropTypes.bool,
    onClose: PropTypes.func,
  };

  static defaultProps = {
    openerNode: null,
    placement: REFS.PLACEMENT.BOTTOM,
    align: REFS.ALIGN.START,
    zIndex: 9,
    isClosingOnEscPress: true,
    isClosingOnMouseLeave: false,
    onClose: () => null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isMounted: false,
      openerNode: this.props.openerNode,
      openerNodeTop: null,
      openerNodeLeft: null,
      openerNodeHeight: null,
      openerNodeWidth: null,
      innerNodeHeight: null,
      innerNodeWidth: null,
    };

    this.node = React.createRef();
    this.innerNode = React.createRef();
    this.bodyNode = document.body;
  }

  componentDidMount() {
    window.addEventListener('resize', this.setOffset);
    document.addEventListener('scroll', this.setOffset);
    document.addEventListener('click', this.handleClick);
    document.addEventListener('touchstart', this.handleClick);
    document.addEventListener('keydown', this.handleKeyPress);
    document.addEventListener('mousemove', this.handleOnPointerMove);
    setTimeout(this.setOffset, 0);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.openerNode && !state.openerNode) {
      return { openerNode: props.openerNode };
    }
    return null;
  }

  componentDidUpdate() {
    const innerNodeBCRect = this.innerNode.getBoundingClientRect();
    if (this.state.innerNodeHeight !== innerNodeBCRect.height || this.state.innerNodeWidth !== innerNodeBCRect.width) {
      this.setOffset();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setOffset);
    document.removeEventListener('scroll', this.setOffset);
    document.removeEventListener('click', this.handleClick);
    document.removeEventListener('touchstart', this.handleClick);
    document.removeEventListener('keydown', this.handleKeyPress);
    document.removeEventListener('mousemove', this.handleOnPointerMove);
  }

  setOffset = () => {
    if (!this.state.openerNode || !this.innerNode) return;
    const openerNodeBCRect = this.state.openerNode.getBoundingClientRect();
    const innerNodeBCRect = this.innerNode.getBoundingClientRect();

    this.setState({
      isMounted: true,
      openerNodeTop: openerNodeBCRect.top,
      openerNodeLeft: openerNodeBCRect.left,
      openerNodeHeight: openerNodeBCRect.height,
      openerNodeWidth: openerNodeBCRect.width,
      innerNodeHeight: innerNodeBCRect.height,
      innerNodeWidth: innerNodeBCRect.width,
    });
  };

  getPositioningParams = () => ({
    openerNodeTop: this.state.openerNodeTop,
    openerNodeLeft: this.state.openerNodeLeft,
    openerNodeHeight: this.state.openerNodeHeight,
    openerNodeWidth: this.state.openerNodeWidth,
    innerNodeHeight: this.state.innerNodeHeight,
    innerNodeWidth: this.state.innerNodeWidth,
  });

  handleClick = e => {
    if (!this.node.contains(e.target) && !this.state.openerNode.contains(e.target)) {
      e.preventDefault();
      this.props.onClose();
    }
  };

  handlePopupClick = e => {
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
  };

  handleKeyPress = e => {
    if (this.props.isClosingOnEscPress && e.which === 27) this.props.onClose();
  };

  handleOnPointerMove = e => {
    if (
      this.props.isClosingOnMouseLeave &&
      !this.node.contains(e.target) &&
      !this.state.openerNode.contains(e.target)
    ) {
      this.props.onClose();
    }
  };

  render() {
    return createPortal(
      <StyledPopupBox
        {...addDataAttributes({ component: COMPONENTS.BOX })}
        innerRef={ref => {
          this.node = ref;
        }}
        isMounted={this.state.isMounted}
        sPlacement={this.props.placement}
        sAlign={this.props.align}
        sPositioningParams={this.getPositioningParams()}
        sZIndex={this.props.zIndex}
        onClick={this.handlePopupClick}
      >
        <StyledPopupBoxInner
          innerRef={ref => {
            this.innerNode = ref;
          }}
        >
          {this.props.children}
        </StyledPopupBoxInner>
      </StyledPopupBox>,
      this.bodyNode,
    );
  }
}

const PopupBox = props => (
  <PopupContext.Consumer>
    {({ openerNode, isOpened, onClose }) =>
      isOpened ? (
        <ModalWindowContext.Consumer>
          {({ popupBoxZIndex: modalWindowPopupBoxZIndex }) => (
            <RboDocLayoutPanelContext.Consumer>
              {({ popupBoxZIndex: rboDocLayoutPanelPopupBoxZIndex }) => (
                <PopupBoxWithContext
                  zIndex={modalWindowPopupBoxZIndex || rboDocLayoutPanelPopupBoxZIndex}
                  {...props}
                  openerNode={openerNode}
                  onClose={onClose}
                />
              )}
            </RboDocLayoutPanelContext.Consumer>
          )}
        </ModalWindowContext.Consumer>
      ) : null
    }
  </PopupContext.Consumer>
);

PopupBox.propTypes = {
  /** children must be only one react element */
  children: PropTypes.element.isRequired,
  /** default placement of popupBox */
  placement: PropTypes.oneOf(Object.values(REFS.PLACEMENT)),
  /** default align of popupBox */
  align: PropTypes.oneOf(Object.values(REFS.ALIGN)),
  /** flag indicating that the PopupBox is closing on clicking on esc */
  isClosingOnEscPress: PropTypes.bool,
  /** flag indicating that the PopupBox is closing on mouse leave */
  isClosingOnMouseLeave: PropTypes.bool,
};

PopupBox.defaultProps = {
  placement: REFS.PLACEMENT.BOTTOM,
  align: REFS.ALIGN.START,
  isClosingOnEscPress: true,
  isClosingOnMouseLeave: false,
};

PopupBox.REFS = {
  ...REFS,
};

export default PopupBox;
