import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import PopupContext from '../_context';
import { StyledPopupOpener } from '../_style';
import { OPENER_REFS as REFS } from '../_config';

class PopupOpenerWithContext extends PureComponent {
  static propTypes = {
    children: PropTypes.element.isRequired,
    display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
    setOpenerNode: PropTypes.func.isRequired,
    onToggle: PropTypes.func,
    isStopPropagation: PropTypes.bool,
    isNotAutoOpen: PropTypes.bool,
  };

  static defaultProps = {
    display: REFS.DISPLAY.INLINE_BLOCK,
    onToggle: () => null,
    isStopPropagation: false,
    isNotAutoOpen: false,
  };

  constructor(props) {
    super(props);

    this.node = React.createRef();
  }

  componentDidMount() {
    this.props.setOpenerNode(this.node);
  }

  handleClick = e => {
    if (this.props.isStopPropagation) {
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
    }
    if (!this.props.isNotAutoOpen) this.props.onToggle(e);
  };

  render() {
    return (
      <StyledPopupOpener
        {...addDataAttributes({ component: COMPONENTS.OPENER })}
        innerRef={ref => {
          this.node = ref;
        }}
        sDisplay={this.props.display}
        onClick={this.handleClick}
      >
        {this.props.children}
      </StyledPopupOpener>
    );
  }
}

const PopupOpener = props => (
  <PopupContext.Consumer>
    {({ setOpenerNode, isStopPropagation, onToggle }) => (
      <PopupOpenerWithContext
        {...props}
        setOpenerNode={setOpenerNode}
        onToggle={onToggle}
        isStopPropagation={isStopPropagation}
      />
    )}
  </PopupContext.Consumer>
);

PopupOpener.propTypes = {
  /** children must be only one react element */
  children: PropTypes.element.isRequired,
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
};

PopupOpener.defaultProps = {
  display: REFS.DISPLAY.INLINE_BLOCK,
};

PopupOpener.REFS = {
  ...REFS,
};

export default PopupOpener;
