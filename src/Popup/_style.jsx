import styled from 'styled-components';
import { BOX_PLACEMENT, BOX_ALIGN } from './_constants';

const popupBoxPositioningVertical = (placement, align, params) => {
  if (params.openerNodeHeight === null || params.innerNodeHeight === null) return { top: 0 };

  let top = 'auto';
  let bottom = 'auto';

  const viewportHeight = document.documentElement.clientHeight || window.innerHeight;
  const spaceTop = params.openerNodeTop;
  const spaceBottom = viewportHeight - spaceTop - params.openerNodeHeight;

  if (params.innerNodeHeight > viewportHeight) {
    top = 0;
    bottom = 0;
  } else if (placement === BOX_PLACEMENT.BOTTOM) {
    top = spaceTop + params.openerNodeHeight;
    if (params.innerNodeHeight > spaceBottom) {
      top = 'auto';
      bottom = 0;
      if (spaceBottom < spaceTop) {
        bottom = spaceBottom + params.openerNodeHeight;
        if (params.innerNodeHeight > spaceTop) {
          top = 0;
          bottom = 'auto';
        }
      }
    }
  } else if (placement === BOX_PLACEMENT.TOP) {
    bottom = spaceBottom + params.openerNodeHeight;
    if (params.innerNodeHeight > spaceTop) {
      top = 0;
      bottom = 'auto';
      if (spaceTop < spaceBottom) {
        top = spaceTop + params.openerNodeHeight;
        if (params.innerNodeHeight > spaceBottom) {
          top = 'auto';
          bottom = 0;
        }
      }
    }
  } else if ([BOX_PLACEMENT.RIGHT, BOX_PLACEMENT.LEFT].includes(placement)) {
    if (align === BOX_ALIGN.START) {
      top = spaceTop;
      if (params.innerNodeHeight > params.openerNodeHeight + spaceBottom) {
        top = 'auto';
        bottom = 0;
      }
    } else if (align === BOX_ALIGN.END) {
      bottom = spaceBottom;
      if (params.innerNodeHeight > spaceTop + params.openerNodeHeight) {
        top = 0;
        bottom = 'auto';
      }
    } else if (align === BOX_ALIGN.CENTER) {
      const diffHeight = (params.innerNodeHeight - params.openerNodeHeight) / 2;
      top = diffHeight > spaceTop ? 0 : spaceTop - diffHeight;
      if (params.innerNodeHeight - diffHeight > params.openerNodeWidth + spaceBottom) {
        top = 'auto';
        bottom = 0;
      }
    }
  }

  return { top, bottom };
};

const popupBoxPositioningHorizontal = (placement, align, params) => {
  if (params.openerNodeWidth === null || params.innerNodeWIdth === null) return { left: 0 };

  let left = 'auto';
  let right = 'auto';
  const viewportWidth = document.documentElement.clientWidth || window.innerWidth;
  const spaceLeft = params.openerNodeLeft;
  const spaceRight = viewportWidth - spaceLeft - params.openerNodeWidth;

  if (params.innerNodeWidth > viewportWidth) {
    left = 0;
    right = 0;
  } else if (placement === BOX_PLACEMENT.RIGHT) {
    left = spaceLeft + params.openerNodeWidth;
    if (params.innerNodeWidth > spaceRight) {
      left = 'auto';
      right = 0;
      if (spaceRight < spaceLeft) {
        right = spaceRight + params.openerNodeWidth;
        if (params.innerNodeWidth > spaceLeft) {
          left = 0;
          right = 'auto';
        }
      }
    }
  } else if (placement === BOX_PLACEMENT.LEFT) {
    right = spaceRight + params.openerNodeWidth;
    if (params.innerNodeWidth > spaceLeft) {
      left = 0;
      right = 'auto';
      if (spaceLeft < spaceRight) {
        left = spaceLeft + params.openerNodeWidth;
        if (params.innerNodeWidth > spaceRight) {
          left = 'auto';
          right = 0;
        }
      }
    }
  } else if ([BOX_PLACEMENT.TOP, BOX_PLACEMENT.BOTTOM].includes(placement)) {
    if (align === BOX_ALIGN.START) {
      left = spaceLeft;
      if (params.innerNodeWidth > params.openerNodeWidth + spaceRight) {
        left = 'auto';
        right = 0;
      }
    } else if (align === BOX_ALIGN.END) {
      right = spaceRight;
      if (params.innerNodeWidth > spaceLeft + params.openerNodeWidth) {
        left = 0;
        right = 'auto';
      }
    } else if (align === BOX_ALIGN.CENTER) {
      const diffWidth = (params.innerNodeWidth - params.openerNodeWidth) / 2;
      left = diffWidth > spaceLeft ? 0 : spaceLeft - diffWidth;
      if (params.innerNodeWidth - diffWidth > params.openerNodeWidth + spaceRight) {
        left = 'auto';
        right = 0;
      }
    }
  }

  return { left, right };
};

export const StyledPopupOpener = styled.div`
  box-sizing: border-box;
  display: ${props => props.sDisplay};
`;

export const StyledPopupBox = styled.div.attrs({
  style: ({ sPlacement, sAlign, sPositioningParams, sZIndex }) => ({
    ...popupBoxPositioningVertical(sPlacement, sAlign, sPositioningParams),
    ...popupBoxPositioningHorizontal(sPlacement, sAlign, sPositioningParams),
    zIndex: sZIndex,
  }),
})`
  position: fixed;
  box-sizing: border-box;
  display: block;
  opacity: ${props => (props.isMounted ? 1 : 0)};
  margin: -4px;
  padding: 4px;
`;

export const StyledPopupBoxInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;
