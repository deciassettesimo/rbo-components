import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENT } from './_constants';
import REFS from './_config';
import { StyledRaiffeisenLogo } from './_style';
import RaiffeisenLogoIcon from './_internal/RaiffeisenLogoIcon';
import RaiffeisenLogoTitle from './_internal/RaiffeisenLogoTitle';

const RaiffeisenLogo = props => (
  <StyledRaiffeisenLogo
    {...addDataAttributes({ component: COMPONENT })}
    sDisplay={props.display}
    sDimension={props.dimension}
    isIcon={props.isIcon}
  >
    <RaiffeisenLogoIcon dimension={props.dimension} />
    {!props.isIcon && <RaiffeisenLogoTitle locale={props.locale} dimension={props.dimension} color={props.color} />}
  </StyledRaiffeisenLogo>
);

RaiffeisenLogo.propTypes = {
  /** show only icon without text */
  isIcon: PropTypes.bool,
  /** language for text */
  locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
  /** css-property display */
  display: PropTypes.oneOf(Object.values(REFS.DISPLAY)),
  /** height in px */
  dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
  /** color for text svg fill (e.g. '#000000') */
  color: PropTypes.string,
};

RaiffeisenLogo.defaultProps = {
  isIcon: false,
  locale: REFS.LOCALES.RU,
  display: REFS.DISPLAY.BLOCK,
  dimension: REFS.DIMENSIONS.M,
  color: REFS.COLORS.BLACK,
};

RaiffeisenLogo.REFS = {
  ...REFS,
};

export default RaiffeisenLogo;
