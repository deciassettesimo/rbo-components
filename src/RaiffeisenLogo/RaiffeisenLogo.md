### REFS

```js static
{
  LOCALES: {
    EN: 'en',
    RU: 'ru',
  },
  DISPLAY: {
    BLOCK: 'block',
    INLINE_BLOCK: 'inline-block',
  },
  DIMENSIONS: {
    XS: 32,
    S: 40,
    M: 48,
    L: 56,
  },
  COLORS: {
    BLACK: '#000000',
    WHITE: '#FFFFFF',
  },
}
```

```js
<div className="list">
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.EN}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.XS}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.EN}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.S}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.EN}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.M}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.EN}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.L}
    />
  </div>
</div>
```

```js
<div className="list">
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.RU}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.XS}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.RU}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.S}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.RU}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.M}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.RU}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.L}
    />
  </div>
</div>
```

```js
<div className="list dark">
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.EN}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.XS}
      color={RaiffeisenLogo.REFS.COLORS.WHITE}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.EN}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.S}
      color={RaiffeisenLogo.REFS.COLORS.WHITE}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.EN}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.M}
      color={RaiffeisenLogo.REFS.COLORS.WHITE}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.EN}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.L}
      color={RaiffeisenLogo.REFS.COLORS.WHITE}
    />
  </div>
</div>
```

```js
<div className="list dark">
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.RU}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.XS}
      color={RaiffeisenLogo.REFS.COLORS.WHITE}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.RU}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.S}
      color={RaiffeisenLogo.REFS.COLORS.WHITE}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.RU}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.M}
      color={RaiffeisenLogo.REFS.COLORS.WHITE}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      locale={RaiffeisenLogo.REFS.LOCALES.RU}
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.L}
      color={RaiffeisenLogo.REFS.COLORS.WHITE}
    />
  </div>
</div>
```

```js
<div className="list">
  <div className="inline">
    <RaiffeisenLogo
      isIcon
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.XS}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      isIcon
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.S}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      isIcon
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.M}
    />
  </div>
  <div className="inline">
    <RaiffeisenLogo
      isIcon
      dimension={RaiffeisenLogo.REFS.DIMENSIONS.L}
    />
  </div>
</div>
```