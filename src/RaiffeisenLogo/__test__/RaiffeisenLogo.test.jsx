import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import RaiffeisenLogo from '../RaiffeisenLogo';

describe('RaiffeisenLogo', () => {
  const localeKeys = Object.keys(RaiffeisenLogo.REFS.LOCALES);

  // loop by types, RaiffeisenLogo.REFS.LOCALES (all icons)
  for (let i = 0; i < localeKeys.length; i += 1) {
    const localeValue = RaiffeisenLogo.REFS.LOCALES[localeKeys[i]];

    describe(`${localeKeys[i]} (${localeValue})`, () => {
      const dimensions = Object.values(RaiffeisenLogo.REFS.DIMENSIONS);

      // loop by dimensions of RaiffeisenLogo
      for (let j = 0; j < dimensions.length; j += 1) {
        const logoDimension = dimensions[j];

        it(`dimensions ${logoDimension}`, () => {
          const wrapper = renderer.create(<RaiffeisenLogo locale={localeValue} dimension={logoDimension} />).toJSON();
          // expect with snapshot
          expect(wrapper).toMatchSnapshot();
        });
      }
    });
  }
});
