import { LOCALES, DISPLAY, DIMENSIONS, COLORS } from './_constants';

const REFS = {
  LOCALES,
  DISPLAY,
  DIMENSIONS,
  COLORS,
};

export default REFS;
