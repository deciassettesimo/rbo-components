import { GLOBAL, STYLES } from '../_constants';

export const COMPONENT = 'RaiffeisenLogo';

export const LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const DISPLAY = {
  BLOCK: STYLES.DISPLAY.BLOCK,
  INLINE_BLOCK: STYLES.DISPLAY.INLINE_BLOCK,
};

export const DIMENSIONS = {
  XS: 32,
  S: 40,
  M: 48,
  L: 56,
};

export const COLORS = {
  BLACK: STYLES.COLORS.BLACK,
  WHITE: STYLES.COLORS.WHITE,
};

export const ICON_COLORS = {
  MAIN: STYLES.COLORS.BLACK,
  BACKGROUND: STYLES.COLORS.CORPORATE_YELLOW,
};
