import styled from 'styled-components';
import { ICON_COLORS } from './_constants';

export const StyledRaiffeisenLogo = styled.div`
  position: relative;
  box-sizing: border-box;
  vertical-align: middle;
  display: ${props => props.sDisplay};
  height: ${props => props.sDimension}px;
  width: ${props => props.sDimension * (props.isIcon ? 1 : 5)}px;
`;

export const StyledRaiffeisenLogoIcon = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  float: left;
  height: ${props => props.sDimension}px;
  width: ${props => props.sDimension}px;
  background-color: ${ICON_COLORS.BACKGROUND};
`;

export const StyledRaiffeisenLogoIconSvg = styled.svg`
  position: relative;
  width: 100%;
  height: 100%;
  fill: ${ICON_COLORS.MAIN};
`;

export const StyledRaiffeisenLogoTitle = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  float: left;
  height: ${props => props.sDimension}px;
  width: ${props => props.sDimension * 4}px;
`;

export const StyledRaiffeisenLogoTitleSvg = styled.svg`
  position: relative;
  display: block;
  width: 100%;
  height: 100%;
  fill: ${props => props.sColor};
`;
