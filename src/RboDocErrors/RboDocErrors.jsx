import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';
import RboDocErrorsItem from './_internal/RboDocErrorsItem';
import { StyledRboDocErrors } from './_style';

const RboDocErrors = props => (
  <StyledRboDocErrors {...addDataAttributes({ component: COMPONENTS.GENERAL })}>
    {!!props.items.length &&
      props.items.map(item => (
        <RboDocErrorsItem
          key={item.id}
          id={item.id}
          level={item.level}
          text={item.text}
          fieldsIds={item.fieldsIds}
          isActive={item.isActive}
          onClick={props.onItemClick}
        />
      ))}
  </StyledRboDocErrors>
);

RboDocErrors.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      level: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      fieldsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
      isActive: PropTypes.bool,
    }),
  ).isRequired,
  onItemClick: PropTypes.func,
};

RboDocErrors.defaultProps = {
  onItemClick: () => null,
};

export default RboDocErrors;
