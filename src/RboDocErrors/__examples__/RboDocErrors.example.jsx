import React, { PureComponent } from 'react';
import RboDocErrors from '../RboDocErrors';

const documentErrors = [
  {
    level: '3',
    id: '1.1',
    text:
      'От ООО АРТ БАЗАР (сокр)  документ "Справка о подтверждающих документах" не может быть направлен ' +
      'в банк (по счету 40702810500003409387 услуга  не предоставляется банком)',
    fieldsIds: ['account'],
    isActive: false,
  },
  {
    level: '2',
    id: '2.3.2',
    text: 'Дата справки должна быть равна текущей дате (22.08.2018)',
    fieldsIds: ['certificateDate'],
    isActive: false,
  },
  {
    level: '1',
    id: '2.2.2',
    text: 'Дата документа указана некорректно (должна быть текущая дата)',
    fieldsIds: ['certificateDate'],
    isActive: false,
  },
];

export default class RboDocErrorsExample extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = { documentErrors };
  }

  handleItemClick = ({ errorId }) => {
    this.setState({
      documentErrors: this.state.documentErrors.map(item => ({
        ...item,
        isActive: item.id === errorId,
      })),
    });
  };

  render() {
    return <RboDocErrors items={this.state.documentErrors} onItemClick={this.handleItemClick} />;
  }
}
