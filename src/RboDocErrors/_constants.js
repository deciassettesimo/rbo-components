export const COMPONENTS = {
  GENERAL: 'RboDocErrors',
  ITEM: 'RboDocErrorsItem',
};

export default COMPONENTS;
