import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocErrorsItem, StyledRboDocErrorsItemInner } from '../_style';

const RboDocErrorsItem = props => {
  const handleOnClick = e => {
    e.preventDefault();
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
    setTimeout(() => {
      props.onClick({ errorId: props.id, fieldsIds: props.fieldsIds });
    }, 0);
  };

  return (
    <StyledRboDocErrorsItem
      {...addDataAttributes({ component: COMPONENTS.ITEM, id: props.id })}
      isActive={props.isActive}
      sLevel={props.level}
      onClick={handleOnClick}
    >
      <StyledRboDocErrorsItemInner isActive={props.isActive} sLevel={props.level}>
        {props.text}
      </StyledRboDocErrorsItemInner>
    </StyledRboDocErrorsItem>
  );
};

RboDocErrorsItem.propTypes = {
  id: PropTypes.string.isRequired,
  level: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  fieldsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  isActive: PropTypes.bool,
  onClick: PropTypes.func,
};

RboDocErrorsItem.defaultProps = {
  isActive: false,
  onClick: () => null,
};

export default RboDocErrorsItem;
