import styled from 'styled-components';
import { STYLES } from '../_constants';

const rboDocErrorsItemColor = sLevel => {
  switch (sLevel) {
    case '3':
      return STYLES.COLORS.TOMATO_RED;
    case '1':
    case '2':
    default:
      return STYLES.COLORS.BLACK;
  }
};

const rboDocErrorsItemInnerBorderColor = sLevel => {
  switch (sLevel) {
    case '3':
    case '2':
      return STYLES.COLORS.TOMATO_RED;
    case '1':
    default:
      return STYLES.COLORS.MARIGOLD;
  }
};

export const StyledRboDocErrors = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  font-size: 12px;
`;

export const StyledRboDocErrorsItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  cursor: pointer;
  margin: 0 -16px;
  padding: 4px 4px 4px 0;
  background: ${props => (props.isActive ? STYLES.COLORS.WHITE : STYLES.COLORS.SOFT_PINK)};
  color: ${props => rboDocErrorsItemColor(props.sLevel)};
`;

export const StyledRboDocErrorsItemInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 4px 12px;
  background: ${props => (props.isActive ? STYLES.COLORS.WHITE : '')};
  border-left: 4px solid ${props => rboDocErrorsItemInnerBorderColor(props.sLevel)};
  word-wrap: break-word;
  white-space: ${props => (props.isActive ? 'pre-wrap' : 'nowrap')};
  text-overflow: ellipsis;
  overflow: hidden;
`;
