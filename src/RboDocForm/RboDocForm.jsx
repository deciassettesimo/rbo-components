import React, { PureComponent, createContext } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';
import RboDocFormSection from './_internal/RboDocFormSection';
import RboDocFormBlockset from './_internal/RboDocFormBlockset';
import RboDocFormBlock from './_internal/RboDocFormBlock';
import RboDocFormFootnote from './_internal/RboDocFormFootnote';
import { StyledRboDocForm } from './_style';

export const RboDocFormContext = createContext({});

export default class RboDocForm extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    wrappingWidth: PropTypes.number,
    dataAttributes: PropTypes.shape(),
  };

  static defaultProps = {
    children: null,
    wrappingWidth: 1200,
    dataAttributes: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      wrappingWidth: this.props.wrappingWidth,
    };
  }

  render() {
    return (
      <StyledRboDocForm {...addDataAttributes({ ...this.props.dataAttributes, component: COMPONENTS.GENERAL })}>
        <RboDocFormContext.Provider value={{ ...this.state }}>{this.props.children}</RboDocFormContext.Provider>
      </StyledRboDocForm>
    );
  }
}

RboDocForm.Section = RboDocFormSection;
RboDocForm.Blockset = RboDocFormBlockset;
RboDocForm.Block = RboDocFormBlock;
RboDocForm.Footnote = RboDocFormFootnote;
