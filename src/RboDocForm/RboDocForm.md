### Rbo Doc Form

```js
<RboDocForm wrappingWidth={1400}>
  <Form dimension={Form.REFS.DIMENSIONS.XS} autoFocus="DocNumber">
    <RboDocForm.Section dataAttributes={{ id: 'Section1' }}>
      <RboDocForm.Section.Header>
        <RboDocForm.Section.Title>
          Общая информация
        </RboDocForm.Section.Title>
        <RboDocForm.Section.Note>
          Примечание для секции
        </RboDocForm.Section.Note>
      </RboDocForm.Section.Header>
      <RboDocForm.Section.Content>
        <RboDocForm.Blockset>
          <RboDocForm.Block isWide>
            <Form.Row>
              <Form.Cell width="50%">
                <Form.Label htmlFor="DocNumber">Описание</Form.Label>
                <Form.Field>
                  <InputText id="InputText" />
                </Form.Field>
              </Form.Cell>
              <Form.Cell width="50%">
                <Form.Minimizer labels={['Свернуть секцию', 'Развернуть секцию']} onToggle={() => null} />
              </Form.Cell>
            </Form.Row>
          </RboDocForm.Block>
        </RboDocForm.Blockset>
        <RboDocForm.Blockset vAlignItems={RboDocForm.Blockset.REFS.V_ALIGN_ITEMS.TOP}>
          <RboDocForm.Block>
            <Form.Row>
              <Form.Cell width="50%">
                <Form.Label htmlFor="DocNumber">Номер документа</Form.Label>
                <Form.Field>
                  <InputDigital id="DocNumber" maxLength={7} />
                </Form.Field>
              </Form.Cell>
              <Form.Cell width="50%">
                <Form.Label htmlFor="DocDate">Дата документа</Form.Label>
                <Form.Field>
                  <InputDateCalendar id="DocDate" />
                </Form.Field>
              </Form.Cell>
            </Form.Row>
          </RboDocForm.Block>
          <RboDocForm.Block>
            <Form.Row>
              <Form.Cell>
                <Form.Label htmlFor="AccountingContarctPt1">Номер УК</Form.Label>
                <Form.Fieldset withAutoTransitions>
                  <Form.Field width="27%">
                    <InputSuggest
                      inputElement={InputDigital}
                      id="AccountingContarctPt1"
                      maxLength={8}
                      options={[]}
                      notFoundLabel="Ничего не найдено"
                      onSearch={() => {}}
                    />
                  </Form.Field>
                  <Form.Field.Separator>/</Form.Field.Separator>
                  <Form.Field width="19%">
                    <InputDigital id="AccountingContarctPt2" maxLength={4} />
                  </Form.Field>
                  <Form.Field.Separator>/</Form.Field.Separator>
                  <Form.Field width="19%">
                    <InputText id="AccountingContarctPt3" maxLength={4} />
                  </Form.Field>
                  <Form.Field.Separator>/</Form.Field.Separator>
                  <Form.Field width="17.5%">
                    <InputSearch
                      inputElement={InputDigital}
                      id="AccountingContarctPt4"
                      maxLength={1}
                      options={[
                        { value: '1', title: '1'},
                        { value: '2', title: '2'},
                        { value: '3', title: '3'},
                      ]}
                      notFoundLabel="Ничего не найдено"
                      onSearch={() => {}}
                    />
                  </Form.Field>
                  <Form.Field.Separator>/</Form.Field.Separator>
                  <Form.Field width="17.5%">
                    <InputSearch
                      inputElement={InputDigital}
                      id="AccountingContarctPt5"
                      maxLength={1}
                      options={[
                        { value: '1', title: '1'},
                        { value: '2', title: '2'},
                        { value: '3', title: '3'},
                      ]}
                      notFoundLabel="Ничего не найдено"
                      onSearch={() => {}}
                    />
                  </Form.Field>
                </Form.Fieldset>
              </Form.Cell>
            </Form.Row>
          </RboDocForm.Block>
        </RboDocForm.Blockset>
        <RboDocForm.Blockset>
          <RboDocForm.Block isWide>
            <Form.Row>
              <Form.Cell width="50%">
                <Form.Label htmlFor="Acoount">Счет</Form.Label>
                <Form.Field>
                  <InputText id="Account" />
                </Form.Field>
              </Form.Cell>
              <Form.Cell width="50%">
                <Form.Label htmlFor="Describe">Описание документа</Form.Label>
                <Form.Field>
                  <InputText id="Describe" />
                </Form.Field>
              </Form.Cell>
            </Form.Row>
            <RboDocForm.Footnote>
              Сноска, примечание, подстрочное примечание
            </RboDocForm.Footnote>
          </RboDocForm.Block>
        </RboDocForm.Blockset>
      </RboDocForm.Section.Content>
    </RboDocForm.Section>
  </Form>
</RboDocForm>
```
