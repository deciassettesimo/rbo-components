import { V_ALIGN_ITEMS } from './_constants';

export const BLOCKSET_REFS = {
  V_ALIGN_ITEMS,
};

export default BLOCKSET_REFS;
