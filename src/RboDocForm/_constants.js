export const COMPONENTS = {
  GENERAL: 'RboDocForm',
  BLOCK: 'RboDocFormBlock',
  BLOCKSET: 'RboDocFormBlockset',
  FOOTNOTE: 'RboDocFormFootnote',
  SECTION: 'RboDocFormSection',
  SECTION_CONTENT: 'RboDocFormSectionContent',
  SECTION_HEADER: 'RboDocFormSectionHeader',
  SECTION_NOTE: 'RboDocFormSectionNote',
  SECTION_TITLE: 'RboDocFormSectionTitle',
};

export const V_ALIGN_ITEMS = {
  TOP: 'top',
  CENTER: 'center',
  BOTTOM: 'bottom',
};
