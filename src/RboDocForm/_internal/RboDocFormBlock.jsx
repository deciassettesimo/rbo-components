import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { RboDocFormContext } from '../RboDocForm';
import { StyledRboDocFormBlock } from '../_style';

const RboDocFormBlockWithContext = props => (
  <StyledRboDocFormBlock
    {...addDataAttributes({ ...props.dataAttributes, component: COMPONENTS.BLOCK })}
    isWide={props.isWide}
    sWrappingWidth={props.wrappingWidth}
  >
    {props.children}
  </StyledRboDocFormBlock>
);

RboDocFormBlockWithContext.propTypes = {
  children: PropTypes.node,
  isWide: PropTypes.bool,
  wrappingWidth: PropTypes.number,
  dataAttributes: PropTypes.shape(),
};

RboDocFormBlockWithContext.defaultProps = {
  children: null,
  isWide: false,
  wrappingWidth: 1200,
  dataAttributes: null,
};

const RboDocFormBlock = props => (
  <RboDocFormContext.Consumer>
    {({ wrappingWidth }) => <RboDocFormBlockWithContext {...props} wrappingWidth={wrappingWidth} />}
  </RboDocFormContext.Consumer>
);

RboDocFormBlock.propTypes = {
  children: PropTypes.node,
  isWide: PropTypes.bool,
  dataAttributes: PropTypes.shape(),
};

RboDocFormBlock.defaultProps = {
  children: null,
  isWide: false,
  dataAttributes: null,
};

export default RboDocFormBlock;
