import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { BLOCKSET_REFS as REFS } from '../_config';
import { StyledRboDocFormBlockset } from '../_style';

const RboDocFormBlockset = props => (
  <StyledRboDocFormBlockset
    {...addDataAttributes({ ...props.dataAttributes, component: COMPONENTS.BLOCKSET })}
    sVAlignItems={props.vAlignItems}
    isHeader={props.isHeader}
  >
    {props.children}
  </StyledRboDocFormBlockset>
);

RboDocFormBlockset.propTypes = {
  children: PropTypes.node,
  vAlignItems: PropTypes.oneOf(Object.values(REFS.V_ALIGN_ITEMS)),
  isHeader: PropTypes.bool,
  dataAttributes: PropTypes.shape(),
};

RboDocFormBlockset.defaultProps = {
  children: null,
  vAlignItems: REFS.V_ALIGN_ITEMS.BOTTOM,
  isHeader: false,
  dataAttributes: null,
};

RboDocFormBlockset.REFS = { ...REFS };

export default RboDocFormBlockset;
