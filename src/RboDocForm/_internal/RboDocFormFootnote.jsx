import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocFormFootnote } from '../_style';

const RboDocFormFootnote = props => (
  <StyledRboDocFormFootnote {...addDataAttributes({ ...props.dataAttributes, component: COMPONENTS.FOOTNOTE })}>
    {props.children}
  </StyledRboDocFormFootnote>
);

RboDocFormFootnote.propTypes = {
  children: PropTypes.node,
  dataAttributes: PropTypes.shape(),
};

RboDocFormFootnote.defaultProps = {
  children: null,
  dataAttributes: null,
};

export default RboDocFormFootnote;
