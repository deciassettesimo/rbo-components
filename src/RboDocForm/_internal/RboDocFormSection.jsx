import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocFormSection } from '../_style';
import RboDocFormSectionHeader from './RboDocFormSectionHeader';
import RboDocFormSectionTitle from './RboDocFormSectionTitle';
import RboDocFormSectionNote from './RboDocFormSectionNote';
import RboDocFormSectionContent from './RboDocFormSectionContent';

const RboDocFormSection = props => (
  <StyledRboDocFormSection {...addDataAttributes({ ...props.dataAttributes, component: COMPONENTS.SECTION })}>
    {props.children}
  </StyledRboDocFormSection>
);

RboDocFormSection.propTypes = {
  children: PropTypes.node,
  dataAttributes: PropTypes.shape(),
};

RboDocFormSection.defaultProps = {
  children: null,
  dataAttributes: null,
};

RboDocFormSection.Header = RboDocFormSectionHeader;
RboDocFormSection.Title = RboDocFormSectionTitle;
RboDocFormSection.Note = RboDocFormSectionNote;
RboDocFormSection.Content = RboDocFormSectionContent;

export default RboDocFormSection;
