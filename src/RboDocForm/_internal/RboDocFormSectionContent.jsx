import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocFormSectionContent } from '../_style';

const RboDocFormSectionContent = props => (
  <StyledRboDocFormSectionContent
    {...addDataAttributes({ ...props.dataAttributes, component: COMPONENTS.SECTION_CONTENT })}
    noPadding={props.noPadding}
  >
    {props.children}
  </StyledRboDocFormSectionContent>
);

RboDocFormSectionContent.propTypes = {
  children: PropTypes.node,
  noPadding: PropTypes.bool,
  dataAttributes: PropTypes.shape(),
};

RboDocFormSectionContent.defaultProps = {
  children: null,
  noPadding: false,
  dataAttributes: null,
};

export default RboDocFormSectionContent;
