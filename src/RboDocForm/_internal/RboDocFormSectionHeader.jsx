import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocFormSectionHeader } from '../_style';

const RboDocFormSectionHeader = props => (
  <StyledRboDocFormSectionHeader
    {...addDataAttributes({ ...props.dataAttributes, component: COMPONENTS.SECTION_HEADER })}
  >
    {props.children}
  </StyledRboDocFormSectionHeader>
);

RboDocFormSectionHeader.propTypes = {
  children: PropTypes.node,
  dataAttributes: PropTypes.shape(),
};

RboDocFormSectionHeader.defaultProps = {
  children: null,
  dataAttributes: null,
};

export default RboDocFormSectionHeader;
