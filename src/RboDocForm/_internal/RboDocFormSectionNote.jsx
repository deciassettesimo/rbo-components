import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocFormSectionNote } from '../_style';

const RboDocFormSectionNote = props => (
  <StyledRboDocFormSectionNote {...addDataAttributes({ ...props.dataAttributes, component: COMPONENTS.SECTION_NOTE })}>
    {props.children}
  </StyledRboDocFormSectionNote>
);

RboDocFormSectionNote.propTypes = {
  children: PropTypes.node,
  dataAttributes: PropTypes.shape(),
};

RboDocFormSectionNote.defaultProps = {
  children: null,
  dataAttributes: null,
};

export default RboDocFormSectionNote;
