import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocFormSectionTitle } from '../_style';

const RboDocFormSectionTitle = props => (
  <StyledRboDocFormSectionTitle
    {...addDataAttributes({ ...props.dataAttributes, component: COMPONENTS.SECTION_TITLE })}
  >
    {props.children}
  </StyledRboDocFormSectionTitle>
);

RboDocFormSectionTitle.propTypes = {
  children: PropTypes.node,
  dataAttributes: PropTypes.shape(),
};

RboDocFormSectionTitle.defaultProps = {
  children: null,
  dataAttributes: null,
};

export default RboDocFormSectionTitle;
