import styled from 'styled-components';

import { STYLES } from '../_constants';
import { V_ALIGN_ITEMS } from './_constants';

const rboDocFormBlockWidth = isWide => (isWide ? '100%' : '50%');

const verticalAlignItems = sVAlignItems => {
  switch (sVAlignItems) {
    case V_ALIGN_ITEMS.TOP:
      return 'flex-start';
    case V_ALIGN_ITEMS.CENTER:
      return 'center';
    case V_ALIGN_ITEMS.BOTTOM:
    default:
      return 'flex-end';
  }
};

export const StyledRboDocForm = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledRboDocFormSection = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledRboDocFormSectionHeader = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: flex-start;
  background-color: ${STYLES.COLORS.BLACK_08};
  padding: 6px 40px 4px;
  color: ${STYLES.COLORS.BLACK_60};
`;

export const StyledRboDocFormSectionTitle = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  font-weight: bold;
  font-size: 12px;
`;

export const StyledRboDocFormSectionNote = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  font-size: 12px;
`;

export const StyledRboDocFormSectionContent = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: ${props => (props.noPadding ? '0' : '12px 40px')};
`;

export const StyledRboDocFormBlockset = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  align-items: ${props => verticalAlignItems(props.sVAlignItems)};
  width: 100%;
  padding-top: ${props => (props.isHeader ? 16 : 0)}px;
`;

export const StyledRboDocFormBlock = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  width: ${props => rboDocFormBlockWidth(props.isWide)};
  padding: 0 12px;

  :first-child {
    padding-left: 0;
  }

  :last-child {
    padding-right: 0;
  }

  @media (max-width: ${props => props.sWrappingWidth}px) {
    width: 100%;
    padding: 0;
  }
`;

export const StyledRboDocFormFootnote = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  margin: 4px 0;
  color: ${STYLES.COLORS.WARM_GRAY};

  :first-child {
    margin-top: 0;
  }
  :last-child {
    margin-bottom: 0;
  }
`;
