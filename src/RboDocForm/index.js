import RboDocForm from './RboDocForm';

export const { Section, Blockset, Block, Footnote } = RboDocForm;
export default RboDocForm;
