import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';
import { REFS } from './_config';
import RboDocHeaderRow from './_internal/RboDocHeaderRow';
import RboDocHeaderBlock from './_internal/RboDocHeaderBlock';
import RboDocHeaderUnit from './_internal/RboDocHeaderUnit';
import { StyledRboDocHeader } from './_style';

const RboDocHeader = props => (
  <StyledRboDocHeader {...addDataAttributes({ component: COMPONENTS.GENERAL })} sBgColor={props.bgColor}>
    {props.children}
  </StyledRboDocHeader>
);

RboDocHeader.propTypes = {
  children: PropTypes.node.isRequired,
  bgColor: PropTypes.oneOf(Object.values(REFS.BG_COLORS)),
};

RboDocHeader.defaultProps = {
  bgColor: REFS.BG_COLORS.WHITE,
};

RboDocHeader.REFS = { ...REFS };

RboDocHeader.Row = RboDocHeaderRow;
RboDocHeader.Block = RboDocHeaderBlock;
RboDocHeader.Unit = RboDocHeaderUnit;

export default RboDocHeader;
