```js
<RboDocHeader>
  <RboDocHeader.Row>
    <Header size={3}>Справка о подтверждающих документах № 44246</Header>
  </RboDocHeader.Row>
  <RboDocHeader.Row>
    <RboDocHeader.Block isWithBottomPadding>
      <RboDocHeader.Unit>От 26.09.2018</RboDocHeader.Unit>
      <RboDocHeader.Unit><Label>Статус:</Label> Новый</RboDocHeader.Unit>
    </RboDocHeader.Block>
    <RboDocHeader.Block isGrow>
      <Tabs
        items={[
            { id: 'main', title: 'Основные поля', isActive: true },
            { id: 'notes', title: 'Заметки', isActive: false },
            { id: 'bankInfo', title: 'Информация из банка', isActive: false }
          ]}
          align={Tabs.REFS.ALIGN.RIGHT}
          dimension={Tabs.REFS.DIMENSIONS.XS}
          onItemClick={() => { console.log('tabClick') }}
      />
    </RboDocHeader.Block>
  </RboDocHeader.Row>
</RboDocHeader>
```

```js
<RboDocHeader bgColor={RboDocHeader.REFS.BG_COLORS.CORPORATE_YELLOW_12}>
  <RboDocHeader.Row>
    <Header size={3}>Шаблон «Платежное поручение для ООО Ромашка»</Header>
  </RboDocHeader.Row>
  <RboDocHeader.Row>
    <RboDocHeader.Block isWithBottomPadding>
      <RboDocHeader.Unit>Для документа: Платежное поручение</RboDocHeader.Unit>
    </RboDocHeader.Block>
    <RboDocHeader.Block isGrow>
      <Tabs
        items={[
            { id: 'main', title: 'Основные поля', isActive: true },
            { id: 'notes', title: 'Заметки', isActive: false },
            { id: 'bankInfo', title: 'Информация из банка', isActive: false }
          ]}
          align={Tabs.REFS.ALIGN.RIGHT}
          dimension={Tabs.REFS.DIMENSIONS.XS}
          onItemClick={() => { console.log('tabClick') }}
      />
    </RboDocHeader.Block>
  </RboDocHeader.Row>
</RboDocHeader>
```
