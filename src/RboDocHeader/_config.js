import { BG_COLORS, ROW_V_ALIGN_ITEMS } from './_constants';

export const REFS = {
  BG_COLORS,
};

export const ROW_REFS = {
  V_ALIGN_ITEMS: ROW_V_ALIGN_ITEMS,
};
