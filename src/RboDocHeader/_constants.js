import { STYLES } from '../_constants';

export const COMPONENTS = {
  GENERAL: 'RboDocHeader',
  BLOCK: 'RboDocHeaderBlock',
  ROW: 'RboDocHeaderRow',
  UNIT: 'RboDocHeaderUnit',
};

export const BG_COLORS = {
  WHITE: STYLES.COLORS.WHITE,
  CORPORATE_YELLOW_12: STYLES.COLORS.CORPORATE_YELLOW_12,
};

export const ROW_V_ALIGN_ITEMS = {
  TOP: 'top',
  CENTER: 'center',
  BOTTOM: 'bottom',
};
