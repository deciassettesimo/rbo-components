import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocHeaderBlock } from '../_style';

const RboDocHeaderBlock = props => (
  <StyledRboDocHeaderBlock
    {...addDataAttributes({ component: COMPONENTS.BLOCK })}
    isGrow={props.isGrow}
    isWithBottomPadding={props.isWithBottomPadding}
  >
    {props.children}
  </StyledRboDocHeaderBlock>
);

RboDocHeaderBlock.propTypes = {
  children: PropTypes.node.isRequired,
  isWithBottomPadding: PropTypes.bool,
  isGrow: PropTypes.bool,
};

RboDocHeaderBlock.defaultProps = {
  isWithBottomPadding: false,
  isGrow: false,
};

export default RboDocHeaderBlock;
