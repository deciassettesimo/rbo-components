import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { ROW_REFS as REFS } from '../_config';
import { StyledRboDocHeaderRow } from '../_style';

const RboDocHeaderRow = props => (
  <StyledRboDocHeaderRow {...addDataAttributes({ component: COMPONENTS.ROW })} sVAlignItems={props.vAlignItems}>
    {props.children}
  </StyledRboDocHeaderRow>
);

RboDocHeaderRow.propTypes = {
  children: PropTypes.node.isRequired,
  vAlignItems: PropTypes.oneOf(Object.values(REFS.V_ALIGN_ITEMS)),
};

RboDocHeaderRow.defaultProps = {
  vAlignItems: REFS.V_ALIGN_ITEMS.BOTTOM,
};

RboDocHeaderRow.REFS = { ...REFS };

export default RboDocHeaderRow;
