import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocHeaderUnit } from '../_style';

const RboDocHeaderUnit = props => (
  <StyledRboDocHeaderUnit {...addDataAttributes({ component: COMPONENTS.UNIT })}>
    {props.children}
  </StyledRboDocHeaderUnit>
);

RboDocHeaderUnit.propTypes = {
  children: PropTypes.node.isRequired,
};

export default RboDocHeaderUnit;
