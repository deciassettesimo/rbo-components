import styled from 'styled-components';

import { STYLES } from '../_constants';
import { ROW_V_ALIGN_ITEMS } from './_constants';

const rboDocHeaderVAlignItems = sVAlignItems => {
  switch (sVAlignItems) {
    case ROW_V_ALIGN_ITEMS.TOP:
      return 'flex-start';
    case ROW_V_ALIGN_ITEMS.CENTER:
      return 'center';
    case ROW_V_ALIGN_ITEMS.BOTTOM:
    default:
      return 'flex-end';
  }
};

export const StyledRboDocHeader = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding-top: 12px;
  min-height: 53px;
  background: ${props => props.sBgColor};
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_20};
  word-break: break-word;
  flex-shrink: 0;
`;

export const StyledRboDocHeaderRow = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: ${props => rboDocHeaderVAlignItems(props.sVAlignItems)};
  padding: 0 40px;
`;

export const StyledRboDocHeaderBlock = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 0 8px ${props => (props.isWithBottomPadding ? 4 : 0)}px 8px;
  flex-grow: ${props => (props.isGrow ? 1 : 0)};

  :first-child {
    padding-left: 0;
  }

  :last-child {
    padding-right: 0;
  }
`;

export const StyledRboDocHeaderUnit = styled.span`
  display: inline-block;
  font-size: 12px;
  margin-right: 16px;
`;
