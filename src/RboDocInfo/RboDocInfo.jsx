import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import IconDocument from '../Icon/IconDocument';
import Label from '../Styles/Label';
import { COMPONENT } from './_constants';
import {
  StyledRboDocInfo,
  StyledRboDocInfoEmpty,
  StyledRboDocInfoEmptyIcon,
  StyledRboDocInfoEmptyLabel,
  StyledRboDocInfoItems,
  StyledRboDocInfoItemsInner,
  StyledRboDocInfoItem,
  StyledRboDocInfoItemLabel,
  StyledRboDocInfoItemValue,
} from './_style';

const RboDocInfo = props => (
  <StyledRboDocInfo {...addDataAttributes({ component: COMPONENT })}>
    {(!props.items || !props.items.length) && (
      <StyledRboDocInfoEmpty>
        <StyledRboDocInfoEmptyIcon>
          <IconDocument dimension={IconDocument.REFS.DIMENSIONS.M} color={IconDocument.REFS.COLORS.WARM_GRAY} />
        </StyledRboDocInfoEmptyIcon>
        <StyledRboDocInfoEmptyLabel>
          <Label>{props.emptyLabel}</Label>
        </StyledRboDocInfoEmptyLabel>
      </StyledRboDocInfoEmpty>
    )}
    {props.items &&
      !!props.items.length && (
        <StyledRboDocInfoItems>
          <StyledRboDocInfoItemsInner>
            {props.items.map(item => (
              <StyledRboDocInfoItem key={item.id}>
                <StyledRboDocInfoItemLabel>{item.label}</StyledRboDocInfoItemLabel>
                <StyledRboDocInfoItemValue>{item.value}</StyledRboDocInfoItemValue>
              </StyledRboDocInfoItem>
            ))}
          </StyledRboDocInfoItemsInner>
        </StyledRboDocInfoItems>
      )}
  </StyledRboDocInfo>
);

RboDocInfo.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string,
      value: PropTypes.node,
    }),
  ),
  emptyLabel: PropTypes.string.isRequired,
};

RboDocInfo.defaultProps = {
  items: null,
};

export default RboDocInfo;
