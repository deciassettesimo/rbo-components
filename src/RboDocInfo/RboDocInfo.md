```js
const items = [
  {
    id: '1',
    label: 'Label 1',
    value: 'Value 1'
  },
  {
    id: '2',
    label: 'Label 2',
    value: 'Value 2'
  },
  {
    id: '3',
    label: 'Label 3',
    value: 'Value 3'
  },
  {
    id: '4',
    label: 'Label 4',
    value: 'Value 4'
  },
];
<RboDocInfo items={items} emptyLabel="Дополнительная информация для документа отсутствует" />
```

```js
<RboDocInfo emptyLabel="Дополнительная информация для документа отсутствует" />
```
