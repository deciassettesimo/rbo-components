import styled from 'styled-components';

import { STYLES } from '../_constants';

export const StyledRboDocInfo = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  font-size: 12px;
  padding: 16px 0;
`;

export const StyledRboDocInfoEmpty = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 32px 40px;
  text-align: center;
`;

export const StyledRboDocInfoEmptyIcon = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  margin-bottom: 16px;
`;

export const StyledRboDocInfoEmptyLabel = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledRboDocInfoItems = styled.table`
  width: 100%;
  border-collapse: collapse;
`;

export const StyledRboDocInfoItemsInner = styled.tbody``;

export const StyledRboDocInfoItem = styled.tr`
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_08};

  :last-child {
    border-bottom: none;
  }
`;

export const StyledRboDocInfoItemLabel = styled.td`
  padding: 8px 8px 8px 40px;
  font-weight: bold;
  color: ${STYLES.COLORS.WARM_GRAY};
  vertical-align: top;
  white-space: nowrap;
`;

export const StyledRboDocInfoItemValue = styled.td`
  padding: 8px 40px 8px 8px;
  vertical-align: top;
  width: 100%;
  white-space: pre-line;
  word-wrap: break-word;
`;
