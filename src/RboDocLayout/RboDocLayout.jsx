import React, { PureComponent, createContext } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';
import RboDocLayoutContent from './_internal/RboDocLayoutContent';
import RboDocLayoutPanel from './_internal/RboDocLayoutPanel';
import RboDocLayoutTable from './_internal/RboDocLayoutTable';

import { StyledRboDocLayout, StyledRboDocLayoutInner } from './_style';

export const RboDocLayoutContext = createContext({});
export const RboDocLayoutPanelContext = createContext({});

export default class RboDocLayout extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    isInModal: PropTypes.bool,
    getContainerNode: PropTypes.func,
  };

  static defaultProps = {
    children: null,
    isInModal: false,
    getContainerNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isInModal: this.props.isInModal,
      getContainerNode: this.props.getContainerNode,
    };
  }

  render() {
    return (
      <StyledRboDocLayout {...addDataAttributes({ component: COMPONENTS.GENERAL })}>
        <StyledRboDocLayoutInner>
          <RboDocLayoutContext.Provider value={{ ...this.state }}>{this.props.children}</RboDocLayoutContext.Provider>
        </StyledRboDocLayoutInner>
      </StyledRboDocLayout>
    );
  }
}

RboDocLayout.Content = RboDocLayoutContent;
RboDocLayout.Panel = RboDocLayoutPanel;
RboDocLayout.Table = RboDocLayoutTable;
