import React, { PureComponent } from 'react';
import styled from 'styled-components';

import RboDocLayout from '../RboDocLayout';
import OperationsPanel from '../../OperationsPanel';
import List from '../../Styles/List';

const operations = [
  {
    id: 'back1',
    icon: 'arrow-left-tailed',
    disabled: false,
    progress: false,
  },
  {
    id: 'save1',
    icon: 'save',
    title: 'Save',
    disabled: false,
    progress: false,
  },
  {
    id: 'print',
    icon: 'print',
    title: 'Print',
    disabled: false,
    progress: false,
  },
  {
    id: 'remove',
    icon: 'remove',
    title: 'Remove',
    disabled: false,
    progress: false,
  },
  {
    id: 'repeat',
    icon: 'repeat',
    title: 'Repeat',
    disabled: false,
    progress: false,
  },
  {
    id: 'document-sign-and-send',
    icon: 'document-sign-and-send',
    title: 'Sign and Send',
    disabled: false,
    progress: false,
  },
  {
    id: 'document-sign',
    icon: 'document-sign',
    title: 'Sign',
    disabled: false,
    progress: false,
  },
  {
    id: 'document-send',
    icon: 'document-send',
    title: 'Send',
    disabled: false,
    progress: false,
  },
  {
    id: 'archive',
    icon: 'archive',
    title: 'Archive',
    disabled: false,
    progress: false,
  },
  {
    id: 'document-save-as-template',
    icon: 'document-save-as-template',
    title: 'Save as template',
    disabled: false,
    progress: false,
  },
];

const StyledRboDocLayoutExampleContainer = styled.div`
  height: 400px;
  overflow: auto;
`;

const StyledRboDocLayoutExampleMain = styled.div`
  height: 1000px;
  text-align: center;
  border-bottom: 4px solid black;
  box-sizing: border-box;
  padding: 32px;
`;

const StyledRboDocLayoutExampleExtra = styled.div`
  height: 200px;
  box-sizing: border-box;
  border-bottom: 1px solid black;
`;

export default class RboDocLayoutExample extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};

    this.node = React.createRef();
  }

  render() {
    return (
      <StyledRboDocLayoutExampleContainer
        innerRef={ref => {
          this.node.current = ref;
        }}
      >
        <RboDocLayout getContainerNode={() => this.node}>
          <RboDocLayout.Panel>
            <OperationsPanel onItemClick={() => null} items={operations} />
          </RboDocLayout.Panel>
          <RboDocLayout.Content maxWidth={720}>
            <RboDocLayout.Content.Main>
              <StyledRboDocLayoutExampleMain>Main</StyledRboDocLayoutExampleMain>
            </RboDocLayout.Content.Main>
            <RboDocLayout.Content.Extra>
              <RboDocLayout.Content.Extra.Section title="Extra Section">
                <StyledRboDocLayoutExampleExtra>Extra 1</StyledRboDocLayoutExampleExtra>
              </RboDocLayout.Content.Extra.Section>
              <RboDocLayout.Content.Extra.Section title="Extra Section 2">
                <StyledRboDocLayoutExampleExtra>Extra 2</StyledRboDocLayoutExampleExtra>
              </RboDocLayout.Content.Extra.Section>
            </RboDocLayout.Content.Extra>
          </RboDocLayout.Content>
          <RboDocLayout.Panel isBottom>
            <List fontSize={List.REFS.FONT_SIZES.S} isInline vPadding={8}>
              <List.Item>
                Всего сотрудников в списке: <strong>0</strong>
              </List.Item>
              <List.Item>
                Итоговая сумма к начислению: <strong>0</strong> руб.
              </List.Item>
            </List>
          </RboDocLayout.Panel>
        </RboDocLayout>
      </StyledRboDocLayoutExampleContainer>
    );
  }
}
