export const COMPONENTS = {
  GENERAL: 'RboDocLayout',
  CONTENT: 'RboDocLayoutContent',
  CONTENT_EXTRA: 'RboDocLayoutContentExtra',
  CONTENT_EXTRA_SECTION: 'RboDocLayoutContentExtraSection',
  CONTENT_MAIN: 'RboDocLayoutContentMain',
  FOOTER: 'RboDocLayoutFooter',
  PANEL: 'RboDocLayoutPanel',
  TABLE: 'RboDocLayoutTable',
};

export default COMPONENTS;
