import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import RboDocLayoutContentMain from './RboDocLayoutContentMain';
import RboDocLayoutContentExtra from './RboDocLayoutContentExtra';

import { RboDocLayoutContext } from '../RboDocLayout';
import { StyledRboDocLayoutContent } from '../_style';

const RboDocLayoutContentWithContext = props => (
  <StyledRboDocLayoutContent
    {...addDataAttributes({ component: COMPONENTS.CONTENT })}
    sMaxWidth={props.maxWidth}
    isInModal={props.isInModal}
  >
    {props.children}
  </StyledRboDocLayoutContent>
);

RboDocLayoutContentWithContext.propTypes = {
  isInModal: PropTypes.bool,
  maxWidth: PropTypes.number,
  children: PropTypes.node.isRequired,
};

RboDocLayoutContentWithContext.defaultProps = {
  isInModal: false,
  maxWidth: null,
};

const RboDocLayoutContent = props => (
  <RboDocLayoutContext.Consumer>
    {({ isInModal }) => <RboDocLayoutContentWithContext {...props} isInModal={isInModal} />}
  </RboDocLayoutContext.Consumer>
);

RboDocLayoutContent.propTypes = {
  maxWidth: PropTypes.number,
  children: PropTypes.node.isRequired,
};

RboDocLayoutContent.defaultProps = {
  maxWidth: null,
};

RboDocLayoutContent.Main = RboDocLayoutContentMain;
RboDocLayoutContent.Extra = RboDocLayoutContentExtra;

export default RboDocLayoutContent;
