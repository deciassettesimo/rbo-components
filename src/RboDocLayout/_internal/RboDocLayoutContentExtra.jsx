import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { RboDocLayoutContext } from '../RboDocLayout';
import {
  StyledRboDocLayoutContentExtra,
  StyledRboDocLayoutContentExtraFixed,
  StyledRboDocLayoutContentExtraInner,
} from '../_style';
import RboDocLayoutContentExtraSection from './RboDocLayoutContentExtraSection';

const RboDocLayoutContentExtraInModal = props => (
  <StyledRboDocLayoutContentExtra {...addDataAttributes({ component: COMPONENTS.CONTENT_EXTRA })}>
    <StyledRboDocLayoutContentExtraInner>{props.children}</StyledRboDocLayoutContentExtraInner>
  </StyledRboDocLayoutContentExtra>
);

RboDocLayoutContentExtraInModal.propTypes = {
  children: PropTypes.node.isRequired,
};

class RboDocLayoutContentExtraWithContext extends PureComponent {
  static propTypes = {
    getContainerNode: PropTypes.func,
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    getContainerNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      containerNode: null,
      scrollTop: null,
      width: null,
      top: null,
      left: null,
      innerNodeHeight: null,
      containerNodeHeight: null,
      containerNodeTop: null,
    };

    this.node = React.createRef();
    this.innerNode = React.createRef();
  }

  componentDidMount() {
    window.addEventListener('resize', this.setOffset);
    document.addEventListener('scroll', this.setOffset);
    document.addEventListener('click', this.setOffset);
    setTimeout(this.addScrollListener, 0);
    setTimeout(this.setOffset, 1);
  }

  componentWillUnmount() {
    this.removeScrollListener();
    window.removeEventListener('resize', this.setOffset);
    document.removeEventListener('scroll', this.setOffset);
    document.removeEventListener('click', this.setOffset);
  }

  setOffset = () => {
    if (!this.node.current || !this.innerNode.current) return;
    const nodeBCRect = this.node.current.getBoundingClientRect();
    const scrollTop = this.state.containerNode ? this.state.containerNode.scrollTop : window.pageYOffset;
    const top = nodeBCRect.top + scrollTop;

    const innerNodeBCRect = this.innerNode.current.getBoundingClientRect();
    const innerNodeHeight = innerNodeBCRect.height;

    const containerNodeBCRect = this.state.containerNode ? this.state.containerNode.getBoundingClientRect() : null;
    const containerNodeHeight = this.state.containerNode
      ? containerNodeBCRect.height
      : document.documentElement.clientHeight || window.innerHeight;

    const containerNodeTop = this.state.containerNode ? containerNodeBCRect.top : 0;

    this.setState({
      scrollTop,
      top,
      width: nodeBCRect.width,
      left: nodeBCRect.left,
      innerNodeHeight,
      containerNodeHeight,
      containerNodeTop,
    });
  };

  addScrollListener = () => {
    if (this.props.getContainerNode) {
      const containerNode = this.props.getContainerNode();
      if (containerNode.current) {
        this.setState({ containerNode: containerNode.current });
        containerNode.current.addEventListener('scroll', this.setOffset);
      }
    }
  };

  removeScrollListener = () => {
    if (this.props.getContainerNode) {
      const containerNode = this.props.getContainerNode();
      if (containerNode.current) containerNode.current.removeEventListener('scroll', this.setOffset);
    }
  };

  render() {
    return (
      <StyledRboDocLayoutContentExtra
        {...addDataAttributes({ component: COMPONENTS.CONTENT_EXTRA })}
        innerRef={ref => {
          this.node.current = ref;
        }}
        sInnerNodeHeight={this.state.innerNodeHeight}
      >
        <StyledRboDocLayoutContentExtraFixed
          sWidth={this.state.width}
          sLeft={this.state.left}
          sTop={this.state.top}
          sContainerNodeHeight={this.state.containerNodeHeight}
          sContainerNodeTop={this.state.containerNodeTop}
        >
          <StyledRboDocLayoutContentExtraInner
            innerRef={ref => {
              this.innerNode.current = ref;
            }}
            sScrollTop={this.state.scrollTop}
            sTop={this.state.top}
            sInnerNodeHeight={this.state.innerNodeHeight}
            sContainerNodeHeight={this.state.containerNodeHeight}
            sContainerNodeTop={this.state.containerNodeTop}
          >
            {this.props.children}
          </StyledRboDocLayoutContentExtraInner>
        </StyledRboDocLayoutContentExtraFixed>
      </StyledRboDocLayoutContentExtra>
    );
  }
}

const RboDocLayoutContentExtra = props => (
  <RboDocLayoutContext.Consumer>
    {({ isInModal, getContainerNode }) => {
      if (isInModal) return <RboDocLayoutContentExtraInModal {...props} />;
      return <RboDocLayoutContentExtraWithContext {...props} getContainerNode={getContainerNode} />;
    }}
  </RboDocLayoutContext.Consumer>
);

RboDocLayoutContentExtra.propTypes = {
  children: PropTypes.node.isRequired,
};

RboDocLayoutContentExtra.Section = RboDocLayoutContentExtraSection;

export default RboDocLayoutContentExtra;
