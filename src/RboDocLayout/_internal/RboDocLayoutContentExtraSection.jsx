import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { Header } from '../../Styles';
import { COMPONENTS } from '../_constants';
import {
  StyledRboDocLayoutContentExtraSection,
  StyledRboDocLayoutContentExtraSectionTitle,
  StyledRboDocLayoutContentExtraSectionContent,
} from '../_style';

const RboDocLayoutContentExtraSection = props => (
  <StyledRboDocLayoutContentExtraSection {...addDataAttributes({ component: COMPONENTS.CONTENT_EXTRA_SECTION })}>
    <StyledRboDocLayoutContentExtraSectionTitle>
      <Header size={7} isUppercase>
        {props.title}
      </Header>
    </StyledRboDocLayoutContentExtraSectionTitle>
    <StyledRboDocLayoutContentExtraSectionContent>{props.children}</StyledRboDocLayoutContentExtraSectionContent>
  </StyledRboDocLayoutContentExtraSection>
);

RboDocLayoutContentExtraSection.propTypes = {
  title: PropTypes.node,
  children: PropTypes.node,
};

RboDocLayoutContentExtraSection.defaultProps = {
  title: null,
  children: null,
};

export default RboDocLayoutContentExtraSection;
