import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocLayoutContentMain, StyledRboDocLayoutContentMainInner } from '../_style';

const RboDocLayoutContentMain = props => (
  <StyledRboDocLayoutContentMain {...addDataAttributes({ component: COMPONENTS.CONTENT_MAIN })} isWide={props.isWide}>
    <StyledRboDocLayoutContentMainInner>{props.children}</StyledRboDocLayoutContentMainInner>
  </StyledRboDocLayoutContentMain>
);

RboDocLayoutContentMain.propTypes = {
  isWide: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

RboDocLayoutContentMain.defaultProps = {
  isWide: false,
};

export default RboDocLayoutContentMain;
