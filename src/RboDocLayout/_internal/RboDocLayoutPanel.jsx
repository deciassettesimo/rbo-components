import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { RboDocLayoutContext, RboDocLayoutPanelContext } from '../RboDocLayout';
import { StyledRboDocLayoutPanel, StyledRboDocLayoutPanelFixed, StyledRboDocLayoutPanelInner } from '../_style';

class RboDocLayoutPanelWithContext extends Component {
  static propTypes = {
    isBottom: PropTypes.bool,
    children: PropTypes.node.isRequired,
    getContainerNode: PropTypes.func,
  };

  static defaultProps = {
    isBottom: false,
    getContainerNode: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      containerNode: null,
      width: null,
      height: null,
      top: null,
      left: null,
    };

    this.node = React.createRef();
    this.innerNode = React.createRef();
  }

  componentDidMount() {
    window.addEventListener('resize', this.setOffset);
    document.addEventListener('scroll', this.setOffset);
    document.addEventListener('click', this.setOffset);
    setTimeout(this.addScrollListener, 0);
    setTimeout(this.setOffset, 1);
  }

  componentWillUnmount() {
    this.removeScrollListener();
    window.removeEventListener('resize', this.setOffset);
    document.removeEventListener('scroll', this.setOffset);
    document.removeEventListener('click', this.setOffset);
  }

  setOffset = () => {
    if (!this.node.current) return;
    const nodeBCRect = this.node.current.getBoundingClientRect();
    const innerNodeBCRect = this.innerNode.current.getBoundingClientRect();
    const containerNodeBCRect = this.state.containerNode ? this.state.containerNode.getBoundingClientRect() : null;
    const top = this.props.isBottom
      ? (this.state.containerNode ? containerNodeBCRect.top + containerNodeBCRect.height : window.innerHeight) -
        innerNodeBCRect.height
      : nodeBCRect.top + (this.state.containerNode ? this.state.containerNode.scrollTop : window.pageYOffset);
    this.setState({
      height: innerNodeBCRect.height,
      top,
      width: nodeBCRect.width,
      left: nodeBCRect.left,
    });
  };

  addScrollListener = () => {
    if (this.props.getContainerNode) {
      const containerNode = this.props.getContainerNode();
      if (containerNode.current) {
        this.setState({ containerNode: containerNode.current });
        containerNode.current.addEventListener('scroll', this.setOffset);
      }
    }
  };

  removeScrollListener = () => {
    if (this.props.getContainerNode) {
      const containerNode = this.props.getContainerNode();
      if (containerNode.current) containerNode.current.removeEventListener('scroll', this.setOffset);
    }
  };

  render() {
    return (
      <StyledRboDocLayoutPanel
        {...addDataAttributes({ component: COMPONENTS.PANEL })}
        innerRef={ref => {
          this.node.current = ref;
        }}
        sHeight={this.state.height}
      >
        <StyledRboDocLayoutPanelFixed
          isBottom={this.props.isBottom}
          sWidth={this.state.width}
          sLeft={this.state.left}
          sTop={this.state.top}
        >
          <StyledRboDocLayoutPanelInner
            innerRef={ref => {
              this.innerNode.current = ref;
            }}
          >
            <RboDocLayoutPanelContext.Provider value={{ popupBoxZIndex: 11 }}>
              {this.props.children}
            </RboDocLayoutPanelContext.Provider>
          </StyledRboDocLayoutPanelInner>
        </StyledRboDocLayoutPanelFixed>
      </StyledRboDocLayoutPanel>
    );
  }
}

const RboDocLayoutPanel = props => (
  <RboDocLayoutContext.Consumer>
    {({ getContainerNode }) => <RboDocLayoutPanelWithContext {...props} getContainerNode={getContainerNode} />}
  </RboDocLayoutContext.Consumer>
);

RboDocLayoutPanel.propTypes = {
  children: PropTypes.node.isRequired,
};

export default RboDocLayoutPanel;
