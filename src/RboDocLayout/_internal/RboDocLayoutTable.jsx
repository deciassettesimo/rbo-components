import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import { StyledRboDocLayoutTable, StyledRboDocLayoutTableInner } from '../_style';

const RboDocLayoutTable = props => (
  <StyledRboDocLayoutTable {...addDataAttributes({ component: COMPONENTS.TABLE })}>
    <StyledRboDocLayoutTableInner>{props.children}</StyledRboDocLayoutTableInner>
  </StyledRboDocLayoutTable>
);

RboDocLayoutTable.propTypes = {
  children: PropTypes.node.isRequired,
};

export default RboDocLayoutTable;
