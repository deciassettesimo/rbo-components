import styled from 'styled-components';

import { STYLES } from '../_constants';

const rboDocLayoutPanelFixedStyle = (sWidth, sLeft, sTop) => {
  if (sWidth === null) return {};
  return {
    position: 'fixed',
    width: sWidth,
    left: sLeft,
    top: sTop,
  };
};

const rboDocLayoutContentExtraStyle = sInnerNodeHeight => ({
  minHeight: sInnerNodeHeight === null ? 'auto' : sInnerNodeHeight,
});

const rboDocLayoutContentExtraFixedStyle = (sWidth, sLeft, sTop, sContainerNodeHeight, sContainerNodeTop) => {
  if (sWidth === null) return {};
  const height = sContainerNodeHeight - (sTop - sContainerNodeTop);
  return {
    position: 'fixed',
    width: sWidth,
    height,
    left: sLeft,
    top: sTop,
  };
};

const rboDocLayoutContentExtraInnerStyle = (
  sScrollTop,
  sTop,
  sInnerNodeHeight,
  sContainerNodeHeight,
  sContainerNodeTop,
) => {
  const height = sContainerNodeHeight - (sTop - sContainerNodeTop);
  const marginTop = height + sScrollTop > sInnerNodeHeight ? -(sInnerNodeHeight - height) : -sScrollTop;
  if (!marginTop) return {};
  return {
    marginTop: height > sInnerNodeHeight ? 0 : marginTop,
  };
};

export const StyledRboDocLayout = styled.div`
  position: relative;
  box-sizing: border-box;
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
  flex-grow: 1;
`;

export const StyledRboDocLayoutInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: column nowrap;
  flex-grow: 1;
  flex-shrink: 0;
`;

export const StyledRboDocLayoutPanel = styled.div.attrs({
  style: ({ sHeight }) => ({
    height: sHeight === null ? 'auto' : sHeight,
  }),
})`
  position: relative;
  box-sizing: border-box;
`;

export const StyledRboDocLayoutPanelFixed = styled.div.attrs({
  style: ({ sWidth, sLeft, sTop }) => ({
    ...rboDocLayoutPanelFixedStyle(sWidth, sLeft, sTop),
  }),
})`
  position: relative;
  box-sizing: border-box;
  background: ${props => (props.isBottom ? STYLES.COLORS.WHITE_THREE : STYLES.COLORS.WHITE)};
  border-bottom: ${props => (props.isBottom ? 'none' : `1px solid ${STYLES.COLORS.PINKISH_GREY}`)};
  box-shadow: ${props =>
    props.isBottom ? `0 -2px 4px ${STYLES.COLORS.BLACK_12}` : `0 2px 4px ${STYLES.COLORS.BLACK_04}`};
  z-index: 10;
`;

export const StyledRboDocLayoutPanelInner = styled.div`
  position: relative;
  box-sizing: border-box;
  padding: 0 40px;
`;

export const StyledRboDocLayoutContent = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  flex-grow: 1;
  width: 100%;
  max-width: ${props => (props.sMaxWidth === null ? 'auto' : `${props.sMaxWidth}px`)};
  margin: 0 auto;
  background: ${STYLES.COLORS.WHITE_THREE};
  box-shadow: ${props => (props.isInModal ? 'none' : `0 0 4px ${STYLES.COLORS.BLACK_38}`)};
`;

export const StyledRboDocLayoutContentMain = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 0;
  flex-shrink: 0;
  width: ${props => (props.isWide ? '100' : '75')}%;
  background-color: ${STYLES.COLORS.WHITE};
  display: flex;
  flex-flow: column nowrap;
  border-right: ${props => (props.isWide ? 'none' : `1px solid ${STYLES.COLORS.PINKISH_GREY}`)};
`;

export const StyledRboDocLayoutContentMainInner = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-shrink: 0;
  flex-grow: 1;
  display: flex;
  flex-flow: column nowrap;
`;

export const StyledRboDocLayoutContentExtra = styled.div.attrs({
  style: ({ sInnerNodeHeight }) => ({
    ...rboDocLayoutContentExtraStyle(sInnerNodeHeight),
  }),
})`
  position: relative;
  box-sizing: border-box;
  flex-grow: 0;
  flex-shrink: 0;
  width: 25%;
  background: ${STYLES.COLORS.WHITE_THREE};
`;

export const StyledRboDocLayoutContentExtraFixed = styled.div.attrs({
  style: ({ sWidth, sLeft, sTop, sContainerNodeHeight, sContainerNodeTop }) => ({
    ...rboDocLayoutContentExtraFixedStyle(sWidth, sLeft, sTop, sContainerNodeHeight, sContainerNodeTop),
  }),
})`
  position: relative;
  box-sizing: border-box;
  overflow: hidden;
`;

export const StyledRboDocLayoutContentExtraInner = styled.div.attrs({
  style: ({ sScrollTop, sTop, sInnerNodeHeight, sContainerNodeHeight, sContainerNodeTop }) => ({
    ...rboDocLayoutContentExtraInnerStyle(sScrollTop, sTop, sInnerNodeHeight, sContainerNodeHeight, sContainerNodeTop),
  }),
})`
  position: relative;
  box-sizing: border-box;
`;

export const StyledRboDocLayoutContentExtraSection = styled.div`
  position: relative;
  box-sizing: border-box;
  padding: 16px;

  :first-child {
    padding-top: 32px;
  }

  :last-child {
    padding-bottom: 32px;
  }
`;

export const StyledRboDocLayoutContentExtraSectionTitle = styled.div`
  position: relative;
  box-sizing: border-box;
  color: ${STYLES.COLORS.WARM_GRAY};
  padding-bottom: 8px;
`;

export const StyledRboDocLayoutContentExtraSectionContent = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
`;

export const StyledRboDocLayoutTable = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
  flex-grow: 1;
`;

export const StyledRboDocLayoutTableInner = styled.div`
  position: absolute;
  display: block;
  box-sizing: border-box;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;
