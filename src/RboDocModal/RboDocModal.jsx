import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import Button from '../Button';
import Loader from '../Loader';
import ModalWindow from '../ModalWindow';
import RboDocLayout from '../RboDocLayout';
import RboDocErrors from '../RboDocErrors';
import { Header, Block } from '../Styles';
import { COMPONENT } from './_constants';
import {
  StyledRboDocModal,
  StyledRboDocModalHeader,
  StyledRboDocModalInner,
  StyledRboDocModalContent,
  StyledRboDocModalFooter,
  StyledRboDocModalOperations,
  StyledRboDocModalOperationsBlock,
  StyledRboDocModalOperationsItem,
  StyledRboDocModalLoader,
} from './_style';
import REFS from './_config';

export default class RboDocModal extends PureComponent {
  static REFS = { ...REFS };

  static propTypes = {
    children: PropTypes.node.isRequired,
    width: PropTypes.number,
    contentMinHeight: PropTypes.number,
    title: PropTypes.string,
    errorsTitle: PropTypes.string,
    errors: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        level: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        fieldsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
        isActive: PropTypes.bool,
      }),
    ),
    onErrorClick: PropTypes.func,
    operations: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        align: PropTypes.oneOf(Object.values(REFS.OPERATIONS_ALIGN)).isRequired,
        type: PropTypes.oneOf(Object.values(REFS.OPERATIONS_BUTTONS_TYPES)),
        disabled: PropTypes.bool.isRequired,
      }),
    ),
    footer: PropTypes.node,
    isLoading: PropTypes.bool,
    onOperationClick: PropTypes.func,
    onClose: PropTypes.func.isRequired,
  };

  static defaultProps = {
    width: 768,
    contentMinHeight: null,
    title: null,
    errors: null,
    errorsTitle: null,
    onErrorClick: () => null,
    operations: null,
    footer: null,
    isLoading: false,
    onOperationClick: () => null,
  };

  constructor(props) {
    super(props);

    this.node = React.createRef();
  }

  render() {
    const isWithErrors = this.props.errors && !!this.props.errors.length;
    const isWithFooter = (this.props.operations && !!this.props.operations.length) || this.props.footer;
    return (
      <ModalWindow ref={this.node} onClose={this.props.onClose} isClosingOnEscPress={false} isClosingOnOutClick={false}>
        <StyledRboDocModal
          {...addDataAttributes({ component: COMPONENT })}
          sWidth={this.props.width}
          isWithErrors={isWithErrors}
        >
          {this.props.title && (
            <StyledRboDocModalHeader>
              <Block borderBottomColor={Block.REFS.BORDER_COLORS.MARIGOLD}>
                <Header size={3}>{this.props.title}</Header>
              </Block>
            </StyledRboDocModalHeader>
          )}
          <StyledRboDocModalInner>
            <StyledRboDocModalContent sMinHeight={this.props.contentMinHeight}>
              <RboDocLayout isInModal getContainerNode={() => this.node}>
                <RboDocLayout.Content>
                  <RboDocLayout.Content.Main isWide={!isWithErrors}>{this.props.children}</RboDocLayout.Content.Main>
                  {isWithErrors && (
                    <RboDocLayout.Content.Extra>
                      <RboDocLayout.Content.Extra.Section title={this.props.errorsTitle}>
                        <RboDocErrors items={this.props.errors} onItemClick={this.props.onErrorClick} />
                      </RboDocLayout.Content.Extra.Section>
                    </RboDocLayout.Content.Extra>
                  )}
                </RboDocLayout.Content>
              </RboDocLayout>
            </StyledRboDocModalContent>
            {isWithFooter && (
              <Block borderTopColor={Block.REFS.BORDER_COLORS.BLACK_24}>
                {this.props.footer && <StyledRboDocModalFooter>{this.props.footer}</StyledRboDocModalFooter>}
                {this.props.operations &&
                  this.props.operations.length && (
                    <StyledRboDocModalOperations>
                      <StyledRboDocModalOperationsBlock>
                        {this.props.operations
                          .filter(operation => operation.align === REFS.OPERATIONS_ALIGN.LEFT)
                          .map(operation => (
                            <StyledRboDocModalOperationsItem key={operation.id}>
                              <Button
                                id={operation.id}
                                dimension={Button.REFS.DIMENSIONS.S}
                                type={operation.type}
                                onClick={this.props.onOperationClick}
                                disabled={operation.disabled}
                              >
                                {operation.label}
                              </Button>
                            </StyledRboDocModalOperationsItem>
                          ))}
                      </StyledRboDocModalOperationsBlock>
                      <StyledRboDocModalOperationsBlock>
                        {this.props.operations
                          .filter(operation => operation.align === REFS.OPERATIONS_ALIGN.RIGHT)
                          .map(operation => (
                            <StyledRboDocModalOperationsItem key={operation.id}>
                              <Button
                                id={operation.id}
                                dimension={Button.REFS.DIMENSIONS.S}
                                type={operation.type}
                                onClick={this.props.onOperationClick}
                                disabled={operation.disabled}
                              >
                                {operation.label}
                              </Button>
                            </StyledRboDocModalOperationsItem>
                          ))}
                      </StyledRboDocModalOperationsBlock>
                    </StyledRboDocModalOperations>
                  )}
              </Block>
            )}
            {this.props.isLoading && (
              <StyledRboDocModalLoader>
                <Loader isCentered dimension={Loader.REFS.DIMENSIONS.L} />
              </StyledRboDocModalLoader>
            )}
          </StyledRboDocModalInner>
        </StyledRboDocModal>
      </ModalWindow>
    );
  }
}
