### RboDocModal without errors

```jsx
const RboDocModalExample = require('./__examples__/RboDocModal.example').default;
<RboDocModalExample title="Подтверждающий документ № 1" contentMinHeight={480} />
```

### RboDocModal with errors

```jsx
const RboDocModalExample = require('./__examples__/RboDocModal.example').default;
const errors = [
  {
    level: '3',
    id: '1.1',
    text: 'От ООО АРТ БАЗАР (сокр)  документ \'Справка о подтверждающих документах\' не может быть направлен в банк (по счету 40702810500003409387 услуга  не предоставляется банком)',
    fieldsIds: ['DocNumber'],
    isActive: false,
  },
  {
    level: '1',
    id: '2.3.2',
    text: 'Дата справки должна быть равна текущей дате (22.08.2018)',
    fieldsIds: ['DocDate'],
    isActive: false,
  },
  {
    level: '1',
    id: '2.2.2',
    text: 'Дата документа указана некорректно (должна быть текущая дата)',
    fieldsIds: ['DocDate'],
    isActive: false,
  },
];
<RboDocModalExample title="Подтверждающий документ № 1" errors={errors} />
```

### RboDocModal isLoading

```jsx
const RboDocModalExample = require('./__examples__/RboDocModal.example').default;
<RboDocModalExample title="Подтверждающий документ № 1" isLoading />
```

#### RboDocModal Example Source Code
```js { "file": "./__examples__/RboDocModal.example.jsx" }
```

