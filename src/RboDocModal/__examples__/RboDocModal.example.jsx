import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

import Button from '../../Button';
import RboDocForm from '../../RboDocForm';
import Form from '../../Form';
import { InputDigital, InputDateCalendar, InputSuggest, InputText, InputSearch } from '../../Input';
import RboDocModal from '../RboDocModal';

const operations = [
  {
    id: 'save',
    label: 'Сохранить',
    align: RboDocModal.REFS.OPERATIONS_ALIGN.LEFT,
    type: RboDocModal.REFS.OPERATIONS_BUTTONS_TYPES.BASE_PRIMARY,
  },
  {
    id: 'remove',
    label: 'Удалить',
    align: RboDocModal.REFS.OPERATIONS_ALIGN.RIGHT,
    type: RboDocModal.REFS.OPERATIONS_BUTTONS_TYPES.DANGER_PRIMARY,
  },
  {
    id: 'cancel',
    label: 'Отмена',
    align: RboDocModal.REFS.OPERATIONS_ALIGN.RIGHT,
    type: RboDocModal.REFS.OPERATIONS_BUTTONS_TYPES.BASE_SECONDARY,
  },
];

const FormExample = () => (
  <RboDocForm>
    <Form dimension={Form.REFS.DIMENSIONS.S} autoFocus="DocNumber">
      <RboDocForm.Section dataAttributes={{ id: 'Section1' }}>
        <RboDocForm.Section.Content>
          <RboDocForm.Blockset>
            <RboDocForm.Block isWide>
              <Form.Row>
                <Form.Cell width="50%">
                  <Form.Label htmlFor="DocNumber">Номер документа</Form.Label>
                  <Form.Field>
                    <InputDigital id="DocNumber" maxLength={7} />
                  </Form.Field>
                </Form.Cell>
                <Form.Cell width="50%">
                  <Form.Label htmlFor="DocDate">Дата документа</Form.Label>
                  <Form.Field>
                    <InputDateCalendar id="DocDate" />
                  </Form.Field>
                </Form.Cell>
              </Form.Row>
            </RboDocForm.Block>
          </RboDocForm.Blockset>
          <RboDocForm.Blockset>
            <RboDocForm.Block isWide>
              <Form.Row>
                <Form.Cell>
                  <Form.Label htmlFor="AccountingContarctPt1">Номер УК</Form.Label>
                  <Form.Fieldset withAutoTransitions>
                    <Form.Field width="27%">
                      <InputSuggest
                        inputElement={InputDigital}
                        id="AccountingContarctPt1"
                        maxLength={8}
                        options={[]}
                        notFoundLabel="Ничего не найдено"
                        onSearch={() => {}}
                      />
                    </Form.Field>
                    /
                    <Form.Field width="19%">
                      <InputDigital id="AccountingContarctPt2" maxLength={4} />
                    </Form.Field>
                    /
                    <Form.Field width="19%">
                      <InputText id="AccountingContarctPt3" maxLength={4} />
                    </Form.Field>
                    /
                    <Form.Field width="17.5%">
                      <InputSearch
                        inputElement={InputDigital}
                        id="AccountingContarctPt4"
                        maxLength={1}
                        options={[{ value: '1', title: '1' }, { value: '2', title: '2' }, { value: '3', title: '3' }]}
                        notFoundLabel="Ничего не найдено"
                        onSearch={() => {}}
                      />
                    </Form.Field>
                    /
                    <Form.Field width="17.5%">
                      <InputSearch
                        inputElement={InputDigital}
                        id="AccountingContarctPt5"
                        maxLength={1}
                        options={[{ value: '1', title: '1' }, { value: '2', title: '2' }, { value: '3', title: '3' }]}
                        notFoundLabel="Ничего не найдено"
                        onSearch={() => {}}
                      />
                    </Form.Field>
                  </Form.Fieldset>
                </Form.Cell>
              </Form.Row>
            </RboDocForm.Block>
          </RboDocForm.Blockset>
          <RboDocForm.Blockset>
            <RboDocForm.Block isWide>
              <Form.Row>
                <Form.Cell width="50%">
                  <Form.Label htmlFor="Acoount">Счет</Form.Label>
                  <Form.Field>
                    <InputText id="Account" />
                  </Form.Field>
                </Form.Cell>
                <Form.Cell width="50%">
                  <Form.Label htmlFor="Describe">Описание документа</Form.Label>
                  <Form.Field>
                    <InputText id="Describe" />
                  </Form.Field>
                </Form.Cell>
              </Form.Row>
            </RboDocForm.Block>
          </RboDocForm.Blockset>
        </RboDocForm.Section.Content>
      </RboDocForm.Section>
    </Form>
  </RboDocForm>
);

export default class RboDocModalExample extends PureComponent {
  static propTypes = {
    errors: PropTypes.arrayOf(PropTypes.shape()),
  };

  static defaultProps = {
    errors: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      errors: this.props.errors,
    };
  }

  handleModalOpen = () => {
    this.setState({ isOpen: true });
  };

  handleModalClose = () => {
    this.setState({ isOpen: false });
  };

  handleErrorClick = ({ errorId }) => {
    const { errors } = this.state;

    this.setState({
      errors: errors.map(error => ({ ...error, isActive: error.id === errorId })),
    });
  };

  handleOperationClick = () => {
    this.handleModalClose();
  };

  render() {
    return (
      <Fragment>
        <Button id="RboDocModalOpener" onClick={this.handleModalOpen}>
          Открыть документ
        </Button>
        {this.state.isOpen && (
          <RboDocModal
            {...this.props}
            errorsTitle="Ошибки"
            errors={this.state.errors}
            onErrorClick={this.handleErrorClick}
            onClose={this.handleModalClose}
            operations={operations}
            onOperationClick={this.handleOperationClick}
          >
            <FormExample />
          </RboDocModal>
        )}
      </Fragment>
    );
  }
}
