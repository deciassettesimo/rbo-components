import { OPERATIONS_ALIGN, OPERATIONS_BUTTONS_TYPES } from './_constants';

const REFS = {
  OPERATIONS_ALIGN,
  OPERATIONS_BUTTONS_TYPES,
};

export default REFS;
