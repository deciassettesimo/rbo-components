import { TYPES as BUTTON_TYPES } from '../Button/_constants';

export const COMPONENT = 'RboDocModal';

export const OPERATIONS_BUTTONS_TYPES = BUTTON_TYPES;

export const OPERATIONS_ALIGN = {
  LEFT: 'left',
  RIGHT: 'right',
};
