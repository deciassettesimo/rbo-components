import styled from 'styled-components';

import { STYLES } from '../_constants';

export const StyledRboDocModal = styled.div.attrs({
  style: ({ sWidth, isWithErrors }) => ({
    width: isWithErrors ? (sWidth / 3) * 4 : sWidth,
  }),
})`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledRboDocModalHeader = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledRboDocModalInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledRboDocModalContent = styled.div.attrs({
  style: ({ sMinHeight }) => ({ minHeight: sMinHeight }),
})`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledRboDocModalFooter = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledRboDocModalLoader = styled.div`
  position: absolute;
  box-sizing: border-box;
  display: block;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: ${STYLES.COLORS.WHITE};
  z-index: 12;
`;

export const StyledRboDocModalOperations = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
`;

export const StyledRboDocModalOperationsBlock = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  white-space: nowrap;
  padding: 0 8px;

  :first-child {
    padding-left: 0;
  }

  :last-child {
    padding-right: 0;
  }
`;

export const StyledRboDocModalOperationsItem = styled.div`
  display: inline-block;
  padding: 0 8px;

  :first-child {
    padding-left: 0;
  }

  :last-child {
    padding-right: 0;
  }
`;
