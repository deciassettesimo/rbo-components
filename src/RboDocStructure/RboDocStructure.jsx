import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';
import RboDocStructureTab from './_internal/RboDocStructureTab';
import { StyledRboDocStructure } from './_style';

const RboDocStructure = props => (
  <StyledRboDocStructure {...addDataAttributes({ component: COMPONENTS.GENERAL })}>
    {props.structure.map(tab => (
      <RboDocStructureTab
        key={tab.id}
        id={tab.id}
        title={tab.title}
        sections={tab.sections}
        isActive={tab.isActive}
        onClick={props.onTabClick}
        onSectionClick={props.onSectionClick}
        onErrorClick={props.onErrorClick}
      />
    ))}
  </StyledRboDocStructure>
);

RboDocStructure.propTypes = {
  structure: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string,
      isActive: PropTypes.bool,
      sections: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          title: PropTypes.string,
          isVisible: PropTypes.bool.isRequired,
          isActive: PropTypes.bool,
          errors: PropTypes.arrayOf(
            PropTypes.shape({
              level: PropTypes.string.isRequired,
              id: PropTypes.string.isRequired,
              text: PropTypes.string.isRequired,
              fieldsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
              isActive: PropTypes.bool.isRequired,
            }),
          ),
        }),
      ),
    }),
  ).isRequired,
  onTabClick: PropTypes.func,
  onSectionClick: PropTypes.func,
  onErrorClick: PropTypes.func,
};

RboDocStructure.defaultProps = {
  onTabClick: () => null,
  onSectionClick: () => null,
  onErrorClick: () => null,
};

export default RboDocStructure;
