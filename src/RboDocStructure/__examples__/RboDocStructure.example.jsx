import React, { PureComponent } from 'react';
import RboDocStructure from '../RboDocStructure';

const documentStructure = [
  {
    id: 'main',
    title: 'Основные поля',
    isActive: true,
    sections: [
      {
        id: 'templateName',
        title: 'Наименование шаблона',
        isVisible: false,
        errors: [],
      },
      {
        id: 'primary',
        isVisible: true,
        title: 'Общая информация',
        errors: [
          {
            level: '3',
            id: '1.1',
            text:
              "От ООО АРТ БАЗАР (сокр)  документ 'Справка о подтверждающих документах' не может быть направлен в банк (по счету 40702810500003409387 услуга  не предоставляется банком)",
            fieldsIds: ['account'],
            isActive: false,
          },
          {
            level: '1',
            id: '2.3.2',
            text: 'Дата справки должна быть равна текущей дате (22.08.2018)',
            fieldsIds: ['certificateDate'],
            isActive: false,
          },
          {
            level: '1',
            id: '2.2.2',
            text: 'Дата документа указана некорректно (должна быть текущая дата)',
            fieldsIds: ['certificateDate'],
            isActive: false,
          },
        ],
      },
      {
        id: 'responsiblePerson',
        isVisible: true,
        title: 'Ответственное лицо',
        errors: [
          {
            level: '2',
            id: '2.4.1',
            text: 'Не указаны ФИО ответственного лица',
            fieldsIds: ['responsiblePersonName'],
            isActive: false,
          },
          {
            level: '2',
            id: '2.4.2',
            text: 'Не указан номер телефона ответственного лица',
            fieldsIds: ['responsiblePersonPhone'],
            isActive: false,
          },
        ],
      },
      {
        id: 'relatedDocs',
        isVisible: true,
        title: 'Подтверждающие документы',
        errors: [
          {
            level: '2',
            id: '3.1',
            text: 'Документ должен содержать хотя бы одну запись о подтверждающих документах',
            fieldsIds: ['relatedDocs'],
            isActive: false,
          },
        ],
      },
      {
        id: 'attachments',
        isVisible: true,
        title: 'Вложения',
        errors: [],
      },
    ],
  },
  {
    id: 'notes',
    title: 'Заметки',
    isActive: false,
    sections: null,
  },
  {
    id: 'bankInfo',
    title: 'Информация из банка',
    isActive: false,
    sections: null,
  },
];

export default class RboDocStructureExample extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = { documentStructure };
  }

  handleTabClick = ({ tabId }) => {
    this.setState({
      documentStructure: this.state.documentStructure.map(tab => ({
        ...tab,
        isActive: tab.id === tabId,
        sections:
          tab.sections &&
          tab.sections.map(section => ({
            ...section,
            errors:
              section.errors &&
              section.errors.map(error => ({
                ...error,
                isActive: false,
              })),
          })),
      })),
    });
  };

  handleSectionClick = ({ tabId }) => {
    this.setState({
      documentStructure: this.state.documentStructure.map(tab => ({
        ...tab,
        isActive: tab.id === tabId,
        sections:
          tab.sections &&
          tab.sections.map(section => ({
            ...section,
            errors:
              section.errors &&
              section.errors.map(error => ({
                ...error,
                isActive: false,
              })),
          })),
      })),
    });
  };

  handleErrorClick = ({ errorId, tabId }) => {
    this.setState({
      documentStructure: this.state.documentStructure.map(tab => ({
        ...tab,
        isActive: tab.id === tabId,
        sections:
          tab.sections &&
          tab.sections.map(section => ({
            ...section,
            errors:
              section.errors &&
              section.errors.map(error => ({
                ...error,
                isActive: error.id === errorId,
              })),
          })),
      })),
    });
  };

  render() {
    return (
      <RboDocStructure
        structure={this.state.documentStructure}
        onTabClick={this.handleTabClick}
        onSectionClick={this.handleSectionClick}
        onErrorClick={this.handleErrorClick}
      />
    );
  }
}
