export const COMPONENTS = {
  GENERAL: 'RboDocStructure',
  TAB: 'RboDocStructureTab',
  TAB_SECTION: 'RboDocStructureTabSection',
};

export default COMPONENTS;
