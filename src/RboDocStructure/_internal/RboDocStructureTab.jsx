import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';
import {
  StyledRboDocStructureTab,
  StyledRboDocStructureTabHeader,
  StyledRboDocStructureTabTitle,
  StyledRboDocStructureTabSections,
} from '../_style';
import RboDocStructureTabSection from './RboDocStructureTabSection';

const RboDocStructureTab = props => {
  const handleOnTitleClick = () => {
    if (!props.isActive) {
      props.onClick({ tabId: props.id });
    }
  };

  return (
    <StyledRboDocStructureTab {...addDataAttributes({ component: COMPONENTS.TAB })} isActive={props.isActive}>
      {props.title && (
        <StyledRboDocStructureTabHeader>
          <StyledRboDocStructureTabTitle isActive={props.isActive} onMouseDown={handleOnTitleClick}>
            {props.title}
          </StyledRboDocStructureTabTitle>
        </StyledRboDocStructureTabHeader>
      )}
      {props.sections &&
        !!props.sections.length && (
          <StyledRboDocStructureTabSections>
            {props.sections.filter(section => section.isVisible).map(section => (
              <RboDocStructureTabSection
                key={section.id}
                id={section.id}
                tabId={props.id}
                title={section.title}
                errors={section.errors}
                isActive={section.isActive}
                onClick={props.onSectionClick}
                onErrorClick={props.onErrorClick}
              />
            ))}
          </StyledRboDocStructureTabSections>
        )}
    </StyledRboDocStructureTab>
  );
};

RboDocStructureTab.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string,
  isActive: PropTypes.bool,
  sections: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string,
      isVisible: PropTypes.bool.isRequired,
      isActive: PropTypes.bool,
      errors: PropTypes.arrayOf(
        PropTypes.shape({
          level: PropTypes.string.isRequired,
          id: PropTypes.string.isRequired,
          text: PropTypes.string.isRequired,
          fieldsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
          isActive: PropTypes.bool.isRequired,
        }),
      ),
    }),
  ),
  onClick: PropTypes.func,
  onSectionClick: PropTypes.func,
  onErrorClick: PropTypes.func,
};

RboDocStructureTab.defaultProps = {
  title: null,
  isActive: false,
  sections: null,
  onClick: () => null,
  onSectionClick: () => null,
  onErrorClick: () => null,
};

export default RboDocStructureTab;
