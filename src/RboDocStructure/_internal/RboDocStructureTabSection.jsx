import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import RboDocErrors from '../../RboDocErrors';
import StylesLink from '../../Styles/Link';
import { COMPONENTS } from '../_constants';
import {
  StyledRboDocStructureTabSection,
  StyledRboDocStructureTabSectionHeader,
  StyledRboDocStructureTabSectionErrors,
} from '../_style';

const RboDocStructureTabSection = props => {
  const handleOnTitleClick = () => {
    setTimeout(() => {
      props.onClick({ tabId: props.tabId, sectionId: props.id });
    }, 0);
  };

  const handleOnErrorClick = ({ errorId, fieldsIds }) => {
    props.onErrorClick({ tabId: props.tabId, sectionId: props.id, errorId, fieldsIds });
  };

  return (
    <StyledRboDocStructureTabSection {...addDataAttributes({ component: COMPONENTS.TAB_SECTION })}>
      {props.title && (
        <StyledRboDocStructureTabSectionHeader isActive={props.isActive}>
          <StylesLink isPseudo onClick={handleOnTitleClick}>
            {props.title}
          </StylesLink>
        </StyledRboDocStructureTabSectionHeader>
      )}
      {props.errors &&
        !!props.errors.length && (
          <StyledRboDocStructureTabSectionErrors>
            <RboDocErrors items={props.errors} onItemClick={handleOnErrorClick} />
          </StyledRboDocStructureTabSectionErrors>
        )}
    </StyledRboDocStructureTabSection>
  );
};

RboDocStructureTabSection.propTypes = {
  tabId: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  title: PropTypes.string,
  isActive: PropTypes.bool,
  errors: PropTypes.arrayOf(
    PropTypes.shape({
      level: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      fieldsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
      isActive: PropTypes.bool.isRequired,
    }),
  ),
  onClick: PropTypes.func,
  onErrorClick: PropTypes.func,
};

RboDocStructureTabSection.defaultProps = {
  title: null,
  isActive: false,
  errors: null,
  onClick: () => null,
  onErrorClick: () => null,
};

export default RboDocStructureTabSection;
