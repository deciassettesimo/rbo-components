import styled from 'styled-components';
import { STYLES } from '../_constants';

const rboDocStructureTabTitleBorderBottomColor = (isActive, isHovered) => {
  if (isActive) return STYLES.COLORS.DARK_SKY_BLUE;
  if (isHovered) return STYLES.COLORS.BLACK;
  return STYLES.COLORS.TRANSPARENT;
};

export const StyledRboDocStructure = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  font-size: 12px;
`;

export const StyledRboDocStructureTab = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px 0;

  :first-child {
    padding-top: 0;
  }

  :last-child {
    padding-bottom: 0;
  }
`;

export const StyledRboDocStructureTabHeader = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding-bottom: 8px;

  :last-child {
    padding-bottom: 0;
  }
`;

export const StyledRboDocStructureTabTitle = styled.span`
  display: inline-block;
  color: ${({ isActive }) => (isActive ? STYLES.COLORS.BLACK : STYLES.COLORS.WARM_GRAY)};
  cursor: ${props => (props.isActive ? 'default' : 'pointer')};
  line-height: 18px;
  border-bottom: 2px solid ${props => rboDocStructureTabTitleBorderBottomColor(props.isActive)};
  transition: all 0.32s ease-out;

  :hover {
    border-bottom-color: ${props => rboDocStructureTabTitleBorderBottomColor(props.isActive, true)};
  }
`;

export const StyledRboDocStructureTabSections = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

export const StyledRboDocStructureTabSection = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 4px 0;

  :first-child {
    padding-top: 0;
  }

  :last-child {
    padding-bottom: 0;
  }
`;

export const StyledRboDocStructureTabSectionHeader = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding-bottom: 4px;
  font-weight: ${props => (props.isActive ? 'bold' : 'normal')};

  :last-child {
    padding-bottom: 0;
  }
`;

export const StyledRboDocStructureTabSectionErrors = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;
