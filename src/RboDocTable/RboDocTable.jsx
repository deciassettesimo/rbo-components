import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import REFS from './_config';
import RboDocTableHead from './_internal/RboDocTableHead';
import RboDocTableBody from './_internal/RboDocTableBody';
import { StyledRboDocTable, StyledRboDocTableInner } from './_style';

export default class RboDocTable extends PureComponent {
  static REFS = { ...REFS };

  static propTypes = {
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        isWarning: PropTypes.bool,
        isError: PropTypes.bool,
      }),
    ),
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        width: PropTypes.number.isRequired,
        align: PropTypes.oneOf(Object.values(REFS.COLUMNS_ALIGN)),
        label: PropTypes.string,
      }),
    ).isRequired,
    operations: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        icon: PropTypes.string,
      }),
    ),
    isMultiLine: PropTypes.bool,
    emptyLabel: PropTypes.string,
    cellRenderer: PropTypes.func,
    onItemClick: PropTypes.func,
    onOperationClick: PropTypes.func,
  };

  static defaultProps = {
    locale: REFS.LOCALES.RU,
    items: [],
    operations: [],
    isMultiLine: false,
    emptyLabel: null,
    cellRenderer: null,
    onItemClick: null,
    onOperationClick: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      width: null,
      scrollLeft: null,
    };

    this.node = React.createRef();
  }

  componentDidMount() {
    window.addEventListener('resize', this.setOffset);
    this.node.current.addEventListener('scroll', this.setOffset);
    setTimeout(this.setOffset, 0);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setOffset);
    this.node.current.removeEventListener('scroll', this.setOffset);
  }

  setOffset = () => {
    if (!this.node || !this.node.current) return;
    const nodeBCRect = this.node.current.getBoundingClientRect();

    this.setState({
      width: nodeBCRect.width,
      scrollLeft: this.node.current.scrollLeft,
    });
  };

  render() {
    return (
      <StyledRboDocTable
        innerRef={ref => {
          this.node.current = ref;
        }}
      >
        <StyledRboDocTableInner>
          <RboDocTableHead columns={this.props.columns} />
          <RboDocTableBody
            locale={this.props.locale}
            items={this.props.items}
            columns={this.props.columns}
            operations={this.props.operations}
            isMultiLine={this.props.isMultiLine}
            emptyLabel={this.props.emptyLabel}
            cellRenderer={this.props.cellRenderer}
            onItemClick={this.props.onItemClick}
            onOperationClick={this.props.onOperationClick}
            parentNodeWidth={this.state.width}
            parentNodeScrollLeft={this.state.scrollLeft}
          />
        </StyledRboDocTableInner>
      </StyledRboDocTable>
    );
  }
}
