```js
<RboDocTable
  items={[
    {
      countryCode: "KR / 091",
      countryISOCode: "KR",
      countryName: "Республика Корея",
      id: "1",
      isError: false,
      isWarning: false,
      nonResidentName: "Корея",
    },
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан",
      id: "2",
      isError: false,
      isWarning: false,
      nonResidentName: "Азербайджан",
    },
    {
      countryCode: "RU / 007",
      countryISOCode: "RU",
      countryName: "Российская Федерация",
      id: "3",
      isError: false,
      isWarning: false,
      nonResidentName: "Россия",
    },
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан",
      id: "4",
      isError: false,
      isWarning: false,
      nonResidentName: "Азербайджан",
    },
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан Республика Азербайджан Республика Азербайджан Республика Азербайджан Республика Азербайджан Республика Азербайджан",
      id: "5",
      isError: false,
      isWarning: true,
      nonResidentName: "Азербайджан",
    },
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан",
      id: "6",
      isError: true,
      isWarning: false,
      nonResidentName: "Азербайджан",
    },
  ]}
  columns={[
    {
      id: 'nonResidentName',
      label: 'Наименование нерезидента',
      align: 'left',
      width: 300,
    },
    {
      id: 'countryName',
      label: 'Страна',
      align: 'left',
      width: 500,
    },
    {
      id: 'countryCode',
      label: 'Код',
      align: 'right',
      width: 200,
    },
  ]}
  operations={[
    {
      id: 'edit',
      icon: 'edit',
      title: 'Редактировать',
    },
    {
      id: 'copy',
      icon: 'document-copy',
      title: 'Копировать',
    },
    {
      id: 'remove',
      icon: 'remove',
      title: 'Удалить',
    },
  ]}
  onItemClick={({ itemId }) => {console.log(itemId);}}
  onOperationClick={({ operationId, itemId }) => {console.log(operationId, itemId);}}
/>
```

```js
<RboDocTable
  items={[
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан",
      id: "1",
      isError: false,
      isWarning: false,
      nonResidentName: "Азербайджан",
    },
    {
      countryCode: "AZ / 031",
      countryISOCode: "AZ",
      countryName: "Республика Азербайджан Республика Азербайджан Республика Азербайджан Республика Азербайджан Республика Азербайджан Республика Азербайджан",
      id: "2",
      isError: false,
      isWarning: false,
      nonResidentName: "Азербайджан",
    },
  ]}
  columns={[
    {
      id: 'nonResidentName',
      label: 'Наименование нерезидента',
      align: 'left',
      width: 300,
    },
    {
      id: 'countryName',
      label: 'Страна',
      align: 'left',
      width: 500,
    },
    {
      id: 'countryCode',
      label: 'Код',
      align: 'right',
      width: 200,
    },
  ]}
  isMultiLine
/>
```

```js
<RboDocTable
  columns={[
    {
      id: 'nonResidentName',
      label: 'Наименование нерезидента',
      align: 'left',
      width: 150,
    },
    {
      id: 'countryName',
      label: 'Страна',
      align: 'left',
      width: 200,
    },
    {
      id: 'countryCode',
      label: 'Код',
      align: 'right',
      width: 100,
    },
  ]}
/>
```

```js
<RboDocTable
  locale={RboDocTable.REFS.LOCALES.EN}
  columns={[
    {
      id: 'nonResidentName',
      label: 'Non Resident Name',
      align: 'left',
      width: 150,
    },
    {
      id: 'countryName',
      label: 'Country Name',
      align: 'left',
      width: 200,
    },
    {
      id: 'countryCode',
      label: 'Country Code',
      align: 'right',
      width: 100,
    },
  ]}
/>
```

```js
<RboDocTable
  locale={RboDocTable.REFS.LOCALES.EN}
  columns={[
    {
      id: 'nonResidentName',
      label: 'Non Resident Name',
      align: 'left',
      width: 150,
    },
    {
      id: 'countryName',
      label: 'Country Name',
      align: 'left',
      width: 200,
    },
    {
      id: 'countryCode',
      label: 'Country Code',
      align: 'right',
      width: 100,
    },
  ]}
  emptyLabel={'Нет документов'}
/>
```
