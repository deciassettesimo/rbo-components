import { LOCALES, COLUMNS_ALIGN } from './_constants';

const REFS = {
  LOCALES,
  COLUMNS_ALIGN,
};

export default REFS;
