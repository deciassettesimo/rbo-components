import { GLOBAL } from '../_constants';

export const LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const LABELS = {
  ...GLOBAL.LABELS.RBO_DOC_TABLE,
};

export const COLUMNS_ALIGN = {
  LEFT: 'left',
  CENTER: 'center',
  RIGHT: 'right',
};
