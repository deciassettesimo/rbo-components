import React from 'react';
import PropTypes from 'prop-types';

import RboDocTableItem from './RboDocTableItem';
import { StyledRboDocTableBody, StyledRboDocTableEmpty } from '../_style';
import { LOCALES, COLUMNS_ALIGN, LABELS } from '../_constants';

const RboDocTableBody = props => {
  const getEmptyLabel = locale => {
    switch (locale) {
      case LOCALES.EN:
        return LABELS.EN.EMPTY;
      case LOCALES.RU:
      default:
        return LABELS.RU.EMPTY;
    }
  };

  return (
    <StyledRboDocTableBody>
      {!props.items.length && (
        <StyledRboDocTableEmpty>
          {!props.emptyLabel && getEmptyLabel(props.locale)}
          {props.emptyLabel}
        </StyledRboDocTableEmpty>
      )}
      {props.items.map(item => (
        <RboDocTableItem
          key={item.id}
          id={item.id}
          columns={props.columns}
          data={item}
          cellRenderer={props.cellRenderer}
          operations={props.operations}
          isWarning={item.isWarning}
          isError={item.isError}
          isMultiLine={props.isMultiLine}
          onClick={props.onItemClick}
          onOperationClick={props.onOperationClick}
          parentNodeWidth={props.parentNodeWidth}
          parentNodeScrollLeft={props.parentNodeScrollLeft}
        />
      ))}
    </StyledRboDocTableBody>
  );
};

RboDocTableBody.propTypes = {
  locale: PropTypes.oneOf(Object.values(LOCALES)).isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      isWarning: PropTypes.bool,
      isError: PropTypes.bool,
    }),
  ).isRequired,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      width: PropTypes.number.isRequired,
      align: PropTypes.oneOf(Object.values(COLUMNS_ALIGN)),
      label: PropTypes.string,
    }),
  ).isRequired,
  operations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string,
      icon: PropTypes.string,
    }),
  ),
  isMultiLine: PropTypes.bool,
  emptyLabel: PropTypes.string,
  cellRenderer: PropTypes.func,
  onItemClick: PropTypes.func,
  onOperationClick: PropTypes.func,
  parentNodeWidth: PropTypes.number,
  parentNodeScrollLeft: PropTypes.number,
};

RboDocTableBody.defaultProps = {
  operations: [],
  isMultiLine: false,
  emptyLabel: null,
  cellRenderer: null,
  onItemClick: null,
  onOperationClick: null,
  parentNodeWidth: null,
  parentNodeScrollLeft: null,
};

export default RboDocTableBody;
