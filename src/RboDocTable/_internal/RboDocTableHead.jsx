import React from 'react';
import PropTypes from 'prop-types';

import { StyledRboDocTableHead, StyledRboDocTableRow, StyledRboDocTableCell } from '../_style';
import { COLUMNS_ALIGN } from '../_constants';

const RboDocTableHead = props => (
  <StyledRboDocTableHead>
    <StyledRboDocTableRow>
      {props.columns.map(column => (
        <StyledRboDocTableCell key={column.id} sWidth={column.width} sAlign={column.align}>
          {column.label}
        </StyledRboDocTableCell>
      ))}
    </StyledRboDocTableRow>
  </StyledRboDocTableHead>
);

RboDocTableHead.propTypes = {
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      width: PropTypes.number.isRequired,
      align: PropTypes.oneOf(Object.values(COLUMNS_ALIGN)),
      label: PropTypes.string,
    }),
  ).isRequired,
};

export default RboDocTableHead;
