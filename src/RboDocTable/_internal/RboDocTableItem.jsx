import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { COLUMNS_ALIGN } from '../_constants';
import { StyledRboDocTableItem, StyledRboDocTableRow, StyledRboDocTableCell, StyledRboDocTableValue } from '../_style';
import RboDocTableOperations from './RboDocTableOperations';

export default class RboDocTableItem extends PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        width: PropTypes.number.isRequired,
        align: PropTypes.oneOf(Object.values(COLUMNS_ALIGN)),
        label: PropTypes.string,
      }),
    ).isRequired,
    data: PropTypes.shape().isRequired,
    cellRenderer: PropTypes.func,
    operations: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        icon: PropTypes.string,
      }),
    ),
    isWarning: PropTypes.bool,
    isError: PropTypes.bool,
    isMultiLine: PropTypes.bool,
    onClick: PropTypes.func,
    onOperationClick: PropTypes.func,
    parentNodeWidth: PropTypes.number,
    parentNodeScrollLeft: PropTypes.number,
  };

  static defaultProps = {
    cellRenderer: null,
    operations: [],
    isWarning: false,
    isError: false,
    isMultiLine: false,
    onClick: null,
    onOperationClick: null,
    parentNodeWidth: null,
    parentNodeScrollLeft: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isClickable: !!this.props.onClick,
      isHovered: false,
    };

    this.node = React.createRef();
  }

  handleMouseEnter = () => {
    if (this.props.onClick || this.props.onOperationClick) this.setState({ isHovered: true });
  };

  handleMouseLeave = () => {
    this.setState({ isHovered: false });
  };

  handleClick = () => {
    if (this.props.onClick) this.props.onClick({ itemId: this.props.id });
  };

  handleOperationClick = operationId => {
    if (this.props.onOperationClick) this.props.onOperationClick({ operationId, itemId: this.props.id });
  };

  render() {
    return (
      <StyledRboDocTableItem
        innerRef={ref => {
          this.node.current = ref;
        }}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
        isClickable={this.state.isClickable}
        isHovered={this.state.isHovered}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        onClick={this.handleClick}
      >
        <StyledRboDocTableRow>
          {this.props.columns.map(column => (
            <StyledRboDocTableCell key={column.id} sWidth={column.width} sAlign={column.align}>
              <StyledRboDocTableValue isMultiLine={this.props.isMultiLine}>
                {this.props.cellRenderer && this.props.cellRenderer({ column: column.id, data: this.props.data })}
                {!this.props.cellRenderer && this.props.data[column.id]}
              </StyledRboDocTableValue>
            </StyledRboDocTableCell>
          ))}
        </StyledRboDocTableRow>
        {!!this.props.operations.length &&
          this.state.isHovered && (
            <RboDocTableOperations
              parentNodeWidth={this.props.parentNodeWidth}
              parentNodeScrollLeft={this.props.parentNodeScrollLeft}
              items={this.props.operations}
              onItemClick={this.handleOperationClick}
            />
          )}
      </StyledRboDocTableItem>
    );
  }
}
