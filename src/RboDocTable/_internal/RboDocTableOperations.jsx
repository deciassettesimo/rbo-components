import React from 'react';
import PropTypes from 'prop-types';

import { StyledRboDocTableOperations, StyledRboDocTableOperationsInner } from '../_style';
import RboDocTableOperationsItem from './RboDocTableOperationsItem';

const RboDocTableOperations = props => (
  <StyledRboDocTableOperations
    sParentNodeWidth={props.parentNodeWidth}
    sParentNodeScrollLeft={props.parentNodeScrollLeft}
  >
    <StyledRboDocTableOperationsInner>
      {props.items.map(item => (
        <RboDocTableOperationsItem
          key={item.id}
          id={item.id}
          title={item.title}
          icon={item.icon}
          onClick={props.onItemClick}
        />
      ))}
    </StyledRboDocTableOperationsInner>
  </StyledRboDocTableOperations>
);

RboDocTableOperations.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string,
      icon: PropTypes.string,
    }),
  ),
  onItemClick: PropTypes.func,
  parentNodeWidth: PropTypes.number,
  parentNodeScrollLeft: PropTypes.number,
};

RboDocTableOperations.defaultProps = {
  items: [],
  onItemClick: null,
  parentNodeWidth: null,
  parentNodeScrollLeft: null,
};

export default RboDocTableOperations;
