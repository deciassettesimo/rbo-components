import React from 'react';
import PropTypes from 'prop-types';

import ClickableIcon from '../../ClickableIcon';
import { StyledRboDocTableOperationsItem } from '../_style';

const RboDocTableOperationsItem = props => {
  const handleClick = e => {
    e.stopPropagation();
    props.onClick(props.id);
  };

  return (
    <StyledRboDocTableOperationsItem>
      <ClickableIcon
        type={props.icon}
        dimension={ClickableIcon.REFS.DIMENSIONS.XS}
        title={props.title}
        onClick={handleClick}
      />
    </StyledRboDocTableOperationsItem>
  );
};

RboDocTableOperationsItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string,
  icon: PropTypes.oneOf(Object.values(ClickableIcon.REFS.TYPES)),
  onClick: PropTypes.func.isRequired,
};

RboDocTableOperationsItem.defaultProps = {
  title: null,
  icon: null,
};

export default RboDocTableOperationsItem;
