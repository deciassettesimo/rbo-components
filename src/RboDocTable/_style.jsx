import styled from 'styled-components';

import StyledClickableIcon from '../ClickableIcon/_style';
import { STYLES } from '../_constants';

const rboDocTableItemBackgroundColor = (isWarning, isError, isHovered) => {
  if (isHovered) return STYLES.COLORS.ICE_BLUE;
  if (isError) return STYLES.COLORS.TOMATO_RED_12;
  if (isWarning) return STYLES.COLORS.MARIGOLD_12;
  return STYLES.COLORS.TRANSPARENT;
};

const styledRboDocTableOperationsLeft = (sParentNodeWidth, sParentNodeScrollLeft) => {
  if (sParentNodeWidth === null || sParentNodeScrollLeft === null) return {};
  const left = sParentNodeWidth + sParentNodeScrollLeft;
  return { left };
};

export const StyledRboDocTable = styled.div`
  position: relative;
  box-sizing: border-box;
  overflow-x: auto;
  font-size: 12px;
  line-height: 16px;
`;

export const StyledRboDocTableInner = styled.div`
  position: relative;
  box-sizing: border-box;
  white-space: nowrap;
  display: inline-block;
  min-width: 100%;
`;

export const StyledRboDocTableRow = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  white-space: nowrap;
  padding: 8px 0;
`;

export const StyledRboDocTableCell = styled.div.attrs({
  style: ({ sWidth }) => ({
    width: sWidth,
  }),
})`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: top;
  padding: 0 8px;
  text-align: ${props => props.sAlign};
  white-space: normal;
`;

export const StyledRboDocTableHead = styled.div`
  position: relative;
  box-sizing: border-box;
  padding: 4px 32px;
  font-weight: bold;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_20};

  ${StyledRboDocTableCell} {
    vertical-align: bottom;
  }
`;

export const StyledRboDocTableBody = styled.div`
  position: relative;
  box-sizing: border-box;
`;

export const StyledRboDocTableEmpty = styled.div`
  position: relative;
  box-sizing: border-box;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_08};
  padding: 16px 40px;
  font-style: italic;
`;

export const StyledRboDocTableItem = styled.div`
  position: relative;
  box-sizing: border-box;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_08};
  padding: 4px 32px;
  background-color: ${props => rboDocTableItemBackgroundColor(props.isWarning, props.isError, false)};
  transition: background-color 0.32s ease-out;
  cursor: ${props => (props.isClickable ? 'pointer' : 'default')};

  &:hover {
    background-color: ${props => rboDocTableItemBackgroundColor(props.isWarning, props.isError, props.isHovered)};
    outline: none;
  }
`;

export const StyledRboDocTableValue = styled.div`
  position: relative;
  box-sizing: border-box;
  width: 100%;
  white-space: ${props => (props.isMultiLine ? 'normal' : 'nowrap')};
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const StyledRboDocTableOperations = styled.div.attrs({
  style: ({ sParentNodeWidth, sParentNodeScrollLeft }) => ({
    ...styledRboDocTableOperationsLeft(sParentNodeWidth, sParentNodeScrollLeft),
  }),
})`
  position: absolute;
  box-sizing: border-box;
  top: 0;
  bottom: 0;
`;

export const StyledRboDocTableOperationsInner = styled.div`
  position: absolute;
  box-sizing: border-box;
  top: 0;
  right: 0;
  height: 100%;
  padding: 8px 40px 8px 32px;
  background: linear-gradient(to right, ${STYLES.COLORS.TRANSPARENT} 0, ${STYLES.COLORS.ICE_BLUE} 24px);
  white-space: nowrap;
`;

export const StyledRboDocTableOperationsItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  height: 24px;
  width: 24px;
  background: ${STYLES.COLORS.WHITE};
  border-radius: 50%;
  overflow: hidden;
  margin: 0 4px;

  :first-child {
    margin-left: 0;
  }

  :last-child {
    margin-right: 0;
  }

  ${StyledClickableIcon} {
    padding: 4px;
    height: 24px;
    width: 24px;
  }
`;
