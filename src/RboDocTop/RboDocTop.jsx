import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import ClickableIcon from '../ClickableIcon';
import { COMPONENT } from './_constants';
import { StyledRboDocTop, StyledRboDocTopClose, StyledRboDocTopContent } from './_style';

const RboDocTop = props => (
  <StyledRboDocTop {...addDataAttributes({ component: COMPONENT })}>
    <StyledRboDocTopContent>{props.children}</StyledRboDocTopContent>
    <StyledRboDocTopClose>
      <ClickableIcon
        type={ClickableIcon.REFS.TYPES.CLOSE}
        dimension={ClickableIcon.REFS.DIMENSIONS.XS}
        isExpanded
        onClick={props.onClose}
      />
    </StyledRboDocTopClose>
  </StyledRboDocTop>
);

RboDocTop.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default RboDocTop;
