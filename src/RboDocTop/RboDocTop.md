```js
const historyItems = [
 {
   "id": "1",
   "date": "25.09.2015 11:58:28",
   "user": "<system>",
   "endState": "Принят в обработку",
   "isLastState": true,
 },
 {
   "id": "2",
   "date": "25.09.2015 11:58:26",
   "user": "ostmro_client",
   "endState": "Доставлен",
 },
 {
   "id": "3",
   "date": "25.09.2015 11:58:23",
   "user": "ostmro_client",
   "endState": "Подписан",
 },
 {
   "id": "4",
   "date": "25.09.2015 11:58:11",
   "user": "ostmro_client",
   "endState": "Создан",
 }
];
const historyColumns = [
    {
      id: 'date',
      label: 'Дата',
      align: 'left',
      width: 180,
    },
    {
      id: 'user',
      label: 'Логин',
      align: 'left',
      width: 140,
    },
    {
      id: 'endState',
      label: 'Статус',
      align: 'left',
      width: 320,
    },
];
const historyCellRenderer = ({ column, data }) => {
  if (column === 'endState') {
    const color = data.endState === 'Принят в обработку' ? 'green' : 'intherit';
    return (
      <span>
        <span style={{ color }}>{data.endState}</span>
        {data.isLastState && ' (текущий статус)'}
      </span>
    );
  }
  else return data[column];
};

<RboDocTop onClose={() => {console.log('close')}}>
  <RboDocTable items={historyItems} columns={historyColumns} cellRenderer={historyCellRenderer} isMultiLine />
</RboDocTop>
```

```js
const signItems = [
    {
      id:  "3c532ea1-ef03-4b4f-a88a-e8d5d36c39f5",
      signDateTime: "25.09.2015 11:58:23",
      signHash: "341067953",
      signType: "Единственная подпись",
      signValid: "Подпись неверна",
      userLogin: "ostmro_client",
      userName: "Масорова Олеся",
      userPosition: "Рулевой",
    }
];
const signColumns = [
    {
      id: 'signDateTime',
      label: 'Дата',
      align: 'left',
      width: 180,
    },
    {
      id: 'userLogin',
      label: 'Логин',
      align: 'left',
      width: 140,
    },
    {
      id: 'userName',
      label: 'Имя',
      align: 'left',
      width: 140,
    },
    {
      id: 'userPosition',
      label: 'Должность',
      align: 'left',
      width: 140,
    },
    {
      id: 'signType',
      label: 'Тип подписи',
      align: 'left',
      width: 140,
    },
    {
      id: 'signValid',
      label: 'Результат проверки',
      align: 'left',
      width: 140,
    },
    {
      id: 'signHash',
      label: 'ЭП документа',
      align: 'left',
      width: 140,
    },
];
const signCellRenderer = ({ column, data }) => {
  if (column === 'signValid') {
    const color = data.signValid === 'Подпись неверна' ? 'red' : 'intherit';
    return (
      <span style={{ color }}>{data.signValid}</span>
    );
  }
  else return data[column];
};
const signOperations=[
    {
      id: 'download',
      icon: 'download',
      title: 'Скачать',
    },
    {
      id: 'print',
      icon: 'print',
      title: 'Печатать',
    },
];

<RboDocTop onClose={() => {console.log('close')}}>
  <RboDocTable
    items={signItems}
    columns={signColumns}
    cellRenderer={signCellRenderer}
    isMultiLine
    operations={signOperations}
    onOperationClick={({ operationId, itemId }) => {console.log(operationId, itemId);}}
  />
</RboDocTop>
```
