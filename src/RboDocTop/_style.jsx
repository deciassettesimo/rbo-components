import styled from 'styled-components';

import { STYLES } from '../_constants';

export const StyledRboDocTop = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  min-height: 48px;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_20};
  flex-shrink: 0;
`;

export const StyledRboDocTopClose = styled.div`
  position: absolute;
  top: 0;
  right: 4px;
`;

export const StyledRboDocTopContent = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding-top: 16px;
  overflow: auto;
`;
