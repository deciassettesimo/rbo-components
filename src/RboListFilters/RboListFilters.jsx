import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import Popup, { Box, Opener } from '../Popup';
import Card from '../Card';
import Button from '../Button';
import InputSelect from '../Input/InputSelect';
import RboPopupBoxOption from '../RboPopupBoxOption';
import IconFilter from '../Icon/IconFilter';
import IconSave from '../Icon/IconSave';
import IconClose from '../Icon/IconClose';
import StylesLink from '../Styles/Link';
import { COMPONENTS, FORM_IDS } from './_constants';
import REFS from './_config';
import {
  StyledRboListFilters,
  StyledRboListFiltersInner,
  StyledRboListFiltersField,
  StyledRboListFiltersText,
} from './_style';
import { getLabel, getSelectOptions } from './_utils';
import RboListFiltersSaveAsForm from './_internal/RboListFiltersSaveAsForm';

export default class RboListFilters extends PureComponent {
  static REFS = { ...REFS };

  static propTypes = {
    /** language for text */
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        title: PropTypes.string,
        isPreset: PropTypes.bool,
        isNew: PropTypes.bool,
        isChanged: PropTypes.bool,
        isSelected: PropTypes.bool,
      }),
    ),
    onRemove: PropTypes.func.isRequired,
    onChoose: PropTypes.func.isRequired,
    onCreate: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onSaveAs: PropTypes.func.isRequired,
    onReset: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: REFS.LOCALES.RU,
    items: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      isSaveAsPopupOpen: false,
    };
  }

  handleChoose = ({ value }) => {
    this.props.onChoose(value);
  };

  handleCreate = () => {
    this.props.onCreate();
  };

  handleSave = () => {
    this.props.onSave();
  };

  handleSaveAs = value => {
    this.props.onSaveAs(value);
    this.handleSaveAsPopupClose();
  };

  handleSaveAsPopupOpen = () => {
    this.setState({ isSaveAsPopupOpen: true });
  };

  handleSaveAsPopupClose = () => {
    this.setState({ isSaveAsPopupOpen: false });
  };

  handleReset = () => {
    this.props.onReset();
  };

  renderSelectOption = ({ option }) => (
    <RboPopupBoxOption
      value={option.value}
      label={option.title}
      onRemoveClick={!option.isPreset ? this.props.onRemove : null}
    />
  );

  render() {
    const selected = this.props.items.find(item => item.isSelected);

    return (
      <StyledRboListFilters {...addDataAttributes({ component: COMPONENTS.GENERAL })}>
        <StyledRboListFiltersInner>
          {!selected && (
            <Fragment>
              <StyledRboListFiltersField>
                <Button
                  id={FORM_IDS.CREATE_BUTTON}
                  type={Button.REFS.TYPES.BASE_SECONDARY}
                  dimension={Button.REFS.DIMENSIONS.S}
                  isWithIcon
                  onClick={this.handleCreate}
                >
                  <IconFilter dimension={IconFilter.REFS.DIMENSIONS.XS} />
                  <span>{getLabel('TITLE', this.props.locale)}</span>
                </Button>
              </StyledRboListFiltersField>
              {!!this.props.items.length && (
                <Fragment>
                  <StyledRboListFiltersField>
                    <InputSelect
                      id={FORM_IDS.CHOOSE_SELECT}
                      dimension={InputSelect.REFS.DIMENSIONS.S}
                      placeholder={getLabel('SELECT_PLACEHOLDER', this.props.locale)}
                      options={getSelectOptions(this.props.items)}
                      optionRenderer={this.renderSelectOption}
                      onChange={this.handleChoose}
                      width={200}
                      maxLength={120}
                    />
                  </StyledRboListFiltersField>
                </Fragment>
              )}
            </Fragment>
          )}
          {!!selected && (
            <Fragment>
              <StyledRboListFiltersText>
                {getLabel('TITLE', this.props.locale)}
                {selected.title && ` «${selected.title}»`}
              </StyledRboListFiltersText>
              {selected.isChanged && (
                <Fragment>
                  {!selected.isPreset &&
                    !selected.isNew && (
                      <StyledRboListFiltersText>
                        <StylesLink isPseudo isWithIcon onClick={this.handleSave}>
                          <IconSave dimension={IconSave.REFS.DIMENSIONS.XXS} />
                          <span>{getLabel('SAVE', this.props.locale)}</span>
                        </StylesLink>
                      </StyledRboListFiltersText>
                    )}
                  {(selected.isPreset || selected.isNew) && (
                    <StyledRboListFiltersText>
                      <Popup
                        isOpened={this.state.isSaveAsPopupOpen}
                        onOpen={this.handleSaveAsPopupOpen}
                        onClose={this.handleSaveAsPopupClose}
                      >
                        <Opener display={Opener.REFS.DISPLAY.INLINE_BLOCK}>
                          <StylesLink isPseudo isWithIcon>
                            <IconSave dimension={IconSave.REFS.DIMENSIONS.XXS} />
                            <span>{getLabel('SAVE_AS', this.props.locale)}</span>
                          </StylesLink>
                        </Opener>
                        <Box>
                          <Card isShadowed isBordered={false}>
                            <RboListFiltersSaveAsForm locale={this.props.locale} onSubmit={this.handleSaveAs} />
                          </Card>
                        </Box>
                      </Popup>
                    </StyledRboListFiltersText>
                  )}
                </Fragment>
              )}
              <StyledRboListFiltersText>
                <StylesLink isPseudo isWithIcon onClick={this.handleReset}>
                  <span>{getLabel('RESET', this.props.locale)}</span>
                  <IconClose dimension={IconClose.REFS.DIMENSIONS.XXS} />
                </StylesLink>
              </StyledRboListFiltersText>
            </Fragment>
          )}
        </StyledRboListFiltersInner>
      </StyledRboListFilters>
    );
  }
}
