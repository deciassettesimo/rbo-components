import { GLOBAL } from '../_constants';

export const COMPONENTS = {
  GENERAL: 'RboListFilters',
  SAVE_AS_FORM: 'RboListFiltersSaveAsForm',
};

export const FORM_IDS = {
  CHOOSE_SELECT: 'rboListFiltersChooseSelect',
  CREATE_BUTTON: 'rboListFiltersCreateButton',
  SAVE_AS_INPUT: 'rboListFiltersSaveAsFormInput',
  SAVE_AS_BUTTON: 'rboListFiltersSaveAsFormButton',
};

export const LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const LABELS = {
  ...GLOBAL.LABELS.RBO_LIST_FILTERS,
};
