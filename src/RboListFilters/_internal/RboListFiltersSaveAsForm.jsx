import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Form, { Row, Cell, Field } from '../../Form';
import InputText from '../../Input/InputText';
import Button from '../../Button';

import { COMPONENTS, LOCALES, FORM_IDS } from '../_constants';
import { StyledRboListFiltersSaveAsForm } from '../_style';
import { getLabel } from '../_utils';

export default class RboListFiltersSaveAsForm extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    onSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: null,
      isValid: false,
    };
  }

  handleChange = ({ value }) => {
    this.setState({ isValid: !!value, value });
  };

  handleSubmit = () => {
    const { onSubmit } = this.props;
    const { value } = this.state;
    onSubmit(value);
  };

  render() {
    return (
      <StyledRboListFiltersSaveAsForm {...addDataAttributes({ component: COMPONENTS.SAVE_AS_FORM })}>
        <Form dimension={Form.REFS.DIMENSIONS.S} autoFocus={FORM_IDS.SAVE_AS_INPUT} onSubmit={this.handleSubmit}>
          <Row>
            <Cell width="auto">
              <Field width={206}>
                <InputText
                  id={FORM_IDS.SAVE_AS_INPUT}
                  placeholder={getLabel('SAVE_AS_FORM_INPUT_PLACEHOLDER', this.props.locale)}
                  onChange={this.handleChange}
                  value={this.state.value}
                  maxLength={200}
                />
              </Field>
            </Cell>
            <Cell width="auto">
              <Field>
                <Button
                  id={FORM_IDS.SAVE_AS_BUTTON}
                  type={Button.REFS.TYPES.BASE_PRIMARY}
                  onClick={this.handleSubmit}
                  disabled={!this.state.isValid}
                >
                  {getLabel('SAVE_AS_FORM_BUTTON', this.props.locale)}
                </Button>
              </Field>
            </Cell>
          </Row>
        </Form>
      </StyledRboListFiltersSaveAsForm>
    );
  }
}
