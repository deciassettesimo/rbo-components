import styled from 'styled-components';

export const StyledRboListFilters = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px 0;
`;

export const StyledRboListFiltersInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  margin: 0 -6px;
`;

export const StyledRboListFiltersField = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  height: 32px;
  padding: 0 6px;
`;

export const StyledRboListFiltersText = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  height: 32px;
  padding: 8px 6px;
  font-size: 14px;
  line-height: 16px;
  max-width: 240px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const StyledRboListFiltersSaveAsForm = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  width: 360px;
  padding: 8px 16px;
`;
