import { LABELS, LOCALES } from './_constants';

export const getLabel = (key, locale) => {
  switch (locale) {
    case LOCALES.EN:
      return LABELS.EN[key];
    case LOCALES.RU:
    default:
      return LABELS.RU[key];
  }
};

export const getSelectOptions = items =>
  items.map(item => ({
    value: item.id,
    title: item.title,
    isPreset: item.isPreset,
  }));
