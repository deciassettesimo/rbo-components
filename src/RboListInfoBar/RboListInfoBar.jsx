import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENT } from './_constants';
import {
  StyledRboListInfoBar,
  StyledRboListInfoBarInner,
  StyledRboListInfoBarValue,
  StyledRboListInfoBarDescribe,
} from './_style';

const RboListInfoBar = props => (
  <StyledRboListInfoBar {...addDataAttributes({ component: COMPONENT })}>
    <StyledRboListInfoBarInner>
      <StyledRboListInfoBarValue>{props.value}</StyledRboListInfoBarValue>
      {props.describe && <StyledRboListInfoBarDescribe>{props.describe}</StyledRboListInfoBarDescribe>}
    </StyledRboListInfoBarInner>
  </StyledRboListInfoBar>
);

RboListInfoBar.propTypes = {
  value: PropTypes.string.isRequired,
  describe: PropTypes.string,
};

RboListInfoBar.defaultProps = {
  describe: null,
};

export default RboListInfoBar;
