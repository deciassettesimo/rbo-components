import styled from 'styled-components';

import { STYLES } from '../_constants';

export const StyledRboListInfoBar = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  font-size: 14px;
  line-height: 16px;
`;

export const StyledRboListInfoBarInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  border: 1px solid ${STYLES.COLORS.SUNFLOWER_YELLOW};
`;

export const StyledRboListInfoBarValue = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  vertical-align: middle;
  padding: 4px 8px;
  background: ${STYLES.COLORS.PARCHMENT};
  font-weight: bold;
  white-space: nowrap;
`;

export const StyledRboListInfoBarDescribe = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  vertical-align: middle;
  padding: 4px 8px;
  background: ${STYLES.COLORS.WHITE};
`;
