import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';
import RboListLayoutHeader from './_internal/RboListLayoutHeader';
import RboListLayoutToolbar from './_internal/RboListLayoutToolbar';
import RboListLayoutFilter from './_internal/RboListLayoutFilter';
import RboListLayoutContent from './_internal/RboListLayoutContent';
import RboListLayoutFooter from './_internal/RboListLayoutFooter';

import { StyledRboListLayout } from './_style';

const RboListLayout = props => (
  <StyledRboListLayout {...addDataAttributes({ component: COMPONENTS.GENERAL })}>{props.children}</StyledRboListLayout>
);

RboListLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

RboListLayout.Header = RboListLayoutHeader;
RboListLayout.Toolbar = RboListLayoutToolbar;
RboListLayout.Filter = RboListLayoutFilter;
RboListLayout.Content = RboListLayoutContent;
RboListLayout.Footer = RboListLayoutFooter;

export default RboListLayout;
