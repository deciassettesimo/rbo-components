import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { v4 } from 'uuid';

import { formatAccountValue, getStringFromMomentValue, formatNumberDecimalValue } from '../../Input/_utils';
import Group from '../../Styles/Group';
import Button from '../../Button';
import IconRefresh from '../../Icon/IconRefresh';
import Table from '../../Table';
import Tabs from '../../Tabs';
import Header from '../../Styles/Header';
import RboPopupBoxOption from '../../RboPopupBoxOption';
import RboListFilters from '../../RboListFilters';
import Filter from '../../Filter';
import OperationsPanel from '../../OperationsPanel';
import Pagination from '../../Pagination';
import RboListInfoBar from '../../RboListInfoBar';
import RboListLayout from '../RboListLayout';

const StyledRboDocLayoutExampleContainer = styled.div`
  height: 560px;
  border: 1px solid gray;
`;

const PAGES = {
  selected: 3,
  visibleQty: 1,
  totalQty: 3,
  settings: { options: [30, 60, 120, 240], value: 30 },
};

const TABS = [
  {
    id: 'working',
    title: 'Рабочие',
    isActive: true,
  },
  {
    id: 'archive',
    title: 'Архив',
  },
  {
    id: 'trash',
    title: 'Удаленные',
  },
];

const COLUMNS = [
  {
    id: 'docNumber',
    label: 'Номер',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'docDate',
    label: 'Дата',
    sorting: -1,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'amount',
    label: 'Сумма',
    sorting: 0,
    width: 100,
    align: Table.REFS.COLUMNS_ALIGN.RIGHT,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'status',
    label: 'Статус',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'bankMessage',
    label: 'Сообщение из банка',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
    isSortingDisabled: true,
  },
  {
    id: 'lastChangeStateDate',
    label: 'Изменено',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'payerName',
    label: 'Плательщик',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'payerAccount',
    label: 'Счет плательщика',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'payerBankBic',
    label: 'БИК банка плательщика',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'receiverName',
    label: 'Получатель',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'receiverAccount',
    label: 'Счет получателя',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'receiverInn',
    label: 'ИНН получателя',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'paymentPurpose',
    label: 'Назначение платежа',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: false,
  },
  {
    id: 'docNote',
    label: 'Заметки',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: false,
  },
];

const ITEMS = [
  {
    id: '1',
    docNumber: '1',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    isWarning: true,
    operations: { edit: true, copy: true },
  },
  {
    id: '2',
    docNumber: '2',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    isError: true,
    operations: { edit: true, remove: true },
  },
  {
    id: '3',
    docNumber: '3',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    operations: { copy: true },
    isSelectDisabled: true,
  },
  {
    id: '4',
    docNumber: '4',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    operations: { edit: true, copy: true, remove: true },
    isSelectDisabled: true,
  },
  {
    id: '5',
    docNumber: '5',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    isSelectDisabled: true,
  },
  {
    id: '6',
    docNumber: '6',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    isSelectDisabled: true,
  },
  {
    id: '7',
    docNumber: '7',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
  },
  {
    id: '8',
    docNumber: '8',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
  },
  {
    id: '9',
    docNumber: '9',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
  },
];

const OPERATIONS = [
  {
    id: 'edit',
    icon: 'edit',
    title: 'Редактировать',
  },
  {
    id: 'copy',
    icon: 'document-copy',
    title: 'Копировать',
  },
  {
    id: 'remove',
    icon: 'remove',
    title: 'Remove',
  },
  {
    id: 'repeat',
    icon: 'repeat',
    title: 'Repeat',
  },
  {
    id: 'document-sign-and-send',
    icon: 'document-sign-and-send',
    title: 'Sign and Send',
  },
  {
    id: 'document-sign',
    icon: 'document-sign',
    title: 'Sign',
  },
  {
    id: 'document-send',
    icon: 'document-send',
    title: 'Send',
  },
  {
    id: 'archive',
    icon: 'archive',
    title: 'Archive',
  },
  {
    id: 'document-save-as-template',
    icon: 'document-save-as-template',
    title: 'Save as template',
  },
];

const CONDITIONS = [
  {
    id: 'string',
    title: 'Строковый фильтр',
    type: Filter.REFS.CONDITIONS_TYPES.STRING,
    maxLength: 100,
    inputWidth: 300,
  },
  {
    id: 'account',
    title: 'Строковый фильтр (счет)',
    type: Filter.REFS.CONDITIONS_TYPES.STRING,
    inputType: Filter.REFS.CONDITIONS_INPUT_TYPES.ACCOUNT,
  },
  {
    id: 'select',
    title: 'Выбор (радиогрупп)',
    type: Filter.REFS.CONDITIONS_TYPES.SELECT,
    options: [
      { value: 'id1', title: 'Радио 1' },
      { value: 'id2', title: 'Радио 2' },
      { value: 'id3', title: 'Радио 3' },
    ],
  },
  {
    id: 'multiSelect',
    title: 'Выбор (чекбокс групп)',
    type: Filter.REFS.CONDITIONS_TYPES.MULTI_SELECT,
    options: [
      { value: 'id1', title: 'Чекбокс 1' },
      { value: 'id2', title: 'Чекбокс 2' },
      { value: 'id3', title: 'Чекбокс 3' },
    ],
  },
  {
    id: 'date',
    title: 'Дата',
    type: Filter.REFS.CONDITIONS_TYPES.RANGE,
    rangeType: Filter.REFS.CONDITIONS_RANGE_TYPES.DATE,
  },
  {
    id: 'amount',
    title: 'Сумма',
    type: Filter.REFS.CONDITIONS_TYPES.RANGE,
    rangeType: Filter.REFS.CONDITIONS_RANGE_TYPES.AMOUNT,
    isNegative: true,
  },
  {
    id: 'integer',
    title: 'Номер',
    type: Filter.REFS.CONDITIONS_TYPES.RANGE,
    rangeType: Filter.REFS.CONDITIONS_RANGE_TYPES.INTEGER,
    maxLength: 6,
  },
  {
    id: 'search',
    title: 'Поиск',
    type: Filter.REFS.CONDITIONS_TYPES.SEARCH,
    inputType: Filter.REFS.CONDITIONS_INPUT_TYPES.TEXT,
    options: [
      { value: 'id1', title: 'Lorem ipsum dolor sit amet' },
      { value: 'id2', title: 'consectetur adipiscing elit' },
      { value: 'id3', title: 'Integer eu lectus venenatis purus lobortis' },
    ],
  },
  {
    id: 'searchAccount',
    title: 'Поиск (счет)',
    type: Filter.REFS.CONDITIONS_TYPES.SEARCH,
    inputType: Filter.REFS.CONDITIONS_INPUT_TYPES.ACCOUNT,
    options: [
      { value: 'id1', title: '11111111111111111111' },
      { value: 'id2', title: '22222222222222222222' },
      { value: 'id3', title: '33333333333333333333' },
    ],
  },
  {
    id: Filter.REFS.CONDITIONS_TYPES.SUGGEST,
    title: 'Строка с подсказкой',
    type: 'suggest',
    inputType: Filter.REFS.CONDITIONS_INPUT_TYPES.TEXT,
    options: [
      { value: 'id1', title: 'Lorem ipsum dolor sit amet' },
      { value: 'id2', title: 'consectetur adipiscing elit' },
      { value: 'id3', title: 'Integer eu lectus venenatis purus lobortis' },
    ],
  },
];

const FILTERS = [
  {
    id: 'today',
    title: 'За сегодня',
    isPreset: true,
    params: [
      {
        id: 'string',
        value: 'Строка',
      },
      {
        id: 'date',
        value: {
          from: '2017-01-01T00:00:00.000+03:00',
          to: '2018-12-31T00:00:00.000+03:00',
        },
      },
    ],
  },
  {
    id: 'example1',
    title: 'Example 1',
    isPreset: false,
    params: [
      {
        id: 'date',
        value: {
          from: '2017-01-01T00:00:00.000+03:00',
          to: '2018-12-31T00:00:00.000+03:00',
        },
      },
    ],
  },
];

const getParamLabel = (value, id) => {
  let option;
  switch (id) {
    case 'search':
    case 'select':
      if (!value) return null;
      option = CONDITIONS.find(condition => condition.id === id).options.find(item => item.value === value);
      return option ? option.title : null;

    case 'searchAccount':
      if (!value) return null;
      option = CONDITIONS.find(condition => condition.id === id).options.find(item => item.value === value);
      return option ? formatAccountValue(option.title) : null;

    case 'multiSelect':
      if (!value) return null;
      option = CONDITIONS.find(condition => condition.id === id).options.filter(item =>
        (value || []).includes(item.value),
      );
      return option.length ? option.map(item => item.title).join(', ') : null;

    case 'account':
      if (!value) return null;
      return formatAccountValue(value);

    case 'date':
      if (!value) return null;
      return `${value.from ? `с ${getStringFromMomentValue(value.from)}` : ''} ${
        value.to ? `по ${getStringFromMomentValue(value.to)}` : ''
      }`;

    case 'integer':
      if (!value) return null;
      return `${value.from || value.from === 0 ? `с ${value.from}` : ''} ${
        value.to || value.to === 0 ? `по ${value.to}` : ''
      }`;

    case 'amount':
      if (!value) return null;
      return `${
        value.from || value.from === 0 ? `от ${formatNumberDecimalValue(value.from, true, true, 14, 2, true)}` : ''
      } ${value.to || value.to === 0 ? `до ${formatNumberDecimalValue(value.to, true, true, 14, 2, true)}` : ''}`;

    case 'string':
    case 'suggest':
    default:
      return value;
  }
};

const getOptions = (id, value) => {
  const { options } = CONDITIONS.find(item => item.id === id);
  const searchRegExp = new RegExp(value, 'ig');
  return options.filter(item => item.title.search(searchRegExp) >= 0);
};

export default class RboListLayoutExample extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      locale: 'ru',
      columns: COLUMNS.map(column => column),
      items: ITEMS.map(item => item),
      conditions: CONDITIONS.map(item => item),
      filters: FILTERS.map(item => item),
      filter: null,
    };
  }

  onItemClick = param => {
    console.log(param); /* eslint-disable-line */
  };

  onOperationClick = param => {
    console.log(param); /* eslint-disable-line */
  };

  onSortingChange = param => {
    const { sorting } = param;
    const { columns } = this.state;
    this.setState({
      columns: columns.map(
        column => (column.id === sorting.id ? { ...column, sorting: sorting.direction } : { ...column, sorting: 0 }),
      ),
    });
  };

  onResizeChange = param => {
    const { widths } = param;
    const { columns } = this.state;
    this.setState({ columns: columns.map(column => ({ ...column, width: widths[column.id] })) });
  };

  onSelectChange = param => {
    const { selected } = param;
    const { items } = this.state;
    this.setState({ items: items.map(item => ({ ...item, isSelected: selected.includes(item.id) })) });
  };

  filtersOptionRenderer = ({ id, option }) => {
    switch (id) {
      case 'search':
      case 'suggest':
        return (
          <RboPopupBoxOption
            label={option.title}
            highlight={{
              value: option.searchValue,
              inLabel: true,
            }}
          />
        );
      case 'searchAccount':
        return (
          <RboPopupBoxOption
            label={formatAccountValue(option.title)}
            highlight={{
              value: option.searchValue,
              inLabel: true,
            }}
          />
        );
      default:
        return <RboPopupBoxOption isInline label={option.title} />;
    }
  };

  handleRemove = id => {
    this.setState({ filters: this.state.filters.filter(item => item.id !== id) });
  };

  handleChoose = id => {
    const filters = this.state.filters.map(item => ({ ...item, isSelected: item.id === id }));
    const filter = filters.find(item => item.isSelected);

    this.setState({
      filters,
      filter: {
        ...filter,
        params: filter.params.map(param => ({ ...param, label: getParamLabel(param.value, param.id) })),
      },
    });
  };

  handleReset = () => {
    const filters = this.state.filters
      .filter(item => !item.isNew)
      .map(item => ({ ...item, isSelected: false, isChanged: false }));

    this.setState({ filters, filter: null });
  };

  handleCreate = () => {
    const filter = { params: [], isPreset: false, isNew: true, isSelected: true, isChanged: false };
    const filters = this.state.filters.map(item => item);
    filters.push(filter);
    this.setState({ filters, filter });
  };

  handleSave = () => {
    const filter = { ...this.state.filter, isChanged: false };
    const filters = this.state.filters.map(item => (item.isSelected ? { ...item, ...filter } : item));
    this.setState({ filters, filter });
  };

  handleSaveAs = title => {
    const filter = {
      ...this.state.filter,
      id: v4(),
      title,
      isNew: false,
      isPreset: false,
      isSelected: true,
      isChanged: false,
    };
    const filters = this.state.filters.filter(item => !item.isNew).map(item => ({ ...item, isSelected: false }));
    filters.push(filter);
    this.setState({ filters, filter });
  };

  handleChange = ({ params, isChanged }) => {
    if (params) {
      const filters = isChanged
        ? this.state.filters.map(item => (item.isSelected ? { ...item, isChanged } : item))
        : this.state.filters;
      const filter = {
        ...this.state.filter,
        params: params.map(param => ({ ...param, label: getParamLabel(param.value, param.id) })),
      };
      this.setState({ filters, filter });
    } else {
      this.handleReset();
    }
  };

  handleSearch = ({ id, value }) => {
    const conditions = this.state.conditions.map(
      condition => (condition.id === id ? { ...condition, options: getOptions(id, value) } : condition),
    );
    this.setState({ conditions });
  };

  handleSettingsChange = param => {
    const { visibility, grouped } = param;
    const { columns } = this.state;
    if (visibility) {
      this.setState({ columns: columns.map(column => ({ ...column, isVisible: visibility.includes(column.id) })) });
    }
    if (grouped) {
      this.setState({ columns: columns.map(column => ({ ...column, isGrouped: grouped === column.id })) });
    }
  };

  handleExportToExcelClick = param => {
    console.log(param); /* eslint-disable-line */
  };

  render() {
    const { columns, items } = this.state;
    const isSelected = !!items.filter(item => item.isSelected).length;

    return (
      <StyledRboDocLayoutExampleContainer>
        <RboListLayout>
          <RboListLayout.Header>
            <RboListLayout.Header.Left>
              <RboListLayout.Header.Title>
                <Header size={3}>Платежные поручения</Header>
              </RboListLayout.Header.Title>
              <RboListLayout.Header.Buttons>
                <Group>
                  <Button
                    id="refreshButton"
                    type={Button.REFS.TYPES.LINK_SECONDARY}
                    dimension={Button.REFS.DIMENSIONS.S}
                    isWithIcon
                    onClick={() => {}}
                  >
                    <IconRefresh dimension={IconRefresh.REFS.DIMENSIONS.XS} />
                  </Button>
                  <Button
                    id="new"
                    type={Button.REFS.TYPES.ACCENT_PRIMARY}
                    dimension={Button.REFS.DIMENSIONS.S}
                    onClick={() => {}}
                  >
                    Создать платеж
                  </Button>
                  <Button
                    id="import"
                    type={Button.REFS.TYPES.BASE_PRIMARY}
                    dimension={Button.REFS.DIMENSIONS.S}
                    onClick={() => {}}
                  >
                    Импорт
                  </Button>
                </Group>
              </RboListLayout.Header.Buttons>
            </RboListLayout.Header.Left>
            <RboListLayout.Header.Right>
              <Tabs
                dimension={Tabs.REFS.DIMENSIONS.L}
                align={Tabs.REFS.ALIGN.RIGHT}
                isEmphasis
                items={TABS}
                onItemClick={() => {}}
              />
            </RboListLayout.Header.Right>
          </RboListLayout.Header>
          <RboListLayout.Toolbar>
            <RboListLayout.Toolbar.Left>
              <RboListFilters
                locale={this.state.locale}
                items={this.state.filters}
                filter={this.state.filter}
                optionRenderer={this.optionRenderer}
                onRemove={this.handleRemove}
                onChoose={this.handleChoose}
                onCreate={this.handleCreate}
                onReset={this.handleReset}
                onSave={this.handleSave}
                onSaveAs={this.handleSaveAs}
              />
            </RboListLayout.Toolbar.Left>
            <RboListLayout.Toolbar.Right>
              <OperationsPanel
                locale={this.state.locale}
                dimension={OperationsPanel.REFS.DIMENSIONS.M}
                align={OperationsPanel.REFS.ALIGN.RIGHT}
                items={OPERATIONS}
                onItemClick={() => null}
              />
            </RboListLayout.Toolbar.Right>
          </RboListLayout.Toolbar>
          {this.state.filter && (
            <RboListLayout.Filter>
              <Filter
                locale={this.state.locale}
                conditions={this.state.conditions}
                params={this.state.filter.params}
                optionRenderer={this.filtersOptionRenderer}
                onSearch={this.handleSearch}
                onChange={this.handleChange}
              />
            </RboListLayout.Filter>
          )}
          <RboListLayout.Content>
            <Table
              dataAttributes={{ id: 'tableExample' }}
              locale={this.state.locale}
              dimension={Table.REFS.DIMENSIONS.S}
              columns={columns}
              items={items}
              onItemClick={this.onItemClick}
              headBgColor={Table.REFS.HEAD_BG_COLORS.GRAY}
              operations={OPERATIONS}
              isOperationsCompact
              onOperationClick={this.onOperationClick}
              isSortable
              onSortingChange={this.onSortingChange}
              isResizable
              onResizeChange={this.onResizeChange}
              isSelectable
              onSelectChange={this.onSelectChange}
              settings={{
                visibility: true,
                onVisibilityChange: this.handleSettingsChange,
                grouping: true,
                onGroupingChange: this.handleSettingsChange,
              }}
            />
          </RboListLayout.Content>
          <RboListLayout.Footer>
            <RboListLayout.Footer.Left>
              {!isSelected && (
                <Button
                  id="ExportToExcel"
                  dimension={Button.REFS.DIMENSIONS.S}
                  type={Button.REFS.TYPES.LINK}
                  onClick={this.handleExportToExcelClick}
                >
                  Выгрузить список в Excel
                </Button>
              )}
              {isSelected && <RboListInfoBar value="Выбрано: 4" describe="Итого: 540 002 017.51 RUB" />}
            </RboListLayout.Footer.Left>
            <RboListLayout.Footer.Right>
              <Pagination
                locale={this.state.locale}
                dimension={Pagination.REFS.DIMENSIONS.M}
                {...PAGES}
                onItemClick={() => {}}
                onSettingsChange={() => {}}
              />
            </RboListLayout.Footer.Right>
          </RboListLayout.Footer>
        </RboListLayout>
      </StyledRboDocLayoutExampleContainer>
    );
  }
}
