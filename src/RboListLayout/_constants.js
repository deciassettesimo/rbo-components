export const COMPONENTS = {
  GENERAL: 'RboListLayout',
  HEADER: 'RboListLayoutHeader',
  TOOLBAR: 'RboListLayoutToolbar',
  FILTER: 'RboListLayoutFilter',
  CONTENT: 'RboListLayoutContent',
  FOOTER: 'RboListLayoutFooter',
};

export default COMPONENTS;
