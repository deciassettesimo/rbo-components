import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';

import { StyledRboListLayoutContent, StyledRboListLayoutContentInner } from '../_style';

const RboListLayoutContent = props => (
  <StyledRboListLayoutContent {...addDataAttributes({ component: COMPONENTS.CONTENT })}>
    <StyledRboListLayoutContentInner>{props.children}</StyledRboListLayoutContentInner>
  </StyledRboListLayoutContent>
);

RboListLayoutContent.propTypes = {
  children: PropTypes.node.isRequired,
};

export default RboListLayoutContent;
