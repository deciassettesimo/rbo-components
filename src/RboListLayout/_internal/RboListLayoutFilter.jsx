import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';

import { StyledRboListLayoutFilter, StyledRboListLayoutFilterInner } from '../_style';

const RboListLayoutFilter = props => (
  <StyledRboListLayoutFilter {...addDataAttributes({ component: COMPONENTS.FILTER })}>
    <StyledRboListLayoutFilterInner>{props.children}</StyledRboListLayoutFilterInner>
  </StyledRboListLayoutFilter>
);

RboListLayoutFilter.propTypes = {
  children: PropTypes.node.isRequired,
};

export default RboListLayoutFilter;
