import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';

import {
  StyledRboListLayoutFooter,
  StyledRboListLayoutFooterInner,
  StyledRboListLayoutFooterLeft,
  StyledRboListLayoutFooterRight,
} from '../_style';

const RboListLayoutFooter = props => (
  <StyledRboListLayoutFooter {...addDataAttributes({ component: COMPONENTS.FOOTER })}>
    <StyledRboListLayoutFooterInner>{props.children}</StyledRboListLayoutFooterInner>
  </StyledRboListLayoutFooter>
);

RboListLayoutFooter.propTypes = {
  children: PropTypes.node.isRequired,
};

RboListLayoutFooter.Left = props => <StyledRboListLayoutFooterLeft>{props.children}</StyledRboListLayoutFooterLeft>;

RboListLayoutFooter.Left.propTypes = { children: PropTypes.node.isRequired };

RboListLayoutFooter.Right = props => <StyledRboListLayoutFooterRight>{props.children}</StyledRboListLayoutFooterRight>;

RboListLayoutFooter.Right.propTypes = { children: PropTypes.node.isRequired };

export default RboListLayoutFooter;
