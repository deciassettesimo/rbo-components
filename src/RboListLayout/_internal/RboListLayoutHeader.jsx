import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';

import {
  StyledRboListLayoutHeader,
  StyledRboListLayoutHeaderInner,
  StyledRboListLayoutHeaderLeft,
  StyledRboListLayoutHeaderRight,
  StyledRboListLayoutHeaderTitle,
  StyledRboListLayoutHeaderButtons,
} from '../_style';

const RboListLayoutHeader = props => (
  <StyledRboListLayoutHeader {...addDataAttributes({ component: COMPONENTS.HEADER })}>
    <StyledRboListLayoutHeaderInner>{props.children}</StyledRboListLayoutHeaderInner>
  </StyledRboListLayoutHeader>
);

RboListLayoutHeader.propTypes = {
  children: PropTypes.node.isRequired,
};

RboListLayoutHeader.Left = props => <StyledRboListLayoutHeaderLeft>{props.children}</StyledRboListLayoutHeaderLeft>;

RboListLayoutHeader.Left.propTypes = { children: PropTypes.node.isRequired };

RboListLayoutHeader.Right = props => <StyledRboListLayoutHeaderRight>{props.children}</StyledRboListLayoutHeaderRight>;

RboListLayoutHeader.Right.propTypes = { children: PropTypes.node.isRequired };

RboListLayoutHeader.Title = props => <StyledRboListLayoutHeaderTitle>{props.children}</StyledRboListLayoutHeaderTitle>;

RboListLayoutHeader.Title.propTypes = { children: PropTypes.node.isRequired };

RboListLayoutHeader.Buttons = props => (
  <StyledRboListLayoutHeaderButtons>{props.children}</StyledRboListLayoutHeaderButtons>
);

RboListLayoutHeader.Buttons.propTypes = { children: PropTypes.node.isRequired };

export default RboListLayoutHeader;
