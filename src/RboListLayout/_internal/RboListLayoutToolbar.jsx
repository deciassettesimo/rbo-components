import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS } from '../_constants';

import {
  StyledRboListLayoutToolbar,
  StyledRboListLayoutToolbarInner,
  StyledRboListLayoutToolbarWide,
  StyledRboListLayoutToolbarLeft,
  StyledRboListLayoutToolbarRight,
} from '../_style';

const RboListLayoutToolbar = props => (
  <StyledRboListLayoutToolbar {...addDataAttributes({ component: COMPONENTS.TOOLBAR })}>
    <StyledRboListLayoutToolbarInner>{props.children}</StyledRboListLayoutToolbarInner>
  </StyledRboListLayoutToolbar>
);

RboListLayoutToolbar.propTypes = {
  children: PropTypes.node.isRequired,
};

RboListLayoutToolbar.Wide = props => <StyledRboListLayoutToolbarWide>{props.children}</StyledRboListLayoutToolbarWide>;

RboListLayoutToolbar.Wide.propTypes = { children: PropTypes.node.isRequired };

RboListLayoutToolbar.Left = props => <StyledRboListLayoutToolbarLeft>{props.children}</StyledRboListLayoutToolbarLeft>;

RboListLayoutToolbar.Left.propTypes = { children: PropTypes.node.isRequired };

RboListLayoutToolbar.Right = props => (
  <StyledRboListLayoutToolbarRight>{props.children}</StyledRboListLayoutToolbarRight>
);

RboListLayoutToolbar.Right.propTypes = { children: PropTypes.node.isRequired };

export default RboListLayoutToolbar;
