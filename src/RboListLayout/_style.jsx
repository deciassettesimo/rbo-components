import styled from 'styled-components';

import { STYLES } from '../_constants';

export const StyledRboListLayout = styled.div`
  position: relative;
  box-sizing: border-box;
  height: 100%;
  flex-grow: 1;
  display: flex;
  flex-flow: column nowrap;
  background: ${STYLES.COLORS.WHITE};
`;

export const StyledRboListLayoutHeader = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-basis: auto;
  flex-grow: 0;
  flex-shrink: 0;
  display: flex;
  min-height: 57px;
  justify-content: stretch;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_12};
`;

export const StyledRboListLayoutHeaderInner = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 1;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  padding: 0 40px;
`;

export const StyledRboListLayoutHeaderLeft = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 0;
  flex-shrink: 1;
  display: flex;
  padding-right: 8px;
  align-items: center;
`;

export const StyledRboListLayoutHeaderRight = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 1;
  flex-shrink: 1;
  display: flex;
  padding-left: 8px;
  align-items: flex-end;
`;

export const StyledRboListLayoutHeaderTitle = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 1;
  flex-shrink: 1;
  display: block;
  padding: 8px 8px 8px 0;
`;

export const StyledRboListLayoutHeaderButtons = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 0;
  flex-shrink: 0;
  display: block;
  padding: 8px 0 8px 8px;
`;

export const StyledRboListLayoutToolbar = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-basis: auto;
  flex-grow: 0;
  flex-shrink: 0;
  display: flex;
  flex-flow: column nowrap;
  height: 49px;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_12};
`;

export const StyledRboListLayoutToolbarInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  padding: 0 40px;
`;

export const StyledRboListLayoutToolbarWide = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  flex-grow: 1;
  flex-shrink: 1;
`;

export const StyledRboListLayoutToolbarLeft = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding-right: 8px;
  flex-grow: 0;
  flex-shrink: 0;
`;

export const StyledRboListLayoutToolbarRight = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 1;
  flex-shrink: 1;
  display: block;
  padding-left: 8px;
`;

export const StyledRboListLayoutFilter = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-basis: auto;
  flex-grow: 0;
  flex-shrink: 0;
  display: flex;
  flex-flow: column nowrap;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_12};
`;

export const StyledRboListLayoutFilterInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 14px 40px;
`;

export const StyledRboListLayoutContent = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  flex-basis: auto;
  flex-grow: 1;
  flex-shrink: 1;
`;

export const StyledRboListLayoutContentInner = styled.div`
  position: absolute;
  box-sizing: border-box;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
`;

export const StyledRboListLayoutFooter = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-basis: auto;
  flex-grow: 0;
  flex-shrink: 0;
  display: flex;
  flex-flow: row nowrap;
  min-height: 48px;
  background: ${STYLES.COLORS.WHITE_THREE};
`;

export const StyledRboListLayoutFooterInner = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 1;
  display: flex;
  flex-flow: row nowrap;
  justify-content: stretch;
  align-items: center;
  padding: 8px 40px;
`;

export const StyledRboListLayoutFooterLeft = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  flex-grow: 0;
  flex-shrink: 1;
  padding-right: 8px;
`;

export const StyledRboListLayoutFooterRight = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 1;
  flex-shrink: 0;
  display: flex;
  justify-content: flex-end;
  padding-left: 8px;
`;
