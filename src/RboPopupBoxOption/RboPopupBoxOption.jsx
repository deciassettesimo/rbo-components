import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import Highlighter from 'react-highlight-words';

import { addDataAttributes } from '../_utils';

import ClickableIcon from '../ClickableIcon';
import { COMPONENT } from './_constants';
import {
  StyledRboPopupBoxOption,
  StyledRboPopupBoxOptionInner,
  StyledRboPopupBoxOptionBlock,
  StyledRboPopupBoxOptionMain,
  StyledRboPopupBoxOptionTitle,
  StyledRboPopupBoxOptionExtra,
  StyledRboPopupBoxOptionDescribe,
  StyledRboPopupBoxOptionOperations,
  StyledRboPopupBoxOptionOperationsItem,
  RboPopupBoxOptionHighlightStyle,
} from './_style';

export default class RboPopupBoxOption extends PureComponent {
  static propTypes = {
    /** Option value */
    value: PropTypes.string,
    title: PropTypes.string,
    label: PropTypes.string,
    extra: PropTypes.string,
    describe: PropTypes.string,
    isInline: PropTypes.bool,
    highlight: PropTypes.shape({
      value: PropTypes.string,
      inTitleValue: PropTypes.string,
      inLabelValue: PropTypes.string,
      inDescribeValue: PropTypes.string,
      inExtraValue: PropTypes.string,
      inTitle: PropTypes.bool,
      inLabel: PropTypes.bool,
      inDescribe: PropTypes.bool,
      inExtra: PropTypes.bool,
    }),
    /** Edit Operation Click handler
     * @param {string}
     * @param {string} value Option value (if specified)
     * */
    onEditClick: PropTypes.func,
    /** Remove Operation Click handler
     * @param {string}
     * @param {string} value Option value (if specified)
     * */
    onRemoveClick: PropTypes.func,
    handlePopupClose: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    title: null,
    label: null,
    extra: null,
    describe: null,
    isInline: false,
    highlight: {},
    onEditClick: null,
    onRemoveClick: null,
    handlePopupClose: () => null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isWithOperations: !!this.props.onEditClick || !!this.props.onRemoveClick,
      isHovered: false,
    };
  }

  handleMouseEnter = () => {
    this.setState({ isHovered: true });
  };

  handleMouseLeave = () => {
    this.setState({ isHovered: false });
  };

  handleEditClick = e => {
    e.stopPropagation();
    this.props.handlePopupClose();
    if (this.props.onEditClick) this.props.onEditClick(this.props.value);
  };

  handleRemoveClick = e => {
    e.stopPropagation();
    this.props.handlePopupClose();
    if (this.props.onRemoveClick) this.props.onRemoveClick(this.props.value);
  };

  titleRenderer = () => {
    if (!this.props.highlight.inTitle) {
      return <StyledRboPopupBoxOptionTitle>{this.props.title}</StyledRboPopupBoxOptionTitle>;
    }
    return (
      <StyledRboPopupBoxOptionTitle>
        <Highlighter
          searchWords={[this.props.highlight.inTitleValue, this.props.highlight.value]}
          textToHighlight={this.props.title}
          highlightStyle={RboPopupBoxOptionHighlightStyle}
          autoEscape
        />
      </StyledRboPopupBoxOptionTitle>
    );
  };

  labelRenderer = () => {
    if (!this.props.highlight.inLabel) return this.props.label;
    return (
      <Highlighter
        searchWords={[this.props.highlight.inLabelValue, this.props.highlight.value]}
        textToHighlight={this.props.label}
        highlightStyle={RboPopupBoxOptionHighlightStyle}
        autoEscape
      />
    );
  };

  extraRenderer = () => {
    if (!this.props.highlight.inExtra) return this.props.extra;
    return (
      <Highlighter
        searchWords={[this.props.highlight.inExtraValue, this.props.highlight.value]}
        textToHighlight={this.props.extra}
        highlightStyle={RboPopupBoxOptionHighlightStyle}
        autoEscape
      />
    );
  };

  describeRenderer = () => {
    if (!this.props.highlight.inDescribe) return this.props.describe;
    return (
      <Highlighter
        searchWords={[this.props.highlight.inDescribeValue, this.props.highlight.value]}
        textToHighlight={this.props.describe}
        highlightStyle={RboPopupBoxOptionHighlightStyle}
        autoEscape
      />
    );
  };

  render() {
    return (
      <StyledRboPopupBoxOption
        {...addDataAttributes({ component: COMPONENT })}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        isWithOperations={this.state.isWithOperations}
      >
        <StyledRboPopupBoxOptionInner>
          {this.props.isInline && (
            <Fragment>
              {this.props.title && this.titleRenderer()}
              {this.props.label && this.labelRenderer()}
              {(this.props.extra || this.props.describe) && ' — '}
              {this.props.extra && this.extraRenderer()}
              {this.props.describe && this.describeRenderer()}
            </Fragment>
          )}
          {!this.props.isInline && (
            <Fragment>
              <StyledRboPopupBoxOptionBlock>
                <StyledRboPopupBoxOptionMain>
                  {this.props.title && this.titleRenderer()}
                  {this.props.label && this.labelRenderer()}
                </StyledRboPopupBoxOptionMain>
                {this.props.extra && (
                  <StyledRboPopupBoxOptionExtra>{this.extraRenderer()}</StyledRboPopupBoxOptionExtra>
                )}
              </StyledRboPopupBoxOptionBlock>
              {this.props.describe && (
                <StyledRboPopupBoxOptionBlock>
                  <StyledRboPopupBoxOptionDescribe>{this.describeRenderer()}</StyledRboPopupBoxOptionDescribe>
                </StyledRboPopupBoxOptionBlock>
              )}
            </Fragment>
          )}
        </StyledRboPopupBoxOptionInner>
        {this.state.isWithOperations &&
          this.state.isHovered && (
            <StyledRboPopupBoxOptionOperations>
              {!!this.props.onEditClick && (
                <StyledRboPopupBoxOptionOperationsItem>
                  <ClickableIcon
                    type={ClickableIcon.REFS.TYPES.EDIT}
                    dimension={ClickableIcon.REFS.DIMENSIONS.XS}
                    display={ClickableIcon.REFS.DISPLAY.BLOCK}
                    onClick={this.handleEditClick}
                  />
                </StyledRboPopupBoxOptionOperationsItem>
              )}
              {!!this.props.onRemoveClick && (
                <StyledRboPopupBoxOptionOperationsItem>
                  <ClickableIcon
                    type={ClickableIcon.REFS.TYPES.REMOVE}
                    dimension={ClickableIcon.REFS.DIMENSIONS.XS}
                    display={ClickableIcon.REFS.DISPLAY.BLOCK}
                    onClick={this.handleRemoveClick}
                  />
                </StyledRboPopupBoxOptionOperationsItem>
              )}
            </StyledRboPopupBoxOptionOperations>
          )}
      </StyledRboPopupBoxOption>
    );
  }
}
