import styled from 'styled-components';

import { STYLES } from '../_constants';
import StyledClickableIcon from '../ClickableIcon/_style';

export const RboPopupBoxOptionHighlightStyle = {
  background: STYLES.COLORS.CORPORATE_YELLOW,
};

export const StyledRboPopupBoxOption = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  min-height: ${props => (props.isWithOperations ? '24px' : 'auto')};
  word-wrap: break-word;
`;

export const StyledRboPopupBoxOptionInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  width: 100%;
  vertical-align: middle;
`;

export const StyledRboPopupBoxOptionBlock = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  margin: 2px 0;
  max-width: 100%;

  :first-child {
    margin-top: 0;
  }

  :last-child {
    margin-bottom: 0;
  }
`;

export const StyledRboPopupBoxOptionMain = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  max-width: 100%;
  font-weight: ${props => (props.isTitle ? 'bold' : 'normal')};
`;

export const StyledRboPopupBoxOptionTitle = styled.span`
  font-weight: bold;
`;

export const StyledRboPopupBoxOptionExtra = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  text-align: right;
  margin-left: 8px;
`;

export const StyledRboPopupBoxOptionDescribe = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  max-width: 100%;
`;

export const StyledRboPopupBoxOptionOperations = styled.div`
  position: absolute;
  box-sizing: border-box;
  display: block;
  height: 24px;
  top: 50%;
  margin-top: -12px;
  right: 0;
  white-space: nowrap;
`;

export const StyledRboPopupBoxOptionOperationsItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  height: 24px;
  width: 24px;
  background: ${STYLES.COLORS.WHITE};
  border-radius: 50%;
  overflow: hidden;
  margin: 0 4px;

  :first-child {
    margin-left: 0;
  }

  :last-child {
    margin-right: 0;
  }

  ${StyledClickableIcon} {
    padding: 4px;
    height: 24px;
    width: 24px;
  }
`;
