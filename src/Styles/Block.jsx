import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { addDataAttributes } from '../_utils';

import { STYLES } from '../_constants';
import { COMPONENTS } from './_constants';

const REFS = {
  BORDER_COLORS: STYLES.COLORS,
  BACKGROUND_COLORS: STYLES.COLORS,
};

const StyledBlock = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 16px 40px;
  background: ${props => props.sBackgroundColor};
  border-top: ${props => (props.sBorderTopColor ? `1px solid ${props.sBorderTopColor}` : 'none')};
  border-bottom: ${props => (props.sBorderBottomColor ? `1px solid ${props.sBorderBottomColor}` : 'none')};
`;

const Block = props => (
  <StyledBlock
    {...addDataAttributes({ component: COMPONENTS.BLOCK })}
    sBackgroundColor={props.backgroundColor}
    sBorderTopColor={props.borderTopColor}
    sBorderBottomColor={props.borderBottomColor}
  >
    {props.children}
  </StyledBlock>
);

Block.propTypes = {
  backgroundColor: PropTypes.oneOf(Object.values(REFS.BACKGROUND_COLORS)),
  borderTopColor: PropTypes.oneOf(Object.values(REFS.BORDER_COLORS)),
  borderBottomColor: PropTypes.oneOf(Object.values(REFS.BORDER_COLORS)),
  children: PropTypes.node,
};

Block.defaultProps = {
  backgroundColor: REFS.BACKGROUND_COLORS.TRANSPARENT,
  borderTopColor: null,
  borderBottomColor: null,
  children: null,
};

Block.REFS = { ...REFS };

export default Block;
