```js
<div>
  <Block>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Block>
</div>
```

```js
<div>
  <Block borderTopColor={Block.REFS.BORDER_COLORS.TOMATO_RED}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Block>
</div>
```

```js
<div>
  <Block borderBottomColor={Block.REFS.BORDER_COLORS.TANGERINE_DARK}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Block>
</div>
```

```js
<div>
  <Block
    borderTopColor={Block.REFS.BORDER_COLORS.PEACOCK_BLUE}
    borderBottomColor={Block.REFS.BORDER_COLORS.PEACOCK_BLUE}
  >
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  </Block>
</div>
```
