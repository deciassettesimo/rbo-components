import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';

const REFS = {
  DIRECTIONS: {
    HORIZONTAL: 'horizontal',
    VERTICAL: 'vertical',
  },
};

const StyledGroup = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  white-space: nowrap;

  ${props =>
    props.sDirection === REFS.DIRECTIONS.HORIZONTAL &&
    `
      > * {
        display: inline-block;
        margin: 0 8px;
        :first-child { margin-left: 0; }
        :last-child { margin-right: 0; }
      }
    `};

  ${props =>
    props.sDirection === REFS.DIRECTIONS.VERTICAL &&
    `
      > * {
        display: block;
        margin: 16px 0;
        :first-child { margin-top: 0; }
        :last-child { margin-bottom: 0; }
      }
    `};
`;

const Group = props => (
  <StyledGroup {...addDataAttributes({ component: COMPONENTS.GROUP })} sDirection={props.direction}>
    {props.children}
  </StyledGroup>
);

Group.propTypes = {
  direction: PropTypes.oneOf(Object.values(REFS.DIRECTIONS)),
  children: PropTypes.node,
};

Group.defaultProps = {
  direction: REFS.DIRECTIONS.HORIZONTAL,
  children: null,
};

Group.REFS = { ...REFS };

export default Group;
