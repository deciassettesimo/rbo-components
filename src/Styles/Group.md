```js
<div>
  <Group>
    <Button id="Button1" onClick={() => null}>Button 1</Button>
    <Button id="Button2" onClick={() => null}>Button 2</Button>
    <Button id="Button3" onClick={() => null}>Button 3</Button>
  </Group>
</div>
```

```js
<div>
  <Group direction={Group.REFS.DIRECTIONS.VERTICAL}>
    <Button id="Button1" onClick={() => null}>Button 1</Button>
    <Button id="Button2" onClick={() => null}>Button 2</Button>
    <Button id="Button3" onClick={() => null}>Button 3</Button>
  </Group>
</div>
```
