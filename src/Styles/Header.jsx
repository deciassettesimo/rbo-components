import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';

const headerFontSize = sSize => {
  switch (sSize) {
    case 7:
      return 12;
    case 6:
      return 14;
    case 5:
      return 16;
    case 4:
      return 18;
    case 3:
      return 20;
    case 2:
      return 22;
    case 1:
    default:
      return 24;
  }
};

const headerLineHeight = sSize => {
  switch (sSize) {
    case 7:
      return 1.4;
    case 6:
      return 1.42;
    case 5:
      return 1.5;
    case 4:
      return 1.56;
    case 3:
      return 1.4;
    case 2:
      return 1.46;
    case 1:
    default:
      return 1.5;
  }
};

const StyledHeader = styled.div`
  font-weight: bold;
  font-size: ${props => headerFontSize(props.sSize)}px;
  line-height: ${props => headerLineHeight(props.sSize)};
  text-transform: ${props => (props.isUppercase ? 'uppercase' : 'none')};
  letter-spacing: ${props => (props.isUppercase ? '0.032em' : '0')};
`;

const Header = props => (
  <StyledHeader
    {...addDataAttributes({ component: COMPONENTS.HEADER })}
    sSize={props.size}
    isUppercase={props.isUppercase}
  >
    {props.children}
  </StyledHeader>
);

Header.propTypes = {
  size: PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7]),
  isUppercase: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

Header.defaultProps = {
  size: 1,
  isUppercase: false,
};

export default Header;
