```js
<div className="list">
  <div>
    <Header size={1}>H1-Text/Bold/24 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={2}>H2-Text/Bold/22 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={3}>H3-Text/Bold/20 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={4}>H4-Text/Bold/18 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={5}>H5-Text/Bold/16 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={6}>H6-Text/Bold/14 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={7}>H6-Text/Bold/14 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
</div>
```

```js
<div className="list">
  <div>
    <Header size={1} isUppercase>H1-Text/Bold/24 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={2} isUppercase>H2-Text/Bold/22 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={3} isUppercase>H3-Text/Bold/20 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={4} isUppercase>H4-Text/Bold/18 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={5} isUppercase>H5-Text/Bold/16 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={6} isUppercase>H6-Text/Bold/14 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
  <div>
    <Header size={7} isUppercase>H6-Text/Bold/14 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Header>
  </div>
</div>
```