import styled from 'styled-components';

import { addDataAttributes } from '../_utils';

import { STYLES } from '../_constants';

import { COMPONENTS } from './_constants';

const Label = styled.span.attrs({ ...addDataAttributes({ component: COMPONENTS.LABEL }) })`
  color: ${STYLES.COLORS.WARM_GRAY};
`;

/** @component */
export default Label;
