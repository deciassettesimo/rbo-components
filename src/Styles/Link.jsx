import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { addDataAttributes } from '../_utils';

import { STYLES } from '../_constants';
import { StyledIcon, StyledIconSvg } from '../Icon/_style';
import { COMPONENTS } from './_constants';

export const linkColor = (isPrimary, isSecondary, isDanger, isDisabled) => {
  if (isDisabled) return STYLES.COLORS.BLACK_48;
  if (isDanger) return STYLES.COLORS.TOMATO_RED;
  if (isPrimary) return STYLES.COLORS.BLACK;
  if (isSecondary) return STYLES.COLORS.BLACK_64;
  return STYLES.COLORS.PEACOCK_BLUE;
};

const linkUnderline = (isPrimary, isSecondary, isDanger, isDashed, isWithIcon, isDisabled, isHovered) => {
  if (isDisabled) return '';

  let lineColor = STYLES.COLORS.PEACOCK_BLUE_32;
  if (isDanger) lineColor = STYLES.COLORS.TOMATO_RED_32;
  if (isPrimary) lineColor = STYLES.COLORS.BLACK_32;
  if (isSecondary) lineColor = STYLES.COLORS.BLACK_32;
  if (isHovered) {
    lineColor = STYLES.COLORS.PEACOCK_BLUE;
    if (isDanger) lineColor = STYLES.COLORS.TOMATO_RED;
    if (isPrimary) lineColor = STYLES.COLORS.BLACK;
    if (isSecondary) lineColor = STYLES.COLORS.BLACK_64;
  }

  const lineStyle = isDashed
    ? `background: linear-gradient(to left, ${lineColor}, ${lineColor} 50%, transparent 50%) repeat-x 0 100%; background-size: 4px 1px;`
    : `background: linear-gradient(to left, ${lineColor}, ${lineColor} 100%) repeat-x 0 100%; background-size: 100% 1px;`;

  if (isWithIcon) return `> * {${lineStyle}}`;

  return lineStyle;
};

const StyledLink = styled.a`
  display: inline;
  text-decoration: none;
  color: ${props => linkColor(props.isPrimary, props.isSecondary, props.isDanger, props.isDisabled)};
  cursor: ${props => (props.isDisabled ? 'default' : 'pointer')};

  ${props =>
    (props.isUnderline || props.isDashed) &&
    linkUnderline(
      props.isPrimary,
      props.isSecondary,
      props.isDanger,
      props.isDashed,
      props.isWithIcon,
      props.isDisabled,
    )};

  :hover {
    ${props =>
      linkUnderline(
        props.isPrimary,
        props.isSecondary,
        props.isDanger,
        props.isDashed,
        props.isWithIcon,
        props.isDisabled,
        true,
      )};
  }

  ${StyledIcon} {
    top: -1px;
    margin: 0 4px;
    background: none;

    :first-child {
      margin-left: 0;
    }

    :last-child {
      margin-right: 0;
    }
  }

  ${StyledIconSvg} {
    fill: ${props => linkColor(props.isPrimary, props.isSecondary, props.isDange, props.isDisabled)};
  }
`;

const StyledPseudoLink = StyledLink.withComponent('span').extend``;

const Link = props => {
  const {
    isPseudo,
    isPrimary,
    isSecondary,
    isDanger,
    isUnderline,
    isDashed,
    isWithIcon,
    disabled,
    ...restProps
  } = props;
  const styleProps = { isPrimary, isSecondary, isDanger, isUnderline, isDashed, isWithIcon, isDisabled: disabled };

  if (isPseudo || disabled) {
    return (
      <StyledPseudoLink {...addDataAttributes({ component: COMPONENTS.LINK })} {...styleProps} {...restProps}>
        {props.children}
      </StyledPseudoLink>
    );
  }
  return (
    <StyledLink {...addDataAttributes({ component: COMPONENTS.LINK })} {...styleProps} {...restProps}>
      {props.children}
    </StyledLink>
  );
};

Link.propTypes = {
  isPseudo: PropTypes.bool,
  isPrimary: PropTypes.bool,
  isSecondary: PropTypes.bool,
  isDanger: PropTypes.bool,
  isUnderline: PropTypes.bool,
  isDashed: PropTypes.bool,
  isWithIcon: PropTypes.bool,
  disabled: PropTypes.bool,
  children: PropTypes.node,
};

Link.defaultProps = {
  isPseudo: false,
  isPrimary: false,
  isSecondary: false,
  isDanger: false,
  isUnderline: false,
  isDashed: false,
  isWithIcon: false,
  disabled: false,
  children: null,
};

export default Link;
