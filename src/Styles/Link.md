```js
<div className="list">
  <div className="inline">
    <Link href="https://www.raiffeisen.ru" target="_blank">Просто ссылка</Link>
  </div>
  <div className="inline">
    <Link isUnderline href="https://www.raiffeisen.ru" target="_blank">Подчеркнутая ссылка</Link>
  </div>
  <div className="inline">
    <Link isPseudo>Псевдо ссылка</Link>
  </div>
  <div className="inline">
    <Link isPseudo isDashed>Псевдо ссылка (Dashed)</Link>
  </div>
</div>
<div className="list">
  <div className="inline">
    <Link isPrimary href="https://www.raiffeisen.ru" target="_blank">Просто ссылка</Link>
  </div>
  <div className="inline">
    <Link isPrimary isUnderline href="https://www.raiffeisen.ru" target="_blank">Подчеркнутая ссылка</Link>
  </div>
  <div className="inline">
    <Link isPrimary isPseudo>Псевдо ссылка</Link>
  </div>
  <div className="inline">
    <Link isPrimary isDashed>Псевдо ссылка (Dashed)</Link>
  </div>
</div>
<div className="list">
  <div className="inline">
    <Link isSecondary href="https://www.raiffeisen.ru" target="_blank">Просто ссылка</Link>
  </div>
  <div className="inline">
    <Link isSecondary isUnderline href="https://www.raiffeisen.ru" target="_blank">Подчеркнутая ссылка</Link>
  </div>
  <div className="inline">
    <Link isSecondary isPseudo>Псевдо ссылка</Link>
  </div>
  <div className="inline">
    <Link isSecondary isDashed>Псевдо ссылка (Dashed)</Link>
  </div>
</div>
<div className="list">
  <div className="inline">
    <Link isDanger href="https://www.raiffeisen.ru" target="_blank">Просто ссылка</Link>
  </div>
  <div className="inline">
    <Link isDanger isUnderline href="https://www.raiffeisen.ru" target="_blank">Подчеркнутая ссылка</Link>
  </div>
  <div className="inline">
    <Link isDanger isPseudo>Псевдо ссылка</Link>
  </div>
  <div className="inline">
    <Link isDanger isDashed>Псевдо ссылка (Dashed)</Link>
  </div>
</div>
```

```js
<div className="list">
  <div className="inline">
    <Link href="https://www.raiffeisen.ru" target="_blank" isWithIcon>
      <IconCalendar dimension={IconCalendar.REFS.DIMENSIONS.XS} />
      <span>Ссылка с иконкой</span>
    </Link>
  </div>
  <div className="inline">
    <Link isPseudo isUnderline isWithIcon>
      <span>Ссылка с иконкой</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />
    </Link>
  </div>
  <div className="inline">
    <Link isPseudo isDashed isWithIcon>
      <span>Ссылка</span>
      <IconSearch dimension={IconSearch.REFS.DIMENSIONS.XS} />
      <span>с иконкой</span>
    </Link>
  </div>
</div>
<div className="list">
  <div className="inline">
    <Link isPrimary href="https://www.raiffeisen.ru" target="_blank" isWithIcon>
      <IconCalendar dimension={IconCalendar.REFS.DIMENSIONS.XS} />
      <span>Ссылка с иконкой</span>
    </Link>
  </div>
  <div className="inline">
    <Link isPrimary isPseudo isUnderline isWithIcon>
      <span>Ссылка с иконкой</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />
    </Link>
  </div>
  <div className="inline">
    <Link isPrimary isPseudo isDashed isWithIcon>
      <span>Ссылка</span>
      <IconSearch dimension={IconSearch.REFS.DIMENSIONS.XS} />
      <span>с иконкой</span>
    </Link>
  </div>
</div>
<div className="list">
  <div className="inline">
    <Link isSecondary href="https://www.raiffeisen.ru" target="_blank" isWithIcon>
      <IconCalendar dimension={IconCalendar.REFS.DIMENSIONS.XS} />
      <span>Ссылка с иконкой</span>
    </Link>
  </div>
  <div className="inline">
    <Link isSecondary isPseudo isUnderline isWithIcon>
      <span>Ссылка с иконкой</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />
    </Link>
  </div>
  <div className="inline">
    <Link isSecondary isPseudo isDashed isWithIcon>
      <span>Ссылка</span>
      <IconSearch dimension={IconSearch.REFS.DIMENSIONS.XS} />
      <span>с иконкой</span>
    </Link>
  </div>
</div>
<div className="list">
  <div className="inline">
    <Link isDanger href="https://www.raiffeisen.ru" target="_blank" isWithIcon>
      <IconCalendar dimension={IconCalendar.REFS.DIMENSIONS.XS} />
      <span>Ссылка с иконкой</span>
    </Link>
  </div>
  <div className="inline">
    <Link isDanger isPseudo isUnderline isWithIcon>
      <span>Ссылка с иконкой</span>
      <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />
    </Link>
  </div>
  <div className="inline">
    <Link isDanger isPseudo isDashed isWithIcon>
      <span>Ссылка</span>
      <IconSearch dimension={IconSearch.REFS.DIMENSIONS.XS} />
      <span>с иконкой</span>
    </Link>
  </div>
</div>
```
