import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';

const REFS = {
  FONT_SIZES: {
    XS: 10,
    S: 12,
    M: 14,
    L: 16,
    XL: 18,
  },
  V_ALIGN_ITEMS: {
    TOP: 'top',
    MIDDLE: 'middle',
    BASELINE: 'baseline',
    BOTTOM: 'bottom',
  },
};

const StyledListInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
`;

const StyledListItem = styled.div`
  position: relative;
  box-sizing: border-box;
  vertical-align: top;
`;

const StyledList = styled.div`
  position: relative;
  box-sizing: border-box;
  display: ${props => (props.isInline ? 'inline-block' : 'block')};
  padding: ${props => props.vPadding}px ${props => props.hPadding}px;
  font-size: ${props => props.sFontSize}px;

  ${StyledListInner} {
    margin: 0 -${props => props.innerHPadding}px -${props => props.innerVPadding}px 0;
  }

  ${StyledListItem} {
    display: ${props => (props.isInline ? 'inline-block' : 'block')};
    vertical-align: ${props => props.vAlignItems};
    margin: 0 ${props => props.innerHPadding}px ${props => props.innerVPadding}px 0;
  }
`;

const ListItem = props => (
  <StyledListItem {...addDataAttributes({ component: COMPONENTS.LIST_ITEM })}>{props.children}</StyledListItem>
);

ListItem.propTypes = {
  children: PropTypes.node,
};

ListItem.defaultProps = {
  children: PropTypes.node,
};

const List = props => (
  <StyledList
    {...addDataAttributes({ component: COMPONENTS.LIST })}
    isInline={props.isInline}
    sFontSize={props.fontSize}
    vAlignItems={props.vAlignItems}
    vPadding={props.vPadding}
    hPadding={props.hPadding}
    innerVPadding={props.innerVPadding}
    innerHPadding={props.innerHPadding}
  >
    <StyledListInner>{props.children}</StyledListInner>
  </StyledList>
);

List.propTypes = {
  fontSize: PropTypes.oneOf(Object.values(REFS.FONT_SIZES)),
  isInline: PropTypes.bool,
  vAlignItems: PropTypes.oneOf(Object.values(REFS.V_ALIGN_ITEMS)),
  vPadding: PropTypes.number,
  hPadding: PropTypes.number,
  innerVPadding: PropTypes.number,
  innerHPadding: PropTypes.number,
  children: PropTypes.node,
};

List.defaultProps = {
  fontSize: REFS.FONT_SIZES.M,
  isInline: false,
  vAlignItems: REFS.V_ALIGN_ITEMS.BASELINE,
  vPadding: 0,
  hPadding: 0,
  innerVPadding: 12,
  innerHPadding: 8,
  children: null,
};

List.REFS = { ...REFS };

List.Item = ListItem;

export default List;
