```js
<div className="list">
  <div>
    <List fontSize={List.REFS.FONT_SIZES.XS} vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
  <div>
    <List fontSize={List.REFS.FONT_SIZES.S} vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
  <div>
    <List fontSize={List.REFS.FONT_SIZES.M} vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
  <div>
    <List fontSize={List.REFS.FONT_SIZES.L} vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
  <div>
    <List fontSize={List.REFS.FONT_SIZES.XL} vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
</div>
```

### isInline

```js
<div className="list">
  <div>
    <List fontSize={List.REFS.FONT_SIZES.XS} isInline vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
  <div>
    <List fontSize={List.REFS.FONT_SIZES.S} isInline vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
  <div>
    <List fontSize={List.REFS.FONT_SIZES.M} isInline vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
  <div>
    <List fontSize={List.REFS.FONT_SIZES.L} isInline vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
  <div>
    <List fontSize={List.REFS.FONT_SIZES.XL} isInline vPadding={8}>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
      <List.Item>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</List.Item>
    </List>
  </div>
</div>
```
