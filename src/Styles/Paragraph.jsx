import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';

const REFS = {
  FONT_SIZES: {
    XS: 10,
    S: 12,
    M: 14,
    L: 16,
    XL: 18,
  },
};

const StyledParagraph = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  margin: 8px 0;
  font-size: ${props => props.sFontSize}px;

  :first-child {
    margin-top: 0;
  }

  :last-child {
    margin-bottom: 0;
  }
`;

const Paragraph = props => (
  <StyledParagraph {...addDataAttributes({ component: COMPONENTS.PARAGRAPH })} sFontSize={props.fontSize}>
    {props.children}
  </StyledParagraph>
);

Paragraph.propTypes = {
  fontSize: PropTypes.oneOf(Object.values(REFS.FONT_SIZES)),
  children: PropTypes.node,
};

Paragraph.defaultProps = {
  fontSize: REFS.FONT_SIZES.M,
  children: null,
};

Paragraph.REFS = { ...REFS };

export default Paragraph;
