```js
<div className="list">
  <div>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.XS}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.XS}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
  </div>
  <div>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.S}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.S}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
  </div>
  <div>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.M}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.M}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
  </div>
  <div>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.L}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.L}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
  </div>
  <div>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.XL}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
    <Paragraph fontSize={Paragraph.REFS.FONT_SIZES.XL}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
  </div>
</div>
```
