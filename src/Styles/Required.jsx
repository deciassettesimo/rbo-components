import React from 'react';
import styled from 'styled-components';

import { addDataAttributes } from '../_utils';

import { STYLES } from '../_constants';

import { COMPONENTS } from './_constants';

const StyledRequired = styled.span`
  margin-left: 0.2em;
  vertical-align: top;
  font-size: smaller;
  color: ${STYLES.COLORS.TOMATO_RED};
`;

const Required = () => <StyledRequired {...addDataAttributes({ component: COMPONENTS.REQUIRED })}>*</StyledRequired>;

export default Required;
