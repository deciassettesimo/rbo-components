import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { addDataAttributes } from '../_utils';

import { STYLES } from '../_constants';

import { COMPONENTS } from './_constants';

const statusColor = (isSuccess, isWarning, isError) => {
  if (isSuccess) return STYLES.COLORS.TRUE_GREEN;
  if (isError) return STYLES.COLORS.TOMATO_RED;
  if (isWarning) return STYLES.COLORS.TANGERINE_DARK;
  return STYLES.COLORS.BLACK;
};

const StyledStatus = styled.span`
  color: ${props => statusColor(props.isSuccess, props.isWarning, props.isError)};
`;

const Status = props => (
  <StyledStatus
    {...addDataAttributes({ component: COMPONENTS.STATUS })}
    isSuccess={props.isSuccess}
    isWarning={props.isWarning}
    isError={props.isError}
  >
    {props.children}
  </StyledStatus>
);

Status.propTypes = {
  children: PropTypes.node,
  isSuccess: PropTypes.bool,
  isWarning: PropTypes.bool,
  isError: PropTypes.bool,
};

Status.defaultProps = {
  children: null,
  isSuccess: false,
  isWarning: false,
  isError: false,
};

export default Status;
