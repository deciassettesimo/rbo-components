export const COMPONENTS = {
  BLOCK: 'Block',
  GROUP: 'Group',
  HEADER: 'Header',
  LABEL: 'Label',
  LINK: 'Link',
  LIST: 'List',
  LIST_ITEM: 'ListItem',
  PARAGRAPH: 'Paragraph',
  REQUIRED: 'Required',
  STATUS: 'Status',
};

export default COMPONENTS;
