import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import { COMPONENTS, MIN_COLUMN_WIDTH, SELECT_CHANGE_TYPES } from './_constants';
import REFS from './_config';
import TableHead from './_internal/TableHead';
import TableBody from './_internal/TableBody';
import TableSettings from './_internal/TableSettings';
import { StyledTable } from './_style';
import { getAllItemsFromGroups, getSelectedItemsIds } from './_utils';

export default class Table extends PureComponent {
  static REFS = { ...REFS };

  static propTypes = {
    /** language for text */
    locale: PropTypes.oneOf(Object.values(REFS.LOCALES)),
    /** Dimension size (font-size) in px */
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    /** Attributes for element ex. { id: 'Table' } */
    dataAttributes: PropTypes.shape(),
    /** isLoading flag */
    isLoading: PropTypes.bool,
    /** isMultiLine flag */
    isMultiLine: PropTypes.bool,
    /** isHeadMultiLine flag */
    isHeadMultiLine: PropTypes.bool,
    /** headBgColor */
    headBgColor: PropTypes.oneOf(Object.values(REFS.HEAD_BG_COLORS)),
    /** emptyLabel */
    emptyLabel: PropTypes.string,
    /** cellRenderer
     * @param {object}
     * @param {string} column Column Id
     * @param {object} data Data of item
     * */
    /** columns - list of columns */
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        isVisible: PropTypes.bool,
        width: PropTypes.number.isRequired,
        align: PropTypes.oneOf(Object.values(REFS.COLUMNS_ALIGN)),
        label: PropTypes.string,
        grouping: PropTypes.bool,
        isGrouped: PropTypes.bool,
        isSortingDisabled: PropTypes.bool,
      }),
    ).isRequired,
    /** isGrouped flag */
    isGrouped: PropTypes.bool,
    /** items - list of entries or groups with entries
     * entries must be the shape with keys of columns ids
     * */
    items: PropTypes.arrayOf(
      PropTypes.shape({
        /** id of item or group */
        id: PropTypes.string.isRequired,
        /** title of group (if isGrouped = true) */
        title: PropTypes.string,
        /** items of group (if isGrouped = true) */
        items: PropTypes.arrayOf(PropTypes.shape()),
        /** isWarning of item */
        isWarning: PropTypes.bool,
        /** isError of item */
        isError: PropTypes.bool,
        /** isSelected of item */
        isSelected: PropTypes.bool,
        /** isSelectDisabled of item */
        isSelectDisabled: PropTypes.bool,
        /** available operations of item (keys - ids of operations) ex. { copy: true }
         * if operations = null|undefined - all operations are available
         * */
        operations: PropTypes.shape(),
      }),
    ),
    cellRenderer: PropTypes.func,
    /** onItemClick handler
     * @param {object}
     * @param {string} id Item Id
     * */
    onItemClick: PropTypes.func,
    /** operations for items */
    operations: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        icon: PropTypes.string,
      }),
    ),
    /** isOperationsCompact flag */
    isOperationsCompact: PropTypes.bool,
    /** onOperationClick handler
     * @param {object}
     * @param {string} itemId Item Id
     * @param {string} operationId Operation Id
     * */
    onOperationClick: PropTypes.func,
    /** isSortable flag */
    isSortable: PropTypes.bool,
    /** onSortingChange handler
     * @param {object}
     * @param {string} id Column Id
     * @param {string} direction Direction of sorting (1 (asc), -1 (desc))
     * */
    onSortingChange: PropTypes.func,
    /** isResizable flag */
    isResizable: PropTypes.bool,
    /** onResizeChange handler
     * @param {object}
     * @param {object} widths Widths of all columns (keys - columns ids)
     * */
    onResizeChange: PropTypes.func,
    /** isSelectable flag */
    isSelectable: PropTypes.bool,
    /** onSelectChange handler
     * @param {object}
     * @param {object} selected Ids of selected items
     * */
    onSelectChange: PropTypes.func,
    /** settings */
    settings: PropTypes.shape({
      visibility: PropTypes.bool,
      onVisibilityChange: PropTypes.func,
      grouping: PropTypes.bool,
      onGroupingChange: PropTypes.func,
    }),
  };

  static defaultProps = {
    locale: REFS.LOCALES.RU,
    dimension: REFS.DIMENSIONS.M,
    dataAttributes: {},
    items: [],
    isGrouped: false,
    isLoading: false,
    isMultiLine: false,
    emptyLabel: null,
    cellRenderer: null,
    onItemClick: null,
    isHeadMultiLine: false,
    headBgColor: REFS.HEAD_BG_COLORS.WHITE,
    operations: [],
    isOperationsCompact: false,
    onOperationClick: null,
    isSortable: false,
    onSortingChange: () => null,
    isResizable: false,
    onResizeChange: () => null,
    isSelectable: false,
    onSelectChange: () => null,
    settings: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      columns: this.props.columns,
      items: [],
      width: null,
      scrollLeft: 0,
      isResizing: false, /* eslint-disable-line */
    };

    this.node = React.createRef();
    this.headNode = React.createRef();
    this.bodyNode = React.createRef();
    this.timer = null;
  }

  static getDerivedStateFromProps(props, state) {
    if (state.isResizing) return null;

    const propsColumns = JSON.stringify(props.columns);
    const stateColumns = JSON.stringify(state.columns);
    const propsItems = JSON.stringify(props.items);
    const stateItems = JSON.stringify(state.items);

    if (propsColumns !== stateColumns && propsItems !== stateItems) {
      return { columns: props.columns, items: props.items };
    }
    if (propsColumns !== stateColumns) return { columns: props.columns };
    if (propsItems !== stateItems) return { items: props.items };
    return null;
  }

  componentDidMount() {
    window.addEventListener('resize', this.setOffset);
    this.bodyNode.current.addEventListener('scroll', this.syncScroll);
    this.syncScroll();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoading && !this.props.isLoading) {
      window.requestAnimationFrame(() => {
        if (!this.state.items.length) this.headNode.current.scrollLeft = 0;
        this.bodyNode.current.scrollLeft = this.headNode.current.scrollLeft;
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setOffset);
    this.bodyNode.current.removeEventListener('scroll', this.syncScroll);
  }

  setOffset = () => {
    this.setState({
      width: this.bodyNode.current.clientWidth,
      scrollLeft: this.bodyNode.current.scrollLeft,
    });
  };

  syncScroll = () => {
    if (this.timer) clearTimeout(this.timer);
    if (!this.props.isLoading) {
      window.requestAnimationFrame(() => {
        this.headNode.current.scrollLeft = this.bodyNode.current.scrollLeft;
      });
    }
    this.timer = setTimeout(this.setOffset, 50);
  };

  handleSortingChange = ({ id }) => {
    const direction = this.state.columns.find(column => column.id === id).sorting === 1 ? -1 : 1;
    this.props.onSortingChange({ sorting: { id, direction } });
  };

  handleResizeChange = ({ id, width, isStarted, isFinished }) => {
    const columns = this.state.columns.map(
      column => (column.id === id ? { ...column, width: width > MIN_COLUMN_WIDTH ? width : MIN_COLUMN_WIDTH } : column),
    );
    if (isStarted) {
      this.setState({ columns, isResizing: true }); /* eslint-disable-line */
    }
    if (isFinished) {
      this.setState({ columns, isResizing: false }); /* eslint-disable-line */
      const widths = columns.reduce((result, column) => ({ ...result, [column.id]: column.width }), {});
      this.props.onResizeChange({ widths });
    } else {
      this.setState({ columns });
    }
  };

  handleSelectAll = ({ value }) => {
    let items;
    let allItems;
    if (this.props.isGrouped) {
      items = this.state.items.map(group => ({
        ...group,
        items: group.items.map(item => ({ ...item, isSelected: !item.isSelectDisabled && value })),
      }));
      allItems = getAllItemsFromGroups(items);
    } else {
      items = this.state.items.map(item => ({ ...item, isSelected: !item.isSelectDisabled && value }));
      allItems = items;
    }
    this.setState({ items });
    this.props.onSelectChange({ selected: getSelectedItemsIds(allItems) });
  };

  handleSelectChange = ({ type, id, value }) => {
    let items;
    let allItems;
    if (type === SELECT_CHANGE_TYPES.GROUP) {
      items = this.state.items.map(
        group =>
          group.id === id
            ? { ...group, items: group.items.map(item => ({ ...item, isSelected: !item.isSelectDisabled && value })) }
            : group,
      );
      allItems = getAllItemsFromGroups(items);
    }
    if (type === SELECT_CHANGE_TYPES.ITEM) {
      if (this.props.isGrouped) {
        items = this.state.items.map(group => ({
          ...group,
          items: group.items.map(
            item => (item.id === id ? { ...item, isSelected: !item.isSelectDisabled && value } : item),
          ),
        }));
        allItems = getAllItemsFromGroups(items);
      } else {
        items = this.state.items.map(
          item => (item.id === id ? { ...item, isSelected: !item.isSelectDisabled && value } : item),
        );
        allItems = items;
      }
    }
    this.setState({ items });
    this.props.onSelectChange({ selected: getSelectedItemsIds(allItems) });
  };

  handleSettingsVisibilityChange = columns => {
    this.setState({ columns });
    const visibility = columns.filter(column => column.isVisible).map(column => column.id);
    this.props.settings.onVisibilityChange({ visibility });
  };

  handleSettingsGroupingChange = columns => {
    this.setState({ columns });
    const groupedColumn = columns.find(column => column.isGrouped);
    this.props.settings.onGroupingChange({ grouped: groupedColumn ? groupedColumn.id : null });
  };

  render() {
    return (
      <StyledTable {...addDataAttributes({ component: COMPONENTS.GENERAL, ...this.props.dataAttributes })}>
        <TableHead
          ref={this.headNode}
          dimension={this.props.dimension}
          items={this.state.items}
          columns={this.state.columns}
          isMultiLine={this.props.isHeadMultiLine}
          bgColor={this.props.headBgColor}
          isSortable={this.props.isSortable}
          onSortingChange={this.handleSortingChange}
          isResizable={this.props.isResizable}
          onResizeChange={this.handleResizeChange}
          isGrouped={this.props.isGrouped}
          isSelectable={this.props.isSelectable}
          onSelectChange={this.handleSelectAll}
        />
        <TableBody
          ref={this.bodyNode}
          locale={this.props.locale}
          dimension={this.props.dimension}
          items={this.state.items}
          columns={this.state.columns}
          operations={this.props.operations}
          isGrouped={this.props.isGrouped}
          isLoading={this.props.isLoading}
          isMultiLine={this.props.isMultiLine}
          emptyLabel={this.props.emptyLabel}
          cellRenderer={this.props.cellRenderer}
          onItemClick={this.props.onItemClick}
          isOperationsCompact={this.props.isOperationsCompact}
          onOperationClick={this.props.onOperationClick}
          isSelectable={this.props.isSelectable}
          onSelectChange={this.handleSelectChange}
          parentNodeWidth={this.state.width}
          parentNodeScrollLeft={this.state.scrollLeft}
        />
        {this.props.settings && (
          <TableSettings
            locale={this.props.locale}
            dimension={this.props.dimension}
            bgColor={this.props.headBgColor}
            columns={this.state.columns}
            visibility={this.props.settings.visibility}
            onVisibilityChange={this.handleSettingsVisibilityChange}
            grouping={this.props.settings.visibility}
            onGroupingChange={this.handleSettingsGroupingChange}
          />
        )}
      </StyledTable>
    );
  }
}
