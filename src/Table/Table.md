### REFS
```js static
{
  LOCALES = {
    RU: 'ru',
    EN: 'en',
  },
  DIMENSIONS = {
    S: 12,
    M: 14,
    L: 16,
  },
  COLUMNS_ALIGN: {
    LEFT: 'left',
    CENTER: 'center',
    RIGHT: 'right',
  },
  HEAD_BG_COLORS: {
    WHITE: '#FFFFFF',
    GRAY: '#F7F7F7',
  }
}
```

### Table Example

```jsx
const TableExample = require('./__examples__/Table.example').default;
<TableExample />
```

#### Table Example Source Code
```js { "file": "./__examples__/Table.example.jsx" }
```
