import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Form, { Row, Cell, Label, Field } from '../../Form';
import { InputCheckbox, InputSelect } from '../../Input';
import Table from '../Table';

const COLUMNS = [
  {
    id: 'docNumber',
    label: 'Номер',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'docDate',
    label: 'Дата',
    sorting: -1,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'amount',
    label: 'Сумма',
    sorting: 0,
    width: 100,
    align: Table.REFS.COLUMNS_ALIGN.RIGHT,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'status',
    label: 'Статус',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'bankMessage',
    label: 'Сообщение из банка',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
    isSortingDisabled: true,
  },
  {
    id: 'lastChangeStateDate',
    label: 'Изменено',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'payerName',
    label: 'Плательщик',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'payerAccount',
    label: 'Счет плательщика',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'payerBankBic',
    label: 'БИК банка плательщика',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'receiverName',
    label: 'Получатель',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'receiverAccount',
    label: 'Счет получателя',
    sorting: 0,
    width: 100,
    grouping: true,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'receiverInn',
    label: 'ИНН получателя',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: true,
  },
  {
    id: 'paymentPurpose',
    label: 'Назначение платежа',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: false,
  },
  {
    id: 'docNote',
    label: 'Заметки',
    sorting: 0,
    width: 100,
    grouping: false,
    isGrouped: false,
    isVisible: false,
  },
];

const ITEMS = [
  {
    id: '1',
    docNumber: '1',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    isWarning: true,
    operations: { edit: true, copy: true },
  },
  {
    id: '2',
    docNumber: '2',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    isError: true,
    operations: { edit: true, remove: true },
  },
  {
    id: '3',
    docNumber: '3',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    operations: { copy: true },
    isSelectDisabled: true,
  },
  {
    id: '4',
    docNumber: '4',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    operations: { edit: true, copy: true, remove: true },
    isSelectDisabled: true,
  },
  {
    id: '5',
    docNumber: '5',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    isSelectDisabled: true,
  },
  {
    id: '6',
    docNumber: '6',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
    isSelectDisabled: true,
  },
  {
    id: '7',
    docNumber: '7',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
  },
  {
    id: '8',
    docNumber: '8',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
  },
  {
    id: '9',
    docNumber: '9',
    docDate: '29.12.2016',
    amount: 1000.45,
    status: 'Создан',
    bankMessage: 'Сообщение из банка',
    lastChangeStateDate: '29.12.2016',
    payerName: 'ООО Керамика Люкс',
    payerAccount: '40702.810.6.00001489171',
    payerBankBic: '044525700',
    receiverName: 'ООО Ромашка',
    receiverAccount: '40125.810.8.64561024956',
    receiverInn: '444444444444',
    paymentPurpose: 'Платеж за поставку товаров.\\nВ том числе НДС 18.00% - 19.98.',
    docNote: 'Заметка к платежному поручению',
  },
];

const OPERATIONS = [
  {
    id: 'edit',
    icon: 'edit',
    title: 'Редактировать',
  },
  {
    id: 'copy',
    icon: 'document-copy',
    title: 'Копировать',
  },
  {
    id: 'remove',
    icon: 'remove',
    title: 'Удалить',
  },
];

const HEIGHT_OPTIONS = [
  { value: 'auto', title: 'auto' },
  { value: '300', title: '300px' },
  { value: '600', title: '600px' },
];

const DIMENSIONS_OPTIONS = Object.keys(Table.REFS.DIMENSIONS).map(key => ({
  value: Table.REFS.DIMENSIONS[key].toString(),
  title: key,
}));

const LOCALE_OPTIONS = Object.keys(Table.REFS.LOCALES).map(key => ({
  value: Table.REFS.LOCALES[key],
  title: key,
}));

const HEAD_BG_COLORS_OPTIONS = Object.keys(Table.REFS.HEAD_BG_COLORS).map(key => ({
  value: Table.REFS.HEAD_BG_COLORS[key],
  title: key,
}));

const StyledExampleContainer = styled.div`
  position: relative;
`;

const StyledExampleOperations = styled.div`
  position: relative;
  margin-bottom: 20px;
`;

const StyledExampleTable = styled.div.attrs({
  style: ({ sHeight }) => ({
    height: parseInt(sHeight, 10) || 'auto',
  }),
})`
  position: relative;
`;

export default class TableExample extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      columns: COLUMNS.map(column => column),
      items: ITEMS.map(item => item),
      height: 'auto',
      dimension: '14',
      isMultiLine: false,
      isHeadMultiLine: false,
      locale: Table.REFS.LOCALES.RU,
      headBgColor: Table.REFS.HEAD_BG_COLORS.GRAY,
      isGrouped: false,
      isLoading: false,
      isEmpty: false,
      isOperationsCompact: true,
      isWithOnItemClick: true,
      isWithOnOperationClick: true,
      isSortable: true,
      isResizable: true,
      isSelectable: true,
      isWithSettings: true,
    };
  }

  onItemClick = param => {
    console.log(param); /* eslint-disable-line */
  };

  onOperationClick = param => {
    console.log(param); /* eslint-disable-line */
  };

  onSortingChange = param => {
    this.setState({
      columns: this.state.columns.map(
        column =>
          column.id === param.sorting.id ? { ...column, sorting: param.sorting.direction } : { ...column, sorting: 0 },
      ),
    });
    console.log(param); /* eslint-disable-line */
  };

  onResizeChange = param => {
    this.setState({ columns: this.state.columns.map(column => ({ ...column, width: param.widths[column.id] })) });
    console.log(param); /* eslint-disable-line */
  };

  onSelectChange = param => {
    console.log(param); /* eslint-disable-line */
  };

  handleSettingsChange = param => {
    console.log(param); /* eslint-disable-line */
  };

  handleChangeHeight = ({ value }) => {
    COLUMNS.length = 2;
    this.setState({ height: value, columns: COLUMNS.map(column => column) });
  };

  handleChangeDimension = ({ value }) => {
    this.setState({ dimension: value });
  };

  handleChangeLocale = ({ value }) => {
    this.setState({ locale: value });
  };

  handleChangeIsMultiLine = ({ value }) => {
    this.setState({ isMultiLine: value });
  };

  handleChangeIsHeadMultiLine = ({ value }) => {
    this.setState({ isHeadMultiLine: value });
  };

  handleChangeHeadBgColor = ({ value }) => {
    this.setState({ headBgColor: value });
  };

  handleChangeIsWithOnItemClick = ({ value }) => {
    this.setState({ isWithOnItemClick: value });
  };

  handleChangeIsGrouped = ({ value }) => {
    const items = value
      ? [
          {
            id: 'group1',
            title: 'Группа 1',
            items: [ITEMS[0], ITEMS[1], ITEMS[2]],
          },
          {
            id: 'group2',
            title: 'Группа 2',
            items: [ITEMS[3], ITEMS[4], ITEMS[5]],
          },
          {
            id: 'group3',
            title: 'Группа 3',
            items: [ITEMS[6], ITEMS[7], ITEMS[8]],
          },
        ]
      : ITEMS;
    this.setState({ isGrouped: value, items });
  };

  handleChangeIsWithOnOperationClick = ({ value }) => {
    this.setState({ isWithOnOperationClick: value });
  };

  handleChangeIsOperationsCompact = ({ value }) => {
    this.setState({ isOperationsCompact: value });
  };

  handleChangeIsWithSettings = ({ value }) => {
    this.setState({ isWithSettings: value });
  };

  handleChangeIsSortable = ({ value }) => {
    this.setState({ isSortable: value });
  };

  handleChangeIsResizable = ({ value }) => {
    this.setState({ isResizable: value });
  };

  handleChangeIsSelectable = ({ value }) => {
    this.setState({ isSelectable: value });
  };

  handleChangeIsLoading = ({ value }) => {
    this.setState({ isLoading: value });
  };

  handleChangeIsEmpty = ({ value }) => {
    this.setState({ isEmpty: value });
  };

  render() {
    return (
      <StyledExampleContainer>
        <StyledExampleOperations>
          <Form dimension={Form.REFS.DIMENSIONS.XS}>
            <Row>
              <Cell width="120px">
                <Label htmlFor="height">height</Label>
                <Field>
                  <InputSelect
                    id="height"
                    value={this.state.height}
                    options={HEIGHT_OPTIONS}
                    onChange={this.handleChangeHeight}
                  />
                </Field>
              </Cell>
              <Cell width="120px">
                <Label htmlFor="dimension">dimension</Label>
                <Field>
                  <InputSelect
                    id="dimension"
                    value={this.state.dimension}
                    options={DIMENSIONS_OPTIONS}
                    onChange={this.handleChangeDimension}
                  />
                </Field>
              </Cell>
              <Cell width="120px">
                <Label htmlFor="height">locale</Label>
                <Field>
                  <InputSelect
                    id="height"
                    value={this.state.locale}
                    options={LOCALE_OPTIONS}
                    onChange={this.handleChangeLocale}
                  />
                </Field>
              </Cell>
              <Cell width="120px">
                <Label htmlFor="headBgColor">headBgColor</Label>
                <Field>
                  <InputSelect
                    id="headBgColor"
                    value={this.state.headBgColor}
                    options={HEAD_BG_COLORS_OPTIONS}
                    onChange={this.handleChangeHeadBgColor}
                  />
                </Field>
              </Cell>
            </Row>
            <Row>
              <Cell width="auto">
                <Field>
                  <InputCheckbox
                    id="isMultiLine"
                    checked={this.state.isMultiLine}
                    onChange={this.handleChangeIsMultiLine}
                  >
                    isMultiLine
                  </InputCheckbox>
                </Field>
              </Cell>
              <Cell width="auto">
                <Field>
                  <InputCheckbox
                    id="isHeadMultiLine"
                    checked={this.state.isHeadMultiLine}
                    onChange={this.handleChangeIsHeadMultiLine}
                  >
                    Head isMultiLine
                  </InputCheckbox>
                </Field>
              </Cell>
            </Row>
            <Row>
              <Cell width="auto">
                <Field>
                  <InputCheckbox
                    id="isWithOnItemClick"
                    checked={this.state.isWithOnItemClick}
                    onChange={this.handleChangeIsWithOnItemClick}
                  >
                    with onItemClick
                  </InputCheckbox>
                </Field>
              </Cell>
              <Cell width="auto">
                <Field>
                  <InputCheckbox
                    id="isWithOnOperationClick"
                    checked={this.state.isWithOnOperationClick}
                    onChange={this.handleChangeIsWithOnOperationClick}
                  >
                    with operations
                  </InputCheckbox>
                </Field>
              </Cell>
              <Cell width="auto">
                <Field>
                  <InputCheckbox
                    id="isOperationsCompact"
                    checked={this.state.isOperationsCompact}
                    onChange={this.handleChangeIsOperationsCompact}
                  >
                    isOperationsCompact
                  </InputCheckbox>
                </Field>
              </Cell>
              <Cell width="auto">
                <Field>
                  <InputCheckbox
                    id="isWithSettings"
                    checked={this.state.isWithSettings}
                    onChange={this.handleChangeIsWithSettings}
                  >
                    with settings
                  </InputCheckbox>
                </Field>
              </Cell>
            </Row>
            <Row>
              <Cell width="auto">
                <Field>
                  <InputCheckbox id="isSortable" checked={this.state.isSortable} onChange={this.handleChangeIsSortable}>
                    isSortable
                  </InputCheckbox>
                </Field>
              </Cell>
              <Cell width="auto">
                <Field>
                  <InputCheckbox
                    id="isResizable"
                    checked={this.state.isResizable}
                    onChange={this.handleChangeIsResizable}
                  >
                    isResizable
                  </InputCheckbox>
                </Field>
              </Cell>
              <Cell width="auto">
                <Field>
                  <InputCheckbox
                    id="isSelectable"
                    checked={this.state.isSelectable}
                    onChange={this.handleChangeIsSelectable}
                  >
                    isSelectable
                  </InputCheckbox>
                </Field>
              </Cell>
            </Row>
            <Row>
              <Cell width="auto">
                <Field>
                  <InputCheckbox id="isGrouped" checked={this.state.isGrouped} onChange={this.handleChangeIsGrouped}>
                    isGrouped
                  </InputCheckbox>
                </Field>
              </Cell>
              <Cell width="auto">
                <Field>
                  <InputCheckbox id="isLoading" checked={this.state.isLoading} onChange={this.handleChangeIsLoading}>
                    isLoading
                  </InputCheckbox>
                </Field>
              </Cell>
              <Cell width="auto">
                <Field>
                  <InputCheckbox id="isEmpty" checked={this.state.isEmpty} onChange={this.handleChangeIsEmpty}>
                    isEmpty
                  </InputCheckbox>
                </Field>
              </Cell>
            </Row>
          </Form>
        </StyledExampleOperations>

        <StyledExampleTable sHeight={this.state.height}>
          <Table
            dataAttributes={{ id: 'tableExample' }}
            locale={this.state.locale}
            dimension={parseInt(this.state.dimension, 10)}
            columns={this.state.columns}
            items={this.state.isEmpty ? undefined : this.state.items}
            onItemClick={this.state.isWithOnItemClick ? this.onItemClick : null}
            isGrouped={this.state.isGrouped}
            isLoading={this.state.isLoading}
            isMultiLine={this.state.isMultiLine}
            isHeadMultiLine={this.state.isHeadMultiLine}
            headBgColor={this.state.headBgColor}
            operations={this.state.isWithOnOperationClick ? OPERATIONS : undefined}
            isOperationsCompact={this.state.isOperationsCompact}
            onOperationClick={this.state.isWithOnOperationClick ? this.onOperationClick : null}
            isSortable={this.state.isSortable}
            onSortingChange={this.onSortingChange}
            isResizable={this.state.isResizable}
            onResizeChange={this.onResizeChange}
            isSelectable={this.state.isSelectable}
            onSelectChange={this.onSelectChange}
            settings={
              this.state.isWithSettings
                ? {
                    visibility: true,
                    onVisibilityChange: this.handleSettingsChange,
                    grouping: true,
                    onGroupingChange: this.handleSettingsChange,
                  }
                : undefined
            }
          />
        </StyledExampleTable>
      </StyledExampleContainer>
    );
  }
}
