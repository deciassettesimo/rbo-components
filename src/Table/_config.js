import { LOCALES, DIMENSIONS, COLUMNS_ALIGN, HEAD_BG_COLORS } from './_constants';

const REFS = {
  LOCALES,
  DIMENSIONS,
  COLUMNS_ALIGN,
  HEAD_BG_COLORS,
};

export default REFS;
