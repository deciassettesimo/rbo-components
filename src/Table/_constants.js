import { GLOBAL, STYLES } from '../_constants';

export const COMPONENTS = {
  GENERAL: 'Table',
  BODY: 'TableBody',
  GROUP: 'TableGroup',
  HEAD: 'TableHead',
  ITEM: 'TableItem',
  OPERATIONS: 'TableOperations',
  OPERATIONS_ITEM: 'TableOperationsItem',
  OPERATIONS_POPUP_ITEM: 'TableOperationsPopupItem',
  RESIZER: 'TableResizer',
  SETTINGS: 'TableSettings',
  SETTINGS_GROUPING: 'TableSettingsGrouping',
  SETTINGS_VISIBILITY: 'TableSettingsVisibility',
};

export const MIN_COLUMN_WIDTH = 80;

export const SELECT_CHANGE_TYPES = {
  GROUP: 'group',
  ITEM: 'item',
};

export const LOCALES = {
  EN: GLOBAL.LOCALES.EN,
  RU: GLOBAL.LOCALES.RU,
};

export const DIMENSIONS = {
  S: 12,
  M: 14,
  L: 16,
};

export const LABELS = {
  ...GLOBAL.LABELS.TABLE,
};

export const COLUMNS_ALIGN = {
  LEFT: 'left',
  CENTER: 'center',
  RIGHT: 'right',
};

export const HEAD_BG_COLORS = {
  WHITE: STYLES.COLORS.WHITE,
  GRAY: STYLES.COLORS.WHITE_THREE,
};

export const SELECT_ALL_ID = 'TableSelectAll';
export const OPERATIONS_POPUP_OPENER_ID = 'TableOperationsPopupOpener';
export const SETTINGS_GROUPING_SELECT_ID = 'TableSettingsGroupingSelect';
export const SETTINGS_GROUPING_RESET_VALUE = 'reset';
