import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Label from '../../Styles/Label';
import Loader from '../../Loader';
import TableGroup from './TableGroup';
import TableItem from './TableItem';
import { StyledTableBody, StyledTableBodyInner, StyledTableBodyContent, StyledTableBodyEmpty } from '../_style';
import { COMPONENTS, LOCALES, DIMENSIONS, LABELS, COLUMNS_ALIGN } from '../_constants';

const getEmptyLabel = locale => {
  switch (locale) {
    case LOCALES.EN:
      return LABELS.EN.EMPTY;
    case LOCALES.RU:
    default:
      return LABELS.RU.EMPTY;
  }
};

const TableBody = forwardRef((props, ref) => {
  const itemProps = {
    locale: props.locale,
    dimension: props.dimension,
    columns: props.columns,
    cellRenderer: props.cellRenderer,
    isMultiLine: props.isMultiLine,
    operations: props.operations,
    isOperationsCompact: props.isOperationsCompact,
    onOperationClick: props.onOperationClick,
    isSelectable: props.isSelectable,
    onSelectChange: props.onSelectChange,
    parentNodeWidth: props.parentNodeWidth,
    parentNodeScrollLeft: props.parentNodeScrollLeft,
  };

  return (
    <StyledTableBody {...addDataAttributes({ component: COMPONENTS.BODY })} innerRef={ref}>
      {props.isLoading && <Loader isCentered dimension={Loader.REFS.DIMENSIONS.L} />}
      {!props.isLoading &&
        !!props.items.length && (
          <StyledTableBodyInner>
            <StyledTableBodyContent sDimension={props.dimension}>
              {props.isGrouped &&
                props.items.map(item => (
                  <TableGroup
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    items={item.items}
                    onItemClick={props.onItemClick}
                    {...itemProps}
                  />
                ))}
              {!props.isGrouped &&
                props.items.map(item => (
                  <TableItem
                    key={item.id}
                    id={item.id}
                    data={item}
                    isWarning={item.isWarning}
                    isError={item.isError}
                    isSelectDisabled={item.isSelectDisabled}
                    isSelected={item.isSelected}
                    onClick={props.onItemClick}
                    {...itemProps}
                  />
                ))}
            </StyledTableBodyContent>
          </StyledTableBodyInner>
        )}
      {!props.isLoading &&
        !props.items.length && (
          <StyledTableBodyEmpty>
            <Label>
              {!props.emptyLabel && getEmptyLabel(props.locale)}
              {props.emptyLabel}
            </Label>
          </StyledTableBodyEmpty>
        )}
    </StyledTableBody>
  );
});

TableBody.propTypes = {
  locale: PropTypes.oneOf(Object.values(LOCALES)),
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string,
      items: PropTypes.arrayOf(PropTypes.shape()),
      isWarning: PropTypes.bool,
      isError: PropTypes.bool,
      isSelected: PropTypes.bool,
      isSelectDisabled: PropTypes.bool,
      operations: PropTypes.shape(),
    }),
  ).isRequired,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      isVisible: PropTypes.bool,
      width: PropTypes.number.isRequired,
      align: PropTypes.oneOf(Object.values(COLUMNS_ALIGN)),
      label: PropTypes.string,
    }),
  ).isRequired,
  operations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string,
      icon: PropTypes.string,
    }),
  ),
  isGrouped: PropTypes.bool,
  isLoading: PropTypes.bool,
  isMultiLine: PropTypes.bool,
  emptyLabel: PropTypes.string,
  cellRenderer: PropTypes.func,
  onItemClick: PropTypes.func,
  isOperationsCompact: PropTypes.bool,
  onOperationClick: PropTypes.func,
  isSelectable: PropTypes.bool,
  onSelectChange: PropTypes.func,
  parentNodeWidth: PropTypes.number,
  parentNodeScrollLeft: PropTypes.number,
};

TableBody.defaultProps = {
  locale: LOCALES.RU,
  dimension: DIMENSIONS.M,
  operations: [],
  isGrouped: false,
  isLoading: false,
  isMultiLine: false,
  emptyLabel: null,
  cellRenderer: null,
  onItemClick: null,
  isOperationsCompact: false,
  onOperationClick: null,
  isSelectable: false,
  onSelectChange: null,
  parentNodeWidth: null,
  parentNodeScrollLeft: null,
};

export default TableBody;
