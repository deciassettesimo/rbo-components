import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import InputCheckbox from '../../Input/InputCheckbox';
import { COMPONENTS, SELECT_CHANGE_TYPES, LOCALES, DIMENSIONS, COLUMNS_ALIGN } from '../_constants';
import { getIsAllSelected, getIsAllDisabled } from '../_utils';
import {
  StyledTableGroup,
  StyledTableGroupHeader,
  StyledTableGroupTitle,
  StyledTableGroupContent,
  StyledTableSelect,
} from '../_style';
import TableItem from './TableItem';

const TableGroup = props => {
  const handleSelectChange = params => {
    props.onSelectChange({ type: SELECT_CHANGE_TYPES.GROUP, ...params });
  };

  const itemProps = {
    locale: props.locale,
    dimension: props.dimension,
    columns: props.columns,
    cellRenderer: props.cellRenderer,
    isMultiLine: props.isMultiLine,
    operations: props.operations,
    isOperationsCompact: props.isOperationsCompact,
    onOperationClick: props.onOperationClick,
    isSelectable: props.isSelectable,
    onSelectChange: props.onSelectChange,
    parentNodeWidth: props.parentNodeWidth,
    parentNodeScrollLeft: props.parentNodeScrollLeft,
  };

  return (
    <StyledTableGroup {...addDataAttributes({ component: COMPONENTS.GROUP })}>
      <StyledTableGroupHeader sDimension={props.dimension} isSelectable={props.isSelectable}>
        {props.isSelectable && (
          <StyledTableSelect sDimension={props.dimension}>
            <InputCheckbox
              id={props.id}
              dimension={
                props.dimension === DIMENSIONS.S ? InputCheckbox.REFS.DIMENSIONS.XS : InputCheckbox.REFS.DIMENSIONS.S
              }
              checked={getIsAllSelected(props.items)}
              disabled={getIsAllDisabled(props.items)}
              onChange={handleSelectChange}
            />
          </StyledTableSelect>
        )}
        <StyledTableGroupTitle isMultiLine={props.isMultiLine}>{props.title}</StyledTableGroupTitle>
      </StyledTableGroupHeader>
      <StyledTableGroupContent>
        {props.items.map(item => (
          <TableItem
            key={item.id}
            id={item.id}
            data={item}
            isWarning={item.isWarning}
            isError={item.isError}
            isSelectable={props.isSelectable}
            isSelectDisabled={item.isSelectDisabled}
            isSelected={item.isSelected}
            onClick={props.onItemClick}
            {...itemProps}
          />
        ))}
      </StyledTableGroupContent>
    </StyledTableGroup>
  );
};

TableGroup.propTypes = {
  locale: PropTypes.oneOf(Object.values(LOCALES)),
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  id: PropTypes.string.isRequired,
  title: PropTypes.string,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      isVisible: PropTypes.bool,
      width: PropTypes.number.isRequired,
      align: PropTypes.oneOf(Object.values(COLUMNS_ALIGN)),
      label: PropTypes.string,
    }),
  ).isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      isWarning: PropTypes.bool,
      isError: PropTypes.bool,
      isSelected: PropTypes.bool,
      isSelectDisabled: PropTypes.bool,
      operations: PropTypes.shape(),
    }),
  ),
  cellRenderer: PropTypes.func,
  operations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string,
      icon: PropTypes.string,
    }),
  ),
  isMultiLine: PropTypes.bool,
  onItemClick: PropTypes.func,
  isOperationsCompact: PropTypes.bool,
  onOperationClick: PropTypes.func,
  isSelectable: PropTypes.bool,
  onSelectChange: PropTypes.func,
  parentNodeWidth: PropTypes.number,
  parentNodeScrollLeft: PropTypes.number,
};

TableGroup.defaultProps = {
  locale: LOCALES.RU,
  dimension: DIMENSIONS.M,
  title: null,
  items: [],
  cellRenderer: null,
  operations: [],
  isMultiLine: false,
  onItemClick: null,
  isOperationsCompact: false,
  onOperationClick: null,
  isSelectable: false,
  onSelectChange: () => null,
  parentNodeWidth: null,
  parentNodeScrollLeft: null,
};

export default TableGroup;
