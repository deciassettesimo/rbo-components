import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Icon from '../../Icon';
import InputCheckbox from '../../Input/InputCheckbox';
import {
  StyledTableHead,
  StyledTableHeadInner,
  StyledTableRow,
  StyledTableCell,
  StyledTableValue,
  StyledTableSorting,
  StyledTableSelect,
} from '../_style';
import { COMPONENTS, DIMENSIONS, COLUMNS_ALIGN, HEAD_BG_COLORS, SELECT_ALL_ID } from '../_constants';
import { getIsAllSelected, getIsAllDisabled } from '../_utils';
import TableResizer from './TableResizer';

const getIconSortingType = sorting => {
  switch (sorting) {
    case 1:
      return Icon.REFS.TYPES.SORTING_ASC;
    case -1:
      return Icon.REFS.TYPES.SORTING_DESC;
    case 0:
    default:
      return Icon.REFS.TYPES.SORTING;
  }
};

const TableHead = forwardRef((props, ref) => (
  <StyledTableHead {...addDataAttributes({ component: COMPONENTS.HEAD })} innerRef={ref} sBgColor={props.bgColor}>
    <StyledTableHeadInner isSelectable={props.isSelectable} sDimension={props.dimension}>
      {props.isSelectable && (
        <StyledTableSelect sDimension={props.dimension} isInHead>
          <InputCheckbox
            id={SELECT_ALL_ID}
            dimension={
              props.dimension === DIMENSIONS.S ? InputCheckbox.REFS.DIMENSIONS.XS : InputCheckbox.REFS.DIMENSIONS.S
            }
            checked={getIsAllSelected(props.items, props.isGrouped)}
            disabled={getIsAllDisabled(props.items, props.isGrouped)}
            onChange={props.onSelectChange}
          />
        </StyledTableSelect>
      )}
      <StyledTableRow>
        {props.columns.filter(column => column.isVisible).map(column => (
          <StyledTableCell key={column.id} sWidth={column.width} sAlign={column.align}>
            <StyledTableValue
              title={!props.isMultiLine ? column.label : undefined}
              isMultiLine={props.isMultiLine}
              isWithSorting={props.isSortable && !column.isSortingDisabled}
              onClick={() => {
                if (!column.isSortingDisabled && props.onSortingChange) props.onSortingChange({ id: column.id });
              }}
            >
              {column.label}
              {!!props.isSortable &&
                !column.isSortingDisabled && (
                  <StyledTableSorting>
                    <Icon
                      type={getIconSortingType(column.sorting)}
                      dimension={Icon.REFS.DIMENSIONS.XXS}
                      display={Icon.REFS.DISPLAY.BLOCK}
                    />
                  </StyledTableSorting>
                )}
            </StyledTableValue>
            {props.isResizable && <TableResizer id={column.id} width={column.width} onChange={props.onResizeChange} />}
          </StyledTableCell>
        ))}
      </StyledTableRow>
    </StyledTableHeadInner>
  </StyledTableHead>
));

TableHead.propTypes = {
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  items: PropTypes.arrayOf(PropTypes.shape()),
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      isVisible: PropTypes.bool,
      width: PropTypes.number.isRequired,
      align: PropTypes.oneOf(Object.values(COLUMNS_ALIGN)),
      label: PropTypes.string,
      isSortingDisabled: PropTypes.bool,
    }),
  ).isRequired,
  isMultiLine: PropTypes.bool,
  bgColor: PropTypes.oneOf(Object.values(HEAD_BG_COLORS)),
  isSortable: PropTypes.bool,
  onSortingChange: PropTypes.func,
  isResizable: PropTypes.bool,
  onResizeChange: PropTypes.func,
  isGrouped: PropTypes.bool,
  isSelectable: PropTypes.bool,
  onSelectChange: PropTypes.func,
};

TableHead.defaultProps = {
  dimension: DIMENSIONS.M,
  items: [],
  isMultiLine: false,
  bgColor: HEAD_BG_COLORS.WHITE,
  isSortable: false,
  onSortingChange: null,
  isResizable: false,
  onResizeChange: null,
  isGrouped: false,
  isSelectable: false,
  onSelectChange: null,
};

export default TableHead;
