import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import InputCheckbox from '../../Input/InputCheckbox';
import { COMPONENTS, SELECT_CHANGE_TYPES, LOCALES, DIMENSIONS, COLUMNS_ALIGN } from '../_constants';
import { getAllowedOperations } from '../_utils';
import { StyledTableItem, StyledTableSelect, StyledTableRow, StyledTableCell, StyledTableValue } from '../_style';
import TableOperations from './TableOperations';

export default class TableItem extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
    id: PropTypes.string.isRequired,
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        isVisible: PropTypes.bool,
        width: PropTypes.number.isRequired,
        align: PropTypes.oneOf(Object.values(COLUMNS_ALIGN)),
        label: PropTypes.string,
      }),
    ).isRequired,
    data: PropTypes.shape({
      operations: PropTypes.shape(),
    }).isRequired,
    cellRenderer: PropTypes.func,
    operations: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        icon: PropTypes.string,
      }),
    ),
    isWarning: PropTypes.bool,
    isError: PropTypes.bool,
    isMultiLine: PropTypes.bool,
    onClick: PropTypes.func,
    isOperationsCompact: PropTypes.bool,
    onOperationClick: PropTypes.func,
    isSelectable: PropTypes.bool,
    isSelected: PropTypes.bool,
    isSelectDisabled: PropTypes.bool,
    onSelectChange: PropTypes.func,
    parentNodeWidth: PropTypes.number,
    parentNodeScrollLeft: PropTypes.number,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    dimension: DIMENSIONS.M,
    cellRenderer: null,
    operations: [],
    isWarning: false,
    isError: false,
    isMultiLine: false,
    onClick: null,
    isOperationsCompact: false,
    onOperationClick: null,
    isSelectable: false,
    isSelected: false,
    isSelectDisabled: false,
    onSelectChange: () => null,
    parentNodeWidth: null,
    parentNodeScrollLeft: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isHovered: false,
      isOperationsPopupOpened: false,
    };

    this.node = React.createRef();
  }

  handleMouseEnter = () => {
    if (this.props.onClick || this.props.onOperationClick) this.setState({ isHovered: true });
  };

  handleMouseLeave = () => {
    this.setState({ isHovered: false });
  };

  handleClick = () => {
    if (this.props.onClick) this.props.onClick({ id: this.props.id });
  };

  handleOperationClick = operationId => {
    if (this.props.onOperationClick) this.props.onOperationClick({ operationId, itemId: this.props.id });
  };

  handleOperationsPopupOpen = () => {
    this.setState({ isOperationsPopupOpened: true });
  };

  handleOperationsPopupClose = () => {
    this.setState({ isOperationsPopupOpened: false, isHovered: false });
  };

  handleSelectClick = e => {
    e.stopPropagation();
  };

  handleSelectChange = params => {
    this.props.onSelectChange({ type: SELECT_CHANGE_TYPES.ITEM, ...params });
  };

  render() {
    return (
      <StyledTableItem
        {...addDataAttributes({ component: COMPONENTS.ITEM, id: this.props.id })}
        innerRef={ref => {
          this.node.current = ref;
        }}
        sDimension={this.props.dimension}
        isWarning={this.props.isWarning}
        isError={this.props.isError}
        isClickable={!!this.props.onClick}
        isSelectable={this.props.isSelectable}
        isSelected={this.props.isSelected}
        isHovered={this.state.isHovered || this.state.isOperationsPopupOpened}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        onClick={this.handleClick}
      >
        {this.props.isSelectable && (
          <StyledTableSelect sDimension={this.props.dimension} onClick={this.handleSelectClick}>
            <InputCheckbox
              id={this.props.id}
              dimension={
                this.props.dimension === DIMENSIONS.S
                  ? InputCheckbox.REFS.DIMENSIONS.XS
                  : InputCheckbox.REFS.DIMENSIONS.S
              }
              checked={this.props.isSelected}
              disabled={this.props.isSelectDisabled}
              onChange={this.handleSelectChange}
            />
          </StyledTableSelect>
        )}
        <StyledTableRow>
          {this.props.columns.filter(column => column.isVisible).map(column => (
            <StyledTableCell key={column.id} sWidth={column.width} sAlign={column.align}>
              <StyledTableValue isMultiLine={this.props.isMultiLine}>
                {this.props.cellRenderer && this.props.cellRenderer({ column: column.id, data: this.props.data })}
                {!this.props.cellRenderer && this.props.data[column.id]}
              </StyledTableValue>
            </StyledTableCell>
          ))}
        </StyledTableRow>
        {!!this.props.operations.length &&
          this.props.onOperationClick &&
          this.state.isHovered && (
            <TableOperations
              locale={this.props.locale}
              dimension={this.props.dimension}
              isCompact={this.props.isOperationsCompact}
              parentNodeWidth={this.props.parentNodeWidth}
              parentNodeScrollLeft={this.props.parentNodeScrollLeft}
              items={getAllowedOperations(this.props.operations, this.props.data.operations)}
              onItemClick={this.handleOperationClick}
              onPopupOpen={this.handleOperationsPopupOpen}
              onPopupClose={this.handleOperationsPopupClose}
            />
          )}
      </StyledTableItem>
    );
  }
}
