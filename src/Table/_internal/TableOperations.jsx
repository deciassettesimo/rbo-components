import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Popup, { Opener, Box } from '../../Popup';
import Card from '../../Card';
import { TYPES as ICON_TYPES } from '../../Icon/_constants';
import { StyledTableOperations, StyledTableOperationsInner, StyledTableOperationsPopup } from '../_style';
import { COMPONENTS, LOCALES, DIMENSIONS, LABELS, OPERATIONS_POPUP_OPENER_ID } from '../_constants';
import TableOperationsItem from './TableOperationsItem';
import TableOperationsPopupItem from './TableOperationsPopupItem';

export default class TableOperations extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
    isCompact: PropTypes.bool,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        icon: PropTypes.string,
      }),
    ),
    onItemClick: PropTypes.func,
    parentNodeWidth: PropTypes.number,
    parentNodeScrollLeft: PropTypes.number,
    onPopupOpen: PropTypes.func.isRequired,
    onPopupClose: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    dimension: DIMENSIONS.M,
    isCompact: false,
    items: [],
    onItemClick: null,
    parentNodeWidth: null,
    parentNodeScrollLeft: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpened: false,
    };
  }

  componentWillUnmount() {
    if (this.state.isOpened) this.props.onPopupClose();
  }

  onPopupOpen = () => {
    this.setState({ isOpened: true });
    this.props.onPopupOpen();
  };

  onPopupClose = () => {
    this.setState({ isOpened: false });
    this.props.onPopupClose();
  };

  getPopupOpenerLabel = locale => {
    switch (locale) {
      case LOCALES.EN:
        return LABELS.EN.OPERATIONS_POPUP_OPENER;
      case LOCALES.RU:
      default:
        return LABELS.RU.OPERATIONS_POPUP_OPENER;
    }
  };

  handlePopupItemClick = id => {
    this.props.onItemClick(id);
    this.onPopupClose();
  };

  render() {
    return (
      <StyledTableOperations
        {...addDataAttributes({ component: COMPONENTS.OPERATIONS })}
        sParentNodeWidth={this.props.parentNodeWidth}
        sParentNodeScrollLeft={this.props.parentNodeScrollLeft}
      >
        <StyledTableOperationsInner>
          {!this.props.isCompact &&
            this.props.items.map(item => (
              <TableOperationsItem
                key={item.id}
                id={item.id}
                title={item.title}
                icon={item.icon}
                onClick={this.props.onItemClick}
              />
            ))}
          {this.props.isCompact && (
            <Popup isOpened={this.state.isOpened} onClose={this.onPopupClose}>
              <Opener display={Opener.REFS.DISPLAY.BLOCK} isNotAutoOpen>
                <TableOperationsItem
                  id={OPERATIONS_POPUP_OPENER_ID}
                  title={this.getPopupOpenerLabel(this.props.locale)}
                  icon={ICON_TYPES.CONTEXT_VERTICAL}
                  onClick={this.onPopupOpen}
                />
              </Opener>
              <Box align={Box.REFS.ALIGN.END}>
                <Card isShadowed>
                  <StyledTableOperationsPopup>
                    {this.props.items.map(item => (
                      <TableOperationsPopupItem
                        key={item.id}
                        dimension={this.props.dimension}
                        id={item.id}
                        title={item.title}
                        icon={item.icon}
                        onClick={this.handlePopupItemClick}
                      />
                    ))}
                  </StyledTableOperationsPopup>
                </Card>
              </Box>
            </Popup>
          )}
        </StyledTableOperationsInner>
      </StyledTableOperations>
    );
  }
}
