import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import ClickableIcon from '../../ClickableIcon';
import { StyledTableOperationsItem } from '../_style';
import { COMPONENTS } from '../_constants';

const TableOperationsItem = props => {
  const handleClick = e => {
    e.stopPropagation();
    props.onClick(props.id);
  };

  return (
    <StyledTableOperationsItem {...addDataAttributes({ component: COMPONENTS.OPERATIONS_ITEM, id: props.id })}>
      <ClickableIcon
        type={props.icon}
        dimension={ClickableIcon.REFS.DIMENSIONS.XS}
        title={props.title}
        onClick={handleClick}
      />
    </StyledTableOperationsItem>
  );
};

TableOperationsItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string,
  icon: PropTypes.oneOf(Object.values(ClickableIcon.REFS.TYPES)),
  onClick: PropTypes.func.isRequired,
};

TableOperationsItem.defaultProps = {
  title: null,
  icon: null,
};

export default TableOperationsItem;
