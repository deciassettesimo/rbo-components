import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Icon from '../../Icon';
import { COMPONENTS, DIMENSIONS } from '../_constants';
import {
  StyledTableOperationsPopupItem,
  StyledTableOperationsPopupItemIcon,
  StyledTableOperationsPopupItemTitle,
} from '../_style';

const TableOperationsPopupItem = props => {
  const handleOnClick = () => {
    props.onClick(props.id);
  };

  return (
    <StyledTableOperationsPopupItem
      {...addDataAttributes({ component: COMPONENTS.OPERATIONS_POPUP_ITEM, id: props.id })}
      sDimension={props.dimension}
      onClick={handleOnClick}
    >
      {props.icon && (
        <StyledTableOperationsPopupItemIcon>
          <Icon type={props.icon} display={Icon.REFS.DISPLAY.BLOCK} dimension={Icon.REFS.DIMENSIONS.XS} />
        </StyledTableOperationsPopupItemIcon>
      )}
      {props.title && <StyledTableOperationsPopupItemTitle>{props.title}</StyledTableOperationsPopupItemTitle>}
    </StyledTableOperationsPopupItem>
  );
};

TableOperationsPopupItem.propTypes = {
  id: PropTypes.string.isRequired,
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  title: PropTypes.string,
  icon: PropTypes.oneOf(Object.values(Icon.REFS.TYPES)),
  onClick: PropTypes.func.isRequired,
};

TableOperationsPopupItem.defaultProps = {
  dimension: DIMENSIONS.M,
  title: null,
  icon: null,
};

export default TableOperationsPopupItem;
