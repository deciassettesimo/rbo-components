import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { StyledTableResizer } from '../_style';
import { COMPONENTS } from '../_constants';

export default class TableResizer extends PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    onChange: () => null,
  };

  constructor(props) {
    super(props);

    this.state = {
      width: this.props.width,
      isTaken: false,
      clientX: null,
    };

    this.node = React.createRef();
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleMouseDown);
    document.addEventListener('mousemove', this.handleMouseMove);
    document.addEventListener('mouseup', this.handleMouseUp);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleMouseDown);
    document.removeEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseup', this.handleMouseUp);
  }

  handleMouseDown = e => {
    if (e.target === this.node.current) {
      e.preventDefault();
      this.setState({
        width: this.props.width,
        isTaken: true,
        clientX: e.clientX,
      });
      this.props.onChange({ id: this.props.id, width: this.props.width, isStarted: true });
    }
  };

  handleMouseMove = e => {
    if (this.state.isTaken) {
      e.preventDefault();
      const width = this.state.width - (this.state.clientX - e.clientX);
      this.props.onChange({ id: this.props.id, width });
    }
  };

  handleMouseUp = e => {
    if (this.state.isTaken) {
      e.preventDefault();
      const width = this.state.width - (this.state.clientX - e.clientX);
      this.setState({ isTaken: false });
      this.props.onChange({ id: this.props.id, width, isFinished: true });
    }
  };

  render() {
    return (
      <StyledTableResizer
        {...addDataAttributes({ component: COMPONENTS.RESIZER })}
        innerRef={ref => {
          this.node.current = ref;
        }}
      />
    );
  }
}
