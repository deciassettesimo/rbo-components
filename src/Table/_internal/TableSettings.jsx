import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import ClickableIcon from '../../ClickableIcon/ClickableIcon';
import Popup, { Opener, Box } from '../../Popup';
import Card from '../../Card';

import { COMPONENTS, LOCALES, LABELS, DIMENSIONS, COLUMNS_ALIGN, HEAD_BG_COLORS } from '../_constants';
import { StyledTableSettings, StyledTableSettingsPopup, StyledTableSettingsPopupClose } from '../_style';
import TableSettingsVisibility from './TableSettingsVisibility';
import TableSettingsGrouping from './TableSettingsGrouping';

export default class TableSettings extends PureComponent {
  static propTypes = {
    locale: PropTypes.oneOf(Object.values(LOCALES)),
    dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
    bgColor: PropTypes.oneOf(Object.values(HEAD_BG_COLORS)),
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        isVisible: PropTypes.bool,
        width: PropTypes.number.isRequired,
        align: PropTypes.oneOf(Object.values(COLUMNS_ALIGN)),
        label: PropTypes.string,
        grouping: PropTypes.bool,
        isGrouped: PropTypes.bool,
        isSortingDisabled: PropTypes.bool,
      }),
    ).isRequired,
    visibility: PropTypes.bool,
    onVisibilityChange: PropTypes.func.isRequired,
    grouping: PropTypes.bool,
    onGroupingChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    locale: LOCALES.RU,
    dimension: DIMENSIONS.M,
    bgColor: HEAD_BG_COLORS.WHITE,
    visibility: false,
    grouping: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      isPopupOpened: false,
    };
  }

  onPopupOpen = () => {
    this.setState({ isPopupOpened: true });
  };

  onPopupClose = () => {
    this.setState({ isPopupOpened: false });
  };

  getPopupOpenerLabel = locale => {
    switch (locale) {
      case LOCALES.EN:
        return LABELS.EN.SETTINGS_POPUP_OPENER;
      case LOCALES.RU:
      default:
        return LABELS.RU.SETTINGS_POPUP_OPENER;
    }
  };

  getPopupCloseLabel = locale => {
    switch (locale) {
      case LOCALES.EN:
        return LABELS.EN.SETTINGS_POPUP_CLOSE;
      case LOCALES.RU:
      default:
        return LABELS.RU.SETTINGS_POPUP_CLOSE;
    }
  };

  handleVisibilitySelectAll = () => {
    const columns = this.props.columns.map(column => ({ ...column, isVisible: true }));
    this.props.onVisibilityChange(columns);
  };

  handleVisibilityChange = ({ id, value }) => {
    const columns = this.props.columns.map(column => (column.id === id ? { ...column, isVisible: value } : column));
    this.props.onVisibilityChange(columns);
  };

  handleGroupingChange = ({ value }) => {
    const columns = this.props.columns.map(column => ({ ...column, isGrouped: column.id === value }));
    this.props.onGroupingChange(columns);
  };

  render() {
    return (
      <StyledTableSettings
        {...addDataAttributes({ component: COMPONENTS.SETTINGS })}
        sDimension={this.props.dimension}
        sBgColor={this.props.bgColor}
      >
        <Popup isOpened={this.state.isPopupOpened} onClose={this.onPopupClose}>
          <Opener display={Opener.REFS.DISPLAY.BLOCK} isNotAutoOpen>
            <ClickableIcon
              type={ClickableIcon.REFS.TYPES.SETTINGS}
              display={ClickableIcon.REFS.DISPLAY.BLOCK}
              dimension={
                this.props.dimension === DIMENSIONS.L
                  ? ClickableIcon.REFS.DIMENSIONS.S
                  : ClickableIcon.REFS.DIMENSIONS.XS
              }
              title={this.getPopupOpenerLabel(this.props.locale)}
              isExpanded
              onClick={this.onPopupOpen}
            />
          </Opener>
          <Box align={Box.REFS.ALIGN.END}>
            <Card isShadowed>
              <StyledTableSettingsPopup sDimension={this.props.dimension}>
                {this.props.visibility && (
                  <TableSettingsVisibility
                    locale={this.props.locale}
                    dimension={this.props.dimension}
                    columns={this.props.columns}
                    onSelectAll={this.handleVisibilitySelectAll}
                    onChange={this.handleVisibilityChange}
                  />
                )}
                {this.props.grouping && (
                  <TableSettingsGrouping
                    locale={this.props.locale}
                    dimension={this.props.dimension}
                    columns={this.props.columns}
                    onChange={this.handleGroupingChange}
                  />
                )}
                <StyledTableSettingsPopupClose>
                  <ClickableIcon
                    type={ClickableIcon.REFS.TYPES.CLOSE}
                    display={ClickableIcon.REFS.DISPLAY.BLOCK}
                    dimension={ClickableIcon.REFS.DIMENSIONS.XS}
                    title={this.getPopupCloseLabel(this.props.locale)}
                    isExpanded
                    onClick={this.onPopupClose}
                  />
                </StyledTableSettingsPopupClose>
              </StyledTableSettingsPopup>
            </Card>
          </Box>
        </Popup>
      </StyledTableSettings>
    );
  }
}
