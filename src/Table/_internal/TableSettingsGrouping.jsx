import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Header from '../../Styles/Header';
import InputSelect from '../../Input/InputSelect';
import {
  StyledTableSettingsBlock,
  StyledTableSettingsBlockHeader,
  StyledTableSettingsBlockHeaderTitle,
  StyledTableSettingsBlockContent,
} from '../_style';
import {
  COMPONENTS,
  LOCALES,
  DIMENSIONS,
  LABELS,
  SETTINGS_GROUPING_SELECT_ID,
  SETTINGS_GROUPING_RESET_VALUE,
} from '../_constants';

const getTitle = locale => {
  switch (locale) {
    case LOCALES.EN:
      return LABELS.EN.SETTINGS_GROUPING_TITLE;
    case LOCALES.RU:
    default:
      return LABELS.RU.SETTINGS_GROUPING_TITLE;
  }
};

const getResetTitle = locale => {
  switch (locale) {
    case LOCALES.EN:
      return LABELS.EN.SETTINGS_GROUPING_RESET;
    case LOCALES.RU:
    default:
      return LABELS.RU.SETTINGS_GROUPING_RESET;
  }
};

const getOptions = (columns, locale) => {
  const options = columns.filter(column => column.grouping).map(column => ({ value: column.id, title: column.label }));
  options.unshift({ value: SETTINGS_GROUPING_RESET_VALUE, title: getResetTitle(locale) });
  return options;
};

const getValue = columns => {
  const selected = columns.find(column => column.isGrouped);
  return selected ? selected.id : SETTINGS_GROUPING_RESET_VALUE;
};

const TableSettingsGrouping = props => (
  <StyledTableSettingsBlock {...addDataAttributes({ component: COMPONENTS.SETTINGS_GROUPING })}>
    <StyledTableSettingsBlockHeader>
      <StyledTableSettingsBlockHeaderTitle>
        <Header size={6}>{getTitle(props.locale)}</Header>
      </StyledTableSettingsBlockHeaderTitle>
    </StyledTableSettingsBlockHeader>
    <StyledTableSettingsBlockContent>
      <InputSelect
        id={SETTINGS_GROUPING_SELECT_ID}
        options={getOptions(props.columns, props.locale)}
        value={getValue(props.columns)}
        dimension={props.dimension === DIMENSIONS.S ? InputSelect.REFS.DIMENSIONS.XS : InputSelect.REFS.DIMENSIONS.S}
        onChange={props.onChange}
      />
    </StyledTableSettingsBlockContent>
  </StyledTableSettingsBlock>
);

TableSettingsGrouping.propTypes = {
  locale: PropTypes.oneOf(Object.values(LOCALES)),
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string,
      grouping: PropTypes.bool,
      isGrouped: PropTypes.bool,
    }),
  ).isRequired,
  onChange: PropTypes.func.isRequired,
};

TableSettingsGrouping.defaultProps = {
  locale: LOCALES.RU,
  dimension: DIMENSIONS.M,
};

export default TableSettingsGrouping;
