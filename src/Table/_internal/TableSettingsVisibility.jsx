import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import Header from '../../Styles/Header';
import RboLink from '../../Styles/Link';
import InputCheckbox from '../../Input/InputCheckbox';
import {
  StyledTableSettingsBlock,
  StyledTableSettingsBlockHeader,
  StyledTableSettingsBlockHeaderTitle,
  StyledTableSettingsBlockHeaderSelectAll,
  StyledTableSettingsBlockContent,
  StyledTableSettingsVisibilityItems,
  StyledTableSettingsVisibilityItem,
} from '../_style';
import { COMPONENTS, LOCALES, DIMENSIONS, LABELS } from '../_constants';

const getTitle = locale => {
  switch (locale) {
    case LOCALES.EN:
      return LABELS.EN.SETTINGS_VISIBILITY_TITLE;
    case LOCALES.RU:
    default:
      return LABELS.RU.SETTINGS_VISIBILITY_TITLE;
  }
};

const getSelectAllLabel = locale => {
  switch (locale) {
    case LOCALES.EN:
      return LABELS.EN.SETTINGS_VISIBILITY_SELECT_ALL_TITLE;
    case LOCALES.RU:
    default:
      return LABELS.RU.SETTINGS_VISIBILITY_SELECT_ALL_TITLE;
  }
};

const renderItem = params => (
  <StyledTableSettingsVisibilityItem key={params.id}>
    <InputCheckbox
      id={params.id}
      dimension={params.dimension === DIMENSIONS.S ? InputCheckbox.REFS.DIMENSIONS.XS : InputCheckbox.REFS.DIMENSIONS.S}
      checked={params.isVisible}
      disabled={params.disabled}
      onChange={params.onChange}
    >
      {params.label}
    </InputCheckbox>
  </StyledTableSettingsVisibilityItem>
);

const TableSettingsVisibility = props => {
  const isDeselectDisabled = props.columns.filter(column => column.isVisible).length === 1;
  return (
    <StyledTableSettingsBlock {...addDataAttributes({ component: COMPONENTS.SETTINGS_VISIBILITY })}>
      <StyledTableSettingsBlockHeader>
        <StyledTableSettingsBlockHeaderTitle>
          <Header size={6}>{getTitle(props.locale)}</Header>
        </StyledTableSettingsBlockHeaderTitle>
        <StyledTableSettingsBlockHeaderSelectAll>
          <RboLink isPseudo isDashed onClick={props.onSelectAll}>
            {getSelectAllLabel(props.locale)}
          </RboLink>
        </StyledTableSettingsBlockHeaderSelectAll>
      </StyledTableSettingsBlockHeader>
      <StyledTableSettingsBlockContent>
        <StyledTableSettingsVisibilityItems sDimension={props.dimension}>
          {props.columns.map(item =>
            renderItem({
              ...item,
              dimension: props.dimension,
              onChange: props.onChange,
              disabled: item.isVisible && isDeselectDisabled,
            }),
          )}
        </StyledTableSettingsVisibilityItems>
      </StyledTableSettingsBlockContent>
    </StyledTableSettingsBlock>
  );
};

TableSettingsVisibility.propTypes = {
  locale: PropTypes.oneOf(Object.values(LOCALES)),
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      isVisible: PropTypes.bool,
      label: PropTypes.string,
    }),
  ).isRequired,
  onChange: PropTypes.func.isRequired,
  onSelectAll: PropTypes.func.isRequired,
};

TableSettingsVisibility.defaultProps = {
  locale: LOCALES.RU,
  dimension: DIMENSIONS.M,
};

export default TableSettingsVisibility;
