import styled from 'styled-components';

import StyledClickableIcon from '../ClickableIcon/_style';
import { STYLES } from '../_constants';
import { DIMENSIONS } from './_constants';

const tableHeadFontSize = sDimension => {
  if (sDimension === DIMENSIONS.L) return 14;
  return 12;
};

const tableHeadLineHeight = sDimension => {
  if (sDimension === DIMENSIONS.L) return 20;
  return 16;
};

const tableBodyLineHeight = sDimension => {
  if (sDimension === DIMENSIONS.S) return 16;
  if (sDimension === DIMENSIONS.L) return 24;
  return 20;
};

const tableItemMinHeight = sDimension => {
  if (sDimension === DIMENSIONS.S) return 33;
  if (sDimension === DIMENSIONS.L) return 41;
  return 37;
};

const tableItemBackgroundColor = (isWarning, isError, isSelected, isHovered) => {
  if (isHovered) return STYLES.COLORS.ICE_BLUE;
  if (isSelected) return STYLES.COLORS.PARCHMENT;
  if (isError) return STYLES.COLORS.TOMATO_RED_12;
  if (isWarning) return STYLES.COLORS.MARIGOLD_12;
  return STYLES.COLORS.TRANSPARENT;
};

const tableSelectVPadding = (sDimension, isInHead) => {
  if (isInHead) {
    if (sDimension === DIMENSIONS.L) return 5;
    return 3;
  }
  if (sDimension === DIMENSIONS.S) return 4;
  if (sDimension === DIMENSIONS.L) return 4;
  return 2;
};

const tableOperationsLeft = (sParentNodeWidth, sParentNodeScrollLeft) => {
  if (sParentNodeWidth === null || sParentNodeScrollLeft === null) return {};
  const left = sParentNodeWidth + sParentNodeScrollLeft;
  return { left };
};

const tableOperationsPopupItemFontSize = sDimension => {
  if (sDimension === DIMENSIONS.L) return 14;
  return 12;
};

const tableOperationsPopupItemLineHeight = sDimension => {
  if (sDimension === DIMENSIONS.L) return 20;
  return 16;
};

const tableOperationsPopupItemHeight = sDimension => {
  if (sDimension === DIMENSIONS.L) return 36;
  return 32;
};

export const StyledTable = styled.div`
  position: relative;
  box-sizing: border-box;
  height: 100%;
  flex-grow: 1;
  display: flex;
  flex-flow: column nowrap;
  align-items: stretch;
`;

export const StyledTableRow = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  padding: 4px 0;
`;

export const StyledTableCell = styled.div.attrs({
  style: ({ sWidth }) => ({
    width: sWidth,
  }),
})`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: top;
  padding: 0 8px;
  text-align: ${props => props.sAlign};
  white-space: normal;
`;

export const StyledTableValue = styled.div`
  position: relative;
  box-sizing: border-box;
  width: 100%;
  white-space: ${props => (props.isMultiLine ? 'pre-line' : 'nowrap')};
  word-wrap: ${props => (props.isMultiLine ? 'break-word' : 'normal')};
  overflow: hidden;
  text-overflow: ellipsis;
  padding-right: ${props => (props.isWithSorting ? 16 : 0)}px;
  cursor: ${props => (props.isWithSorting ? 'pointer' : 'intherit')};
`;

export const StyledTableHead = styled.div`
  position: relative;
  box-sizing: border-box;
  flex-grow: 0;
  flex-shrink: 0;
  overflow: hidden;
  font-weight: bold;
  background: ${props => props.sBgColor};
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_12};
  padding-right: 32px;
  font-size: 0;
  line-height: 0;

  ${StyledTableCell} {
    display: flex;
    flex-flow: row nowrap;
    align-items: flex-end;
  }
`;

export const StyledTableHeadInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: top;
  white-space: nowrap;
  min-width: 100%;
  padding: 6px 32px 6px ${props => (props.isSelectable ? '68' : '32')}px;
  font-size: ${props => tableHeadFontSize(props.sDimension)}px;
  line-height: ${props => tableHeadLineHeight(props.sDimension)}px;
`;

export const StyledTableBody = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  flex-grow: 1;
  overflow: auto;
  min-height: 48px;
`;

export const StyledTableBodyInner = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: top;
  white-space: nowrap;
  min-width: 100%;
  font-size: 0;
  line-height: 0;
`;

export const StyledTableBodyContent = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  font-size: ${props => props.sDimension}px;
  line-height: ${props => tableBodyLineHeight(props.sDimension)}px;
`;

export const StyledTableBodyEmpty = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  box-sizing: border-box;
  display: block;
  text-align: center;
`;

export const StyledTableGroup = styled.div`
  position: relative;
  box-sizing: border-box;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_20};
`;

export const StyledTableGroupHeader = styled.div`
  position: relative;
  box-sizing: border-box;
  font-weight: bold;
  color: ${STYLES.COLORS.CERULEAN_BLUE};
  padding: 4px 32px 4px ${props => (props.isSelectable ? '68' : '32')}px;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_08};
  min-height: ${props => tableItemMinHeight(props.sDimension)}px;
`;

export const StyledTableGroupTitle = styled.div`
  position: relative;
  box-sizing: border-box;
  padding: 4px 8px;
  white-space: ${props => (props.isMultiLine ? 'pre-line' : 'nowrap')};
  word-wrap: ${props => (props.isMultiLine ? 'break-word' : 'normal')};
`;

export const StyledTableGroupContent = styled.div`
  position: relative;
  box-sizing: border-box;
  margin-bottom: -1px;
`;

export const StyledTableItem = styled.div`
  position: relative;
  box-sizing: border-box;
  border-bottom: 1px solid ${STYLES.COLORS.BLACK_12};
  background-color: ${props =>
    tableItemBackgroundColor(props.isWarning, props.isError, props.isSelected, props.isHovered)};
  transition: background-color 0.32s ease-out;
  cursor: ${props => (props.isClickable ? 'pointer' : 'default')};
  padding: 4px 32px 4px ${props => (props.isSelectable ? '68' : '32')}px;
  min-height: ${props => tableItemMinHeight(props.sDimension)}px;
`;

export const StyledTableSelect = styled.div`
  position: absolute;
  box-sizing: border-box;
  top: ${props => (props.isInHead ? 'auto' : 0)};
  bottom: ${props => (props.isInHead ? 0 : 'auto')};
  left: 32px;
  height: ${props => tableItemMinHeight(props.sDimension)}px;
  width: 36px;
  overflow: hidden;
  padding: ${props => tableSelectVPadding(props.sDimension, props.isInHead)}px 8px;
  cursor: default;
`;

export const StyledTableSorting = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;
  box-sizing: border-box;
  width: 16px;
  height: 16px;
  padding: 2px;
`;

export const StyledTableResizer = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  box-sizing: border-box;
  width: 13px;
  margin-right: -6px;
  padding: 2px;
  cursor: col-resize;
  z-index: 1;
  background: linear-gradient(to right, transparent, transparent 6px, ${STYLES.COLORS.BLACK_32} 6px, transparent 7px);
`;

export const StyledTableOperations = styled.div.attrs({
  style: ({ sParentNodeWidth, sParentNodeScrollLeft }) => ({
    ...tableOperationsLeft(sParentNodeWidth, sParentNodeScrollLeft),
  }),
})`
  position: absolute;
  box-sizing: border-box;
  top: 0;
  bottom: 0;
  width: 0;
`;

export const StyledTableOperationsInner = styled.div`
  position: absolute;
  box-sizing: border-box;
  top: 0;
  right: 0;
  height: 100%;
  padding: 8px 8px 8px 32px;
  background: linear-gradient(to right, ${STYLES.COLORS.TRANSPARENT} 0, ${STYLES.COLORS.ICE_BLUE} 24px);
  white-space: nowrap;
`;

export const StyledTableOperationsItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  height: 24px;
  width: 24px;
  background: ${STYLES.COLORS.WHITE};
  border-radius: 50%;
  overflow: hidden;
  margin: 0 4px;

  :first-child {
    margin-left: 0;
  }

  :last-child {
    margin-right: 0;
  }

  ${StyledClickableIcon} {
    padding: 4px;
    height: 24px;
    width: 24px;
  }
`;

export const StyledTableOperationsPopup = styled.div`
  padding: 8px 0;
`;

export const StyledTableOperationsPopupItem = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px;
  font-size: ${props => tableOperationsPopupItemFontSize(props.sDimension)}px;
  line-height: ${props => tableOperationsPopupItemLineHeight(props.sDimension)}px;
  height: ${props => tableOperationsPopupItemHeight(props.sDimension)}px;
  white-space: nowrap;
  cursor: pointer;
  transition: background-color 0.32s ease-out;

  &:hover {
    background-color: ${STYLES.COLORS.ICE_BLUE};
  }
`;

export const StyledTableOperationsPopupItemIcon = styled.div`
  display: inline-block;
  vertical-align: middle;
`;

export const StyledTableOperationsPopupItemTitle = styled.div`
  display: inline-block;
  vertical-align: middle;
  margin-left: 4px;
`;

export const StyledTableSettings = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  box-sizing: border-box;
  padding: ${props => (props.sDimension === DIMENSIONS.L ? 4 : 2)}px 0;
  background: radial-gradient(ellipse at 100%, ${props => props.sBgColor} 55%, ${STYLES.COLORS.TRANSPARENT} 70%);
  z-index: 1;
`;

export const StyledTableSettingsPopup = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
  padding: 16px;
  font-size: ${props => (props.sDimension === DIMENSIONS.S ? 12 : 14)}px;
  min-width: 240px;
  max-width: 320px;
`;

export const StyledTableSettingsPopupClose = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  box-sizing: border-box;
`;

export const StyledTableSettingsBlock = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
  margin: 16px 0;

  :first-child {
    margin-top: 0;
  }

  :last-child {
    margin-bottom: 0;
  }
`;

export const StyledTableSettingsBlockHeader = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
  margin-bottom: 4px;
`;

export const StyledTableSettingsBlockHeaderTitle = styled.div`
  position: relative;
  display: inline-block;
  box-sizing: border-box;
`;

export const StyledTableSettingsBlockHeaderSelectAll = styled.div`
  position: relative;
  display: inline-block;
  box-sizing: border-box;
  margin-left: 16px;
`;

export const StyledTableSettingsBlockContent = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
`;

export const StyledTableSettingsVisibilityItems = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
  margin-right: -16px;
  padding-right: 16px;
  max-height: 288px;
  overflow-y: auto;
`;

export const StyledTableSettingsVisibilityItem = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
`;
