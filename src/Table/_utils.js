export const getAllowedOperations = (operations, allowedOperations) => {
  if (!allowedOperations) return operations;
  return Object.keys(allowedOperations).map(key => operations.find(operation => operation.id === key));
};

export const getAllItemsFromGroups = items => {
  let allItems = [];
  items.forEach(group => {
    allItems = allItems.concat(group.items);
  });
  return allItems;
};

export const getIsAllSelected = (items, isGrouped) => {
  const allItems = isGrouped ? getAllItemsFromGroups(items) : items;
  const enabledItems = allItems.filter(item => !item.isSelectDisabled);
  return !!enabledItems.length && !enabledItems.filter(item => !item.isSelected).length;
};

export const getIsAllDisabled = (items, isGrouped) => {
  const allItems = isGrouped ? getAllItemsFromGroups(items) : items;
  const enabledItems = allItems.filter(item => !item.isSelectDisabled);
  return !enabledItems.length;
};

export const getSelectedItemsIds = items => items.filter(item => item.isSelected).map(item => item.id);
