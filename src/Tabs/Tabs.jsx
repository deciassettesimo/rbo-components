import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { v4 } from 'uuid';

import { addDataAttributes } from '../_utils';

import { COMPONENTS } from './_constants';
import TabsMain from './_internal/TabsMain';
import TabsExtra from './_internal/TabsExtra';
import { StyledTabs, StyledTabsHidden } from './_style';
import REFS from './_config';

export default class Tabs extends Component {
  static REFS = { ...REFS };

  static propTypes = {
    dimension: PropTypes.oneOf(Object.values(REFS.DIMENSIONS)),
    align: PropTypes.oneOf(Object.values(REFS.ALIGN)),
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        disabled: PropTypes.bool,
        isActive: PropTypes.bool,
      }),
    ).isRequired,
    isEmphasis: PropTypes.bool,
    onItemClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    dimension: REFS.DIMENSIONS.M,
    align: REFS.ALIGN.LEFT,
    isEmphasis: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      isCompactView: false,
    };

    this.iframe = v4();
    this.node = React.createRef();
    this.hiddenNode = React.createRef();
  }

  componentDidMount() {
    window[this.iframe].addEventListener('resize', this.handleUpdate);
    this.handleUpdate();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.items !== this.props.items) {
      this.handleUpdate();
    }
  }

  componentWillUnmount() {
    if (window[this.iframe]) window[this.iframe].removeEventListener('resize', this.handleUpdate);
  }

  handleUpdate = () => {
    const nodeWidth = this.node.current.getBoundingClientRect().width;
    const hiddenNodeWidth = this.hiddenNode.current.getBoundingClientRect().width;
    this.setState({ isCompactView: nodeWidth < hiddenNodeWidth });
  };

  render() {
    const activeItem = this.props.items.find(item => item.isActive);
    const extraItems = this.props.items.filter(item => !item.isActive);

    return (
      <StyledTabs
        {...addDataAttributes({ component: COMPONENTS.GENERAL })}
        innerRef={ref => {
          this.node.current = ref;
        }}
        sDimension={this.props.dimension}
        sAlign={this.props.align}
      >
        <iframe
          name={this.iframe}
          title="tabs"
          width="100%"
          height="0"
          style={{ position: 'absolute', left: 0, top: 0, opacity: 0, zIndex: '-1' }}
        />
        <StyledTabsHidden
          innerRef={ref => {
            this.hiddenNode.current = ref;
          }}
        >
          <TabsMain
            dimension={this.props.dimension}
            items={this.props.items}
            isEmphasis={this.props.isEmphasis}
            onItemClick={this.props.onItemClick}
          />
        </StyledTabsHidden>
        <TabsMain
          dimension={this.props.dimension}
          items={this.state.isCompactView ? [activeItem] : this.props.items}
          isEmphasis={this.props.isEmphasis}
          onItemClick={this.props.onItemClick}
        />
        {this.state.isCompactView && (
          <TabsExtra
            dimension={this.props.dimension}
            items={extraItems}
            isEmphasis={this.props.isEmphasis}
            onItemClick={this.props.onItemClick}
          />
        )}
      </StyledTabs>
    );
  }
}
