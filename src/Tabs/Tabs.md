### REFS

```js static
{
  DIMENSIONS: {
    XS: 24,
    S: 32,
    M: 36,
    L: 40,
    XL: 48,
  },
  ALIGN: {
    LEFT: 'left',
    CENTER: 'center',
    RIGHT: 'right',
  }
}
```

### Tabs Dimensions
```js
const items = [
  {
    id: 'save',
    title: 'Сохранить',
    isActive: true,
  },
  {
    id: 'print',
    title: 'Распечатать',
    disabled: true,
  },
  {
    id: 'remove',
    title: 'Удалить',
  },
  {
    id: 'repeat',
    title: 'Повторить',
  },
  {
    id: 'document-sign-and-send',
    title: 'Подписать и отправить',
  },
  {
    id: 'document-sign',
    title: 'Подписать',
  }
];

<div className="list">
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.XS}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.S}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.M}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.L}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.XL}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
</div>
```

### Tabs isEmphasis
```js
const items = [
  {
    id: 'save',
    title: 'Сохранить',
    isActive: true,
  },
  {
    id: 'print',
    title: 'Распечатать',
  },
  {
    id: 'remove',
    title: 'Удалить',
  },
  {
    id: 'repeat',
    title: 'Повторить',
  },
  {
    id: 'document-sign-and-send',
    title: 'Подписать и отправить',
  },
  {
    id: 'document-sign',
    title: 'Подписать',
  }
];

<div className="list">
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.XS}
      onItemClick={(id) => {console.log(id);}}
      items={items}
      isEmphasis
    />
  </div>
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.S}
      onItemClick={(id) => {console.log(id);}}
      items={items}
      isEmphasis
    />
  </div>
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.M}
      onItemClick={(id) => {console.log(id);}}
      items={items}
      isEmphasis
    />
  </div>
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.L}
      onItemClick={(id) => {console.log(id);}}
      items={items}
      isEmphasis
    />
  </div>
  <div>
    <Tabs
      dimension={Tabs.REFS.DIMENSIONS.XL}
      onItemClick={(id) => {console.log(id);}}
      items={items}
      isEmphasis
    />
  </div>
</div>
```

### Tabs align
```js
const items = [
  {
    id: 'save',
    title: 'Сохранить',
    isActive: true,
  },
  {
    id: 'print',
    title: 'Распечатать',
  },
  {
    id: 'remove',
    title: 'Удалить',
  },
  {
    id: 'repeat',
    title: 'Повторить',
  },
  {
    id: 'document-sign-and-send',
    title: 'Подписать и отправить',
  },
  {
    id: 'document-sign',
    title: 'Подписать',
  }
];

<div className="list">
  <div>
    <Tabs
      align={Tabs.REFS.ALIGN.LEFT}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <Tabs
      align={Tabs.REFS.ALIGN.CENTER}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
  <div>
    <Tabs
      align={Tabs.REFS.ALIGN.RIGHT}
      onItemClick={(id) => {console.log(id);}}
      items={items}
    />
  </div>
</div>
```
