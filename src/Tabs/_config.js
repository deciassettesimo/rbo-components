import { DIMENSIONS, ALIGN } from './_constants';

const REFS = {
  DIMENSIONS,
  ALIGN,
};

export default REFS;
