export const COMPONENTS = {
  GENERAL: 'Tabs',
  EXTRA: 'TabsExtra',
  ITEM: 'TabsItem',
  MAIN: 'TabsMain',
};

export const DIMENSIONS = {
  XS: 24,
  S: 32,
  M: 36,
  L: 40,
  XL: 48,
};

export const ALIGN = {
  LEFT: 'left',
  CENTER: 'center',
  RIGHT: 'right',
};
