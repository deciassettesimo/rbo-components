import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import IconArrowUp from '../../Icon/IconArrowUp';
import IconArrowDown from '../../Icon/IconArrowDown';
import Card from '../../Card';
import Popup, { Opener, Box } from '../../Popup';
import { COMPONENTS, DIMENSIONS } from '../_constants';
import { StyledTabsExtra, StyledTabsExtraPopupOpener, StyledTabsExtraPopupBox } from '../_style';
import TabsItem from './TabsItem';

export default class TabsExtra extends PureComponent {
  static propTypes = {
    dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        isActive: PropTypes.bool,
      }),
    ).isRequired,
    isEmphasis: PropTypes.bool,
    onItemClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    dimension: DIMENSIONS.M,
    isEmphasis: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      isPopupOpen: false,
    };
  }

  handlePopupOpen = () => {
    this.setState({ isPopupOpen: true });
  };

  handlePopupClose = () => {
    this.setState({ isPopupOpen: false });
  };

  handleItemClick = id => {
    this.handlePopupClose();
    this.props.onItemClick(id);
  };

  render() {
    return (
      <StyledTabsExtra {...addDataAttributes({ component: COMPONENTS.EXTRA })}>
        <Popup isOpened={this.state.isPopupOpen} onOpen={this.handlePopupOpen} onClose={this.handlePopupClose}>
          <Opener display={Opener.REFS.DISPLAY.BLOCK}>
            <StyledTabsExtraPopupOpener sDimension={this.props.dimension}>
              {this.state.isPopupOpen && <IconArrowUp dimension={IconArrowDown.REFS.DIMENSIONS.XS} />}
              {!this.state.isPopupOpen && <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XS} />}
            </StyledTabsExtraPopupOpener>
          </Opener>
          <Box placement={Box.REFS.PLACEMENT.BOTTOM} align={Box.REFS.ALIGN.END}>
            <Card isShadowed>
              <StyledTabsExtraPopupBox>
                {this.props.items.map(item => (
                  <TabsItem
                    key={item.id}
                    id={item.id}
                    dimension={this.props.dimension}
                    title={item.title}
                    disabled={item.disabled}
                    isActive={item.isActive}
                    isEmphasis={this.props.isEmphasis}
                    isExtra
                    onClick={this.handleItemClick}
                  />
                ))}
              </StyledTabsExtraPopupBox>
            </Card>
          </Box>
        </Popup>
      </StyledTabsExtra>
    );
  }
}
