import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS, DIMENSIONS } from '../_constants';
import { StyledTabsItemMain, StyledTabsItemExtra } from '../_style';

const TabsItem = props => {
  const handleOnClick = () => {
    if (!props.disabled && !props.isActive) {
      props.onClick(props.id);
    }
  };

  if (props.isExtra) {
    return (
      <StyledTabsItemExtra
        {...addDataAttributes({ component: COMPONENTS.ITEM, id: props.id })}
        sDimension={props.dimension}
        isEmphasis={props.isEmphasis}
        onClick={handleOnClick}
      >
        {props.title}
      </StyledTabsItemExtra>
    );
  }

  return (
    <StyledTabsItemMain
      {...addDataAttributes({ component: COMPONENTS.ITEM, id: props.id })}
      sDimension={props.dimension}
      isDisabled={props.disabled}
      isActive={props.isActive}
      isEmphasis={props.isEmphasis}
      onClick={handleOnClick}
    >
      {props.title}
    </StyledTabsItemMain>
  );
};

TabsItem.propTypes = {
  id: PropTypes.string.isRequired,
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  title: PropTypes.string.isRequired,
  isExtra: PropTypes.bool,
  disabled: PropTypes.bool,
  isActive: PropTypes.bool,
  isEmphasis: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

TabsItem.defaultProps = {
  dimension: DIMENSIONS.M,
  isExtra: false,
  disabled: false,
  isActive: false,
  isEmphasis: false,
};

export default TabsItem;
