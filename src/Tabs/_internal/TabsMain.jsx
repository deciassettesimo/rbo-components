import React from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../../_utils';

import { COMPONENTS, DIMENSIONS } from '../_constants';
import { StyledTabsMain } from '../_style';
import TabsItem from './TabsItem';

const TabsMain = props => (
  <StyledTabsMain {...addDataAttributes({ component: COMPONENTS.MAIN })}>
    {props.items.filter(item => item).map(item => (
      <TabsItem
        key={item.id}
        id={item.id}
        dimension={props.dimension}
        title={item.title}
        disabled={item.disabled}
        isActive={item.isActive}
        isEmphasis={props.isEmphasis}
        onClick={props.onItemClick}
      />
    ))}
  </StyledTabsMain>
);

TabsMain.propTypes = {
  dimension: PropTypes.oneOf(Object.values(DIMENSIONS)),
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      isActive: PropTypes.bool,
    }),
  ).isRequired,
  isEmphasis: PropTypes.bool,
  onItemClick: PropTypes.func.isRequired,
};

TabsMain.defaultProps = {
  dimension: DIMENSIONS.M,
  isEmphasis: false,
};

export default TabsMain;
