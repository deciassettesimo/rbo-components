import styled from 'styled-components';

import { STYLES } from '../_constants';
import { DIMENSIONS } from './_constants';

const tabsItemBorderBottomColor = (isDisabled, isActive, isHovered) => {
  if (isDisabled) return STYLES.COLORS.TRANSPARENT;
  if (isActive) return STYLES.COLORS.DARK_SKY_BLUE;
  if (isHovered) return STYLES.COLORS.BLACK;
  return STYLES.COLORS.TRANSPARENT;
};

const tabsItemFontSize = (sDimension, isEmphasis) => {
  if (isEmphasis) {
    if (sDimension === DIMENSIONS.XS) return 10;
    if (sDimension === DIMENSIONS.XL) return 14;
    return 12;
  }
  if (sDimension === DIMENSIONS.XS) return 12;
  if (sDimension === DIMENSIONS.XL) return 16;
  return 14;
};

const tabsItemLineHeight = sDimension => sDimension - 8;

const tabsIExtraPaddingTop = sDimension => {
  switch (sDimension) {
    case DIMENSIONS.XS:
      return 2;
    case DIMENSIONS.S:
      return 6;
    case DIMENSIONS.L:
      return 10;
    case DIMENSIONS.XL:
      return 14;
    case DIMENSIONS.M:
    default:
      return 8;
  }
};

export const StyledTabs = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  text-align: ${props => props.sAlign};
  height: ${props => props.sDimension}px;
  white-space: nowrap;
  width: 100%;
  overflow: hidden;
`;

export const StyledTabsMain = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  white-space: normal;
`;

export const StyledTabsExtra = styled.div`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  margin-left: 4px;
`;

export const StyledTabsExtraPopupOpener = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  cursor: pointer;
  border-bottom: 4px solid ${STYLES.COLORS.TRANSPARENT};
  height: ${props => props.sDimension}px;
  padding-top: ${props => tabsIExtraPaddingTop(props.sDimension)}px;
  transition: border 0.32s ease-out;

  :hover {
    border-bottom-color: ${tabsItemBorderBottomColor(false, false, true)};
  }
`;

export const StyledTabsExtraPopupBox = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px 0;
`;

export const StyledTabsItem = styled.div`
  position: relative;
  box-sizing: border-box;
  white-space: nowrap;
  font-size: ${props => tabsItemFontSize(props.sDimension, props.isEmphasis)}px;
  font-weight: bold;
  text-transform: ${props => (props.isEmphasis ? 'uppercase' : 'none')};
  letter-spacing: ${props => (props.isEmphasis ? '0.032em' : '0')};
  color: ${props => (props.isDisabled ? STYLES.COLORS.BLACK_48 : STYLES.COLORS.BLACK)};
  cursor: ${props => (props.isDisabled || props.isActive ? 'default' : 'pointer')};
  transition: border 0.32s ease-out, background 0.32s ease-out;
`;

export const StyledTabsItemMain = StyledTabsItem.extend`
  display: inline-block;
  margin: 0 8px;
  padding-top: 4px;
  border-bottom: 4px solid ${props => tabsItemBorderBottomColor(props.isDisabled, props.isActive)};
  height: ${props => props.sDimension}px;
  line-height: ${props => tabsItemLineHeight(props.sDimension)}px;

  :hover {
    border-bottom-color: ${props => tabsItemBorderBottomColor(props.isDisabled, props.isActive, true)};
  }

  :first-child {
    margin-left: 0;
  }

  :last-child {
    margin-right: 0;
  }
`;

export const StyledTabsItemExtra = StyledTabsItem.extend`
  display: block;
  padding: 8px;

  :hover {
    background-color: ${props => (props.isDisabled ? STYLES.COLORS.TRANSPARENT : STYLES.COLORS.ICE_BLUE)};
  }
`;

export const StyledTabsHidden = styled.div`
  position: absolute;
  height: 0;
  overflow: hidden;

  ${StyledTabsMain} {
    white-space: nowrap;
  }
`;
