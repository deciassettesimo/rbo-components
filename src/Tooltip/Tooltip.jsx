import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { addDataAttributes } from '../_utils';

import Popup, { Opener, Box } from '../Popup';
import Card from '../Card';
import ClickableIcon from '../ClickableIcon';
import IconQuestionFilled from '../Icon/IconQuestionFilled';
import { COMPONENTS } from './_constants';
import { StyledTooltipPopupOpener, StyledTooltipPopupBox, StyledTooltipPopupBoxClose } from './_style';
import REFS from './_config';

export default class Tooltip extends PureComponent {
  static REFS = { ...REFS };

  static propTypes = {
    children: PropTypes.node.isRequired,
    placement: PropTypes.oneOf(Object.values(REFS.PLACEMENT)),
    align: PropTypes.oneOf(Object.values(REFS.ALIGN)),
  };

  static defaultProps = {
    placement: REFS.PLACEMENT.TOP,
    align: REFS.ALIGN.START,
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpened: false,
    };
  }

  handlePopupOpen = () => {
    this.setState({ isOpened: true });
  };

  handlePopupClose = () => {
    this.setState({ isOpened: false });
  };

  render() {
    return (
      <Popup
        isOpened={this.state.isOpened}
        onOpen={this.handlePopupOpen}
        onClose={this.handlePopupClose}
        isStopPropagation
      >
        <Opener>
          <StyledTooltipPopupOpener
            {...addDataAttributes({ component: COMPONENTS.POPUP_OPENER })}
            isActive={this.state.isOpened}
          >
            <IconQuestionFilled
              display={IconQuestionFilled.REFS.DISPLAY.BLOCK}
              dimension={IconQuestionFilled.REFS.DIMENSIONS.XS}
              color={
                this.state.isOpened
                  ? IconQuestionFilled.REFS.COLORS.SUNFLOWER_YELLOW
                  : IconQuestionFilled.REFS.COLORS.GRAYISH_BROWN
              }
            />
          </StyledTooltipPopupOpener>
        </Opener>
        <Box placement={this.props.placement} align={this.props.align}>
          <Card isShadowed>
            <StyledTooltipPopupBox {...addDataAttributes({ component: COMPONENTS.POPUP_BOX })}>
              <StyledTooltipPopupBoxClose>
                <ClickableIcon
                  type={ClickableIcon.REFS.TYPES.CLOSE}
                  dimension={ClickableIcon.REFS.DIMENSIONS.XS}
                  onClick={this.handlePopupClose}
                />
              </StyledTooltipPopupBoxClose>
              {this.props.children}
            </StyledTooltipPopupBox>
          </Card>
        </Box>
      </Popup>
    );
  }
}
