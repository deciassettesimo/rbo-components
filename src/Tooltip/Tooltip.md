### Tooltip
```js
<div>
  <Tooltip>
    Укажите номер и дату документа
  </Tooltip>
</div>
```

### Tooltip with align and placement 
```js
<div>
  <Tooltip placement={Tooltip.REFS.PLACEMENT.BOTTOM} align={Tooltip.REFS.ALIGN.CENTER}>
    Укажите номер и дату одного из следующих документов:<br />
    - Платежного поручения;<br />
    - Заявления на перевод в иностранной валюте;<br />
  </Tooltip>
</div>
```

### Tooltip in Form.Label
```js
<div>
  <Form dimension={Form.REFS.DIMENSIONS.S}>
    <Form.Row>
      <Form.Cell>
        <Form.Label htmlFor="DocNumber">
          <span>Номер документа</span>
          <Tooltip>
            Укажите номер и дату одного из следующих документов:<br />
            - Платежного поручения;<br />
            - Заявления на перевод в иностранной валюте;<br />
            - Уведомления о поступлении средств  на транзитный валютный счет;<br />
            - Иного документа (например, подтверждающего осуществление операции по счету за рубежом и т.д.)<br />
            При отсутствии у документа номера поле не заполняется.<br />
          </Tooltip>
        </Form.Label>
        <Form.Field>
          <InputDigital id="DocNumber" maxLength={7} />
        </Form.Field>
      </Form.Cell>
    </Form.Row>
  </Form>
</div>
```
