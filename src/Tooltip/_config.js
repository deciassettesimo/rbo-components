import { BOX_PLACEMENT as PLACEMENT, BOX_ALIGN as ALIGN } from '../Popup/_constants';

const REFS = {
  PLACEMENT,
  ALIGN,
};

export default REFS;
