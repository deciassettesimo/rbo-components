export const COMPONENTS = {
  POPUP_OPENER: 'TooltipPopupOpener',
  POPUP_BOX: 'TooltipPopupBox',
};

export default COMPONENTS;
