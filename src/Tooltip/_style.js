import styled from 'styled-components';

export const StyledTooltipPopupOpener = styled.div`
  box-sizing: border-box;
  display: inline-block;
  vertical-align: middle;
  width: 16px;
  height: 16px;
  position: relative;
  margin: 0 4px;
  cursor: pointer;
  opacity: ${props => (props.isActive ? 1 : 0.8)};
  transition: opacity 0.32s ease-out;

  :hover {
    opacity: 1;
  }
`;

export const StyledTooltipPopupBox = styled.div`
  position: relative;
  box-sizing: border-box;
  display: block;
  padding: 8px 24px 8px 8px;
  max-width: 320px;
  font-size: 12px;
`;

export const StyledTooltipPopupBoxClose = styled.div`
  position: absolute;
  top: 2px;
  right: 2px;
`;
