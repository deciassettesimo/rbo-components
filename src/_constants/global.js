export const LOCALES = {
  EN: 'en',
  RU: 'ru',
};

export const WIDTH_MAP = {
  '1/2': '50%',
  '1/3': '33.33%',
  '2/3': '66.66%',
  '1/4': '25%',
  '2/4': '50%',
  '3/4': '75%',
  '1/6': '16.66%',
  '2/6': '33.33%',
  '3/6': '50%',
  '4/6': '66.66%',
  '5/6': '83.33%',
};

export const LABELS = {
  INPUT: {
    RU: {
      OPTIONS_EMPTY: 'Ничего не найдено',
      TABLE_ADD_BUTTON: 'Добавить запись',
    },
    EN: {
      OPTIONS_EMPTY: 'Not found',
      TABLE_ADD_BUTTON: 'Add entry',
    },
  },
  FILES_MANAGER: {
    RU: {
      ADD_LABEL_LINK: 'Выберите файл',
      ADD_LABEL_TEXT: 'или перетащите в эту область',
      ADD_ERROR_LINK: 'Выберите другой файл',
      ADD_ERROR_TEXT: 'Не удалось загрузить файл, превышен допустимый объем вложения',
      MAX_FILES_ADDED: 'Вы добавили максимальное количество файлов',
      FILE_DATE: 'Создан',
      FILE_SIZE: { GB: 'Гб', MB: 'Мб', KB: 'Кб', BYTES: 'байт' },
      FILE_ADD_COMMENT: 'Добавить комментарий',
      FILE_ADD_COMMENT_PLACEHOLDER: 'Описание файла',
      FILE_ADD_COMMENT_CANCEL: 'Отменить',
      FILE_ADD_COMMENT_SUBMIT: 'Сохранить',
      NO_FILES: 'Нет файлов',
    },
    EN: {
      ADD_LABEL_LINK: 'Choose file',
      ADD_LABEL_TEXT: 'or drop file here',
      ADD_ERROR_LINK: 'Choose another',
      ADD_ERROR_TEXT: 'The file could not be uploaded, the allowed size was exceeded',
      MAX_FILES_ADDED: 'You added the maximum number of files',
      FILE_DATE: 'Created',
      FILE_SIZE: { GB: 'Gb', MB: 'Mb', KB: 'Kb', BYTES: 'bytes' },
      FILE_ADD_COMMENT: 'Add comment',
      FILE_ADD_COMMENT_PLACEHOLDER: 'Describe file',
      FILE_ADD_COMMENT_CANCEL: 'Cancel',
      FILE_ADD_COMMENT_SUBMIT: 'Save',
      NO_FILES: 'No files',
    },
  },
  RBO_LIST_FILTERS: {
    RU: {
      TITLE: 'Фильтр',
      SAVE: 'сохранить',
      SAVE_AS: 'сохранить как...',
      RESET: 'сбросить',
      SELECT_PLACEHOLDER: 'Сохраненные',
      SAVE_AS_FORM_INPUT_PLACEHOLDER: 'Название фильтра',
      SAVE_AS_FORM_BUTTON: 'Сохранить',
    },
    EN: {
      TITLE: 'Filter',
      SAVE: 'save',
      SAVE_AS: 'save as...',
      RESET: 'clear',
      SELECT_PLACEHOLDER: 'Saved',
      SAVE_AS_FORM_INPUT_PLACEHOLDER: 'Filter name',
      SAVE_AS_FORM_BUTTON: 'Save',
    },
  },
  FILTER: {
    RU: {
      ADD_CONDITION: 'Добавить условие',
      PARAMS_ITEM_REMOVE: 'Удалить условие',
      FORM_BUTTON: 'Ок',
      FORM_RANGE_FROM: 'с',
      FORM_RANGE_TO: 'по',
      FORM_RANGE_AMOUNT_FROM: 'от',
      FORM_RANGE_AMOUNT_TO: 'до',
    },
    EN: {
      ADD_CONDITION: 'Add condition',
      PARAMS_ITEM_REMOVE: 'Remove condition',
      FORM_BUTTON: 'Ок',
      FORM_RANGE_FROM: 'from',
      FORM_RANGE_TO: 'to',
      FORM_RANGE_AMOUNT_FROM: 'from',
      FORM_RANGE_AMOUNT_TO: 'to',
    },
  },
  OPERATIONS_PANEL: {
    RU: {
      EXTRA_OPENER: 'Еще',
    },
    EN: {
      EXTRA_OPENER: 'More',
    },
  },
  TABLE: {
    RU: {
      EMPTY: 'Нет записей',
      SETTINGS_POPUP_OPENER: 'Настройки',
      SETTINGS_POPUP_CLOSE: 'Закрыть',
      SETTINGS_VISIBILITY_TITLE: 'Столбцы:',
      SETTINGS_VISIBILITY_SELECT_ALL_TITLE: 'Показать все',
      SETTINGS_GROUPING_TITLE: 'Группировать по:',
      SETTINGS_GROUPING_RESET: 'Без группировки',
      OPERATIONS_POPUP_OPENER: 'Операции',
    },
    EN: {
      EMPTY: 'No entries',
      SETTINGS_POPUP_OPENER: 'Settings',
      SETTINGS_POPUP_CLOSE: 'Close',
      SETTINGS_VISIBILITY_TITLE: 'Columns:',
      SETTINGS_VISIBILITY_SELECT_ALL_TITLE: 'Show all',
      SETTINGS_GROUPING_TITLE: 'Group by:',
      SETTINGS_GROUPING_RESET: 'No grouping',
      OPERATIONS_POPUP_OPENER: 'Operations',
    },
  },
  PAGINATION: {
    RU: {
      SETTINGS_PREFIX: 'Показывать',
      SETTINGS_POSTFIX: 'строк на странице',
      TO_START: 'В начало',
      PREV: 'Предыдущая страница',
      NEXT: 'Следующая страница',
    },
    EN: {
      SETTINGS_PREFIX: 'Show',
      SETTINGS_POSTFIX: 'entries per page',
      TO_START: 'To start',
      PREV: 'Prev page',
      NEXT: 'Next page',
    },
  },
  RBO_DOC_TABLE: {
    RU: {
      EMPTY: 'Нет записей',
    },
    EN: {
      EMPTY: 'No entries',
    },
  },
};
