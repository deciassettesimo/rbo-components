import * as GLOBAL from './global';
import * as STYLES from './styles';

export { GLOBAL, STYLES };
