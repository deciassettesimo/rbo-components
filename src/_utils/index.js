export const addDataAttributes = dataAttributes => {
  if (!dataAttributes) return null;
  return Object.keys(dataAttributes).reduce(
    (previous, key) => ({ ...previous, [`data-${key}`]: dataAttributes[key] }),
    {},
  );
};

export default addDataAttributes;
