const path = require('path');
const fs = require('fs');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const paths = require('react-app-rewired/scripts/utils/paths');
require(paths.scriptVersion + '/config/env');
const webpackConfig =
  process.env.NODE_ENV === 'production'
    ? paths.scriptVersion + '/config/webpack.config.prod'
    : paths.scriptVersion + '/config/webpack.config.dev';
const config = require(webpackConfig);
const override = require(paths.configOverrides);
const overrideFn = typeof override === 'function' ? override : override.webpack || ((config, env) => config);

module.exports = {
  webpackConfig: overrideFn(config, process.env.NODE_ENV),
  title: 'RBO UI Components Library',
  require: [path.join(__dirname, 'styleguide.config.css')],
  getExampleFilename: componentPath => componentPath.replace(/\.jsx?$/, '.md'),
  sortProps: props => props,
  pagePerSection: true,
  getComponentPathLine: componentPath => {
    const name = path.basename(componentPath, '.jsx');
    const dir = path.dirname(componentPath).replace('src/', '');
    return `import ${name} from '@rbo/rbo-components/lib/${dir}${dir !== name ? `/${name}` : ''}';`;
  },
  updateExample(props, exampleFilePath) {
    const { settings, lang } = props;
    if (typeof settings.file === 'string') {
      const filepath = path.resolve(path.dirname(exampleFilePath), settings.file);
      settings.static = true;
      delete settings.file;
      return {
        content: fs.readFileSync(filepath, 'utf8'),
        settings,
        lang,
      }
    }
    return props;
  },
  sections: [
    {
      name: 'Introduction',
      content: 'src/_docs/introduction.md',
    },
    {
      name: 'Installation',
      content: 'src/_docs/installation.md',
    },
    {
      name: 'Configuration',
      content: 'src/_docs/configuration.md',
    },

    {
      name: 'Button',
      components: 'src/Button/[A-Z]*.{js,jsx}',
      sectionDepth: 2,
    },
    {
      name: 'Card',
      components: 'src/Card/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'ClickableIcon',
      components: 'src/ClickableIcon/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'ComboButton',
      components: 'src/ComboButton/[A-Z]*.{js,jsx}',
      sectionDepth: 2,
    },
    {
      name: 'DoubleColorIcon',
      content: 'src/DoubleColorIcon/README.md',
      components: 'src/DoubleColorIcon/[A-Z]*.jsx',
      ignore: ['**/src/DoubleColorIcon/DoubleColorIcon.jsx'],
      sectionDepth: 2,
    },
    {
      name: 'FilesManager',
      components: 'src/FilesManager/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'Form',
      components: 'src/Form/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'GeneralError',
      components: 'src/GeneralError/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'Icon',
      content: 'src/Icon/README.md',
      components: 'src/Icon/[A-Z]*.jsx',
      ignore: ['**/src/Icon/Icon.jsx'],
      sectionDepth: 2,
    },
    {
      name: 'Input',
      components: 'src/Input/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'Loader',
      components: 'src/Loader/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'ModalWindow',
      components: 'src/ModalWindow/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'OperationsPanel',
      components: 'src/OperationsPanel/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'Pagination',
      components: 'src/Pagination/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'PaymentSystemLogo',
      content: 'src/PaymentSystemLogo/README.md',
      components: 'src/PaymentSystemLogo/[A-Z]*.jsx',
      ignore: ['**/src/PaymentSystemLogo/PaymentSystemLogo.jsx'],
      sectionDepth: 2,
    },
    {
      name: 'Popup',
      components: 'src/Popup/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RaiffeisenLogo',
      components: 'src/RaiffeisenLogo/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RboDocErrors',
      components: 'src/RboDocErrors/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RboDocForm',
      components: 'src/RboDocForm/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RboDocHeader',
      components: 'src/RboDocHeader/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RboDocInfo',
      components: 'src/RboDocInfo/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RboDocLayout',
      components: 'src/RboDocLayout/[A-Z]*.jsx',
      sectionDepth: 1,
    },
    {
      name: 'RboDocModal',
      components: 'src/RboDocModal/[A-Z]*.jsx',
      sectionDepth: 1,
    },
    {
      name: 'RboDocStructure',
      components: 'src/RboDocStructure/[A-Z]*.jsx',
      sectionDepth: 1,
    },
    {
      name: 'RboDocTable',
      components: 'src/RboDocTable/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RboDocTop',
      components: 'src/RboDocTop/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RboListInfoBar',
      components: 'src/RboListInfoBar/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RboListLayout',
      components: 'src/RboListLayout/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'RboPopupBoxOption',
      components: 'src/RboPopupBoxOption/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'Styles',
      components: 'src/Styles/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'Table',
      components: 'src/Table/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'Tabs',
      components: 'src/Tabs/[A-Z]*.jsx',
      sectionDepth: 2,
    },
    {
      name: 'Tooltip',
      components: 'src/Tooltip/[A-Z]*.jsx',
      sectionDepth: 2,
    },
  ],
};
